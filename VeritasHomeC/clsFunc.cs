﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using Microsoft.VisualBasic;

namespace VeritasHomeC
{
    public static class clsFunc
    {
        public static string GetUserInfo(long xUserID)
        {
            string sGetUserInfo = "";
            if (xUserID == 0)
                return sGetUserInfo;

            string SQL;
            clsDBO clUI = new clsDBO();
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + xUserID;
            clUI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                sGetUserInfo = clUI.GetFields("fname") + " " + clUI.GetFields("lname");
            }
            return sGetUserInfo;
        }

        public static string GetPayee(long xID)
        {
            if (xID == 0)
                return "";

            clsDBO clP = new clsDBO();
            string SQL = "select * from payee where payeeid = " + xID;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                return clP.GetFields("CompanyName");
            }

            return "";
        }

        public static string GetSubAgentInfo(long xID)
        {
            string sGetSubAgentInfo = "";
            if (xID == 0)
                return sGetSubAgentInfo;

            string SQL;
            clsDBO clSA = new clsDBO();
            SQL = "select * from subagents ";
            SQL = SQL + "where subagentid = " + xID;
            clSA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSA.RowCount() > 0)
            {
                clSA.GetRow();
                sGetSubAgentInfo = clSA.GetFields("subagentno") + " / " + clSA.GetFields("subagentname");
            }
            return sGetSubAgentInfo;
        }

        public static string GetAgentInfo(long xID)
        {
            string sGetAgentInfo = "";
            if (xID == 0)
                return sGetAgentInfo;

            string SQL;
            clsDBO clA = new clsDBO();
            SQL = "select * from agents ";
            SQL = SQL + "where agentid = " + xID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                sGetAgentInfo = clA.GetFields("agentno") + " / " + clA.GetFields("agentname");
            }
            return sGetAgentInfo;
        }

        public static string GetParentAgentInfo(long xAgentID)
        {
            if (xAgentID == 0)
                return "";

            clsDBO clA = new clsDBO();
            string SQL = "select * from agents where agentid = " + xAgentID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                return clA.GetFields("agentno") + " / " + clA.GetFields("agentname");
            }

            return "";
        }

        public static string GetRateType(long xID)
        {
            string sGetRateType = "";
            if (xID == 0)
                return sGetRateType;

            string SQL;
            clsDBO clRT = new clsDBO();
            SQL = "select * from ratetype ";
            SQL = SQL + "where ratetypeid = " + xID;
            clRT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRT.RowCount() > 0)
            {
                clRT.GetRow();
                sGetRateType = clRT.GetFields("ratetypename");
            }
            return sGetRateType;
        }

        public static string GetContractID(string xContractNo)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from contract ";
            SQL = SQL + "where contractNo = '" + xContractNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("contractid");
            }

            return "0";
        }

        public static string GetLienholder(string xContractNo)
        {
            string SQL = "select * from contract where contractNo = '" + xContractNo + "' ";
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                return clC.GetFields("lienholder");
            }

            return "";
        }

        public static bool IsDate(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                DateTime dt;
                return (DateTime.TryParse(input, out dt));
            }
            else
                return false;
        }

        public static string GetFromName(string hfIDValue)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sGetFromName = "";
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfIDValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sGetFromName = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return sGetFromName;
        }

        public static string GetUserName(string hfIDValue)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sGetUserName = "";
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfIDValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sGetUserName = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return sGetUserName;
        }

        public static long DateDiffHours(DateTime d1, DateTime d2)
        {
            TimeSpan datediff = (d2.Subtract(d1));
            return Convert.ToInt64(datediff.TotalHours);
        }

        public static long DateDiffDays(DateTime d1, DateTime d2)
        {
            TimeSpan datediff = (d2.Subtract(d1));
            return datediff.Days;
        }

        public static string Right(string original, int numberCharacters)
        {
            return original.Substring(original.Length - numberCharacters, numberCharacters);

            //return Strings.Right(original, numberCharacters);
        }

        /* Sets the top banner of the page to dark blue if the connection string is on a test db */
        public static void SetTestColor(Panel pnlHeader, Image image)
        {
            if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                image.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                image.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }
        }

        public static bool isValidDateRange(DateTime? xFrom, DateTime? xTo)
        {
            int yearFrom = Convert.ToDateTime(xFrom).Year;
            int yearTo = Convert.ToDateTime(xTo).Year;
            if (xFrom == null || xTo == null ||
               DateTime.Compare(Convert.ToDateTime(xFrom), Convert.ToDateTime(xTo)) < 0 ||
                   yearFrom < 2000 || yearTo > 2049)
            {
                return true;
            }

            return false;
        }


    }
}