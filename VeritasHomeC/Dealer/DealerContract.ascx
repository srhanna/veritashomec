﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerContract.ascx.cs" Inherits="VeritasHomeC.DealerContract" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadGrid ID="rgContract" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" OnHTMLExporting="rgContract_HTMLExporting"
    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsContract">
    <GroupingSettings CaseSensitive="false" />
    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ContractID" PageSize="10" ShowFooter="true">
        <Columns>
            <telerik:GridBoundColumn DataField="ContractID" ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SaleDate" UniqueName="Saledate" HeaderText="Sale Date" DataFormatString="{0:M/d/yyyy}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="EffDate" UniqueName="EffDate" HeaderText="Effective" DataFormatString="{0:M/d/yyyy}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ProgramName" UniqueName="ProgramName" HeaderText="Program" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PlanType" UniqueName="PlanType" HeaderText="Plan" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="TermMonth" UniqueName="TermMonth" HeaderText="Month" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings EnablePostBackOnRowClick="true">
        <Selecting AllowRowSelect="true" />
    </ClientSettings>
</telerik:RadGrid>
<asp:SqlDataSource ID="dsContract"
    ProviderName="System.Data.SqlClient" 
    SelectCommand="select contractid, contractno, status, saledate,  effdate, fname, lname, 
    p.ProgramName, pt.PlanType, termmonth from contract c
    left join plantype pt on c.PlanTypeID = pt.PlanTypeID 
    left join program p on c.ProgramID = p.ProgramID 
    where dealerid = @DealerID" runat="server">
    <SelectParameters>
        <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
    </SelectParameters>

</asp:SqlDataSource>

<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />