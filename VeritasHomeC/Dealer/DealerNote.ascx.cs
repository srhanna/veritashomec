﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class DealerNote : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsDealerNote.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();

            if (!IsPostBack)
            {
                hfToday.Value = String.Format(DateTime.Today.ToString(), "M/d/yyyy");
                hfDealerID.Value = Request.QueryString["dealerid"];
                pnlControl.Visible = true;
                pnlDetail.Visible = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgDealerNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfDealerNoteID.Value = rgDealerNote.SelectedValue.ToString();
            FillNote();
        }

        private void FillNote()
        {
            string SQL;
            clsDBO clRN = new clsDBO();
            SQL = "select note, createdate, moddate, cre.email as CEmail, mod.email as MEMail, claimalways, claimnewentry, contractalways, contractactivate, contractnewentry, contractcancel from dealernote rn ";
            SQL = SQL + "left join userinfo cre on rn.createby = cre.userid ";
            SQL = SQL + "left join userinfo mod on mod.userid = rn.modby ";
            SQL = SQL + "where dealernoteid = " + hfDealerNoteID.Value;

            clRN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRN.RowCount() > 0)
            {
                clRN.GetRow();
                chkClaimAlways.Checked = Convert.ToBoolean(clRN.GetFields("claimalways"));
                chkClaimNew.Checked = Convert.ToBoolean(clRN.GetFields("claimnewentry"));
                chkContractAlways.Checked = Convert.ToBoolean(clRN.GetFields("contractalways"));
                chkContractActivate.Checked = Convert.ToBoolean(clRN.GetFields("contractactivate"));
                chkContractNewEntry.Checked = Convert.ToBoolean(clRN.GetFields("contractnewentry"));
                chkContractCancel.Checked = Convert.ToBoolean(clRN.GetFields("contractcancel"));
                txtCreBy.Text = clRN.GetFields("CEMail");
                txtCreDate.Text = clRN.GetFields("createdate");
                txtModBy.Text = clRN.GetFields("MEMail");
                txtModDate.Text = clRN.GetFields("moddate");
                txtNote.Text = clRN.GetFields("note");
            }
            else
            {
                txtCreBy.Text = "";
                txtCreDate.Text = "";
                txtModBy.Text = "";
                txtModDate.Text = "";
                txtNote.Text = "";
            }


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgDealerNote.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clRN = new clsDBO();
            SQL = "select * from dealernote ";
            SQL = SQL + "where dealernoteid = " + hfDealerNoteID.Value;
            clRN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRN.RowCount() == 0)
            {
                clRN.NewRow();
                clRN.SetFields("createby", hfUserID.Value);
                clRN.SetFields("createdate", DateTime.Today.ToString());
                clRN.SetFields("modby", hfUserID.Value);
                clRN.SetFields("moddate", DateTime.Today.ToString());
            }
            else
            {
                clRN.GetRow();
                clRN.SetFields("modby", hfUserID.Value);
                clRN.SetFields("moddate", DateTime.Today.ToString());
            }

            clRN.SetFields("claimalways", chkClaimAlways.Checked.ToString());
            clRN.SetFields("claimnewentry", chkClaimNew.Checked.ToString());
            clRN.SetFields("contractalways", chkContractAlways.Checked.ToString());
            clRN.SetFields("contractactivate", chkContractActivate.Checked.ToString());
            clRN.SetFields("contractnewentry", chkContractNewEntry.Checked.ToString());
            clRN.SetFields("contractcancel", chkContractCancel.Checked.ToString());
            clRN.SetFields("note", txtNote.Text);
            clRN.SetFields("dealerid", hfDealerID.Value);

            if (clRN.RowCount() == 0)
                clRN.AddRow();

            clRN.SaveDB();
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgDealerNote.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hfDealerNoteID.Value = "0";
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            FillNote();
        }
    }
}