﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerNote.ascx.cs" Inherits="VeritasHomeC.DealerNote" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlControl">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAdd" runat="server" CssClass="button1" Text="Add Agent Note" OnClick="btnAdd_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgDealerNote" runat="server" AutoGenerateColumns="false" AllowAutomaticUpdates="true" OnSelectedIndexChanged="rgDealerNote_SelectedIndexChanged" 
                                AllowSorting="true" AllowPaging="true" AllowFilteringByColumn="true" 
                                ShowFooter="true" DataSourceID="dsDealerNote">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="DealerNoteID" ShowFooter="true" DataSourceID="dsDealerNote">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="DealerNoteID" UniqueName="DealerNoteID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DealerID" UniqueName="DealerID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Note" UniqueName="Note" HeaderText="Note" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Create Date" UniqueName="CreateDate" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ModDate" HeaderText="Modify Date" UniqueName="ModDate" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreateBy" HeaderText="Create By" UniqueName="CreateBy" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ModBy" HeaderText="Modify By" UniqueName="ModBy" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource runat="server" ID="dsDealerNote" 
                                ProviderName="System.Data.SqlClient"
                                SelectCommand="select dn.DealerNoteID, dn.Dealerid, dn.note, dn.CreateDate, dn.Moddate, uic.email as CreateBy, uim.email as ModBy
                                from DealerNote dn 
                                left join UserInfo uic on uic.userid = dn.createby
                                left join UserInfo uim on uim.userid = dn.ModBy
                                where Dealerid = @Dealerid">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlDetail">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Note:
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkClaimAlways" Text="Claim Always" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkClaimNew" Text="Claim New Entry" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkContractAlways" Text="Contract Always" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkContractActivate" Text="Contract Activate" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkContractNewEntry" Text="Contract New Entry" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkContractCancel" Text="Contract Cancel" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtNote" TextMode="MultiLine" Height="500" Width="1000" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Create Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtCreDate" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Create By:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtCreBy" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Modify Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtModDate" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Modify By:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadTextBox ID="txtModBy" runat="server"></telerik:RadTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>    
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right"> 
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="btnSave" runat="server" CssClass="button2" Text="Save" OnClick="btnSave_Click" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button1" Text="Cancel" OnClick="btnCancel_Click" />
                                </asp:TableCell>
                            </asp:TableRow>                            
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfToday" runat="server" />
<asp:HiddenField ID="hfDealerNoteID" runat="server" />
