﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;

namespace VeritasHomeC
{
    public partial class DealerCommission : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsRateType.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["connstring"];
            dsRateCategory.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["connstring"];
            dsPayee.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["connstring"];
            SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["connstring"];

            if (!IsPostBack)
            {
                GetServerInfo();
                hfDealerID.Value = Request.QueryString["dealerid"];
                pnlCommission.Visible = true;
                pnlCommissionDetail.Visible = false;
                pnlSearchPayee.Visible = false;
                pnlSearchRateType.Visible = false;
                pnlAddRateType.Visible = false;
                pnlConfirmDelete.Visible = false;
                pnlAddPayee.Visible = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            hfDealerID.Value = Request.QueryString["dealerid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddCommission_Click(object sender, EventArgs e)
        {
            pnlCommission.Visible = false;
            pnlCommissionDetail.Visible = true;
            ClearCommissionDetail();
        }

        private void ClearCommissionDetail()
        {
            txtRateType.Text = "";
            rdpStartDate.Clear();
            txtPayee.Text = "";
            txtPercent.Text = "";
            txtAmount.Text = "";
            rdpEndDate.Clear();
            hfCommissionID.Value = "0";
            hfPayeeID.Value = "0";
        }

        protected void rgDealerCommission_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsDBO clDC = new clsDBO();
            string SQL = "select * from commissions where commissionid = " + rgDealerCommission.SelectedValue;
            clDC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clDC.RowCount() > 0)
            {
                clDC.GetRow();
                hfCommissionID.Value = clDC.GetFields("commissionid");
                hfRateTypeID.Value = clDC.GetFields("ratetypeid");
                txtRateType.Text = clsFunc.GetRateType(Convert.ToInt64(clDC.GetFields("ratetypeid")));
                rdpStartDate.SelectedDate = Convert.ToDateTime(clDC.GetFields("startdate"));
                txtPayee.Text = clsFunc.GetPayee(Convert.ToInt64(clDC.GetFields("Payeeid")));
                txtAmount.Text = clDC.GetFields("amt");
                hfPayeeID.Value = clDC.GetFields("payeeid");
                if (Information.IsDate(clDC.GetFields("enddate")))
                    rdpEndDate.SelectedDate = Convert.ToDateTime(clDC.GetFields("enddate"));

                lblCreBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clDC.GetFields("creby")));
                lblCreDate.Text = clDC.GetFields("credate");
                lblModBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clDC.GetFields("modby")));
                lblModDate.Text = clDC.GetFields("moddate");
                txtRateType.Focus();
                pnlCommission.Visible = false;
                pnlCommissionDetail.Visible = true;
            }
        }

        protected void btnCancelCommission_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = false;
            pnlCommission.Visible = true;
        }

        protected void btnSaveCommission_Click(object sender, EventArgs e)
        {
            bool bAdd = false;
            bool bChange = false;
            clsDBO clCH = new clsDBO();
            string SQL = "select * from commissions where commissionid = " + hfCommissionID.Value;
            clCH.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCH.RowCount() == 0)
                clCH.NewRow();
            else
                clCH.GetRow();

            if (clCH.GetFields("ratetypeid") != hfRateTypeID.Value)
                bChange = true;

            clCH.SetFields("ratetypeid", hfRateTypeID.Value);
            clCH.SetFields("dealerid", hfDealerID.Value);
            if (clCH.GetFields("startdate").Length > 0) {
                if (clCH.GetFields("startdate") != rdpStartDate.SelectedDate.ToString())
                    bChange = true;
            }
            clCH.SetFields("startdate", rdpStartDate.SelectedDate.ToString());
            if (clCH.GetFields("payeeid") != hfPayeeID.Value)
                bChange = true;

            clCH.SetFields("payeeid", hfPayeeID.Value);
            clCH.SetFields("programid", "1");
            if (clCH.GetFields("amt") != txtAmount.Text)
                bChange = true;

            clCH.SetFields("amt", txtAmount.Text);
            if (txtPercent.Text == "")
                clCH.SetFields("peramt", Convert.ToDouble(0).ToString());
            else
                clCH.SetFields("peramt", txtPercent.Text);
            

            if (Information.IsDate(rdpEndDate.SelectedDate)) {
                if (clCH.GetFields("enddate").Length > 0) {
                    if (clCH.GetFields("enddate") != rdpEndDate.SelectedDate.ToString())
                        bChange = true;
                }
                else
                    bChange = true;

                clCH.SetFields("enddate", rdpEndDate.SelectedDate.ToString());
            }

            if (clCH.RowCount() == 0) {
                clCH.SetFields("creby", hfUserID.Value);
                clCH.SetFields("credate", DateTime.Today.ToString());
                clCH.SetFields("moddate", DateTime.Today.ToString());
                clCH.SetFields("modby", hfUserID.Value);
                clCH.AddRow();
                bAdd = true;
            }
            else {
                clCH.SetFields("moddate", DateTime.Today.ToString());
                clCH.SetFields("modby", hfUserID.Value);
            }

            clCH.SaveDB();

            if (bAdd)
                GetCommissionID();

            if (bAdd)
                AddCommissions();
            else 
            {
                if (bChange) 
                {
                    AddCommissions();
                }
            }
            rgDealerCommission.Rebind();
            pnlCommissionDetail.Visible = false;
            pnlCommission.Visible = true;
        }

        private void AddCommissions()
        {
            rgDealerCommission.Rebind();
        }

        private void GetCommissionID()
        {
            clsDBO clCH = new clsDBO();
            string SQL = "select max(commissionid) as ch from commissions where creby = " + hfUserID.Value;
            clCH.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCH.RowCount() > 0)
            {
                clCH.GetRow();
                hfCommissionID.Value = clCH.GetFields("ch");
            }
        }

        private void DeleteCommission()
        {
            clsDBO clR = new clsDBO();
            string SQL = "delete commissions where commissionid = " + hfCommissionID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgDealerCommission.Rebind();
        }

        protected void btnSearchRateType_Click(object sender, EventArgs e)
        {
            rgRateType.Rebind();
            pnlCommissionDetail.Visible = false;
            pnlSearchRateType.Visible = true;
        }

        protected void rgRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRateTypeID.Value = rgRateType.SelectedValue.ToString();
            txtRateType.Text = clsFunc.GetRateType(Convert.ToInt64(hfRateTypeID.Value));
            pnlCommissionDetail.Visible = true;
            pnlSearchRateType.Visible = false;
        }

        protected void btnPayee_Click(object sender, EventArgs e)
        {
            rgPayee.Rebind();
            pnlSearchPayee.Visible = true;
            pnlCommissionDetail.Visible = false;
        }

        protected void rgPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPayeeID.Value = rgPayee.SelectedValue.ToString();
            txtPayee.Text = clsFunc.GetPayee(Convert.ToInt64(rgPayee.SelectedValue));
            pnlSearchPayee.Visible = false;
            pnlCommissionDetail.Visible = true;
        }

        protected void btnAddRateType_Click(object sender, EventArgs e)
        {
            pnlSearchRateType.Visible = false;
            pnlAddRateType.Visible = true;
        }

        protected void btnSaveCategory_Click(object sender, EventArgs e)
        {
            clsDBO clRT = new clsDBO();
            string SQL = "select * from ratetype where ratetypename = '" + txtRateType.Text + "' ";
            clRT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRT.RowCount() == 0)
                clRT.NewRow();
            else
                clRT.GetRow();

            clRT.SetFields("ratetypename", txtRateTypeAdd.Text);
            clRT.SetFields("ratecategoryid", cboRateCategoryAdd.SelectedValue);
            if (clRT.RowCount() == 0)
                clRT.AddRow();

            clRT.SaveDB();
            rgRateType.Rebind();
            pnlSearchRateType.Visible = true;
            pnlAddRateType.Visible = false;
        }

        protected void btnCancelCategory_Click(object sender, EventArgs e)
        {
            pnlSearchRateType.Visible = true;
            pnlAddRateType.Visible = false;
        }

        protected void btnClearPayeeInfo_Click(object sender, EventArgs e)
        {
            hfPayeeID.Value = "0";
            txtPayee.Text = "";
            pnlSearchPayee.Visible = false;
            pnlCommissionDetail.Visible = true;
        }

        protected void btnDeleteCommission_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = false;
            pnlConfirmDelete.Visible = true;
            lblProgramConfirm.Text = txtPayee.Text;
            lblRateTypeConfirm.Text = txtRateType.Text;
            lblAmtConfirm.Text = txtAmount.Text;
        }

        protected void btnNoConfirm_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = true;
            pnlConfirmDelete.Visible = false;
        }

        protected void btnYesConfirm_Click(object sender, EventArgs e)
        {
            DeleteCommission();
            rgDealerCommission.Rebind();
            pnlCommission.Visible = true;
            pnlConfirmDelete.Visible = false;
        }

        protected void btnCancelRateType_Click(object sender, EventArgs e)
        {
            pnlCommissionDetail.Visible = true;
            pnlSearchRateType.Visible = false;
        }

        protected void btnAddPayee_Click(object sender, EventArgs e)
        {
            cboState.ClearSelection();
            txtCompanyName.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            txtCity.Text = "";
            txtZip.Text = "";
            txtPhone.Text = "";
            pnlAddPayee.Visible = true;
            pnlSearchPayee.Visible = false;
        }

        protected void btnAddPayeeSave_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from payee where payeeid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0) {
                clR.NewRow();
                clR.SetFields("companyname", txtCompanyName.Text);
                clR.SetFields("addr1", txtAddr1.Text);
                clR.SetFields("addr2", txtAddr2.Text);
                clR.SetFields("city", txtCity.Text);
                clR.SetFields("state", cboState.SelectedValue);
                clR.SetFields("zip", txtZip.Text);
                clR.SetFields("phone", txtPhone.Text);
                clR.AddRow();
                clR.SaveDB();
            }
            rgPayee.Rebind();
            pnlAddPayee.Visible = false;
            pnlSearchPayee.Visible = true;
        }

        protected void btnAddPayeeCancel_Click(object sender, EventArgs e)
        {
            pnlAddPayee.Visible = false;
            pnlSearchPayee.Visible = true;
        }
    }
}