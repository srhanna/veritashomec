﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dealer.aspx.cs" Inherits="VeritasHomeC.Dealer" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="DealerContact.ascx" TagName="DealerContact" TagPrefix="uc" %>
<%@ Register Src="DealerNote.ascx" TagName="DealerNote" TagPrefix="uc" %>
<%@ Register Src="DealerContract.ascx" TagName="DealerContract" TagPrefix="uc" %>
<%@ Register Src="DealerCommission.ascx" TagName="DealerCommission" TagPrefix="uc" %>
<%@ Register Src="DealerModify.ascx" TagName="DealerModify" TagPrefix="uc" %>
<%@ Register Src="DealerOverfund.ascx" TagName="DealerOverfund" TagPrefix="uc" %>
<%@ Register Src="DealerDocuments.ascx" TagName="DealerDocuments" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Dealer</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black" OnClick="btnHome_Click">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnAgents_Click">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnDealer_Click">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnContract_Click">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnClaim_Click">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnAccounting_Click">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnReports_Click">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnUsers_Click">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black" OnClick="btnLogOut_Click">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlDetail" runat="server">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true" Font-Size="Large">
                                                                    Seller No:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Label ID="lblDealerNo" runat="server" Font-Size="Large" Text=""></asp:Label>
                                                                </asp:TableCell>
                                                                <asp:TableCell>&nbsp</asp:TableCell>
                                                                <asp:TableCell Font-Bold="true" Font-Size="Large">
                                                                    Seller Name:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Label ID="lblDealerName" runat="server" Font-Size="Large" Text=""></asp:Label>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Panel runat="server" ID="pnlAddress">
                                                                        <asp:Table runat="server">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Address 1:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAddr1" runat="server" Text=""></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Address 2:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAddr2" runat="server" Text=""></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    City, State, Zip:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblAddr3" runat="server"  Text=""></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Font-Bold="true">
                                                                                Status:
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Panel>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Panel runat="server" ID="pnlMisc">
                                                                        <asp:Table runat="server">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    DBA:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblDBA" runat="server" Text=""></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    EIN:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblEIN" runat="server" Text=""></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Moxy Entry Date:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:Label ID="lblMoxyEntry" runat="server" Text=""></asp:Label>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                    </asp:Panel>
                                                                </asp:TableCell>
                                                                <asp:TableCell VerticalAlign="Top">
                                                                    <asp:Table runat="server">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Font-Bold="true">
                                                                                Date Signed:
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:Label ID="lblDateSigned" runat="server" Text=""></asp:Label>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Font-Bold="true">
                                                                                Licensed To Sell FL:
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:CheckBox ID="chkLicensedFL" runat="server" />
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadTabStrip ID="tsDealer" runat="server" OnTabClick="tsDealer_TabClick">
                                            <Tabs>
                                                <telerik:RadTab Text="Contacts" Value="Contact"></telerik:RadTab> 
                                                <telerik:RadTab Text="Notes" Value="Note"></telerik:RadTab>
                                                <telerik:RadTab Text="Documents" Value="Documents"></telerik:RadTab>
                                                <telerik:RadTab Text="Contracts" Value="Contract"></telerik:RadTab>
                                                <telerik:RadTab Text="Commission" Value="Commission"></telerik:RadTab>
                                                <telerik:RadTab Text="Modify" Value="Modify"></telerik:RadTab>
                                                <telerik:RadTab Text="Overfund" Value="Overfund"></telerik:RadTab>
                                            </Tabs>
                                        </telerik:RadTabStrip>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadMultiPage ID="rmDealer" runat="server">
                                            <telerik:RadPageView ID="pvContact" runat="server">
                                                <uc:DealerContact runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvNote" runat="server">
                                                <uc:DealerNote runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvDocument" runat="server">
                                                <uc:DealerDocuments runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvContract" runat="server">
                                                <uc:DealerContract runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvCommission" runat="server">
                                                <uc:DealerCommission runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvModify" runat="server">
                                                <uc:DealerModify runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvOverfund" runat="server">
                                                <uc:DealerOverfund runat="server" />
                                            </telerik:RadPageView>
                                        </telerik:RadMultiPage>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfDealerID" runat="server" />
    </form>
</body>
</html>
