﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class DealerContact : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsContactType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsDealerContact.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            hfDealerID.Value = Request.QueryString["dealerid"];
        }
    }
}