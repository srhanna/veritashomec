﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class DealerDocuments : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfDealerID.Value = Request.QueryString["dealerid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                ReadOnlyButtons();
                pnlList.Visible = true;
                pnlAdd.Visible = false;
                pnlDetail.Visible = false;
                FillGrid();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnSave.Enabled = false;
                    btnAdd.Enabled = false;
                    btnDelete.Enabled = false;
                }
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillGrid() {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select dealerdocumentid, documentname, documentdesc, documentlink ";
            SQL = SQL + "from dealerdocument ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value + " ";
            SQL = SQL + "and deleted = 0 ";
            rgContractDocument.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgContractDocument.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = true;
            pnlList.Visible = false;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string sDocLink, SQL;
            clsDBO clR = new clsDBO();
            GetDealerNo();
            string folderPath = Server.MapPath("~") + @"\documents\dealers\" + hfDealerNo.Value + "_" + FileUpload2.FileName;
            sDocLink = "~/documents/dealers/" + hfDealerNo.Value + "_" + FileUpload2.FileName;
            FileUpload2.SaveAs(folderPath);
            SQL = "select * from dealerdocument ";
            SQL = SQL + "where dealerid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            clR.SetFields("dealerid", hfDealerID.Value);
            clR.SetFields("documentname", txtDocName.Text);
            clR.SetFields("documentdesc", txtDocDesc.Text);
            clR.SetFields("documentlink", sDocLink);
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", (DateTime.Now).ToString());
            clR.SetFields("moddate", (DateTime.Now).ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.AddRow();
            clR.SaveDB();
            pnlAdd.Visible = false;
            pnlList.Visible = false;
            FillGrid();
        }

        private void GetDealerNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                hfDealerNo.Value = clR.GetFields("dealerno");
            }
        }

        protected void rgContractDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            hfDocID.Value = rgContractDocument.SelectedValue.ToString();
            FillDetail();
        }

        private void FillDetail()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from dealerdocument ";
            SQL = SQL + "where dealerdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCreBy.Text = clsFunc.GetUserInfo(long.Parse(clR.GetFields("creby")));
                txtCreDate.Text = clR.GetFields("credate");
                txtDescDetail.Text = clR.GetFields("documentdesc");
                txtModBy.Text = clsFunc.GetUserInfo(long.Parse(clR.GetFields("modby")));
                txtTitleDetail.Text = clR.GetFields("documentname");
                txtModDate.Text = clR.GetFields("moddate");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * dealerdocument ";
            SQL = SQL + "where dealerdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                clR.SetFields("documentname", txtTitleDetail.Text);
                clR.SetFields("documentdesc", txtDescDetail.Text);
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("moddate", (DateTime.Now).ToString());
                clR.SaveDB();
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update dealerdocument ";
            SQL = SQL + "set deleted = 1 ";
            SQL = SQL + "where dealerdocumentid = " + hfDocID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = false;
            pnlList.Visible = true;
        }
    }
}