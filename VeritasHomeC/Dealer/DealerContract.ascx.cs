﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class DealerContract : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsContract.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["connstring"];
            hfDealerID.Value = Request.QueryString["dealerid"];
            GetServerInfo();
        }

        private void GetServerInfo() { 
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgContract_HTMLExporting(object sender, Telerik.Web.UI.GridHTMLExportingEventArgs e)
        {
            Response.Redirect("~/contract/contract.aspx?sid=" + hfID.Value + "&contractid=" + rgContract.SelectedValue);
        }
    }
}