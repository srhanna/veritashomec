﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerContact.ascx.cs" Inherits="VeritasHomeC.DealerContact" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadGrid ID="rgDealerContact" runat="server" AutoGenerateColumns="false" AllowAutomaticUpdates="true" 
    AllowAutomaticDeletes="true" AllowAutomaticInserts="true" GridLines="Both" AllowSorting="true" AllowPaging="true" 
    ShowFooter="true" DataSourceID="dsDealerContact">
    <MasterTableView AutoGenerateColumns="false" DataKeyNames="DealerContactID" ShowFooter="true" 
        CommandItemDisplay="Bottom" EditMode="Batch" DataSourceID="dsDealerContact">
        <BatchEditingSettings EditType="Cell" />
        <Columns>
            <telerik:GridBoundColumn DataField="DealerContactID" UniqueName="DealerContactID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DealerID" UniqueName="DealerID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
            <telerik:GridTemplateColumn HeaderText="Contact Type" DefaultInsertValue="" UniqueName="ContactTypeID" DataField="ContactTypeID">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblContactType" Text='<%# Eval("ContactType") %>'></asp:Label>                                            
                </ItemTemplate>
                <EditItemTemplate>
                    <telerik:RadDropDownList RenderMode="Lightweight" DropDownWidth="300px" runat="server" ID="cboContactType" DataValueField="ContactTypeID"
                    DataTextField="ContactType" DataSourceID="dsContactType">
                    </telerik:RadDropDownList>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="First" DefaultInsertValue="" UniqueName="FName" DataField="FName">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblFName" Text='<%# Eval("FName") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <telerik:RadTextBox RenderMode="Lightweight"  Width="55px" runat="server" ID="tbFName"></telerik:RadTextBox>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Last" DefaultInsertValue="" UniqueName="LName" DataField="LName">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblLName" Text='<%# Eval("LName") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <telerik:RadTextBox RenderMode="Lightweight"  Width="55px" runat="server" ID="tbLName"></telerik:RadTextBox>                                              
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn HeaderText="Phone" DefaultInsertValue="" UniqueName="Phone" DataField="Phone">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblPhone" Text='<%# Eval("Phone") %>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <telerik:RadMaskedTextBox ID="txtDCPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                </EditItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn ConfirmText="Delete this record?" ConfirmDialogType="RadWindow"
                ConfirmTitle="Delete" HeaderText="Delete" HeaderStyle-Width="50px"
                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
            </telerik:GridButtonColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings AllowKeyboardNavigation="true"></ClientSettings>
</telerik:RadGrid>
<asp:SqlDataSource runat="server" ID="dsDealerContact" 
    ProviderName="System.Data.SqlClient"
    DeleteCommand="delete DealerContact where DealerContactID=@DealerContactID" 
    InsertCommand="insert into DealerContact (DealerID, ContactTypeid, FName, LName, Phone) values (@DealerID, @ContactTypeID, @FName, @LName, @Phone)" 
    SelectCommand="select dc.dealerid, dc.DealerContactID, dc.ContactTypeID, dc.FName, dc.LName, dc.Phone, ct.ContactType from DealerContact dc left join ContactType ct on ct.ContactTypeID = dc.ContactTypeID where dealerid = @dealerid "
    UpdateCommand="update dealerContact set contactTypeID = @ContactTypeID, FName = @FName, Lname = @LName, Phone = @Phone where DealerContactID = @DealerContactID">
    <DeleteParameters>
        <asp:Parameter Name="DealerContactID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
        <asp:Parameter Name="ContactTypeID" Type="Int32" />
        <asp:Parameter Name="FName" Type="String" />
        <asp:Parameter Name="LName" Type="String" />
        <asp:Parameter Name="Phone" Type="String" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="ContactTypeID" Type="Int32" />
        <asp:Parameter Name="FName" Type="String" />
        <asp:Parameter Name="LName" Type="String" />
        <asp:Parameter Name="Phone" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="dsContactType"
ProviderName="System.Data.SqlClient" SelectCommand="select contacttypeid, contacttype from contacttype order by sortno" runat="server">
</asp:SqlDataSource>

<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />

