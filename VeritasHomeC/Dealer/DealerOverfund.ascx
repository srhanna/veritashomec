﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerOverfund.ascx.cs" Inherits="VeritasHomeC.DealerOverfund" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlController">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAdd" runat="server" CssClass="button1" Text="Add Bucket Payee" OnClick="btnAdd_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgRateTypePayee" runat="server" AutoGenerateColumns="false" AllowAutomaticUpdates="true" OnSelectedIndexChanged="rgRateTypePayee_SelectedIndexChanged"
                                AllowSorting="true" AllowPaging="true" AllowFilteringByColumn="true" 
                                ShowFooter="true" DataSourceID="dsRateTypePayee">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="RateTypePayeeID" ShowFooter="true" DataSourceID="dsRateTypePayee">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="RateTypePayeeID" UniqueName="DealerNoteID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DealerID" UniqueName="DealerID" Visible="false" ReadOnly="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateTypeName" HeaderText="Rate Type Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreDate" HeaderText="Create Date" UniqueName="CreateDate" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ModDate" HeaderText="Modify Date" UniqueName="ModDate" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CreateBy" HeaderText="Create By" UniqueName="CreateBy" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ModBy" HeaderText="Modify By" UniqueName="ModBy" ReadOnly="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource runat="server" ID="dsRateTypePayee" 
                                ProviderName="System.Data.SqlClient"
                                SelectCommand="select rtp.ratetypepayeeid, rtp.Dealerid, rt.ratetypename, rtp.PayeeName, rtp.city, rtp.state, rtp.CreDate, rtp.Moddate, uic.email as CreateBy, uim.email as ModBy
                                from RateTypePayee rtp
                                left join UserInfo uic on uic.userid = rtp.creby
                                left join UserInfo uim on uim.userid = rtp.ModBy
                                left join RateType rt on rt.ratetypeid = rtp.ratetypeid
                                where Dealerid = @Dealerid">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlDetail">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bucket:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRateType" Enabled="false" runat="server"></asp:TextBox>
                            <asp:Button ID="btnSearchRateType" runat="server" Text="Search Bucket" CssClass="button1" OnClick="btnSearchRateType_Click"/>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Payee Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtPayeeName" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                           Phone: 
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 1:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Address 2:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            City:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            State:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsStates"
                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Zip Code:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtZip" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCreDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Create By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCreBy" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Modified Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtModDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Modified By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtModBy" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button2" OnClick="btnSave_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button1" OnClick="btnCancel_Click"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddRateType" runat="server" Text="Add Rate Type" CssClass="button1" OnClick="btnAddRateType_Click" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgRateType" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" OnSelectedIndexChanged="rgRateType_SelectedIndexChanged"
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsRateType">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="RateTypeID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="RateTypeID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CategoryName" UniqueName="RateCategory" HeaderText="Bucket Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsRateType"
                            ProviderName="System.Data.SqlClient" SelectCommand="select ratetypeid, ratetypeName, categoryname from ratetype rt inner join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid where ratetypeid <> 29 " 
                            runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnCancelRateType" runat="server" Text="Cancel" CssClass="button1" OnClick="btnCancelRateType_Click"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlAddRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bucket Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRateTypeAdd" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Bucket Category
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboRateCategoryAdd" DataSourceID="dsRateCategory" DataTextField="CategoryName" DataValueField="RateCategoryID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsRateCategory"
                            ProviderName="System.Data.SqlClient" SelectCommand="select RateCategoryid, categoryname from ratecategory order by categoryname " runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSaveCategory" runat="server" Text="Save" CssClass="button2" OnClick="btnSaveCategory_Click"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancelCategory" runat="server" Text="Cancel" CssClass="button1" OnClick="btnCancelCategory_Click"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField id="hfRateTypePayeeID" runat="server" />
<asp:HiddenField id="hfRateTypeID" runat="server" />
