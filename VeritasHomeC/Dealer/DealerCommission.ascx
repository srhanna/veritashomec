﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerCommission.ascx.cs" Inherits="VeritasHomeC.DealerCommission" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlCommission" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddCommission" OnClick="btnAddCommission_Click" runat="server" Text="Add Commission" CssClass="button2" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgDealerCommission" OnSelectedIndexChanged="rgDealerCommission_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="CommissionID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="CommissionID" ReadOnly="true" Visible="false" UniqueName="CommissionID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ProgramName" UniqueName="ProgramName" HeaderText="Program Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypename" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CompanyName" UniqueName="CompanyName" HeaderText="Company Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amount" DataFormatString="{0:###.00}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PerAmt" UniqueName="PerAmt" HeaderText="Percent" AutoPostBackOnFilter="true" DataFormatString="{0:p}" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="StartDate" UniqueName="StartDate" HeaderText="Start Date" DataFormatString="{0:M/d/yyyy}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="EndDate" UniqueName="EndDate" HeaderText="End Date" DataFormatString="{0:M/d/yyyy}" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource1"
                            ProviderName="System.Data.SqlClient" 
                            SelectCommand="select commissionid, RateTypeName, ProgramName, class, Amt, peramt, startdate, enddate, pe.CompanyName
                            from Commissions ch
                                inner join RateType rt on ch.ratetypeid = rt.ratetypeid 
                                inner join Program P on p.programid = ch.programid
								inner join payee pe on pe.PayeeID = ch.PayeeID
                            where ch.dealerid = @dealerid order by enddate desc, ratetypename, amt desc " runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfDealerID" Name="DealerID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlCommissionDetail" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Bucket:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtRateType" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnSearchRateType" OnClick="btnSearchRateType_Click" runat="server" Text="Search Bucket" CssClass="button1"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Start Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDatePicker ID="rdpStartDate" runat="server"></telerik:RadDatePicker>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        End Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDatePicker ID="rdpEndDate" runat="server"></telerik:RadDatePicker>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Payee:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtPayee" Enabled="false" runat="server"></asp:TextBox>
                                        <asp:Button ID="btnPayee" OnClick="btnPayee_Click" runat="server" Text="Search Payee" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Amt:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtAmount" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Percent:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtPercent" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Create Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Create by:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Modify Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblModDate" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Modify By:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblModBy" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnDeleteCommission" OnClick="btnDeleteCommission_Click" runat="server" Text="Delete" />
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSaveCommission" OnClick="btnSaveCommission_Click" runat="server" Text="Save" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancelCommission" OnClick="btnCancelCommission_Click" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlConfirmDelete" BorderStyle="Double">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Payee:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblProgramConfirm" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Bucket:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblRateTypeConfirm" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Amount:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblAmtConfirm" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" ForeColor="Red">
                            Do you wish to delete?
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnYesConfirm" OnClick="btnYesConfirm_Click" runat="server" Text="Yes" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnNoConfirm" OnClick="btnNoConfirm_Click" runat="server" Text="No" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddRateType" OnClick="btnAddRateType_Click" runat="server" Text="Add Bucket" BackColor="#1eabe2" BorderColor="#1eabe2" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgRateType" OnSelectedIndexChanged="rgRateType_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsRateType">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="RateTypeID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="RateTypeID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CategoryName" UniqueName="RateCategory" HeaderText="Bucket Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsRateType"
                            ProviderName="System.Data.SqlClient" SelectCommand="select ratetypeid, ratetypeName, categoryname from ratetype rt inner join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid where ratetypeid <> 29 " 
                            runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnCancelRateType" OnClick="btnCancelRateType_Click" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlAddRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bucket Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRateTypeAdd" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Bucket Category
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboRateCategoryAdd" DataSourceID="dsRateCategory" DataTextField="CategoryName" DataValueField="RateCategoryID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsRateCategory"
                            ProviderName="System.Data.SqlClient" SelectCommand="select RateCategoryid, categoryname from ratecategory order by categoryname " runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSaveCategory" OnClick="btnSaveCategory_Click" runat="server" Text="Save" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancelCategory" OnClick="btnCancelCategory_Click" runat="server" Text="Cancel" BackColor="#1eabe2" BorderColor="#1eabe2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchPayee" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddPayee" OnClick="btnAddPayee_Click" runat="server" Text="Add Payee" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgPayee" OnSelectedIndexChanged="rgPayee_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsPayee">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="PayeeID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="PayeeID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CompanyName" UniqueName="CompanyName" HeaderText="Company Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsPayee"
                            ProviderName="System.Data.SqlClient" SelectCommand="select payeeid, CompanyName, city, state from payee" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnClearPayeeInfo" OnClick="btnClearPayeeInfo_Click" runat="server" Text="Clear Payee Info" BorderColor="#1a4688" BackColor="#1a4688" ForeColor="White" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlAddPayee">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Company Name:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtCompanyName" Width="150" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Address 1:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtAddr1" Width="150" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Address 2:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtAddr2" Width="150" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        City:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        State:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsStates"
                                        ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Zip:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Phone:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnAddPayeeSave" OnClick="btnAddPayeeSave_Click" BorderColor="#1a4688" runat="server" Text="Save" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnAddPayeeCancel" OnClick="btnAddPayeeCancel_Click" BorderColor="#1a4688" runat="server" Text="Cancel" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfPayeeID" runat="server" />
<asp:HiddenField ID="hfRateTypeID" runat="server" />
<asp:HiddenField ID="hfCommissionID" runat="server" />