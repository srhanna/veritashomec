﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;

namespace VeritasHomeC
{
    public partial class DealerModify : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsDealerStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();

            if (!IsPostBack) {
                pnlDetail.Visible = true;
                hfDealerID.Value = Request.QueryString["dealerid"];
                FillDealer();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillDealer()
        {
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerEMail.Text = clD.GetFields("email");
                txtPhone.Text = clD.GetFields("phone");
                cboDealerStatus.SelectedValue = clD.GetFields("dealerstatusid");
                hfAgentID.Value = clD.GetFields("agentsid");
                hfSubAgentID.Value = clD.GetFields("subagentid");
                txtAddr1.Text = clD.GetFields("addr1");
                txtAddr2.Text = clD.GetFields("addr2");
                txtCity.Text = clD.GetFields("city");
                cboState.SelectedValue = clD.GetFields("state");
                txtZip.Text = clD.GetFields("zip");
                txtDateSigned.Text = clD.GetFields("datesigned");
                lblCreBy.Text = clsFunc.GetUserInfo(long.Parse(clD.GetFields("creby")));
                lblCreDate.Text = clD.GetFields("credate");
                lblModBy.Text = clD.GetFields("modby");
                lblModDate.Text = clD.GetFields("moddate");
                txtDBA.Text = clD.GetFields("dba");
                txtEIN.Text = clD.GetFields("ein");
                chkLicensedFL.Checked = Convert.ToBoolean(clD.GetFields("licensedfl"));
            }
            else
            {
                txtDealerNo.Text = "";
                txtDealerEMail.Text = "";
                txtDealerName.Text = "";
                txtPhone.Text = "";
                cboDealerStatus.SelectedValue = "1";
                hfAgentID.Value = "0";
                hfSubAgentID.Value = "0";
                txtAddr1.Text = "";
                txtAddr2.Text = "";
                txtCity.Text = "";
                cboState.SelectedValue = "";
                txtZip.Text = "";
                txtDateSigned.Text = "";
                lblCreBy.Text = "";
                lblCreDate.Text = "";
                lblModBy.Text = "";
                lblModDate.Text = "";
                txtDBA.Text = "";
                txtEIN.Text = "";
                chkLicensedFL.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillDealer();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clD.RowCount() > 0)
                clD.GetRow();
            else
                clD.NewRow();

            clD.SetFields("dealerno", txtDealerNo.Text);
            clD.SetFields("dealername", txtDealerName.Text);
            clD.SetFields("addr1", txtAddr1.Text);
            clD.SetFields("addr2", txtAddr2.Text);
            clD.SetFields("city", txtCity.Text);
            clD.SetFields("state", cboState.SelectedValue);
            clD.SetFields("zip", txtZip.Text);
            clD.SetFields("phone", txtPhone.Text);
            
            if (Information.IsDate(txtDateSigned.Text))
                clD.SetFields("datesigned", txtDateSigned.Text);

            clD.SetFields("email", txtDealerEMail.Text);
            clD.SetFields("dba", txtDBA.Text);
            clD.SetFields("ein", txtEIN.Text);
            clD.SetFields("dealerstatusid", cboDealerStatus.SelectedValue);

            if (clD.RowCount() == 0)
            {
                clD.SetFields("creby", hfUserID.Value);
                clD.SetFields("credate", DateTime.Today.ToString());
                clD.AddRow();
            }
            else
            {
                clD.SetFields("moddate", DateTime.Today.ToString());
                clD.SetFields("modby", hfUserID.Value);
            }

            clD.SetFields("licensedfl", chkLicensedFL.Checked.ToString());
            clD.SaveDB();
            RedirectDealer();
        }

        private void RedirectDealer()
        {
            if (hfDealerID.Value != "0")
                Response.Redirect("dealer.aspx?sid=" + hfID.Value + "&dealerid=" + hfDealerID.Value);
            else
                Response.Redirect("dealer.aspx?sid=" + hfID.Value + "&dealerid=" + GetDealerID());
        }

        private long GetDealerID()
        {
            long dealerID = 0;
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select max(dealerid) as DealerID from dealer ";
            SQL = SQL + "where creby = " + hfUserID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                dealerID = long.Parse(clD.GetFields("dealerid"));
            }
            return dealerID;
        }
    }
}