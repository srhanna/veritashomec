﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class DealerOverfund : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsRateType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsRateCategory.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsRateTypePayee.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();

            if (!IsPostBack)
            {
                pnlAddRateType.Visible = false;
                pnlController.Visible = true;
                pnlDetail.Visible = false;
                pnlSearchRateType.Visible = false;
                hfDealerID.Value = Request.QueryString["dealerid"];
                hfRateTypePayeeID.Value = "0";
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddRateType_Click(object sender, EventArgs e)
        {
            pnlSearchRateType.Visible = false;
            pnlAddRateType.Visible = true;
            hfRateTypeID.Value = "0";
            txtRateType.Text = "";
            cboRateCategoryAdd.ClearSelection();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hfRateTypePayeeID.Value = "0";
            FillDetail();
            pnlController.Visible = false;
            pnlDetail.Visible = true;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlController.Visible = true;
            pnlDetail.Visible = false;
        }

        protected void btnCancelRateType_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlSearchRateType.Visible = false;
        }

        protected void btnCancelCategory_Click(object sender, EventArgs e)
        {
            pnlSearchRateType.Visible = true;
            pnlAddRateType.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from ratetypepayee ";
            SQL = SQL + "where ratetypepayeeid = " + hfRateTypePayeeID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
            }
            else
            {
                clR.NewRow();
            }

            clR.SetFields("dealerid", hfDealerID.Value);
            clR.SetFields("ratetypeid", hfRateTypeID.Value);
            clR.SetFields("payeename", txtPayeeName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.Text);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);

            if (clR.RowCount() > 0)
            {
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("moddate", DateTime.Now.ToString());
            }
            else
            {
                clR.SetFields("credate", DateTime.Now.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }

            clR.SaveDB();
            rgRateTypePayee.Rebind();
            pnlController.Visible = true;
            pnlDetail.Visible = false;
        }

        protected void btnSaveCategory_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clRT = new clsDBO();
            SQL = "select * from ratetype ";
            SQL = SQL + "where ratetypename = '" + txtRateType.Text + "' ";
            clRT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clRT.RowCount() == 0)
                clRT.NewRow();
            else
                clRT.GetRow();

            clRT.SetFields("ratetypename", txtRateTypeAdd.Text);
            clRT.SetFields("ratecategoryid", cboRateCategoryAdd.SelectedValue);

            if (clRT.RowCount() == 0)
                clRT.AddRow();

            clRT.SaveDB();
            rgRateType.Rebind();
            pnlSearchRateType.Visible = true;
            pnlAddRateType.Visible = false;

        }

        protected void btnSearchRateType_Click(object sender, EventArgs e)
        {
            rgRateType.Rebind();
            pnlDetail.Visible = false;
            pnlSearchRateType.Visible = true;
        }

        protected void rgRateTypePayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRateTypePayeeID.Value = rgRateTypePayee.SelectedValue.ToString() ;
            FillDetail();
            pnlDetail.Visible = true;
            pnlController.Visible = false;
        }

        private void FillDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from ratetypepayee ";
            SQL = SQL + "where ratetypepayeeid = " + hfRateTypePayeeID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAddr1.Text = clR.GetFields("addr1");
                txtAddr2.Text = clR.GetFields("addr2");
                txtCity.Text = clR.GetFields("city");
                txtCreDate.Text = clR.GetFields("credate");
                txtModDate.Text = clR.GetFields("moddate");
                txtPayeeName.Text = clR.GetFields("payeename");
                cboState.Text = clR.GetFields("state");
                txtZip.Text = clR.GetFields("zip");
                txtPhone.Text = clR.GetFields("phone");
                txtCreBy.Text = clsFunc.GetUserInfo(long.Parse(clR.GetFields("creby")));

                if (clR.GetFields("modby").Length > 0)
                    txtModBy.Text = clsFunc.GetUserInfo(long.Parse(clR.GetFields("modby")));
            }
            else
            {
                txtAddr1.Text = "";
                txtAddr2.Text = "";
                txtCity.Text = "";
                txtCreDate.Text = "";
                txtModDate.Text = "";
                txtPayeeName.Text = "";
                cboState.Text = "";
                txtZip.Text = "";
                txtPhone.Text = "";
                txtCreBy.Text = "";
                txtModDate.Text = "";
            }
        }

        protected void rgRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRateTypeID.Value = rgRateType.SelectedValue.ToString();
            txtRateType.Text = clsFunc.GetRateType(long.Parse(hfRateTypeID.Value));
            pnlDetail.Visible = true;
            pnlSearchRateType.Visible = false;
        }
    }
}