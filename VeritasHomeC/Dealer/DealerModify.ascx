﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealerModify.ascx.cs" Inherits="VeritasHomeC.DealerModify" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel ID="pnlDetail" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Seller No:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDealerNo" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Seller Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDealerName" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Dealer E-Mail:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDealerEMail" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Phone:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Status:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboDealerStatus" DataSourceID="dsDealerStatus" DataTextField="DealerStatus" DataValueField="DealerStatusID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsDealerStatus"
                            ProviderName="System.Data.SqlClient" SelectCommand="select DealerStatusID, DealerStatus from dealerstatus" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 1:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAddr1" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 2:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAddr2" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            City:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtCity" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            State:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsStates"
                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Zip Code:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtZip" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Date Signed:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDateSigned" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Licensed To Sell FL:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkLicensedFL" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            DBA:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtDBA" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            EIN:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtEIN" Width="200" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create by:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Modify Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModDate" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Modify By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModBy" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Table runat="server" HorizontalAlign="Right">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnSave"  runat="server" Width="100" Text="Save" CssClass="button2" OnClick="btnSave_Click" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnCancel" runat="server" Width="100" Text="Cancel" CssClass="button1" OnClick="btnCancel_Click" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:HiddenField ID="hfAgentID" runat="server" />
<asp:HiddenField ID="hfSubAgentID" runat="server" />
<asp:HiddenField ID="hfDealerID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />