﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class GrossSales : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetServerInfo();
            if (!IsPostBack)
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;

            lblInvalid.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            int yearFrom = Convert.ToDateTime(rdpFrom.SelectedDate).Year;
            int yearTo = Convert.ToDateTime(rdpTo.SelectedDate).Year;
            if (rdpFrom.SelectedDate == null || rdpTo.SelectedDate == null ||
                DateTime.Compare(Convert.ToDateTime(rdpTo.SelectedDate), Convert.ToDateTime(rdpFrom.SelectedDate)) < 0 ||
                    yearFrom < 2000 || yearTo > 2049)
            {
                lblInvalid.Visible = true;
                rgSales.Visible = false;
                return;
            }

            lblInvalid.Visible = false;
            GetTotalCount();

            //SQL = "declare @TotalCnt float = " + hfCount.Value +
            //        @"select sellerno, dealername, sellqty, per, case when sellerno = '' then 99 else 1 end as orderby from
            //        (select dealerno as sellerNo, dealername, count(*) as sellqty, (count(*) / @TotalCnt) as per from contract c
            //        inner join dealer d on c.dealerid = d.DealerID 
            //        where saledate >= '" + rdpFrom.SelectedDate + "' and saledate < '" + rdpTo.SelectedDate + "' " +
            //        @"and not saledate is null
            //        group by dealerno, dealername
            //        union
            //        select '' as sellerNo, 'Gross Total' as dealerName, count(*) as sellqty, 1 as per from contract 
            //        where saledate >= '" + rdpFrom.SelectedDate + "' " +
            //        "and saledate < '"  + rdpTo.SelectedDate + "' " +
            //        "and not saledate is null) dealerTotal order by orderby, sellerNo";

            SQL = "declare @TotalCnt float = " + hfCount.Value +
            @"select sellerno, dealername, sellqty, paidcnt, cancelcnt, pendingcnt, cbp, per, activeper, case when sellerno = '' then 99 else 1 end as orderby from
            (select dealerno as sellerNo, dealername, count(*) as sellqty, (count(*) / @TotalCnt) as per,
            count(case status when 'Paid' Then 1 END) as paidcnt,
            count(case status when 'Cancelled' then 1 end) as cancelcnt,
            count(case status when 'Pending' then 1 end) as pendingcnt,
            count(case status when 'Cancelled Before Paid' then 1 end) as cbp,
            cast(count(case status when 'Paid' then 1 end) as float) / count(*) as activeper
            from contract c
            inner
            join dealer d on c.dealerid = d.DealerID
            where saledate >= '" + rdpFrom.SelectedDate + "'  and saledate < '" + rdpTo.SelectedDate + "' " +
            @"and not saledate is null
            group by dealerno, dealername
            union
            select '' as sellerNo, 'Gross Total' as dealerName, count(*) as sellqty, 1 as per,
            count(case status when 'Paid' Then 1 END) as paidcnt,
            count(case status when 'Cancelled' then 1 end) as cancelcnt,
            count(case status when 'Pending' then 1 end) as pendingcnt,
            count(case status when 'Cancelled Before Paid' then 1 end) as cbp,
            cast(count(case status when 'Paid' Then 1 END) as float) / count(*) as activeper
            from contract
            where saledate >= '" + rdpFrom.SelectedDate + "'  and saledate < '"  + rdpTo.SelectedDate + "' " +
            "and not saledate is null) dealerTotal order by orderby, sellerNo";

            rgSales.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgSales.Rebind();
            rgSales.Visible = true;

        }

        private void GetTotalCount()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select count(*) as cnt from contract" +
                " where saledate >= '" + rdpFrom.SelectedDate + 
                "' and saledate < '" + rdpTo.SelectedDate + "' ";
            SQL = SQL + "and not saledate is null ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfCount.Value = clR.GetFields("cnt");
            }
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/accountingreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/agentssearch.aspx?=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contractsearch.aspx?=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void rgSales_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            string a;
            a = e.CommandName;
            if (e.CommandName == "ExportToExcel")
            {
                rgSales.ExportSettings.ExportOnlyData = false;
                rgSales.ExportSettings.IgnorePaging = true;
                rgSales.ExportSettings.OpenInNewWindow = true;
                rgSales.ExportSettings.UseItemStyles = true;
                rgSales.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgSales.ExportSettings.FileName = "GSR";
                rgSales.MasterTableView.ExportToExcel();
            }
        }
    }
}