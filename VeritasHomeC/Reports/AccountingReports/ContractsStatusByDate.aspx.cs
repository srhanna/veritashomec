﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractsStatusByDate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetServerInfo();
            if (!IsPostBack)
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;

            RadAjaxPanel1.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (rdpTo.SelectedDate == null)
                return;
            if (rdpFrom.SelectedDate == null)
                return;

            if (cboStatus.SelectedValue == "Paid" || cboStatus.SelectedValue == "Pending")
            {
                rgPaid.Visible = true;
                rgCancel.Visible = false;
                SQL = @"select contractno, fname + ' ' + lname as CustName, c.state as State, DatePaid, SaleDate, RetailRate, 
                MoxyDealerCost as DealerCost, dealerno, dealername, pt.plantype, termmonth, cr.amt as ClaimReserve, 
                cf.amt as ClipFee, pt2.amt as PremTax, a.amt as Admin, m.amt as Markup, comm.amt as Commission, crm.amt as crmamt from contract c 
                inner join dealer d on d.dealerid = c.dealerid 
                inner join plantype pt on pt.plantypeid = c.plantypeid 
                left join (select contractid, sum(amt) as Amt from contractamt 
                where ratetypeid = 1 group by contractid) cr 
                on cr.contractid = c.contractid 
                left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 2 group by ContractID) cf
		        on cf.ContractID = c.ContractID 
                left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 3 group by ContractID) pt2
		        on pt2.ContractID = c.ContractID 
                left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 4 group by ContractID) a
		        on a.ContractID = c.ContractID 
                left join (select contractid, sum(amt) as Amt from contractamt where ratetypeid = 5 group by ContractID) m
		        on m.ContractID = c.ContractID 
		        left join (select contractid, sum(amt) as Amt from contractamt ca
		        inner join ratetype rt on rt.RateTypeID = ca.RateTypeID 
		        where rt.RateCategoryID = 3 
		        and ca.RateTypeID <> 4017
		        group by contractid) comm
		        on comm.ContractID = c.ContractID 
		        left join (select contractid, sum(amt) as Amt from contractamt where (ratetypeid = 4017) group by ContractID) crm
		        on crm.ContractID = c.ContractID ";
                SQL = SQL + "where saledate >= '" + rdpFrom.SelectedDate + "' ";
                SQL = SQL + "and saledate < '" + rdpTo.SelectedDate + "' ";
                SQL = SQL + "and status = '" + cboStatus.SelectedValue + "' ";
                rgPaid.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
                rgPaid.Rebind();
            }

            if (cboStatus.SelectedValue == "Cancelled" || cboStatus.SelectedValue == "Cancelled Before Paid")
            {
                rgCancel.Visible = true;
                rgPaid.Visible = false;
                SQL = @"select contractno, fname +' ' + lname as CustName, DatePaid, SaleDate, RetailRate, 
                MoxyDealerCost as DealerCost, dealerno, dealername, pt.plantype, termmonth, cr.amt as ClaimReserve, 
                cf.amt as ClipFee, pt2.amt as PremTax, a.amt as Admin, m.amt as Markup, comm.amt as Commission, crm.amt as crmamt, cc.cancelfactor from contract c
                inner join dealer d on d.dealerid = c.dealerid
                inner join contractcancel cc on cc.contractid = c.contractid
                inner join plantype pt on pt.plantypeid = c.plantypeid
                left join(select contractid, sum(cancelamt) as Amt from contractamt
                where ratetypeid = 1
                group by contractid) cr
                on cr.contractid = c.contractid
                left join(select contractid, sum(cancelamt) as Amt from contractamt where ratetypeid = 2 group by ContractID) cf
                on cf.ContractID = c.ContractID
                left join(select contractid, sum(cancelamt) as Amt from contractamt where ratetypeid = 3 group by ContractID) pt2
                on pt2.ContractID = c.ContractID
                left join(select contractid, sum(cancelamt) as Amt from contractamt where ratetypeid = 4 group by ContractID) a
                on a.ContractID = c.ContractID
                left join(select contractid, sum(cancelamt) as Amt from contractamt where ratetypeid = 5 group by ContractID) m
                on m.ContractID = c.ContractID
                left join(select contractid, sum(cancelamt) as Amt from contractamt ca
                inner
                join ratetype rt on rt.RateTypeID = ca.RateTypeID
                where rt.RateCategoryID = 3
                and ca.RateTypeID <> 4017
                group by contractid) comm
                on comm.ContractID = c.ContractID
                left join(select contractid, sum(cancelamt) as Amt from contractamt where (ratetypeid = 4017) group by ContractID) crm
                on crm.ContractID = c.ContractID";
                SQL = SQL + " where saledate >= '" + rdpFrom.SelectedDate + "' ";
                SQL = SQL + "and saledate < '" + rdpTo.SelectedDate + "' ";
                SQL = SQL + "and status = '" + cboStatus.SelectedValue + "' ";
                rgCancel.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
                rgCancel.Rebind();
            }

            
            RadAjaxPanel1.Visible = true;
        }

        protected void cboStatus_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {

        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void rgPaid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            clsDBO clR = new clsDBO();
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }
    }


}