﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class MonthlyBreakdownIndividual : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            dsPayeeListName.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            hfPayeeID.Value = Request.QueryString["payeeid"];
            hfMonth.Value = Request.QueryString["month"];
            hfYear.Value = Request.QueryString["year"];

            GetServerInfo();
            if (!IsPostBack)
            {
               
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

               

                FillSummary();

                pvSummations.Selected = true;
                lblSubmitError.Visible = false;
                tsCommissions.Visible = true;
            }

            
            clsFunc.SetTestColor(pnlHeader, Image1);

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void tsCommissions_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsCommissions.SelectedTab.Value == "Summations")
                pvSummations.Selected = true;
            if (tsCommissions.SelectedTab.Value == "Sold")
                pvSold.Selected = true;
            if (tsCommissions.SelectedTab.Value == "Cancels")
                pvCancels.Selected = true;
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/accountingreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/agentssearch.aspx?=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contractsearch.aspx?=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        private void GetPayeeName()
        {
            clsDBO clP = new clsDBO();
            string SQL = "select * from payee where payeeid = " + hfPayeeID.Value;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                lblPayeeName.Text = clP.GetFields("CompanyName");
            }
        }


        private void FillSummary() 
        {

            GetPayeeName();
            lblYear.Text = hfYear.Value;
            
            //if(Convert.ToInt32(hfYear.Value) < 2000 ||Convert.ToInt32(hfYear.Value) > 2049)     
            //{
            //    lblSubmitError.Visible = true;
            //    tsCommissions.Visible = false;
            //    rmMonthlyCommissions.Visible = false;
            //    return;   
            //}

            DateTime? From;
            DateTime? To;
            
            if (hfMonth.Value == "Jan")
            {
                From = Convert.ToDateTime("1/1/" + hfYear.Value);
                To = Convert.ToDateTime("2/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Feb")
            {
                From = Convert.ToDateTime("2/1/" + hfYear.Value);
                To = Convert.ToDateTime("3/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Mar")
            {
                From = Convert.ToDateTime("3/1/" + hfYear.Value);
                To = Convert.ToDateTime("4/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Apr")
            {
                From = Convert.ToDateTime("4/1/" + hfYear.Value);
                To = Convert.ToDateTime("5/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "May")
            {
                From = Convert.ToDateTime("5/1/" + hfYear.Value);
                To = Convert.ToDateTime("6/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Jun")
            {
                From = Convert.ToDateTime("6/1/" + hfYear.Value);
                To = Convert.ToDateTime("7/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "July")
            {
                From = Convert.ToDateTime("7/1/" + hfYear.Value);
                To = Convert.ToDateTime("8/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Aug")
            {
                From = Convert.ToDateTime("8/1/" + hfYear.Value);
                To = Convert.ToDateTime("9/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Sep")
            {
                From = Convert.ToDateTime("9/1/" + hfYear.Value);
                To = Convert.ToDateTime("10/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Oct")
            {
                From = Convert.ToDateTime("10/1/" + hfYear.Value);
                To = Convert.ToDateTime("11/1/" + hfYear.Value);
            }
           else if (hfMonth.Value == "Nov")
            {
                From = Convert.ToDateTime("11/1/" + hfYear.Value);
                To = Convert.ToDateTime("12/1/" + hfYear.Value);
            }
            else if (hfMonth.Value == "Dec")
            {
                From = Convert.ToDateTime("12/1/" + hfYear.Value);
                To = Convert.ToDateTime("1/1/" + (Convert.ToInt32(hfYear.Value) + 1));
            }
            else
            {
                From = From = Convert.ToDateTime("1/1/" + hfYear.Value);
                To = Convert.ToDateTime("1/1/" + (Convert.ToInt32(hfYear.Value) + 1));
            }




            tsCommissions.Visible = true;
            tsCommissions.Tabs[0].Selected = true;
            lblSubmitError.Visible = false;
            pvSummations.ContentUrl = mGlobal.rDirMCSummation + hfID.Value + "&PayeeID=" + hfPayeeID.Value
                   + "&From=" + From + "&To=" + To;
            pvSold.ContentUrl = mGlobal.rDirMCSold + hfID.Value + "&PayeeID=" + hfPayeeID.Value
                    + "&From=" + From + "&To=" + To;
            pvCancels.ContentUrl = mGlobal.rDirMCCancels + hfID.Value + "&PayeeID=" + 
            hfPayeeID.Value + "&From=" + From + "&To=" + To;
            
        }
    }
}