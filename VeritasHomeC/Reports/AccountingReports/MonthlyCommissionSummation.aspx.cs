﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{

    public partial class MonthlyCommissionSummation : System.Web.UI.Page
    {
        private const double dEmptyValue = 0.00;

        protected void Page_Load(object sender, EventArgs e)
        {
            hfPayeeID.Value = Request.QueryString["payeeid"];
            hfTo.Value = Request.QueryString["to"];
            hfFrom.Value = Request.QueryString["from"];

            lblFromDate.Text = Convert.ToDateTime(hfFrom.Value).ToString("MMMM d, yyyy");
            lblToDate.Text = Convert.ToDateTime(hfTo.Value).ToString("MMMM d, yyyy");
            lblYearDate.Text = DateTime.Parse(hfFrom.Value).Year.ToString();


            if (!IsPostBack)
            {
                GetServerInfo();
                DetermineMonth();
                FillMonthlySummary();
                FillYearToDate();
                FindCommPer();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillMonthlySummary()
        {
            double NetTotal = 0;
            int NetCount = 0;
            double dCommPer = 0;
            double dAdjust = 0;
            double dCancelSum = 0, dContractSum = 0;

            //FIND CONTRACT COUNT
            clsDBO clR = new clsDBO();
            string SQL = @"select count(contractid) as contractCount from contractcommissions
                        where payeeid = " + hfPayeeID.Value + 
                        " and reportdate >= '" + hfFrom.Value + "' and reportdate < '" + hfTo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtContractCount.Text = clR.GetFields("contractCount");
            }

            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and reportdate >= '" + hfFrom.Value + "' and reportdate < '" + hfTo.Value + "' ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                {
                    dContractSum = 0;
                    txtContractCommission.Text = "0.00";
                }
                else
                {
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
                    if (dContractSum < 0)
                        txtContractCommission.Text = dContractSum.ToString("#,##0.00");
                    else
                        txtContractCommission.Text = dContractSum.ToString("#,##0.00");
                }
            }

            NetTotal = dContractSum;

            //FIND ADJUST COUNT
            txtAdjustCount.Text = "0";
            txtAdjustCommission.Text = dEmptyValue.ToString();
            NetTotal = NetTotal - dAdjust;


            //FIND CANCEL COUNT
            SQL = "select count(contractid) as cancelcount from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + hfFrom.Value + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCancelCount.Text = clR.GetFields("cancelcount");
            }

            //FIND CANCEL COMMISSIONS
            SQL = "select sum(cancelamt) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + hfFrom.Value + "' and cancelreportdate < '" + hfTo.Value + "' " +   
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                {
                    dCancelSum = 0;
                    txtCancelCommission.Text = "0.00";
                }
                else
                {
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
                    if (dCancelSum < 0)
                        txtCancelCommission.Text = dCancelSum.ToString("#,##0.00");
                    else
                        txtCancelCommission.Text = dCancelSum.ToString("#,##0.00");
                }
            }

            //FIND NET COUNT
            NetCount = Convert.ToInt32(txtContractCount.Text) - Convert.ToInt32(txtAdjustCount.Text) - Convert.ToInt32(txtCancelCount.Text);
            txtNetCount.Text = NetCount.ToString();

            txtNetCommission.Text = (dContractSum - dCancelSum).ToString("#,##0.00");
            CalcTotal();
            
        }


        private void FindCommPer()
        {
            string SQL;
            double dCommPer = 0;
            double dReservedPer = 0;
            clsDBO clR = new clsDBO();
            SQL = "select CommissionPer from payee where payeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("commissionper")))
                    dCommPer = 1;
                else
                    dCommPer = Convert.ToDouble(clR.GetFields("commissionper"));
            }

            dReservedPer = 1 - dCommPer;
            lblRatio.Text = (dCommPer * 100) + "/" + (dReservedPer * 100);

            txtAdvance.Text = (Convert.ToDouble(txtContractCommission.Text) * dCommPer).ToString("#,##0.00");
            txtReserved.Text = (Convert.ToDouble(txtContractCommission.Text) * dReservedPer).ToString("#,##0.00");

            txtYearAdvanced.Text = (Convert.ToDouble(txtYearContractCommission.Text) * dCommPer).ToString("#,##0.00");
            txtYearReserved.Text = (Convert.ToDouble(txtYearContractCommission.Text) * dReservedPer).ToString("#,##0.00");
        }


        private void CalcTotal ()
        {
            double dContractSum = 0;
            double dCancelSum = 0;
            clsDBO clR = new clsDBO();
            string SQL;
            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt * commissionper) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and reportdate >= '" + hfFrom.Value + "' and reportdate < '" + hfTo.Value + "' ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                    dContractSum = 0;
                else
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
            }

            SQL = "select sum(cancelamt * commissionper) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + hfFrom.Value + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                    dCancelSum = 0;             
                else
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
            }

            if (txtNetCommission.Text.Length == 0)
                txtNetCommission.Text = "0";

        }

        private void FillYearToDate()
        {
            double GrossTotal = 0;
            int NetCount;
            double dAdjust = 0;
            double dCancelSum = 0, dContractSum = 0;

            string startYear = "1/1/" + DateTime.Parse(hfFrom.Value).Year + " 12:00:00 AM";

            //FIND CONTRACT COUNT
            clsDBO clR = new clsDBO();
            string SQL = @"select count(contractid) as contractCount from contractcommissions
                        where payeeid = " + hfPayeeID.Value +
                        " and reportdate >= '" + startYear + "' and reportdate < '" + hfTo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtYearContractCount.Text = clR.GetFields("contractCount");
            }

            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and reportdate >= '" + startYear + "' and reportdate < '" + hfTo.Value + "' ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                {
                    dContractSum = 0;
                    txtYearContractCommission.Text = "0.00";
                }
                else
                {
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
                    if (dContractSum < 0)
                        txtContractCommission.Text = dContractSum.ToString("#,##0.00");
                    else
                        txtYearContractCommission.Text = dContractSum.ToString("#,##0.00");
                }
            }

            GrossTotal = dContractSum;

            //FIND ADJUST COUNT
            txtYearAdjustCount.Text = "0";
            txtYearAdjustCommission.Text = dEmptyValue.ToString();
            GrossTotal = GrossTotal - dAdjust;


            //FIND CANCEL COUNT
            SQL = "select count(contractid) as cancelcount from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + startYear + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtYearCancelCount.Text = clR.GetFields("cancelcount");
            }

            //FIND CANCEL COMMISSIONS
            SQL = "select sum(cancelamt) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + startYear + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                {
                    dCancelSum = 0;
                    txtYearCancelCommission.Text = "0.00";
                }
                else
                {
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
                    if (dCancelSum < 0)
                        txtContractCommission.Text = dCancelSum.ToString("#,##0.00");
                    else
                        txtYearCancelCommission.Text = dCancelSum.ToString("#,##0.00");
                }
            }

            //FIND NET COUNT
            NetCount = Convert.ToInt32(txtYearContractCount.Text) - Convert.ToInt32(txtYearAdjustCount.Text) - Convert.ToInt32(txtYearCancelCount.Text);
            txtYearNetCount.Text = NetCount.ToString();

            txtYearNetCommission.Text = (GrossTotal - dCancelSum).ToString("#,##0.00");
            CalcTotalYear(startYear);
        }


        private void CalcTotalYear(string startYear)
        {
            double dContractSum = 0;
            double dCancelSum = 0;
            clsDBO clR = new clsDBO();
            string SQL;
            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt * commissionper) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and reportdate >= '" + startYear  + "' and reportdate < '" + hfTo.Value + "' ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                    dContractSum = 0;
                else
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
            }

            SQL = "select sum(cancelamt * commissionper) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + startYear + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                    dCancelSum = 0;
                else
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
            }


            if (txtYearNetCommission.Text.Length == 0)
                txtYearNetCommission.Text = "0";

           // txtYearNetCommission.Text = (dContractSum - dCancelSum).ToString("#,##0.00");
           // txtYearReserved.Text = (Convert.ToDouble(txtYearGrossCommission.Text) - Convert.ToDouble(txtYearNetCommission.Text)).ToString("#,##0.00");
        }

        public void DetermineMonth()
        {
            string year = DateTime.Parse(hfFrom.Value).Year.ToString();
            if (lblFromDate.Text == "January 1, " + year && lblToDate.Text == "February 1, " + year )
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "January - " + year;
            }
            else if (lblFromDate.Text == "February 1, " + year && lblToDate.Text == "March 1, " + year)
            {
                    lblFromDate.Visible = false;
                    lblToDate.Text = "February - " + year;
            }
            else if (lblFromDate.Text == "March 1, " + year && lblToDate.Text == "April 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "March - " + year;
            }
            else if (lblFromDate.Text == "April 1, " + year && lblToDate.Text == "May 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "April - " + year;
            }
            else if (lblFromDate.Text == "May 1, " + year && lblToDate.Text == "June 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "May - " + year;
            }
            else if (lblFromDate.Text == "June 1, " + year && lblToDate.Text == "July 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "June - " + year;
            }
            else if (lblFromDate.Text == "July 1, " + year && lblToDate.Text == "August 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "July - " + year;
            }
            else if (lblFromDate.Text == "August 1, " + year && lblToDate.Text == "September 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "August - " + year;
            }
            else if (lblFromDate.Text == "September 1, " + year && lblToDate.Text == "October 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "September - " + year;
            }
            else if (lblFromDate.Text == "October 1, " + year && lblToDate.Text == "November 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "October - " + year;
            }
            else if (lblFromDate.Text == "November 1, " + year && lblToDate.Text == "December 1, " + year)
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "November - " + year;
            }
            else if (lblFromDate.Text == "December 1, " + year && lblToDate.Text == "January 1, " + (Convert.ToInt32(year) + 1))
            {
                lblFromDate.Visible = false;
                lblToDate.Text = "December - " + year;
            }
            else
            {
                lblFromDate.Visible = true;
            }
        }
    }
}