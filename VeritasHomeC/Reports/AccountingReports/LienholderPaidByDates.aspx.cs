﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC.Reports.AccountingReports
{
    public partial class LienholderPaidByDates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsLienholderSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            GetServerInfo();
            if (!IsPostBack)
            {

                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                pvPaid.Selected = true;
                lblSubmitError.Visible = false;
                tsLienholderContracts.Visible = false;
            }


            clsFunc.SetTestColor(pnlHeader, Image1);

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void tsLienholderContracts_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsLienholderContracts.SelectedTab.Value == "Paid")
                pvPaid.Selected = true;
            if (tsLienholderContracts.SelectedTab.Value == "Cancels")
                pvCancels.Selected = true;
        }

        protected void LienholderSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfLienholder.Value = LienholderSelect.SelectedText;
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            int yearFrom = Convert.ToDateTime(rdpFrom.SelectedDate).Year;
            int yearTo = Convert.ToDateTime(rdpTo.SelectedDate).Year;
            if (rdpFrom.SelectedDate == null || rdpTo.SelectedDate == null ||
                DateTime.Compare(Convert.ToDateTime(rdpTo.SelectedDate), Convert.ToDateTime(rdpFrom.SelectedDate)) < 0 ||
                    yearFrom < 2000 || yearTo > 2049)

            {
                lblSubmitError.Visible = true;
                tsLienholderContracts.Visible = false;
                rmLienholderContracts.Visible = false;
                return;

            }

            tsLienholderContracts.Visible = true;
            tsLienholderContracts.Tabs[0].Selected = true;
            lblSubmitError.Visible = false;

            pvPaid.ContentUrl = "~/reports/accountingreports/lienholderpaid.aspx?sid=" + hfID.Value + "&Lienholder=" + LienholderSelect.SelectedText
                    + "&From=" + rdpFrom.SelectedDate + "&To=" + rdpTo.SelectedDate;
            pvCancels.ContentUrl = "~/reports/accountingreports/lienholdercancels.aspx?sid=" + hfID.Value + "&Lienholder=" +
            LienholderSelect.SelectedText + "&From=" + rdpFrom.SelectedDate + "&To=" + rdpTo.SelectedDate;
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/accountingreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/agentssearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contractsearch.aspx?=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }
    }
}