﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class MonthlyBreakdowns : System.Web.UI.Page
    {
        private const double dEmptyValue = 0.00;

        protected void Page_Load(object sender, EventArgs e)
        {
            dsPayeeListName.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            lblYearDate.Text = txtYear.Text;
            lblYearDate2.Text = txtYear.Text;


            GetServerInfo();
            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                pnlBody.Visible = false;
                lblSelectError.Visible = false;

            }

            clsFunc.SetTestColor(pnlHeader, Image1);

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void PayeeListName_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfPayeeID.Value = PayeeListName.SelectedValue;
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            hfPayeeID.Value = PayeeListName.SelectedValue;

            if (String.IsNullOrEmpty(txtYear.Text) || !Information.IsNumeric(txtYear.Text) || txtYear.Text.Length < 4)
            {

                lblSelectError.Visible = true;
                pnlBody.Visible = false;
                return;
            }
                int y = Convert.ToInt32(txtYear.Text);

                if (y < 2000 || y > 2049)
                {
                    lblSelectError.Visible = true;
                    pnlBody.Visible = false;
                    return;
                }
                lblSelectError.Visible = false;
                pnlBody.Visible = true;
                pnlMonthlySummary.Visible = true;
                pnlQtrSummary.Visible = false;

                FillMonth();
                FillQuarter();
            
        }

        private void FillMonth()
        {
            clsDBO clR = new clsDBO();
            string SQL = "declare @year as nvarchar(4) = '" + txtYear.Text + "' " +
                "declare @PayeeID as nvarchar(10) = '" + PayeeListName.SelectedValue + "' " +
                @"select MonthName, PaidCnt,
                case when CommSumm is null then 0 else CommSumm end as CommSumm,
                0 as AdjCnt, 0 as AdjComm, CancelCnt, (Paidcnt - 0 - Cancelcnt) as NetCnt,
                case when CommSumm is null then 0 else CommSumm end - case when CancelAmt is null then 0 else CancelAmt end as NetAmt,
                case when CancelAmt is null then 0 else CancelAmt end as CancelAmt,  
                case when CommSumm is null then 0 else CommSumm end * commper as AdvanceAmt,
                case when CommSumm is null then 0 else CommSumm end * (1 - commper) as ReserveAmt
                from(
                select 'Jan' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '2/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '2/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '2/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '2/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Feb' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '2/1/' + @year and reportdate < '3/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '2/1/' + @year and reportdate < '3/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '2/1/' + @year and cancelreportdate < '3/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '2/1/' + @year and cancelreportdate < '3/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Mar' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '3/1/' + @year and reportdate < '4/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '3/1/' + @year and reportdate < '4/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '3/1/' + @year and cancelreportdate < '4/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '3/1/' + @year and cancelreportdate < '4/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Apr' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '4/1/' + @year and reportdate < '5/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '4/1/' + @year and reportdate < '5/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '4/1/' + @year and cancelreportdate < '5/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '4/1/' + @year and cancelreportdate < '5/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'May' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '5/1/' + @year and reportdate < '6/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '5/1/' + @year and reportdate < '6/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '5/1/' + @year and cancelreportdate< '6/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '5/1/' + @year and cancelreportdate< '6/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Jun' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '6/1/' + @year and reportdate < '7/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '6/1/' + @year and reportdate < '7/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '6/1/' + @year and cancelreportdate< '7/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '6/1/' + @year and cancelreportdate < '7/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Jul' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '7/1/' + @year and reportdate < '8/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '7/1/' + @year and reportdate < '8/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '7/1/' + @year and cancelreportdate < '8/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '7/1/' + @year and cancelreportdate < '8/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Aug' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '8/1/' + @year and reportdate < '9/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '8/1/' + @year and reportdate < '9/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '8/1/' + @year and cancelreportdate < '9/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '8/1/' + @year and cancelreportdate < '9/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Sep' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '9/1/' + @year and reportdate< '10/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '9/1/' + @year and reportdate< '10/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '9/1/' + @year and cancelreportdate< '10/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '9/1/' + @year and cancelreportdate < '10/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Oct' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '10/1/' + @year and reportdate < '11/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '10/1/' + @year and reportdate < '11/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '10/1/' + @year and cancelreportdate < '11/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '10/1/' + @year and cancelreportdate < '11/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Nov' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '11/1/' + @year and reportdate < '12/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '11/1/' + @year and reportdate < '12/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '11/1/' + @year and cancelreportdate < '12/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '11/1/' + @year and cancelreportdate < '12/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Dec' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '12/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '12/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '12/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '12/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Total' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                ) BreakDown order by case 
                when MonthName = 'Jan' Then '1'
                when MonthName = 'Feb' Then '2'
                when MonthName = 'Mar' Then '3'
                when MonthName = 'Apr' Then '4'
                when MonthName = 'May' Then '5'
                when MonthName = 'Jun' Then '6'
                when MonthName = 'Jul' Then '7'
                when MonthName = 'Aug' Then '8'
                when MonthName = 'Sep' Then '9'
                when MonthName = 'Oct' Then 'A'
                when MonthName = 'Nov' Then 'B'
                when MonthName = 'Dec' Then 'C'
                when MonthName = 'Total' Then 'D'
                else MonthName end asc";



                //"declare @PayeeID as nvarchar(10) = '" + PayeeListName.SelectedValue + "' " +
                //@"select MonthName, PaidCnt,
                //case when CommSumm is null then 0 else CommSumm end as CommSumm,
                //0 as AdjCnt, 0 as AdjComm, CancelCnt, 
                //case when CancelAmt is null then 0 else CancelAmt end as CancelAmt, 
                //case when CommSumm is null then 0 else CommSumm end - case when CancelAmt is null then 0 else CancelAmt end as GrossAmt, 
                //(case when NetAmt is null then 0 else NetAmt end - case when NetCancelAmt is null then 0 else NetCancelAmt end) as NetAmt,
                //case when CommSumm is null then 0 else CommSumm end - case when CancelAmt is null then 0 else CancelAmt end - ( case when NetAmt is null then 0 else NetAmt end - case when NetCancelAmt is null then 0 else NetCancelAmt end) as ReserveAmt
                //from (
                //select 'Jan' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '1/1/' + @year and datepaid < '2/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '1/1/' + @year and datepaid < '2/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '1/1/' + @year and canceldate < '2/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '1/1/' + @year and canceldate < '2/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '1/1/' + @year and datepaid < '2/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '1/1/' + @year and canceldate < '2/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Feb' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '2/1/' + @year and datepaid < '3/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '2/1/' + @year and datepaid < '3/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '2/1/' + @year and canceldate < '3/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '2/1/' + @year and canceldate < '3/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '2/1/' + @year and datepaid < '3/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '2/1/' + @year and canceldate < '3/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Mar' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '3/1/' + @year and datepaid < '4/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '4/1/' + @year and datepaid < '4/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '3/1/' + @year and canceldate < '4/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '3/1/' + @year and canceldate < '4/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '3/1/' + @year and datepaid < '4/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '3/1/' + @year and canceldate < '4/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Apr' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '4/1/' + @year and datepaid < '5/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '4/1/' + @year and datepaid < '5/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '4/1/' + @year and canceldate < '5/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '4/1/' + @year and canceldate < '5/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '4/1/' + @year and datepaid < '5/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '4/1/' + @year and canceldate < '5/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'May' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '5/1/' + @year and datepaid < '6/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '5/1/' + @year and datepaid < '6/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '5/1/' + @year and canceldate < '6/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '5/1/' + @year and canceldate < '6/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '5/1/' + @year and datepaid < '6/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '5/1/' + @year and canceldate < '6/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Jun' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '6/1/' + @year and datepaid < '7/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '6/1/' + @year and datepaid < '7/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '6/1/' + @year and canceldate < '7/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '6/1/' + @year and canceldate < '7/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '6/1/' + @year and datepaid < '7/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '6/1/' + @year and canceldate < '7/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Jul' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '7/1/' + @year and datepaid < '8/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '7/1/' + @year and datepaid < '8/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '7/1/' + @year and canceldate < '8/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '7/1/' + @year and canceldate < '8/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '7/1/' + @year and datepaid < '8/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '7/1/' + @year and canceldate < '8/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Aug' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '8/1/' + @year and datepaid < '9/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '8/1/' + @year and datepaid < '9/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '8/1/' + @year and canceldate < '9/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '8/1/' + @year and canceldate < '9/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '8/1/' + @year and datepaid < '9/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '8/1/' + @year and canceldate < '9/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Sep' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '9/1/' + @year and datepaid < '10/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '9/1/' + @year and datepaid < '10/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '9/1/' + @year and canceldate < '10/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '9/1/' + @year and canceldate < '10/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '9/1/' + @year and datepaid < '10/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '9/1/' + @year and canceldate < '10/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Oct' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '10/1/' + @year and datepaid < '11/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '10/1/' + @year and datepaid < '11/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '10/1/' + @year and canceldate < '11/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '10/1/' + @year and canceldate < '11/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '10/1/' + @year and datepaid < '11/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '10/1/' + @year and canceldate < '11/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Nov' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '11/1/' + @year and datepaid < '12/1/' + @year) as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '11/1/' + @year and datepaid < '12/1/' + @year) as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '11/1/' + @year and canceldate < '12/1/' + @year and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '11/1/' + @year and canceldate < '12/1/' + @year and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '11/1/' + @year and datepaid < '12/1/' + @year) as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '11/1/' + @year and canceldate < '12/1/' + @year and not datepaid is null) as NetCancelAmt
                //union
                //select 'Dec' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '12/1/' + @year and datepaid <= '12/31/' + @year + ' 11:59:59 PM') as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '12/1/' + @year and datepaid <= '12/31/' + @year + ' 11:59:59 PM') as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '12/1/' + @year and canceldate <= '12/31/' + @year + ' 11:59:59 PM'  and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '12/1/' + @year and canceldate <= '12/31/' + @year + ' 11:59:59 PM' and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '12/1/' + @year and datepaid <= '12/31/' + @year + ' 11:59:59 PM') as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '12/1/' + @year and canceldate <= '12/31/' +  @year + ' 11:59:59 PM' and not datepaid is null) as NetCancelAmt
                //union
                //select 'Total' as MonthName,
                //(select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and datepaid >= '1/1/' + @year and datepaid <= '12/31/' + @year + ' 11:59:59 PM') as PaidCnt,
                //(select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '1/1/' + @year and datepaid <= '12/31/' + @year + ' 11:59:59 PM') as CommSumm,
                //(select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and Canceldate >= '1/1/' + @year and canceldate <= '12/31/' + @year + ' 11:59:59 PM'  and not datepaid is null) as CancelCnt,
                //(select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '1/1/' + @year and canceldate <= '12/31/' + @year + ' 11:59:59 PM' and not datepaid is null) as CancelAmt,
                //(select sum(amt * commissionper) as summation from contractcommissions where payeeid = @PayeeID and datepaid >= '1/1/' + @year and datepaid <= '12/31/' + @year + ' 11:59:59 PM') as NetAmt,
                //(select sum(cancelamt * commissionPer) as cancelsum from contractcommissions where payeeid = @PayeeID and Canceldate >= '1/1/' + @year and canceldate <= '12/31/' +  @year + ' 11:59:59 PM' and not datepaid is null) as NetCancelAmt
                //) BreakDown order by case 
                //when MonthName = 'Jan' Then '1'
                //when MonthName = 'Feb' Then '2'
                //when MonthName = 'Mar' Then '3'
                //when MonthName = 'Apr' Then '4'
                //when MonthName = 'May' Then '5'
                //when MonthName = 'Jun' Then '6'
                //when MonthName = 'Jul' Then '7'
                //when MonthName = 'Aug' Then '8'
                //when MonthName = 'Sep' Then '9'
                //when MonthName = 'Oct' Then 'A'
                //when MonthName = 'Nov' Then 'B'
                //when MonthName = 'Dec' Then 'C'
                //when MonthName = 'Total' Then 'D'
                //else MonthName end asc";

            rgPaid.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgPaid.Rebind();
        }

        private void FillQuarter()
        {
            clsDBO clR = new clsDBO();
            string SQL = "declare @year as nvarchar(4) = '" + txtYear.Text + "' " +
                "declare @PayeeID as nvarchar(10) = '" + PayeeListName.SelectedValue + "' " +
                @"select MonthName, PaidCnt,
                case when CommSumm is null then 0 else CommSumm end as CommSumm,
                0 as AdjCnt, 0 as AdjComm, CancelCnt, (Paidcnt - 0 - Cancelcnt) as NetCnt,
                case when CommSumm is null then 0 else CommSumm end - case when CancelAmt is null then 0 else CancelAmt end as NetAmt,
                case when CancelAmt is null then 0 else CancelAmt end as CancelAmt,  
                case when CommSumm is null then 0 else CommSumm end * commper as AdvanceAmt,
                case when CommSumm is null then 0 else CommSumm end * (1 - commper) as ReserveAmt
                from(
                select 'Q1' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '4/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '4/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '4/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '4/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Q2' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '4/1/' + @year and reportdate < '7/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '4/1/' + @year and reportdate < '7/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '4/1/' + @year and cancelreportdate < '7/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '4/1/' + @year and cancelreportdate < '7/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Q3' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '7/1/' + @year and reportdate < '10/1/' + @year) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '7/1/' + @year and reportdate < '10/1/' + @year) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '7/1/' + @year and cancelreportdate < '10/1/' + @year and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '7/1/' + @year and cancelreportdate < '10/1/' + @year and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Q4' as MonthName,
                 (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '10/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '10/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '10/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '10/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                union
                select 'Total' as MonthName,
                (select count(contractid) as contractCount from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as PaidCnt,
                (select sum(amt) as summation from contractcommissions where payeeid = @PayeeID and reportdate >= '1/1/' + @year and reportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CommSumm,
                (select count(contractid) as cancelcount from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelCnt,
                (select sum(cancelamt) as cancelsum from contractcommissions where payeeid = @PayeeID and cancelreportdate >= '1/1/' + @year and cancelreportdate < '1/1/' + cast((cast(@year as int) + 1) as nvarchar(4)) and not reportdate is null) as CancelAmt,
                (select commissionPer from payee where payeeid = @PayeeID) as commper
                ) BreakDown order by case 
                when MonthName = 'Q1' Then '1'
                when MonthName = 'Q2' Then '2'
                when MonthName = 'Q3' Then '3'
                when MonthName = 'Q4' Then '4'
                when MonthName = 'Total' Then '5'
                else MonthName end asc";

            rgQuarterly.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgQuarterly.Rebind();
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/accountingreports.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/agentssearch.aspx?=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contractsearch.aspx?=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void rgPaid_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                rgPaid.ExportSettings.ExportOnlyData = false;
                rgPaid.ExportSettings.IgnorePaging = true;
                rgPaid.ExportSettings.OpenInNewWindow = true;
                rgPaid.ExportSettings.UseItemStyles = true;
                rgPaid.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgPaid.ExportSettings.FileName = "MonthlyCommissionsBreakdown";
                rgPaid.MasterTableView.ExportToExcel();

            }
        }

        

        protected void btnMonthly_Click(object sender, EventArgs e)
        {
            if (pnlMonthlySummary.Visible == false)
            {
                pnlMonthlySummary.Visible = true;
                pnlQtrSummary.Visible = false;
            }
        }

        protected void btnQuarterly_Click(object sender, EventArgs e)
        {
            if (pnlQtrSummary.Visible == false)
            {
                pnlMonthlySummary.Visible = false;
                pnlQtrSummary.Visible = true;
            }
        }

        protected void rgQuarterly_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                rgQuarterly.ExportSettings.ExportOnlyData = false;
                rgQuarterly.ExportSettings.IgnorePaging = true;
                rgQuarterly.ExportSettings.OpenInNewWindow = true;
                rgQuarterly.ExportSettings.UseItemStyles = true;
                rgQuarterly.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgQuarterly.ExportSettings.FileName = "QuarterlyCommissionsBreakdown";
                rgQuarterly.MasterTableView.ExportToExcel();

            }
        }

        protected void rgPaid_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/accountingreports/monthlybreakdownindividual.aspx?sid=" + hfID.Value
                + "&month=" + rgPaid.SelectedValue + "&payeeid=" + PayeeListName.SelectedValue + "&year=" + txtYear.Text);
        }
    }
}