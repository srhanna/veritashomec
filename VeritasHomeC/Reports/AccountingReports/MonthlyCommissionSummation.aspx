﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyCommissionSummation.aspx.cs" Inherits="VeritasHomeC.MonthlyCommissionSummation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
        * {
            font-family: Helvetica, Arial, sans-serif;
            font-size: small;
        }
    </style>
    <title>Monthly Commission Summation</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlError">
                    <asp:Panel runat="server" Font-Bold="true" fontsize="medium" style="padding-bottom: 10px;">
                        Payee Summary: 
                                    <asp:Label ID="lblFromDate" runat="server" Font-Size="small" Text="" ForeColor="#1a4688"></asp:Label>
                        -
                                    <asp:Label ID="lblToDate" runat="server" Font-Size="small" Text="" ForeColor="#1a4688"></asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" Font-Bold="true">
                     Ratio Adv/Res: 
                                <asp:Label ID="lblRatio" runat="server" ForeColor="#1a4688"></asp:Label>
                        </asp:Panel>
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                    &nbsp
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell >
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Style="text-align: left" Font-Bold="true" Font-Underline="true">
                                           Gross Contracts Paid
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                                    <asp:TableRow>
                                        <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtContractCount" runat="server" ReadOnly="true" Width="40" ></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Style="text-align: left">Commission: </asp:TableCell>
                                        <asp:TableCell Style="text-align: left">
                                            <asp:TextBox runat="server" ID="txtContractCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                        </asp:TableCell>
                                </asp:TableRow> 
                            </asp:Table>
                            </asp:TableCell>                     
                            <asp:TableCell>
                                <asp:Table runat="server" >
                                        <asp:TableRow>
                                    <asp:TableCell Style="text-align: left">
                                        <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Adjusted</asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                    </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                                <asp:TableRow>
                                    <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtAdjustCount" runat="server" ReadOnly="true"  Width="40"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Style="text-align: left">Commission: </asp:TableCell>
                                    <asp:TableCell Style="text-align: left">
                                        <asp:TextBox runat="server" ID="txtAdjustCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>  
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Cancelled</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                                    </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtCancelCount" runat="server" ReadOnly="true"  Width="40"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Style="text-align: left">Commission: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                    <asp:TextBox runat="server" ID="txtCancelCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Net Paid Commission</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                                    </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                            <asp:TableRow>
                                 <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtNetCount" runat="server" ReadOnly="true"  Width="40"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Style="text-align: left">Amount: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                    <asp:TextBox runat="server" ID="txtNetCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Advanced</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Amount: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                    <asp:TextBox runat="server" ID="txtAdvance" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                             <asp:TableCell>
                                <asp:Table runat="server" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Reserved</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Amount: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                    <asp:TextBox runat="server" ID="txtReserved" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                            <asp:TableRow><asp:TableCell>&nbsp</asp:TableCell></asp:TableRow>
                        </asp:Table>
                        <hr />
                        <asp:Table runat="server"><asp:TableRow><asp:TableCell>&nbsp</asp:TableCell></asp:TableRow></asp:Table>
                        <asp:Panel runat="server" Font-Bold="true" fontsize="medium">
                            Payee Summary: Year to Date - 
                                        <asp:Label ID="lblYearDate" runat="server" Font-Size="small" Text="" ForeColor="#1a4688"></asp:Label>
                        </asp:Panel>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                        &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell >
                                   <asp:Table runat="server" >
                                        <asp:TableRow>
                                    <asp:TableCell Style="text-align: left" Font-Bold="true" Font-Underline="true">
                                       Gross Contracts Paid
                                    </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                                <asp:TableRow>
                                    <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtYearContractCount" runat="server" ReadOnly="true" Width="40" ></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Style="text-align: left">Commission: </asp:TableCell>
                                    <asp:TableCell Style="text-align: left">
                                        <asp:TextBox runat="server" ID="txtYearContractCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow> 
                                   </asp:Table>
                            </asp:TableCell>                     
                            <asp:TableCell>
                                <asp:Table runat="server" >
                                        <asp:TableRow>
                                    <asp:TableCell Style="text-align: left">
                                        <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Adjusted</asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                               </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                                <asp:TableRow>
                                    <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtYearAdjustCount" runat="server" ReadOnly="true"  Width="40"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Style="text-align: left">Commission: </asp:TableCell>
                                    <asp:TableCell Style="text-align: left">
                                        <asp:TextBox runat="server" ID="txtYearAdjustCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>  
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Cancelled</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                                    </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtYearCancelCount" runat="server" ReadOnly="true"  Width="40"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Style="text-align: left">Commission: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                    <asp:TextBox runat="server" ID="txtYearCancelCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server"  >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Net Paid Commission</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                                    </asp:Table>
                                <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden">
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Count: </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtYearNetCount" runat="server" ReadOnly="true"  Width="40"></asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell Style="text-align: left">Amount: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                    <asp:TextBox runat="server" ID="txtYearNetCommission" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table  runat="server" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Advanced</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                                    </asp:Table>
                                 <asp:Table runat="server" BorderWidth="1" style="border-left:hidden; border-top:hidden; border-bottom:hidden" >
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Amount: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                  <asp:TextBox runat="server" ID="txtYearAdvanced" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                             <asp:TableCell>
                                <asp:Table runat="server" >
                                    <asp:TableRow>
                                <asp:TableCell Style="text-align: left">
                                    <asp:Label runat="server" Font-Bold="true" Font-Underline="true">Reserved</asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                                    </asp:Table>
                                 <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Style="text-align: left">Amount: </asp:TableCell>
                                <asp:TableCell Style="text-align: left">
                                  <asp:TextBox runat="server" ID="txtYearReserved" ReadOnly="true" Width="100"></asp:TextBox>
                                </asp:TableCell>
                                </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                            <asp:TableRow><asp:TableCell>&nbsp</asp:TableCell></asp:TableRow>
                        </asp:Table>

                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfPayeeID" runat="server" />
        <asp:HiddenField ID="hfFrom" runat="server" />
        <asp:HiddenField ID="hfTo" runat="server" />
    </form>
</body>
</html>
