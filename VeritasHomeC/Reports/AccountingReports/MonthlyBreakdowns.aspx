﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyBreakdowns.aspx.cs" Inherits="VeritasHomeC.MonthlyBreakdowns" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
   <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Monthly Breakdowns</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="200">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" OnClick="btnSalesReports_Click" runat="server" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" OnClick="btnClaimsReports_Click" runat="server" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPhoneReports" runat="server" Text="Phone Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" OnClick="btnAccountingReports_Click" runat="server" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" runat="server" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top" Width="1000">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Payee Name:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDropDownList AutoPostBack="true"
                                             ID="PayeeListName" 
                                             runat="server" 
                                             DataValueField="PayeeID"
                                             OnSelectedIndexChanged="PayeeListName_SelectedIndexChanged"
                                             DataTextField="CompanyName" 
                                             DataSourceID="dsPayeeListName">
                                        </telerik:RadDropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                         Enter Year: 
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" DefaultButton="btnGenerate">
                                        <telerik:RadNumericTextBox ID="txtYear" runat="server" MaxLength="4" Width="50" Type="Number">
                                            <NumberFormat AllowRounding="false" DecimalDigits="0" GroupSeparator="" />
                                        </telerik:RadNumericTextBox>
                                            </asp:Panel>
                                    </asp:TableCell>
                               </asp:TableRow>
                               <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnGenerate" runat="server" OnClick="btnGenerate_Click" Text="Generate Report" CssClass="button2" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblSelectError" runat="server" Text="Please enter a valid year." ForeColor="Red" />
                                    </asp:TableCell>
                               </asp:TableRow>
                           </asp:Table>
                           <asp:Panel ID="pnlBody" runat="server">
                           <hr />
                           <asp:Button ID="btnMonthly" runat="server" Text="Monthly" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" OnClick="btnMonthly_Click" />
                           <asp:Button ID="btnQuarterly" runat="server" Text="Quarterly" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" OnClick="btnQuarterly_Click" />
                           <asp:Panel ID="pnlMonthlySummary" runat="server" fontsize="medium" style="padding-top:15px;"> 
                               <asp:Label Text="Payee Summary: Monthly Commissions Breakdown - " runat="server" Font-Bold="true" /> 
                               <asp:Label ID="lblYearDate" runat="server" Font-Size="small" Text="" Font-Bold="true" ForeColor="#1a4688"></asp:Label>
                               &nbsp
                               <asp:Panel runat="server">&nbsp</asp:Panel>
                                   <asp:Table runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPaid">
                                                <telerik:RadGrid ID="rgPaid" runat="server" OnSelectedIndexChanged="rgPaid_SelectedIndexChanged" AutoGenerateColumns="false" AllowSorting="false" Width="1500" OnItemCommand="rgPaid_ItemCommand">
                                                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" DataKeyNames="MonthName"
                                                        CommandItemDisplay="TopAndBottom">
                                                        <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                        <ColumnGroups>
                                                            <telerik:GridColumnGroup HeaderText=" Gross Contracts Paid" Name="ContractsPaid" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Adjusted" Name="Adjusted" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Cancelled" Name="Cancelled" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Net Contracts Paid" Name="Net" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Advanced" Name="Advanced" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Reserved" Name="Reserved" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                        </ColumnGroups>
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="MonthName" UniqueName="MonthName" HeaderText="Month" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PaidCnt" UniqueName="PaidCnt" HeaderText="Count" ColumnGroupName="ContractsPaid"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CommSumm" UniqueName="CommSumm" HeaderText="Commission" ColumnGroupName="ContractsPaid" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AdjCnt" UniqueName="AdjCnt" HeaderText="Count" ColumnGroupName="Adjusted"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AdjComm" UniqueName="AdjComm" HeaderText="Commission" ColumnGroupName="Adjusted" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CancelCnt" UniqueName="CancelCnt" HeaderText="Count" ColumnGroupName="Cancelled"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CancelAmt" UniqueName="CancelAmt" HeaderText="Commission" ColumnGroupName="Cancelled" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="NetCnt" UniqueName="NetCnt" HeaderText="Count" ColumnGroupName="Net"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="NetAmt" UniqueName="NetAmt" HeaderText="Commission" ColumnGroupName="Net" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AdvanceAmt" UniqueName="AdvanceAmt" HeaderText="Amount" ColumnGroupName="Advanced" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ReserveAmt" UniqueName="ReserveAmt" HeaderText="Amount" ColumnGroupName="Reserved" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </telerik:RadAjaxPanel>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp</asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel ID="pnlQtrSummary" runat="server" fontsize="medium" style="padding-top:15px;">
                                <asp:Label runat="server" Text="Payee Summary: Quarterly Commissions Breakdown - " Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblYearDate2" runat="server" Font-Size="small" Text="" Font-Bold="true" ForeColor="#1a4688"></asp:Label>
                                <asp:Panel runat="server">&nbsp</asp:Panel>
                                <asp:Table ID="tblQtrSummary" runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="rgQuarterly">
                                                <telerik:RadGrid ID="rgQuarterly" runat="server" OnSelectedIndexChanged="rgPaid_SelectedIndexChanged" AutoGenerateColumns="false" AllowSorting="false" Width="1500" OnItemCommand="rgPaid_ItemCommand">
                                                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" DataKeyNames="MonthName"
                                                        CommandItemDisplay="TopAndBottom">
                                                        <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                                        <ColumnGroups>
                                                            <telerik:GridColumnGroup HeaderText=" Gross Contracts Paid" Name="ContractsPaid" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Adjusted" Name="Adjusted" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Cancelled" Name="Cancelled" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Net Contracts Paid" Name="Net" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Advanced" Name="Advanced" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                            <telerik:GridColumnGroup HeaderText="Reserved" Name="Reserved" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                                        </ColumnGroups>
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="MonthName" UniqueName="MonthName" HeaderText="Month" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PaidCnt" UniqueName="PaidCnt" HeaderText="Count" ColumnGroupName="ContractsPaid"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CommSumm" UniqueName="CommSumm" HeaderText="Commission" ColumnGroupName="ContractsPaid" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AdjCnt" UniqueName="AdjCnt" HeaderText="Count" ColumnGroupName="Adjusted"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AdjComm" UniqueName="AdjComm" HeaderText="Commission" ColumnGroupName="Adjusted" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CancelCnt" UniqueName="CancelCnt" HeaderText="Count" ColumnGroupName="Cancelled"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="CancelAmt" UniqueName="CancelAmt" HeaderText="Commission" ColumnGroupName="Cancelled" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="NetCnt" UniqueName="NetCnt" HeaderText="Count" ColumnGroupName="Net"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="NetAmt" UniqueName="NetAmt" HeaderText="Commission" ColumnGroupName="Net" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AdvanceAmt" UniqueName="AdvanceAmt" HeaderText="Amount" ColumnGroupName="Advanced" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ReserveAmt" UniqueName="ReserveAmt" HeaderText="Amount" ColumnGroupName="Reserved" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </telerik:RadAjaxPanel>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                        <asp:TableRow><asp:TableCell>&nbsp</asp:TableCell></asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:SqlDataSource ID="dsPayeeListName"
            ProviderName="System.Data.SqlClient" SelectCommand="select PayeeID, PayeeNo, CompanyName, Addr1 as Address, City, State, Zip, Phone from payee" runat="server">
        </asp:SqlDataSource>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfPayeeID" runat="server" />
        <asp:HiddenField ID="hfFrom" runat="server" />
        <asp:HiddenField ID="hfTo" runat="server" />
    </form>
</body>
</html>
