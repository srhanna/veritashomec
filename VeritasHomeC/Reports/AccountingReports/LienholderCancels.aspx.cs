﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC.Reports.AccountingReports
{
    public partial class LienholderCancels : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfLienholder.Value = Request.QueryString["lienholder"];
            hfTo.Value = Request.QueryString["to"];
            hfFrom.Value = Request.QueryString["from"];

            GetServerInfo();
            if (!IsPostBack)
            {
                FillGrid();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillGrid()
        {
            clsDBO clR = new clsDBO();
            string SQL = @"select contractno as ContractNo, fname + ' ' + lname as CustName, DealerNo, DealerName, pt.plantype, termmonth,
                        saledate, c.datepaid, moxydealercost as DealerCost, lienholder
                        from contract c
                        inner join dealer d on d.dealerid = c.dealerid
                        inner join plantype pt on pt.plantypeid = c.plantypeid
                        inner join contractcancel cc on c.contractid = cc.contractid
                        where cc.canceldate >= '" + hfFrom.Value + "' and cc.canceldate < '" + hfTo.Value + 
                        "' and c.lienholder = '" + hfLienholder.Value + "' and not datepaid is null order by contractno";

            rgCancels.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgCancels.Rebind();
        }

        protected void rgCancels_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                string from = Convert.ToDateTime(hfFrom.Value).ToString("d");
                string to = Convert.ToDateTime(hfTo.Value).ToString("d");
                rgCancels.ExportSettings.ExportOnlyData = false;
                rgCancels.ExportSettings.IgnorePaging = true;
                rgCancels.ExportSettings.OpenInNewWindow = true;
                rgCancels.ExportSettings.UseItemStyles = true;
                rgCancels.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgCancels.ExportSettings.FileName = "LienholderCancels";
                rgCancels.MasterTableView.ExportToExcel();

            }
        }
    }
}