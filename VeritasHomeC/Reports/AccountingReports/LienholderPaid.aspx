﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LienholderPaid.aspx.cs" Inherits="VeritasHomeC.Reports.AccountingReports.LienholderPaid" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
     <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Lienholder Contracts Paid</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="updatePanel1" runat="server">
            <ContentTemplate>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPaid">
                <telerik:RadGrid ID="rgPaid" runat="server" AutoGenerateColumns="false" AllowSorting="true" Width="1500" OnItemCommand="rgPaid_ItemCommand">
                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                        CommandItemDisplay="TopAndBottom">
                        <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CustName" UniqueName="CustName" HeaderText="Customer"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Seller No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DealerName" UniqueName="DealerName" HeaderText="Seller Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PlanType" UniqueName="PlanType" HeaderText="Plan"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="TermMonth" UniqueName="TermMonth" HeaderText="Term Month"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DealerCost" UniqueName="DealerCost" HeaderText="Dealer Cost" DataFormatString="{0:#,##0.00}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Lienholder" UniqueName="Lienholder" HeaderText="Lienholder"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </telerik:RadAjaxPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
                
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfLienholder" runat="server" />
<asp:HiddenField ID="hfFrom" runat="server" />
<asp:HiddenField ID="hfTo" runat="server" />
    </form>
</body>
</html>
