﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class RatingVerification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetServerInfo();
            if (!IsPostBack)
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

            FillGrid();

            RadAjaxPanel1.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                {
                    btnSalesReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                {
                    btnAccountingReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                {
                    btnClaimsReports.Enabled = true;
                    btnReports.Enabled = true;
                }
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                {
                    btnCustomReports.Enabled = true;
                    btnReports.Enabled = true;
                }
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnClaimsReports.Enabled = false;
            btnSalesReports.Enabled = false;
            btnAccountingReports.Enabled = false;
            btnCustomReports.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void FillGrid()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = @"select contractno, c.Status, MoxyDealerCost, CustomerCost, pt.PlanType, TermMonth, (cr.Amt) as ClaimReserve, (cp.amt) as ClipFee, 
                (tax.Amt) as PremiumTax, (a.Amt) as Admin, (m.Amt) as Markup, 
                case when (jd.Amt) is null then 0 else (jd.Amt) end  as Comm, case when f.Amt is null then 0 else f.Amt end as CRM,
                (MoxyDealerCost - case when (cr.Amt) is null then 0 else (cr.Amt) end - 
                case when (cp.amt - cp.CancelAmt) is null then 0 else (cp.amt) end - 
                case when (tax.Amt - tax.CancelAmt) is null then 0 else (tax.Amt) end - 
                case when (a.Amt) is null then 0 else (a.Amt) end - 
                case when (m.Amt) is null then 0 else (m.Amt) end -
                case when (jd.Amt) is null then 0 else (jd.Amt) end - 
                case when (f.Amt) is null then 0 else (f.Amt) end) as DiffAmt
                from contract c
                inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=1) cr on cr.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=2) cp on cp.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=3) tax on tax.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=4) a on a.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=5) m on m.ContractID = c.ContractID
                left join (select contractid, sum(amt) as Amt from contractamt where RateTypeID=4015 group by contractid) jd on jd.ContractID = c.ContractID
                left join (select contractid, amt, CANCELamt from contractamt where RateTypeID=4017) f on f.ContractID = c.ContractID
                where ((MoxyDealerCost - case when (cr.Amt) is null then 0 else (cr.Amt) end - 
                case when (cp.amt) is null then 0 else (cp.amt) end - 
                case when (tax.Amt) is null then 0 else (tax.Amt) end - 
                case when (a.Amt) is null then 0 else (a.Amt) end - 
                case when (m.Amt) is null then 0 else (m.Amt) end -
                case when (jd.Amt) is null then 0 else (jd.Amt) end - 
                case when (f.Amt) is null then 0 else (f.Amt) end) > 1
                or
                (MoxyDealerCost - case when (cr.Amt) is null then 0 else (cr.Amt) end - 
                case when (cp.amt) is null then 0 else (cp.amt) end - 
                case when (tax.Amt) is null then 0 else (tax.Amt) end - 
                case when (a.Amt) is null then 0 else (a.Amt) end - 
                case when (m.Amt) is null then 0 else (m.Amt) end -
                case when (jd.Amt) is null then 0 else (jd.Amt) end - 
                case when (f.Amt) is null then 0 else (f.Amt) end) < -1)";

            rgPaid.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgPaid.Rebind();
            RadAjaxPanel1.Visible = true;
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);

        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnSalesReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/salesreports.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/claimsreports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccountingReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/AccountingReports.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void rgPaid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string a;
            a = e.CommandName;
            if (e.CommandName == "ExportToExcel")
            {
                rgPaid.ExportSettings.ExportOnlyData = false;
                rgPaid.ExportSettings.IgnorePaging = true;
                rgPaid.ExportSettings.OpenInNewWindow = true;
                rgPaid.ExportSettings.UseItemStyles = true;
                rgPaid.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgPaid.ExportSettings.FileName = "RatingVerification";
                rgPaid.MasterTableView.ExportToExcel();
            }
        }
    }
}