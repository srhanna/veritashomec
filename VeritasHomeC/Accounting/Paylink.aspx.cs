﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;
using System.Data;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class Paylink : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
           
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                pnlMassUpdate.Visible = true;
                pnlResult.Visible = false;
                FillContractPayee();

                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                clsFunc.SetTestColor(pnlHeader, Image1);

                dtPayDate.SelectedDate = DateTime.Today;

             
            }

          
            rgContractsUpdate.Visible = false;
        }

        private void FillContractPayee()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select contractpayeeid, payeename from contractpayee order by payeename";
            cboPayee.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            cboPayee.DataBind();
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            clsDBO clTD = new clsDBO();
            string SQL = "select * from usermessage where toid = " + hfUserID.Value + " "
                + "and completedmessage = 0 and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            //dtPayDate.SelectedDate = DateTime.Today;
            //txtContracts.Text = "";
            ClearMassUpdateTable();
            Response.Redirect("~/accounting/paylink.aspx?sid=" + hfID.Value);

        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
           
            ClearMassUpdateTable();

            foreach (var row in rgContractsUpdate.Items)
            {
                GridDataItem dataRow = row as GridDataItem;
                TableCell ContractNo = dataRow["ContractNo"];
                string sContractNo = ContractNo.Text;
                TableCell DealerCost = dataRow["DealerCost"];
                string sDealerCost = DealerCost.Text;

                if (!Information.IsDate(dtPayDate.SelectedDate))
                {
                    goto MoveHere;
                }

                
                SetMassUpdateTable(sContractNo, sDealerCost);

                SQL = "select * from contractmassupdate where contractno = '" + sContractNo + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    if (clR.GetFields("moxydealercost") != clR.GetFields("dealercost"))
                    {
                        goto MoveHere;
                        
                    }
                    else
                    {
                            UpdateContract(sContractNo, sDealerCost);
                    }
                }
               
                MoveHere:;
            }

            FillResultGrids();

            RadTab tabNoErrors = tsResult.Tabs.FindTabByValue("NoErrors");
            RadTab tabErrors = tsResult.Tabs.FindTabByValue("Errors");

            if (rgError.Items.Count == 0)
            {
                tabNoErrors.Selected = true;
                pnlErrors.Visible = false;
                rwSuccess.RadAlert("All contracts updated successfully", 400, 100, "Success", "");
            }
            else
            {
                tabErrors.Selected = true;
                pnlNoErrors.Visible = false;
                rwError.RadAlert("Some contracts were not processed. Please check the error list.", 400, 100, "Error", "");
                
            }
            
            pnlMassUpdate.Visible = false;
            pnlResult.Visible = true;
        }


        private void FillResultGrids()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select contractno, moxydealercost, dealercost from contractmassupdate where moxydealercost <> dealercost";
            rgError.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgError.DataBind();

            SQL = "select contractno, moxydealercost, dealercost from contractmassupdate where moxydealercost = dealercost";
            rgNoError.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgNoError.DataBind();
        }

        private void SetMassUpdateTable(string xContractNo, string  xDealerCost)
        {
            clsDBO clR = new clsDBO();
            string sMoxyDealerCost = GetDealerCost(xContractNo);
            string SQL = "insert into contractmassupdate (ContractNo, DatePaid, MoxyDealerCost, DealerCost)" +
                " values ('" + xContractNo + "', '" + 
                Convert.ToDateTime(dtPayDate.SelectedDate).ToString("d") + "', " + sMoxyDealerCost + ", " + xDealerCost + ") ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);

        }

        private string GetDealerCost(string xContractNo)
        {
            string sMoxyDealerCost;
            clsDBO clR = new clsDBO();
            string SQL = "select * from contract where contractno = '" + xContractNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sMoxyDealerCost = clR.GetFields("moxydealercost");
                SQL = "select sum(paidamt) as amt from contractpayment " +
                    "where contractid = " + GetContractID(xContractNo);
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    if (clR.GetFields("amt").Length > 0)
                    {
                        sMoxyDealerCost = (Convert.ToDouble(sMoxyDealerCost) - Convert.ToDouble(clR.GetFields("amt"))).ToString();
                        
                    }
                }

                return sMoxyDealerCost;
            }

            return "0";
        }


        private void UpdateContract(string xContractNo, string xDealerCost)
        {
            clsDBO clR = new clsDBO();
            string SQL;

            if (!Information.IsDate(dtPayDate.SelectedDate))
                return;

            SQL = "update contract ";
            SQL = SQL + "set datepaid = '" + Convert.ToDateTime(dtPayDate.SelectedDate).ToString("d") + "', ";
            SQL = SQL + "status = 'Paid' ";
            SQL = SQL + "where (status <> 'Paid' ";
            SQL = SQL + "or status is null) ";
            SQL = SQL + "and datepaid is null ";
            SQL = SQL + "and contractno = '" + xContractNo + "' ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);

            SQL = "insert into contractpayment (ContractID, DatePaid, PaidAmt, PayeeID, PaymentType, CheckNo) values (" + GetContractID(xContractNo) +
                ", '" + Convert.ToDateTime(dtPayDate.SelectedDate).ToString("d") + "', " + xDealerCost +
                ", " + hfPayeeID.Value + ", 'Income', 'ACH') ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private string GetContractID(string xContractNo)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contract where contractno = '" + xContractNo + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("contractid");
            }

            return "0";
        }

        private void ClearMassUpdateTable()
        {
            clsDBO clR = new clsDBO();
            string SQL = "truncate table contractmassupdate ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPayee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/payee.aspx?sid=" + hfID.Value);
        }

        protected void btnCancellation_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/bulkcancellation.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissions_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/commissions/commissions.aspx?sid=" + hfID.Value);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlResult.Visible = false;
            pnlMassUpdate.Visible = true;
            rgContractsUpdate.Visible = true;
        }

        protected void tsResult_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsResult.SelectedTab.Value == "NoErrors")
            {
                pnlNoErrors.Visible = true;
                pnlErrors.Visible = false;
            }

            if (tsResult.SelectedTab.Value == "Errors")
            {
                pnlErrors.Visible = true;
                pnlNoErrors.Visible = false;
            }

        }

        protected void txtContracts_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2]
                {
                new DataColumn("ContractNo", typeof(string)),
                new DataColumn("DealerCost", typeof(double))
                });
                int rowCount = 0;

                string copiedContent = Request.Form[txtContracts.UniqueID];
                foreach (string row in copiedContent.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (string cell in row.Split('\t'))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;

                        }
                    }

                    rowCount++;
                }
                rgContractsUpdate.DataSource = dt;
                rgContractsUpdate.DataBind();
                txtContracts.Text = "";


                rgContractsUpdate.Visible = true;
                txtContracts.Visible = false;
            }
            catch
            {
                rwFormatError.RadAlert("Format not correct. Please try again", 400, 100, "Error", "");
            }
        }

        protected void rgError_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string a;
            a = e.CommandName;
            if (e.CommandName == "ExportToExcel")
            {
                rgError.ExportSettings.ExportOnlyData = false;
                rgError.ExportSettings.IgnorePaging = true;
                rgError.ExportSettings.OpenInNewWindow = true;
                rgError.ExportSettings.UseItemStyles = true;
                rgError.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgError.ExportSettings.FileName = "BulkPayErrors";
                rgError.MasterTableView.ExportToExcel();
            }
        }

        protected void rgNoError_ItemCommand(object sender, GridCommandEventArgs e)
        {
            string a;
            a = e.CommandName;
            if (e.CommandName == "ExportToExcel")
            {
                rgNoError.ExportSettings.ExportOnlyData = false;
                rgNoError.ExportSettings.IgnorePaging = true;
                rgNoError.ExportSettings.OpenInNewWindow = true;
                rgNoError.ExportSettings.UseItemStyles = true;
                rgNoError.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgNoError.ExportSettings.FileName = "BulkPaySuccessful";
                rgNoError.MasterTableView.ExportToExcel();
            }
        }

        protected void cboPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPayeeID.Value = cboPayee.SelectedValue;
        }

        protected void btnContractPayee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/contractpayee.aspx?sid=" + hfID.Value);
        }
    }
}