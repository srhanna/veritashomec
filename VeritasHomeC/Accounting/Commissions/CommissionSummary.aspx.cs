﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{

    public partial class CommissionSummary : System.Web.UI.Page
    {
        private const double dEmptyValue = 0.00;

        protected void Page_Load(object sender, EventArgs e)
        {
            hfPayeeID.Value = Request.QueryString["payeeid"];
            hfTo.Value = Request.QueryString["to"];

            lblToDate.Text = Convert.ToDateTime(hfTo.Value).ToString("MMMM d, yyyy");
            lblYearDate.Text = DateTime.Parse(hfTo.Value).AddMonths(-1).Year.ToString();


            if (!IsPostBack)
            {
                GetServerInfo();
                FillMonthlySummary();
                FillYearToDate();
                FindCommPer();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillMonthlySummary()
        {
            double NetTotal = 0;
            int NetCount = 0;
            double dCommPer = 0;
            double dAdjust = 0;
            double dCancelSum = 0, dContractSum = 0;

            //FIND CONTRACT COUNT
            clsDBO clR = new clsDBO();
            string SQL = @"select count(contractid) as contractCount from contractcommissions
                        where payeeid = " + hfPayeeID.Value + 
                        " and datepaid < '" + hfTo.Value + "' and reportdate is null ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtContractCount.Text = clR.GetFields("contractCount");
            }

            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and datepaid < '" + hfTo.Value + "' and reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                {
                    dContractSum = 0;
                    txtContractCommission.Text = "0.00";
                }
                else
                {
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
                    if (dContractSum < 0)
                        txtContractCommission.Text = dContractSum.ToString("#,##0.00");
                    else
                        txtContractCommission.Text = dContractSum.ToString("#,##0.00");
                }
            }

            NetTotal = dContractSum;

            //FIND ADJUST COUNT
            txtAdjustCount.Text = "0";
            txtAdjustCommission.Text = dEmptyValue.ToString();
            NetTotal = NetTotal - dAdjust;


            //FIND CANCEL COUNT
            SQL = "select count(contractid) as cancelcount from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and canceldate < '" + hfTo.Value + "' and cancelreportdate is null " +
                "and not datepaid is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCancelCount.Text = clR.GetFields("cancelcount");
            }

            //FIND CANCEL COMMISSIONS
            SQL = "select sum(cancelamt) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and canceldate < '" + hfTo.Value + "' and cancelreportdate is null " +   
                "and not datepaid is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                {
                    dCancelSum = 0;
                    txtCancelCommission.Text = "0.00";
                }
                else
                {
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
                    if (dCancelSum < 0)
                        txtCancelCommission.Text = dCancelSum.ToString("#,##0.00");
                    else
                        txtCancelCommission.Text = dCancelSum.ToString("#,##0.00");
                }
            }

            //FIND NET COUNT
            NetCount = Convert.ToInt32(txtContractCount.Text) - Convert.ToInt32(txtAdjustCount.Text) - Convert.ToInt32(txtCancelCount.Text);
            txtNetCount.Text = NetCount.ToString();

            txtNetCommission.Text = (dContractSum - dCancelSum).ToString("#,##0.00");
            CalcTotal();
            
        }


        private void FindCommPer()
        {
            string SQL;
            double dCommPer = 0;
            double dReservedPer = 0;
            clsDBO clR = new clsDBO();
            SQL = "select CommissionPer from payee where payeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("commissionper")))
                    dCommPer = 1;
                else
                    dCommPer = Convert.ToDouble(clR.GetFields("commissionper"));
            }

            dReservedPer = 1 - dCommPer;
            lblRatio.Text = (dCommPer * 100) + "/" + (dReservedPer * 100);

            txtAdvance.Text = (Convert.ToDouble(txtContractCommission.Text) * dCommPer).ToString("#,##0.00");
            txtReserved.Text = (Convert.ToDouble(txtContractCommission.Text) * dReservedPer).ToString("#,##0.00");

            txtYearAdvanced.Text = (Convert.ToDouble(txtYearContractCommission.Text) * dCommPer).ToString("#,##0.00");
            txtYearReserved.Text = (Convert.ToDouble(txtYearContractCommission.Text) * dReservedPer).ToString("#,##0.00");
        }


        private void CalcTotal ()
        {
            double dContractSum = 0;
            double dCancelSum = 0;
            clsDBO clR = new clsDBO();
            string SQL;
            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt * commissionper) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and datepaid < '" + hfTo.Value + "' and reportdate is null";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                    dContractSum = 0;
                else
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
            }

            SQL = "select sum(cancelamt * commissionper) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
               " and canceldate < '" + hfTo.Value + "' and cancelreportdate is null " +
                "and not datepaid is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                    dCancelSum = 0;             
                else
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
            }

            if (txtNetCommission.Text.Length == 0)
                txtNetCommission.Text = "0";

        }

        private void FillYearToDate()
        {
            double GrossTotal = 0;
            int NetCount;
            double dAdjust = 0;
            double dCancelSum = 0, dContractSum = 0;

            string startYear = "1/1/" + DateTime.Parse(hfTo.Value).AddMonths(-1).Year + " 12:00:00 AM";

            //FIND CONTRACT COUNT
            clsDBO clR = new clsDBO();
            string SQL = @"select count(contractid) as contractCount from contractcommissions
                        where payeeid = " + hfPayeeID.Value +
                        " and reportdate >= '" + startYear + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();

                txtYearContractCount.Text = clR.GetFields("contractCount");
            }
            else
            {
                txtYearContractCount.Text = "0";
            }

            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and reportdate >= '" + startYear + "' and reportdate < '" + hfTo.Value + "' ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                {
                    dContractSum = 0;
                    txtYearContractCommission.Text = "0.00";
                }
                else
                {
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
                    if (dContractSum < 0)
                        txtContractCommission.Text = dContractSum.ToString("#,##0.00");
                    else
                        txtYearContractCommission.Text = dContractSum.ToString("#,##0.00");
                }
            }

            GrossTotal = dContractSum;

            //FIND ADJUST COUNT
            txtYearAdjustCount.Text = "0";
            txtYearAdjustCommission.Text = dEmptyValue.ToString();
            GrossTotal = GrossTotal - dAdjust;


            //FIND CANCEL COUNT
            SQL = "select count(contractid) as cancelcount from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + startYear + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtYearCancelCount.Text = clR.GetFields("cancelcount");
            }

            //FIND CANCEL COMMISSIONS
            SQL = "select sum(cancelamt) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + startYear + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                {
                    dCancelSum = 0;
                    txtYearCancelCommission.Text = "0.00";
                }
                else
                {
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
                    if (dCancelSum < 0)
                        txtContractCommission.Text = dCancelSum.ToString("#,##0.00");
                    else
                        txtYearCancelCommission.Text = dCancelSum.ToString("#,##0.00");
                }
            }

            //FIND NET COUNT
            NetCount = Convert.ToInt32(txtYearContractCount.Text) - Convert.ToInt32(txtYearAdjustCount.Text) - Convert.ToInt32(txtYearCancelCount.Text);
            txtYearNetCount.Text = NetCount.ToString();

            txtYearNetCommission.Text = (GrossTotal - dCancelSum).ToString("#,##0.00");
            CalcTotalYear(startYear);
        }


        private void CalcTotalYear(string startYear)
        {
            double dContractSum = 0;
            double dCancelSum = 0;
            clsDBO clR = new clsDBO();
            string SQL;
            //FIND CONTRACT COMMISSIONS SUMMATION
            SQL = "select sum(amt * commissionper) as summation from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and reportdate >= '" + startYear  + "' and reportdate < '" + hfTo.Value + "' ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("summation")))
                    dContractSum = 0;
                else
                    dContractSum = Convert.ToDouble(clR.GetFields("summation"));
            }

            SQL = "select sum(cancelamt * commissionper) as cancelsum from contractcommissions where payeeid = " + hfPayeeID.Value +
                " and cancelreportdate >= '" + startYear + "' and cancelreportdate < '" + hfTo.Value + "' " +
                "and not reportdate is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("cancelsum")))
                    dCancelSum = 0;
                else
                    dCancelSum = Convert.ToDouble(clR.GetFields("cancelsum"));
            }


            if (txtYearNetCommission.Text.Length == 0)
                txtYearNetCommission.Text = "0";

           // txtYearNetCommission.Text = (dContractSum - dCancelSum).ToString("#,##0.00");
           // txtYearReserved.Text = (Convert.ToDouble(txtYearGrossCommission.Text) - Convert.ToDouble(txtYearNetCommission.Text)).ToString("#,##0.00");
        }

    }
}