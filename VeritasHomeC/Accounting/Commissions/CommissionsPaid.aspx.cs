﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class CommissionsPaid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfPayeeID.Value = Request.QueryString["payeeid"];
            hfTo.Value = Request.QueryString["to"];
            hfFrom.Value = Request.QueryString["from"];

            GetServerInfo();
            if (!IsPostBack)
            {
                rgSold.Visible = true;
                FillGrid();
            }
            else
                rgSold.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillGrid()
        {
            clsDBO clR = new clsDBO();
            string SQL = @"select contractno as ContractNo, fname + ' ' + lname as CustName, DealerNo, DealerName, pt.plantype, termmonth,
                saledate, cc.datepaid, cc.amt as Commission
                from contract c
                inner join contractcommissions cc on c.contractid = cc.contractid
                inner join dealer d on d.dealerid=c.dealerid
                inner join payee p on cc.payeeid = p.payeeid
                inner join plantype pt on pt.plantypeid = c.plantypeid
                where (cc.datepaid < '" + hfTo.Value + "' and cc.reportdate is null)" +
                " and p.payeeid = " + hfPayeeID.Value + " order by contractno";

            rgSold.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgSold.Rebind();
        }

        protected void rgSold_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                string from = Convert.ToDateTime(hfFrom.Value).ToString("d");
                string to = Convert.ToDateTime(hfTo.Value).ToString("d");
                rgSold.ExportSettings.ExportOnlyData = false;
                rgSold.ExportSettings.IgnorePaging = true;
                rgSold.ExportSettings.OpenInNewWindow = true;
                rgSold.ExportSettings.UseItemStyles = true;
                rgSold.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgSold.ExportSettings.FileName = "MonthlyCommissionPaid";
                rgSold.MasterTableView.ExportToExcel();
            }
        }
    }
}