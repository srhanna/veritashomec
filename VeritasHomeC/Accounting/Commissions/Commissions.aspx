﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Commissions.aspx.cs" Inherits="VeritasHomeC.Commissions" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Commissions</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="200">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                     <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPayee" OnClick="btnPayee_Click" runat="server" Text="Commissions Payee"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContractPayee" OnClick="btnContractPayee_Click" runat="server" Text="Contract Payee"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnPaylink" OnClick="btnPaylink_Click" runat="server" Text="Bulk Pay/Activate"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCancellation" OnClick="btnCancellation_Click" runat="server" Text="Bulk Cancellation"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCommissions" OnClick="btnCommissions_Click" runat="server" Text="Commissions" EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top" Width="1000">
                            <asp:Table runat="server">
                                            <asp:TableRow>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Payee Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                        <telerik:RadDropDownList AutoPostBack="true"
                                                            ID="PayeeListName" 
                                                            runat="server" 
                                                            DataValueField="PayeeID" 
                                                            OnSelectedIndexChanged="PayeeListName_SelectedIndexChanged"
                                                            DataTextField="CompanyName" 
                                                            DataSourceID="dsPayeeListName">
                                                        </telerik:RadDropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    To: 
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpTo" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Button ID="btnGenerate" OnClick="btnGenerate_Click" runat="server" Text="Generate Report" CssClass="button1" />
                                                </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow><asp:TableCell>&nbsp</asp:TableCell></asp:TableRow>
                                                <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblSubmitError" style="color:red" runat="server" Text="Please select a valid date range"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                                <asp:TableRow ID="tblReportDate">
                                                <asp:TableCell Font-Bold="true">
                                                    Set Report Date: 
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpReportDate" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                            <asp:TableCell>
                                                    <asp:Button ID="btnSetReportDate" runat="server" OnClick="btnSetReportDate_Click" Text="Set Report Date" CssClass="button2" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>&nbsp</asp:TableCell>
                                            </asp:TableRow>
                                            </asp:Table>
                                           <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                <telerik:RadTabStrip ID="tsCommissions" OnTabClick="tsCommissions_TabClick" runat="server">
                                                <Tabs>
                                                    <telerik:RadTab Text="Summary" Value="Summations"></telerik:RadTab>
                                                    <telerik:RadTab Text="Paid" Value="Sold"></telerik:RadTab>
                                                    <telerik:RadTab Text="Cancels" Value="Cancels"></telerik:RadTab>
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadMultiPage ID="rmMonthlyCommissions" runat="server" Width="1600" Height="650">
                                            <telerik:RadPageView ID="pvSummations" runat="server" />
                                            <telerik:RadPageView ID="pvSold" runat="server" />
                                             <telerik:RadPageView ID="pvCancels" runat="server" />
                                        </telerik:RadMultiPage>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:SqlDataSource ID="dsPayeeListName"
            ProviderName="System.Data.SqlClient" SelectCommand="select PayeeID, PayeeNo, CompanyName, Addr1 as Address, City, State, Zip, Phone from payee" runat="server">
        </asp:SqlDataSource>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfPayeeID" runat="server" />
        <asp:HiddenField ID="hfFrom" runat="server" />
        <asp:HiddenField ID="hfTo" runat="server" />
    </form>
</body>
</html>
