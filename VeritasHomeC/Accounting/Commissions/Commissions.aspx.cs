﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class Commissions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsPayeeListName.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            GetServerInfo();
            if (!IsPostBack)
            {
               
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                pvSummations.Selected = true;
                lblSubmitError.Visible = false;
                tsCommissions.Visible = false;

                btnSetReportDate.Visible = false;
                tblReportDate.Visible = false;
            }

            
            clsFunc.SetTestColor(pnlHeader, Image1);

            if (hfError.Value == "Visible")
                rwError.VisibleOnPageLoad = true;
            else
                rwError.VisibleOnPageLoad = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void tsCommissions_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsCommissions.SelectedTab.Value == "Summations")
                pvSummations.Selected = true;
            if (tsCommissions.SelectedTab.Value == "Sold")
                pvSold.Selected = true;
            if (tsCommissions.SelectedTab.Value == "Cancels")
                pvCancels.Selected = true;
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/agentssearch.aspx?=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contractsearch.aspx?=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void PayeeListName_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfPayeeID.Value = PayeeListName.SelectedValue;
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            int yearTo = Convert.ToDateTime(rdpTo.SelectedDate).Year;
            if (rdpTo.SelectedDate == null || yearTo > 2049)
                    
            {
                lblSubmitError.Visible = true;
                tsCommissions.Visible = false;
                rmMonthlyCommissions.Visible = false;
                return;
                
            }
            
            tsCommissions.Visible = true;
            tsCommissions.Tabs[0].Selected = true;
            lblSubmitError.Visible = false;
            pvSummations.ContentUrl = "~/accounting/commissions/CommissionSummary.aspx?sid=" + hfID.Value + " &PayeeID=" + PayeeListName.SelectedValue
                   + "&To=" + rdpTo.SelectedDate;
            pvSold.ContentUrl = "~/accounting/commissions/CommissionsPaid.aspx?sid=" + "&PayeeID=" + PayeeListName.SelectedValue
                    + "&To=" + rdpTo.SelectedDate;
            pvCancels.ContentUrl = "~/accounting/commissions/CommissionCancels.aspx?sid=" + hfID.Value + " &PayeeID=" + 
            PayeeListName.SelectedValue + "&To=" + rdpTo.SelectedDate;

            btnSetReportDate.Visible = true;
            tblReportDate.Visible = true;
            
        }

        protected void btnSetReportDate_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL = "update ContractCommissions set reportdate =  '" + Convert.ToDateTime(rdpReportDate.SelectedDate).ToString("d") +
                "' from contract c inner join contractcommissions cc on cc.contractid = c.contractid where" +
                " c.datepaid < '" + rdpTo.SelectedDate + "' and reportdate is null and not cc.datepaid is null " +
                " and payeeid = " + PayeeListName.SelectedValue;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);

            SQL = "update ContractCommissions set cancelreportdate = '" + Convert.ToDateTime(rdpReportDate.SelectedDate).ToString("d") +
                "' from contract c inner join contractcommissions cc on cc.contractid = c.contractid where" +
                " cc.canceldate < '" + rdpTo.SelectedDate + "' and cancelreportdate is null" +
                " and cc.payeeid = " + PayeeListName.SelectedValue + " and not cc.datepaid is null";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);

        }

        protected void btnPayee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/payee.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnCancellation_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/bulkcancellation.aspx?sid=" + hfID.Value);
        }

        protected void btnCommissions_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/commissions/commissions.aspx?sid=" + hfID.Value);
        }

        protected void btnContractPayee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/contractpayee.aspx?sid=" + hfID.Value);
        }
    }
}