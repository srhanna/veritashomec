﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Paylink.aspx.cs" Inherits="VeritasHomeC.Paylink" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Paylink/Mepco</title>
</head>
<body>
    <script type="text/javascript">
        window.onload = function () {
            document.getElementById("<%=txtContracts.ClientID %>").onpaste = function () {
                var txt = this;
                setTimeout(function () {
                    __doPostBack(txt.name, '');
                }, 100);
            }
        };
    </script>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>              
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                    <asp:Table runat="server" Width="250">
                                        <asp:TableRow>
                                            <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                                Menu
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <hr />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnPayee" OnClick="btnPayee_Click" runat="server" Text="Commissions Payee"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                         <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContractPayee" OnClick="btnContractPayee_Click" runat="server" Text="Contract Payee"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnPaylink" runat="server" Text="Bulk Pay/Activate"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnCancellation" onclick="btnCancellation_Click" runat="server" Text="Bulk Cancellation"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnCommissions" onclick="btnCommissions_Click" runat="server" Text="Commissions"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Accounting.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <hr />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                    <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                                </telerik:RadButton>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                              <asp:Panel runat="server" ID="pnlMassUpdate">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true"> Paste the Contract Number and the Dealer Cost</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>&nbsp</asp:TableCell>
                                    </asp:TableRow>  
                                       <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                 <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true"> 
                                                        Pay Date:
                                                    </asp:TableCell> 
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="dtPayDate" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Payee: 
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="cboPayee" runat="server" DataTextField="PayeeName" DataValueField="ContractPayeeID" OnSelectedIndexChanged="cboPayee_SelectedIndexChanged"></asp:DropDownList>
                                                    </asp:TableCell> 
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgContractsUpdate" runat="server" AutoGenerateColumns="false" AllowSorting="true" Height="600" Width="600" style="margin-top:15px;">                  
                                                    <ClientSettings>
                                                                <Scrolling AllowScroll="True" UseStaticHeaders="True" ScrollHeight="10px"  />
                                                    </ClientSettings> 
                                                        <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true">
                                                            <Columns>
                                                            <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="DealerCost" UniqueName="DealerCost" HeaderText="Dealer Cost"></telerik:GridBoundColumn>
                                                         </Columns>
                                                     </MasterTableView>
                                                 </telerik:RadGrid>
                                            <asp:TextBox ID="txtContracts" TextMode="MultiLine" Height="400" Width="325" runat="server" OnTextChanged="txtContracts_TextChanged"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnReset" OnClick="btnReset_Click" CssClass="button1" runat="server" Text="Reset" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnRun" OnClick="btnRun_Click" CssClass="button2" runat="server" Text="Process Contracts" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                  </asp:Panel>
                                <asp:Panel ID="pnlResult" runat="server">
                    <telerik:RadTabStrip runat="server" ID="tsResult" OnTabClick="tsResult_TabClick">
                        <Tabs>
                            <telerik:RadTab Text="Processed" Value="NoErrors"></telerik:RadTab>
                            <telerik:RadTab Text="Errors" Value="Errors"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>                               
                <asp:Panel runat="server" ID="pnlNoErrors">
                     <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgNoError" style="margin-bottom:10px;">
                    <telerik:RadGrid ID="rgNoError" runat="server" AutoGenerateColumns="false" AllowSorting="true" Height="600" Width="600" style="margin-top:15px;" OnItemCommand="rgNoError_ItemCommand">                  
                        <ClientSettings>
                              <Scrolling AllowScroll="True" UseStaticHeaders="True" ScrollHeight="10px"  />
                        </ClientSettings> 
                            <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MoxyDealerCost" UniqueName="MoxyDealerCost" HeaderText="Dealer Balance" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DealerCost" UniqueName="DealerCost" HeaderText="Pasted Dealer Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                         </telerik:RadAjaxPanel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlErrors">
                     <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="rgError" style="margin-bottom:10px;">
                    <telerik:RadGrid ID="rgError" runat="server" AutoGenerateColumns="false" AllowSorting="true" Height="600" Width="600" style="margin-top:15px;" OnItemCommand="rgError_ItemCommand">                  
                        <ClientSettings>
                              <Scrolling AllowScroll="True" UseStaticHeaders="True" ScrollHeight="10px"  />
                        </ClientSettings> 
                            <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false" />
                                <Columns>
                                <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MoxyDealerCost" UniqueName="MoxyDealerCost" HeaderText="Dealer Balance" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DealerCost" UniqueName="DealerCost" HeaderText="Pasted Dealer Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                         </telerik:RadAjaxPanel>
                    </asp:Panel>
                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button1" OnClick="btnClose_Click" />
                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                 <asp:HiddenField ID="hfID" runat="server" />
                 <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfPayeeID" runat="server" />
                
                <telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="rwSuccess"></telerik:RadWindowManager>
                <telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="rwError"></telerik:RadWindowManager>
                <telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="rwFormatError"></telerik:RadWindowManager>
            </ContentTemplate>
        </asp:UpdatePanel>


       
    </form>
</body>
</html>
