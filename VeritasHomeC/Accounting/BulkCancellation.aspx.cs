﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using System.Data;
using Microsoft.VisualBasic;

namespace VeritasHomeC
{
    public partial class Cancellation1 : System.Web.UI.Page
    {
        private long lUserID;

        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                if (lUserID == 0)
                {

                    LockButtons();
                }

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                tblcButtons.Visible = false;
                testPaste.Visible = false;
                lblSuccess.Visible = false;
                lblError.Visible = false;
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + lUserID + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }


        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            lUserID = 0;
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                lUserID = Convert.ToInt64(clSI.GetFields("userid"));

            }

            UnlockButtons();
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + lUserID;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;

            }
        }


        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnPaylink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/paylink.aspx?sid=" + hfID.Value);
        }

        protected void btnCancellation_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/bulkcancellation.aspx?sid=" + hfID.Value);
        }

        protected void btnPayee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/payee.aspx?sid=" + hfID.Value);
        }


        protected void PasteToGridView(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2]
                {
                new DataColumn("ContractNo", typeof(string)),
                new DataColumn("CancelDate", typeof(string))
                });
                int rowCount = 0;

                string copiedContent = Request.Form[txtCopied.UniqueID];
                foreach (string row in copiedContent.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (string cell in row.Split('\t'))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;

                        }
                    }

                    rowCount++;
                }
                testPaste.DataSource = dt;
                testPaste.DataBind();
                txtCopied.Text = "";
                lblCount.Text = testPaste.Items.Count.ToString();


                testPaste.Visible = true;
                txtCopied.Visible = false;
                tblcButtons.Visible = true;
                lblError.Visible = false;
            }
            catch
            {
                lblError.Visible = true;
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            ShowOpen();
        }

        private void ShowOpen()
        {
            btnUpdate.Enabled = false;
            btnClear.Enabled = false;
            hfOpen.Value = "Visible";
            string script = "function f(){$find(\"" + rwOpen.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwOpen", script, true);
            rwOpen.Visible = true;
        }

        private void HideOpen()
        {
            btnUpdate.Enabled = true;
            btnClear.Enabled = true;
            hfOpen.Value = "";
            string script = "function f(){$find(\"" + rwOpen.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            rwOpen.Visible = false;
        }

        private void ProcessContractAmt(string sContractID, double dCancelPer)
        {
            string SQL = "select * from contractamt where contractid = " + sContractID;
            clsDBO clCA = new clsDBO();
            clsDBO clCA2 = new clsDBO();
            clCA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCA.RowCount() > 0)
            {
                for (int i = 0; i <= clCA.RowCount() - 1; i++)
                {
                    clCA.GetRowNo(i);
                    SQL = "select * from contractamt where contractamtid = " + clCA.GetFields("contractamtid");
                    clCA2.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCA2.RowCount() > 0)
                    {
                        clCA2.GetRow();
                        clCA2.SetFields("cancelamt", (Convert.ToDouble(clCA2.GetFields("amt")) * dCancelPer).ToString());
                        clCA2.SaveDB();
                    }
                }
            }
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (testPaste.Visible == true)
            {
                lblSuccess.Visible = false;
                lblError.Visible = false;
                tblcButtons.Visible = false;
                testPaste.Visible = false;
                txtCopied.Text = "";
                txtCopied.Visible = true;
            }
        }

        protected void btnOpenClose_Click(object sender, EventArgs e)
        {
            HideOpen();
            Response.Redirect("~/accounting/bulkcancellation.aspx?sid=" + hfID.Value);
        }

        protected void btnOpenSave_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;


            foreach (var row in testPaste.Items)
            {
                GridDataItem dataRow = row as GridDataItem;
                TableCell ContractNo = dataRow["ContractNo"];
                string sContractNo = ContractNo.Text;
                TableCell CancelDate = dataRow["CancelDate"];
                string sCancelDate = CancelDate.Text;
                if (!Information.IsDate(sCancelDate))
                {
                    lblError.Text = "Error: Invalid Record";
                    lblError.Visible = true;
                    goto MoveHere;
                }

                var clC2 = new VeritasGlobalToolsV2.clsCancellation();
                clC2.CancelDate = sCancelDate;
                clC2.ContractID = Convert.ToInt64(clsFunc.GetContractID(sContractNo));
                clC2.Quote = false;
                clC2.CalculateCancellation();
                clsDBO clCC = new clsDBO();
                SQL = "select * from contractcancel where contractid = " + clC2.ContractID;
                clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clCC.RowCount() == 0)
                    clCC.NewRow();
                else
                    clCC.GetRow();


                clCC.SetFields("contractid", clC2.ContractID.ToString());
                clCC.SetFields("cancelstatus", "Cancelled");
                clCC.SetFields("termfactor", (Convert.ToDouble(clC2.CancelPer).ToString()));
                clCC.SetFields("claimamt", clC2.Claims.ToString());
                clCC.SetFields("cancelfactor", (Convert.ToDouble(clC2.CancelPer).ToString()));
                clCC.SetFields("canceleffdate", clC2.CancelDate.ToString());
                clCC.SetFields("canceldate", DateTime.Today.ToString());
                clCC.SetFields("adminamt", clC2.AdminAmt.ToString());
                clCC.SetFields("dealeramt", clC2.DealerAmt.ToString());
                clCC.SetFields("dealer", false.ToString());
                clCC.SetFields("customer", false.ToString());
                clCC.SetFields("Other", true.ToString());
                clCC.SetFields("GrossCustomer", clC2.GrossCustomer.ToString());
                clCC.SetFields("grossadmin", clC2.GrossAdmin.ToString());
                clCC.SetFields("grossdealer", clC2.GrossDealer.ToString());
                clCC.SetFields("DealerCancel", false.ToString());

                VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();

                clP.CompanyName = clsFunc.GetLienholder(sContractNo);
                

                clP.AddOtherToPayee(clC2.ContractID.ToString());
                clCC.SetFields("payeeid", clP.PayeeID.ToString());


                clCC.SetFields("cancelfeeamt", clC2.CancelFee.ToString());

                if (clCC.RowCount() == 0)
                {
                    clCC.SetFields("credate", DateTime.Today.ToString());
                    clCC.SetFields("creby", "1");
                    clCC.AddRow();
                }
                else
                {
                    clCC.SetFields("moddate", DateTime.Today.ToString());
                    clCC.SetFields("modby", "1");
                }

                clCC.SaveDB();
                clsDBO clC = new clsDBO();
                SQL = "select * from contract where contractid = " + clC2.ContractID;
                clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clC.RowCount() > 0)
                {
                    clC.GetRow();
                    if (clC.GetFields("datepaid").Length > 0)
                        clC.SetFields("status", "Cancelled");
                    else
                        clC.SetFields("status", "Cancelled Before Paid");

                    clC.SaveDB();
                }

                ProcessContractAmt(clC2.ContractID.ToString(), clC2.CancelPer);
                lblSuccess.Visible = true;

            MoveHere:;
            }

            btnClear.Enabled = true;
            btnUpdate.Enabled = true;
        }

        protected void btnCommissions_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/commissions/commissions.aspx?sid=" + hfID.Value);
        }

        protected void btnContractPayee_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/contractpayee.aspx?sid=" + hfID.Value);
        }
    }
}