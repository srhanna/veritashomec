﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace VeritasHomeC
{
    public partial class Default : System.Web.UI.Page
    {

        private long lUserID;

        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                if (lUserID == 0)
                {
                    hfSigninPopup.Value = "Visible";
                    string script = @"function f(){$find(""" + rwSignIn.ClientID + @""").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", script, true);
                    rwSignIn.VisibleOnPageLoad = true;
                    LockButtons();
                }

                if (ConfigurationManager.AppSettings["connstring"].Contains("test")) {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                } else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + lUserID + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }


        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            lUserID = 0;
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                lUserID = Convert.ToInt64(clSI.GetFields("userid"));
            }
        }

        private void LockButtons() 
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + lUserID;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            string SQL;
            lblError.Text = "";
            if (txtEMail.Text == "") 
            {
                lblError.Text = "No E-Mail was entered.";
                ShowError();
                return;
            }

            if (txtPassword.Text == "") 
            {
                lblError.Text = "No Password was entered.";
                ShowError();
                return;
            }

            SQL = "select * from userinfo ";
            SQL = SQL + "where email = '" + txtEMail.Text + "' ";
            SQL = SQL + "and password = '" + txtPassword.Text + "' ";
            SQL = SQL + "and active <> 0";
            clsDBO clU = new clsDBO();
            clU.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clU.RowCount() == 0) {
                lblError.Text = "Cannot locate user name/password.";
                ShowError();
                return;
            }

            clU.GetRow();
            lUserID = Convert.ToInt64(clU.GetFields("userid"));
            hfID.Value = System.Guid.NewGuid().ToString();
            clsDBO clSI = new clsDBO();
            //        SQL = "delete serverinfo where userid = " + lUserID;
            //clSI.RunSQL(SQL, sCON);
            SQL = "select * from serverinfo ";
            SQL = SQL + "where userid = " + lUserID;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clSI.NewRow();
            clSI.SetFields("userid", lUserID.ToString());
            clSI.SetFields("systemid", hfID.Value);
            clSI.SetFields("signindate", DateTime.Now.ToString());
            clSI.AddRow();
            clSI.SaveDB();
            hfSigninPopup.Value = "";
            string script = @"function f(){$find(""" + rwSignIn.ClientID + @""").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            UnlockButtons();
            CheckToDo();
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = @"function f(){$find(""" + rwError.ClientID + @""").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            script = @"function f(){$find(""" + rwSignIn.ClientID + @""").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwSignIn", script, true);
            rwSignIn.VisibleOnPageLoad = true;
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

    }
}