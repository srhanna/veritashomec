﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class AgentsNote : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsAgentNote.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();

            if (!IsPostBack)
            {
                hfToday.Value = DateTime.Today.ToString("d");
                hfAgentID.Value = Request.QueryString["AgentID"];
                pnlControl.Visible = true;
                pnlDetail.Visible = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgAgentNote_SelectedCellChanged(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfAgentNoteID.Value = rgAgentNote.SelectedValue.ToString();
            FillNote();
        }

        private void FillNote()
        {
            clsDBO clRN = new clsDBO();
            string SQL = "select note, createdate, moddate, cre.email as CEmail, mod.email as MEMail from agentsnote rn ";
            SQL = SQL + "left join userinfo cre on rn.createby = cre.userid ";
            SQL = SQL + "left join userinfo mod on mod.userid = rn.modby ";
            SQL = SQL + "where agentnoteid = " + hfAgentNoteID.Value;

            clRN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRN.RowCount() > 0)
            {
                clRN.GetRow();
                txtNote.Text = clRN.GetFields("Note");
                txtCreBy.Text = clRN.GetFields("CEMail");
                txtCreDate.Text = clRN.GetFields("createdate");
                txtModBy.Text = clRN.GetFields("MEMail");
                txtModDate.Text = clRN.GetFields("moddate");
                txtNote.Text = clRN.GetFields("note");
                chkClaimAlways.Checked = Convert.ToBoolean(clRN.GetFields("claimalways"));
                chkClaimEntry.Checked = Convert.ToBoolean(clRN.GetFields("claimnewentry"));
                chkContractAlways.Checked = Convert.ToBoolean(clRN.GetFields("contractalways"));
                chkContractCancel.Checked = Convert.ToBoolean(clRN.GetFields("contractcancel"));
                chkContractNewEntry.Checked = Convert.ToBoolean(clRN.GetFields("ContractNewEntry"));
            }
            else
            {
                txtNote.Text = "";
                txtCreBy.Text = "";
                txtCreDate.Text = "";
                txtModBy.Text = "";
                txtModDate.Text = "";
                txtNote.Text = "";
                chkContractNewEntry.Checked = false;
                chkContractCancel.Checked = false;
                chkContractAlways.Checked = false;
                chkClaimEntry.Checked = false;
                chkClaimAlways.Checked = false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgAgentNote.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            clsDBO clRN = new clsDBO();
            string SQL = "select * from Agentsnote where agentnoteid = " + hfAgentNoteID.Value;
            clRN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRN.RowCount() == 0)
            {
                clRN.NewRow();
                clRN.SetFields("createby", hfUserID.Value);
                clRN.SetFields("createdate", DateTime.Today.ToString());
            }
            else
            {
                clRN.GetRow();
                clRN.SetFields("modby", hfUserID.Value);
                clRN.SetFields("moddate", DateTime.Today.ToString());
            }

            clRN.SetFields("note", txtNote.Text);
            clRN.SetFields("agentid", hfAgentID.Value);
            clRN.SetFields("claimalways", chkClaimAlways.Checked.ToString());
            clRN.SetFields("claimnewentry", chkClaimEntry.Checked.ToString());
            clRN.SetFields("contractalways", chkContractAlways.Checked.ToString());
            clRN.SetFields("contractnewentry", chkContractNewEntry.Checked.ToString());
            clRN.SetFields("contractcancel", chkContractCancel.Checked.ToString());

            if (clRN.RowCount() == 0)
                clRN.AddRow();

            clRN.SaveDB();
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgAgentNote.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hfAgentNoteID.Value = "0";
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            FillNote();
        }
    }
}