﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgentsModify.ascx.cs" Inherits="VeritasHomeC.AgentsModify" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel ID="pnlModify" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Agent No:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAgentNo" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Agent Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAgentName" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 1:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAddr1" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 2:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtAddr2" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            City:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtCity" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            State:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsStates"
                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Zip:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtZip" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Phone:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            E-Mail:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtEMail" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Status:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboAgentStatus" DataSourceID="dsAgentStatus" DataTextField="Status" DataValueField="StatusID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsAgentStatus"
                            ProviderName="System.Data.SqlClient" SelectCommand="select StatusID, Status from Agentstatus" runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Alias:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtContact" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            EIN:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtEIN" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Parent Agent:
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadTextBox ID="txtParentAgent" runat="server"></telerik:RadTextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Agent Search" CssClass="button2" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Modify By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModBy" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Modify Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="lblModifyDate" runat="server" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" CssClass="button2" Text="Save" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" CssClass="button1" Text="Cancel" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlSearch" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgAgents" OnSelectedIndexChanged="rgAgents_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="AgentID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="AgentID" ReadOnly="true" Visible="false" UniqueName="AgentsID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AgentNo" UniqueName="AgentsNo" HeaderText="Agents No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentsName" HeaderText="Agents Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="City" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="State" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1"
                ProviderName="System.Data.SqlClient" 
                SelectCommand="select Agentid, Agentno, Agentname, city, state from Agents "
                runat="server"></asp:SqlDataSource>
            </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Button ID="btnCancelSearch" runat="server" CssClass="button1" Text="Cancel Search" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfAgentID" runat="server" />
<asp:HiddenField ID="hfParentAgentID" runat="server" />