﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Agents.aspx.cs" Inherits="VeritasHomeC.Agents" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="AgentsContact.ascx" TagName="AgentContact" TagPrefix="uc" %>
<%@ Register Src="AgentsNote.ascx" TagName="AgentNote" TagPrefix="uc" %>
<%@ Register Src="AgentsDealer.ascx" TagName="AgentsDealer" TagPrefix="uc" %>
<%@ Register Src="AgentsContract.ascx" TagName="AgentsContract" TagPrefix="uc" %>
<%@ Register Src="AgentsModify.ascx" TagName="AgentsModify" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Veritas.css" type="text/css" />
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Agent</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <%--<asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSubAgent" runat="server" Text="Sub Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnFulFillment" runat="server" Text="Fulfillment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agents.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnFulfillmentStock" runat="server" Text="Fulfillment Stock"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agents.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>--%>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true" Font-Size="Large">
                                                    Agent No:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblAgentNo" runat="server" Font-Size="Large" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true" Font-Size="Large">
                                                    Agent Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblAgentName" runat="server" Font-Size="Large" Text=""></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <asp:Table runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Address 1:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblAddr1" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Address 2:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblAddr2" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                City, State, Zip:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblAddr3" runat="server"  Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                E-Mail:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblEMail" runat="server"  Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>

                                                    </asp:Table>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <asp:Table runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                DBA:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblDBA" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                EIN:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblEIN" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Parent Agent:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblParentAgent" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Status:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <asp:Table runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Create By:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblCreBy" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Create Date:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblCreDate" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Modified By:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblModifyBy" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Font-Bold="true">
                                                                Modified Date:
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblModifyDate" runat="server" Text=""></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadTabStrip ID="tsAgent" OnTabClick="tsAgent_TabClick" runat="server">
                                            <Tabs>
                                                <telerik:RadTab Text="Contacts" Value="Contact"></telerik:RadTab> 
                                                <telerik:RadTab Text="Notes" Value="Note"></telerik:RadTab>
                                                <telerik:RadTab Text="Dealers" Value="Dealers"></telerik:RadTab>
                                                <telerik:RadTab Text="Contracts" Value="Contracts"></telerik:RadTab>
                                                <telerik:RadTab Text="Modify" Value="Modify"></telerik:RadTab>
                                            </Tabs>
                                        </telerik:RadTabStrip>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadMultiPage ID="rmReferral" runat="server">
                                            <telerik:RadPageView ID="pvContact" runat="server">
                                                <uc:AgentContact runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvNote" runat="server">
                                                <uc:AgentNote runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvDealers" runat="server">
                                                <uc:AgentsDealer runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvContracts" runat="server">
                                                <uc:AgentsContract runat="server" />
                                            </telerik:RadPageView>
                                            <telerik:RadPageView ID="pvModify" runat="server">
                                                <uc:AgentsModify runat="server" />
                                            </telerik:RadPageView>
                                        </telerik:RadMultiPage>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" CssClass="button2" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfAgentID" runat="server" />
    </form>
</body>
</html>
