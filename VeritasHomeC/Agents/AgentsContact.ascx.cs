﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class AgentsContact : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            if (!IsPostBack)
            {
                pnlControl.Visible = true;
                pnlDetail.Visible = false;
                hfAgentID.Value = Request.QueryString["AgentID"];
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgAgentContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfAgentContactID.Value = rgAgentContact.SelectedValue.ToString();
            FillAgentContact();
        }

        private void FillAgentContact()
        {
            clsDBO clAC = new clsDBO();
            string SQL = "select * from agentscontact where agentcontactid = " + hfAgentContactID.Value;
            clAC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clAC.RowCount() > 0)
            {
                clAC.GetRow();
                txtEMail.Text = clAC.GetFields("email");
                txtFName.Text = clAC.GetFields("fname");
                txtLName.Text = clAC.GetFields("lname");
                txtPhone.Text = clAC.GetFields("phone");
                txtPosition.Text = clAC.GetFields("position");
            }
            else
            {
                txtEMail.Text = "";
                txtFName.Text = "";
                txtLName.Text = "";
                txtPhone.Text = "";
                txtPosition.Text = "";
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = false;
            pnlDetail.Visible = true;
            hfAgentContactID.Value = "0";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgAgentContact.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            clsDBO clAC = new clsDBO();
            string SQL = "select * from agentscontact where agentcontactid = " + hfAgentContactID.Value;
            clAC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clAC.RowCount() == 0)
                clAC.NewRow();
            else
                clAC.GetRow();

            clAC.SetFields("agentid", hfAgentID.Value);
            clAC.SetFields("fname", txtFName.Text);
            clAC.SetFields("lname", txtLName.Text);
            clAC.SetFields("phone", txtEMail.Text);
            clAC.SetFields("email", txtEMail.Text);
            clAC.SetFields("position", txtPosition.Text);
            if (clAC.RowCount() == 0)
                clAC.AddRow();

            clAC.SaveDB();
            pnlControl.Visible = true;
            pnlDetail.Visible = false;
            rgAgentContact.Rebind();
        }
    }
}