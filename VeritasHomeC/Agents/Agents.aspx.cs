﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class Agents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            hfAgentID.Value = Request.QueryString["AgentID"];
            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");
                else
                {
                    pvContact.Selected = true;
                    FillAgent();
                }
            }

            clsFunc.SetTestColor(pnlHeader, Image1);

            if (hfError.Value == "visible")
                rwError.VisibleOnPageLoad = true;
            else 
                rwError.VisibleOnPageLoad = false;
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            clsDBO clTD = new clsDBO();
            string SQL = "select * from usermessage where toid = " + hfUserID.Value + " " +
                "and completedmessage = 0 and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void FillAgent()
        {
            clsDBO clA = new clsDBO();
            string SQL = "select  a.*, ags.status from agents a left join agentstatus ags on ags.statusid = a.statusid "
                + "where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                lblAddr1.Text = clA.GetFields("addr1");
                lblAddr2.Text = clA.GetFields("addr2");
                lblAddr3.Text = clA.GetFields("city") + " " + clA.GetFields("state") + " " + clA.GetFields("zip");
                lblCreBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clA.GetFields("creby")));
                lblCreDate.Text = clA.GetFields("credate");
                lblDBA.Text = clA.GetFields("dba");
                lblEIN.Text = clA.GetFields("ein");
                lblAgentName.Text = clA.GetFields("agentname");
                lblAgentNo.Text = clA.GetFields("agentno");
                lblModifyBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clA.GetFields("modby")));
                lblEMail.Text = clA.GetFields("email");
                lblModifyDate.Text = clA.GetFields("moddate");
                lblParentAgent.Text = clsFunc.GetParentAgentInfo(Convert.ToInt64(clA.GetFields("parentagentid")));
                lblStatus.Text = clA.GetFields("status");
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = @"function f(){$find(""" + rwError.ClientID + @""").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = @"function f(){$find(""" + rwError.ClientID + @""").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            }

        protected void tsAgent_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsAgent.SelectedTab.Value == "Contact")
                pvContact.Selected = true;
            if (tsAgent.SelectedTab.Value == "Note")
                pvNote.Selected = true;
            if (tsAgent.SelectedTab.Value == "Agents") { }
            //pvSubAgents.Selected = true
            if (tsAgent.SelectedTab.Value == "Dealers")
                pvDealers.Selected = true;
            if (tsAgent.SelectedTab.Value == "Contracts")
                pvContracts.Selected = true;
            if (tsAgent.SelectedTab.Value == "Modify")
                pvModify.Selected = true;
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }
    }
}