﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSettings.aspx.cs" Inherits="VeritasHomeC.UserSettings" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/styles.css" type="text/css" rel="stylesheet" />
   <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>User Settings</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnToDoReader" OnClick="btnToDoReader_Click" runat="server" Text="To Do Reader"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnToDoCreate" OnClick="btnToDoCreate_Click" runat="server" Text="To Do Creation"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUserSettings" OnClick="btnUserSettings_Click" runat="server" Text="User Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnChangePassword" OnClick="btnChangePassword_Click" runat="server" Text="Change Password"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlSearch" runat="server">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="button1" Text="Add User" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgUser" OnSelectedIndexChanged="rgUser_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true"
                                                            allowsorting="true" Width="1000" ShowFooter="true" DataSourceID="dsUser">
                                                            <GroupingSettings CaseSensitive="false" />
                                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="UserID" PageSize="10" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="UserID" ReadOnly="true" Visible="false" UniqueName="UserID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="EMail" UniqueName="EMail" HeaderText="E-Mail" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsUser"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select userid, fname, lname, email from UserInfo"
                                                        runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:Tablerow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlDetail" runat="server">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    First Name:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadTextBox ID="txtFName" Width="150" runat="server"></telerik:RadTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Last Name:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadTextBox ID="txtLName" Width="150" runat="server"></telerik:RadTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    E-Mail:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadTextBox ID="txtEMail" Width="150" runat="server"></telerik:RadTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Password:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadTextBox ID="txtPassword" Width="150" runat="server"></telerik:RadTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Active:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkActive" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Read Only:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkReadOnly" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Insurance Carrier:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:DropDownList ID="cboInsCarrier" runat="server"></asp:DropDownList>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    AutoNation Only:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkANOnly" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Vero Only:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkVeroOnly" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Team Lead:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkTeamLead" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Team:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:DropDownList ID="cboTeams" runat="server"></asp:DropDownList>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Agent:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:DropDownList ID="cboAgent" OnSelectedIndexChanged="cboAgent_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                                </asp:TableCell>
                                                                <%--<asp:TableCell Font-Bold="true">
                                                                    Sub Agent:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:DropDownList ID="cboSubAgent" runat="server"></asp:DropDownList>
                                                                </asp:TableCell>--%>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Table runat="server">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Font-Bold="true">
                                                                                Security Info
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                        User Settings:
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell VerticalAlign="Top">
                                                        <asp:Table runat="server" BorderStyle="Solid" BorderWidth="1">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Contract Information
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkContract" Text="Contract" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkContractMod" Text="Modification" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkCancel" Text="Cancellation" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkCancelGap" Text="Cancel Gap" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkActivateContract" Text="Activate Contract" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkCancelMod" Text="Cancel Modification" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkCancelPayment" Text="Cancel Payment" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkCancel100" Text="Cancel 100%" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                    <asp:TableCell VerticalAlign="Top">
                                                        <asp:Table runat="server" BorderStyle="Solid" BorderWidth="1"> 
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Dealer Information
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkDealer" Text="Dealers" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAgents" Text="Agents" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                    <asp:TableCell VerticalAlign="Top">
                                                        <asp:Table runat="server" BorderStyle="Solid" BorderWidth="1">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Misc Information
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAccounting" Text="Accounting" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkUserSettings" Text="User Settings" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkSettings" Text="Settings" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                    <asp:TableCell VerticalAlign="Top">
                                                        <asp:Table runat="server" BorderStyle="Solid" BorderWidth="1">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Claims Information:
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkClaim" Text="Claims" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkUnlockClaim" Text="Unlock Claims" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkClaimPayment" Text="Claim Payment" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkHCCApprovalManager" Text="HCC Approval Manager" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkHCCApprovalCEO" Text="HCC Approval CEO" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAllowCancelExpire" Text="Allow Cancel Expire" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkWithIn3Days" Text="Within 3 Days" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAllCoverage" Text="All Coverage" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAllowSlush" Text="Allow Slush" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAllowUndeny" Text="Allow Undeny Claim" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkBypassMissingInfo" Text="Bypass Missing Info" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkClaimAudit" Text="Allow Claim Audit" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Max Authorize:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtMaxAuth" TextMode="Number" runat="server"></asp:TextBox>                                                                    
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Max Approved:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtClaimMax" TextMode="Number" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                    <asp:TableCell VerticalAlign="Top">
                                                        <asp:Table runat="server" BorderStyle="Solid" BorderWidth="1">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Reports:
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkSaleReport" Text="Sales" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkClaimsReports" Text="Claim" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkAccountReports" Text="Accounting" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <telerik:RadCheckBox ID="chkCustomReports" Text="Custom" Font-Bold="true" runat="server"></telerik:RadCheckBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="button2" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CssClass="button1" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:Tablerow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfUserDetailID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1eabe2" BorderColor="#1eabe2" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
