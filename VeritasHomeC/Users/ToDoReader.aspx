﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToDoReader.aspx.cs" Inherits="VeritasHomeC.ToDoReader" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <title>To Do Reader</title>
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlList">
                    <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid runat="server" ID="rgToDoMessengerList" AutoGenerateColumns="false" DataSourceID="dsMessage" Width="1000" Height="500" OnSelectedIndexChanged="rgToDoMessengerList_SelectedIndexChanged">
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="idx" PageSize="10" ShowFooter="true" >
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="idx" ReadOnly="true" Visible="false" UniqueName="idx"></telerik:GridBoundColumn>                                        
                                        <telerik:GridBoundColumn DataField="FName" FilterCheckListWebServiceMethod="LoadFName" UniqueName="FromFName" HeaderText="From First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LName" FilterCheckListWebServiceMethod="LoadLName" UniqueName="FromLName" HeaderText="From Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Header" FilterCheckListWebServiceMethod="LoadHeader" UniqueName="Header" HeaderText="Header" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="MessageDate" FilterCheckListWebServiceMethod="LoadMessageDate" UniqueName="Date" HeaderText="DateReceived" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CompletedMessage" AllowFiltering="false" UniqueName="CommpletedMessage" HeaderText="Completed"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsMessage"
                            ProviderName="System.Data.SqlClient" 
                            SelectCommand="select um.idx, ui.FName, ui.LName, um.Header, um.MessageDate, completedmessage  from userinfo ui 
                            inner join usermessage um on ui.UserID = um.fromID where um.toid = @UserID  and deletemessage = 0 order by um.messagedate desc" runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfUserID" Name="UserID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table ID="Table1" runat="server" HorizontalAlign="Left">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            To:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                             <asp:Label runat="server" ID="lblToDisplay" Text=" "></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            From:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label runat="server" ID="lblFromDisplay" Text=" "></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Sent Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Label runat="server" ID="lblDate" Text=" "></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>

                                <asp:TextBox ID="txtHeader" Width="1000" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Literal ID="txtMessageDisplay" runat="server"></asp:Literal>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnClosed" runat="server" CssClass="button2" Width="75" Text="Close" OnClick="btnClosed_Click" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCompleted" runat="server" CssClass="button2" Width="75" Text="Completed" OnClick="btnCompleted_Click"/>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnRemoveMessage" runat="server" CssClass="button2" Width="75" Text="Delete" OnClick="btnRemoveMessage_Click"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>                
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfIDX" runat="server" />
                <asp:HiddenField ID="hfFromID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
