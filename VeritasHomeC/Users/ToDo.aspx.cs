﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ToDo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsMessage.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsUserInfo.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                pnlDetail.Visible = false;
                pnlList.Visible = true;
                pnlSeekUser.Visible = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("usersettings")))
                    btnUserSettings.Enabled = true;
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUserSettings.Enabled = false;
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/ChangePassword.aspx?sid=" + hfID.Value);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnUserSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/usersettings.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contractsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnToDoCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todoreader.aspx?sid=" + hfID.Value);
        }

        protected void btnAddMessage_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            ClearDetail();
        }

        private void ClearDetail()
        {
            hfIDX.Value = "0";
            hfToID.Value = "0";
            txtFrom.Text = GetUserName();
            txtTo.Text = "";
            txtHeader.Text = "";
            txtMessage.Text = "";
            txtMessageDate.Text = "";
            btnSendMessage.Visible = true;
        }

        protected void rgMessage_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            txtFrom.Text = GetUserName();
            hfIDX.Value = rgMessage.SelectedValue.ToString();
            btnSendMessage.Visible = false;
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where idx = " + hfIDX.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                hfToID.Value = clR.GetFields("toid");
                txtTo.Text = GetToName();
                txtHeader.Text = clR.GetFields("header");
                txtMessage.Text = clR.GetFields("message");
                txtMessageDate.Text = clR.GetFields("messagedate");
            }
        }


        protected void btnSeekUser_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekUser.Visible = true;
        }

        protected void rgUserInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfToID.Value = rgUserInfo.SelectedValue.ToString();
            txtTo.Text = GetToName();
            pnlDetail.Visible = true;
            pnlSeekUser.Visible = false;
        }

        protected void btnCancelMessage_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSendMessage_Click(object sender, EventArgs e)
        {
            if (hfIDX.Value != "0" || hfToID.Value == "0")
                return;

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where idx = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("toid", hfToID.Value);
                clR.SetFields("fromid", hfUserID.Value);
                clR.SetFields("header", txtHeader.Text);
                clR.SetFields("message", txtMessage.Text);
                clR.SetFields("messagedate", DateTime.Now.ToString());
                clR.AddRow();
                clR.SaveDB();
            }
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgMessage.Rebind();
        }

        private string GetToName()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sGetToName = "";
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfToID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sGetToName = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return sGetToName;
        }


        private string GetUserName()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sGetUserName = "";
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if(clR.RowCount() > 0)
            {
                clR.GetRow();
                sGetUserName = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            return sGetUserName;
        }

        protected void btnToDoReader_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todoreader1.aspx?sid=" + hfID.Value);
        }
    }
}