﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class UserSettings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsUser.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();

            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                clsFunc.SetTestColor(pnlHeader, Image1);

                pnlSearch.Visible = true;
                pnlDetail.Visible = false;
                FillInsCarrier();
                FillTeams();
                FillAgents();
            }
        }

        private void FillAgents()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboAgent.Items.Add(li);
            SQL = "select * from agents ";
            SQL = SQL + "where not agentno is null ";
            SQL = SQL + "and statusid = 2 ";
            SQL = SQL + "order by agentname ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount() -1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("agentid");
                    li.Text = clR.GetFields("agentname");
                    cboAgent.Items.Add(li);
                }
            }
        }

        private void FillInsCarrier()
        {
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboInsCarrier.Items.Add(li);
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from inscarrier ";
            SQL = SQL + "order by inscarrierid ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("inscarrierid");
                    li.Text = clR.GetFields("inscarriername");
                    cboInsCarrier.Items.Add(li);
                }
            }
        }

        private void FillTeams()
        {
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboTeams.Items.Add(li);
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from userteam ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("teamid");
                    li.Text = clR.GetFields("teamname");
                    cboTeams.Items.Add(li);
                }
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("usersettings")))
                    btnUserSettings.Enabled = true;
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUserSettings.Enabled = false;
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnUserSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/usersettings.aspx?sid=" + hfID.Value);
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/changepassword.aspx?sid=" + hfID.Value);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;
            pnlDetail.Visible = true;
            hfUserDetailID.Value = "0";
            ClearFields();
        }

        private void ClearFields()
        {
            txtFName.Text = "";
            txtLName.Text = "";
            txtEMail.Text = "";
            txtPassword.Text = "";
            txtClaimMax.Text = "0";
            txtMaxAuth.Text = "0";
            chkActive.Checked = false;
            chkUserSettings.Checked = false;
            chkAccounting.Checked = false;
            chkSettings.Checked = false;
            chkAgents.Checked = false;
            chkDealer.Checked = false;
            chkClaim.Checked = false;
            txtClaimMax.Text = "0";
            chkContract.Checked = false;
            chkSaleReport.Checked = false;
            chkClaimsReports.Checked = false;
            chkAccountReports.Checked = false;
            chkCustomReports.Checked = false;
            chkCancel.Checked = false;
            chkCancelGap.Checked = false;
            chkCancelMod.Checked = false;
            chkContractMod.Checked = false;
            chkHCCApprovalCEO.Checked = false;
            chkHCCApprovalManager.Checked = false;
            cboTeams.SelectedValue = "0";
            chkTeamLead.Checked = false;
            chkANOnly.Checked = false;
            chkVeroOnly.Checked = false;
            chkCancel100.Checked = false;
        }

        protected void rgUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfUserDetailID.Value = rgUser.SelectedValue.ToString();
            ClearFields();
            FillUser();
            pnlDetail.Visible = true;
            pnlSearch.Visible = false;
        }

        private void FillUser()
        {
            string SQL;
            clsDBO clU = new clsDBO();
            clsDBO clSI = new clsDBO();
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfUserDetailID.Value;
            clU.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                txtEMail.Text = clU.GetFields("email");
                txtFName.Text = clU.GetFields("fname");
                txtLName.Text = clU.GetFields("lname");
                txtPassword.Text = clU.GetFields("password");
                chkActive.Checked = Convert.ToBoolean((clU.GetFields("active")));
            }

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserDetailID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0) {
                clSI.GetRow();
                chkUserSettings.Checked = Convert.ToBoolean(clSI.GetFields("usersettings"));
                chkAccounting.Checked = Convert.ToBoolean(clSI.GetFields("Accounting"));
                chkSettings.Checked = Convert.ToBoolean(clSI.GetFields("settings"));
                chkAgents.Checked = Convert.ToBoolean(clSI.GetFields("agents"));
                chkDealer.Checked = Convert.ToBoolean(clSI.GetFields("dealer"));
                chkActivateContract.Checked = Convert.ToBoolean(clSI.GetFields("activatecontract"));
                chkClaim.Checked = Convert.ToBoolean(clSI.GetFields("claim"));
                chkContract.Checked = Convert.ToBoolean(clSI.GetFields("contract"));
                chkSaleReport.Checked = Convert.ToBoolean(clSI.GetFields("salesreports"));
                chkClaimsReports.Checked = Convert.ToBoolean(clSI.GetFields("claimsreports"));
                chkAccountReports.Checked = Convert.ToBoolean(clSI.GetFields("accountreports"));
                chkCustomReports.Checked = Convert.ToBoolean(clSI.GetFields("customreports"));
                chkCancelMod.Checked = Convert.ToBoolean(clSI.GetFields("cancelmodification"));
                chkCancelGap.Checked = Convert.ToBoolean(clSI.GetFields("cancelgap"));
                chkCancel.Checked = Convert.ToBoolean(clSI.GetFields("cancellation"));
                chkHCCApprovalManager.Checked = Convert.ToBoolean(clSI.GetFields("hccapprovalManager"));
                chkHCCApprovalCEO.Checked = Convert.ToBoolean(clSI.GetFields("hccapprovalCEO"));
                chkCancelPayment.Checked = Convert.ToBoolean(clSI.GetFields("CancelPay"));
                chkUnlockClaim.Checked = Convert.ToBoolean(clSI.GetFields("UnlockClaim"));
                chkClaimPayment.Checked = Convert.ToBoolean(clSI.GetFields("ClaimPayment"));
                txtClaimMax.Text = clSI.GetFields("claimapprove");
                txtMaxAuth.Text = clSI.GetFields("claimauth");
                cboInsCarrier.SelectedValue = clSI.GetFields("inscarrierid");
                chkReadOnly.Checked = Convert.ToBoolean(clSI.GetFields("readonly"));
                chkContractMod.Checked = Convert.ToBoolean(clSI.GetFields("contractmodification"));
                chkTeamLead.Checked = Convert.ToBoolean(clSI.GetFields("TeamLead"));
                chkANOnly.Checked = Convert.ToBoolean(clSI.GetFields("autonationonly"));
                chkVeroOnly.Checked = Convert.ToBoolean(clSI.GetFields("veroonly"));
                cboAgent.SelectedValue = clSI.GetFields("agentid");
                //cboSubAgent.SelectedValue = clSI.GetFields("subagentid");
                chkAllowCancelExpire.Checked = Convert.ToBoolean(clSI.GetFields("allowcancelexpire"));
                chkWithIn3Days.Checked = Convert.ToBoolean(clSI.GetFields("within3days"));
                chkAllCoverage.Checked = Convert.ToBoolean(clSI.GetFields("allcoverage"));
                chkAllowSlush.Checked = Convert.ToBoolean(clSI.GetFields("allowslush"));
                chkCancel100.Checked = Convert.ToBoolean(clSI.GetFields("cancel100"));
                chkAllowUndeny.Checked = Convert.ToBoolean(clSI.GetFields("AllowUndenyClaim"));
                chkClaimAudit.Checked = Convert.ToBoolean(clSI.GetFields("allowclaimaudit"));
                chkBypassMissingInfo.Checked = Convert.ToBoolean(clSI.GetFields("bypassmissinginfo"));
                if (clSI.GetFields("TeamID").Length == 0)
                        clSI.SetFields("TeamID", "0");
                cboTeams.SelectedValue = clSI.GetFields("TeamID");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clUSI = new clsDBO();
            clsDBO clU = new clsDBO();
            SQL = "select * from userinfo ";
            
            if (hfUserDetailID.Value.Length == 0)
                SQL = SQL + "where userid = 0 ";
            else
                SQL = SQL + "where userid = " + hfUserDetailID.Value;

            clU.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clU.RowCount() > 0)
                clU.GetRow();
            else
                clU.NewRow();

            clU.SetFields("email", txtEMail.Text);
            clU.SetFields("password", txtPassword.Text);
            clU.SetFields("fname", txtFName.Text);
            clU.SetFields("lname", txtLName.Text);
            clU.SetFields("active", chkActive.Checked.ToString());
            
            if (clU.RowCount() == 0)
                clU.AddRow();

            clU.SaveDB();
            GetUserInfo();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserDetailID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clUSI.RowCount() > 0)
                clUSI.GetRow();
            else
                clUSI.NewRow();

            clUSI.SetFields("userid", hfUserDetailID.Value);
            clUSI.SetFields("usersettings", chkUserSettings.Checked.ToString());
            clUSI.SetFields("accounting", chkAccounting.Checked.ToString());
            clUSI.SetFields("settings", chkSettings.Checked.ToString());
            clUSI.SetFields("agents", chkAgents.Checked.ToString());
            clUSI.SetFields("dealer", chkDealer.Checked.ToString());
            clUSI.SetFields("claim", chkClaim.Checked.ToString());
            clUSI.SetFields("unlockClaim", chkUnlockClaim.Checked.ToString());
            clUSI.SetFields("contract", chkContract.Checked.ToString());
            clUSI.SetFields("salesreports", chkSaleReport.Checked.ToString());
            clUSI.SetFields("activatecontract", chkActivateContract.Checked.ToString());
            clUSI.SetFields("claimsreports", chkClaimsReports.Checked.ToString());
            clUSI.SetFields("accountreports", chkAccountReports.Checked.ToString());
            clUSI.SetFields("customreports", chkCustomReports.Checked.ToString());
            clUSI.SetFields("Cancellation", chkCancel.Checked.ToString());
            clUSI.SetFields("cancelgap", chkCancelGap.Checked.ToString());
            clUSI.SetFields("cancelmodification", chkCancelMod.Checked.ToString());
            clUSI.SetFields("hccapprovalmanager", chkHCCApprovalManager.Checked.ToString());
            clUSI.SetFields("hccapprovalceo", chkHCCApprovalCEO.Checked.ToString());
            clUSI.SetFields("claimpayment", chkClaimPayment.Checked.ToString());
            clUSI.SetFields("cancelpay", chkCancelPayment.Checked.ToString());
            clUSI.SetFields("claimauth", txtMaxAuth.Text);
            clUSI.SetFields("claimapprove", txtClaimMax.Text);
            clUSI.SetFields("readonly", chkReadOnly.Checked.ToString());
            clUSI.SetFields("inscarrierid", cboInsCarrier.SelectedValue);
            clUSI.SetFields("contractmodification", chkContractMod.Checked.ToString());
            clUSI.SetFields("teamid", cboTeams.SelectedValue);
            clUSI.SetFields("teamlead", chkTeamLead.Checked.ToString());
            clUSI.SetFields("autonationonly", chkANOnly.Checked.ToString());
            clUSI.SetFields("veroonly", chkVeroOnly.Checked.ToString());
            clUSI.SetFields("agentid", cboAgent.SelectedValue);
            clUSI.SetFields("allowcancelexpire", chkAllowCancelExpire.Checked.ToString());
            clUSI.SetFields("within3days", chkWithIn3Days.Checked.ToString());
            clUSI.SetFields("allcoverage", chkAllCoverage.Checked.ToString());
            clUSI.SetFields("allowslush", chkAllowSlush.Checked.ToString());
            clUSI.SetFields("cancel100", chkCancel100.Checked.ToString());
            clUSI.SetFields("AllowUndenyClaim", chkAllowUndeny.Checked.ToString());
            clUSI.SetFields("allowclaimaudit", chkClaimAudit.Checked.ToString());
            clUSI.SetFields("bypassmissinginfo", chkBypassMissingInfo.Checked.ToString());

            //if (cboSubAgent.SelectedValue.Length == 0)
            //    clUSI.SetFields("subagentid", "0");
            //else
            //    clUSI.SetFields("subagentid", cboSubAgent.SelectedValue);

            if (clUSI.RowCount() == 0)
                clUSI.AddRow();

            clUSI.SaveDB();
            ClearFields();
            rgUser.Rebind();
            pnlDetail.Visible = false;
            pnlSearch.Visible = true;
        }

        private void GetUserInfo()
        {
            string SQL;
            clsDBO clU = new clsDBO();
            SQL = "select * from userinfo ";
            SQL = SQL + "where email = '" + txtEMail.Text + "' ";
            SQL = SQL + "and password = '" + txtPassword.Text + "' ";
            clU.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                hfUserDetailID.Value = clU.GetFields("userid");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearFields();
            rgUser.Rebind();
            pnlDetail.Visible = false;
            pnlSearch.Visible = true;
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnToDoCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todo.aspx?sid=" + hfID.Value);
        }

        protected void btnToDoReader_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/todoreader1.aspx?sid=" + hfID.Value);
        }

        protected void cboAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItem li = new ListItem();
            string SQL;
            clsDBO clR = new clsDBO();

            //cboSubAgent.Items.Clear();
            li.Value = "0";
            li.Text = "All";
            //cboSubAgent.Items.Add(li);

            if (cboAgent.SelectedValue == "0")
                return;

            SQL = "select * from subagents ";
            SQL = SQL + "where agentid = " + cboAgent.SelectedValue + " ";
            SQL = SQL + "order by subagentname ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("subagentid");
                    li.Text = clR.GetFields("subagentname");
                    //cboSubAgent.Items.Add(li);
                }
            }
        }
    }
}