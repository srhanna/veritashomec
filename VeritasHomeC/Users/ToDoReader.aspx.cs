﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ToDoReader : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsMessage.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
           
            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                pnlDetail.Visible = false;
                pnlList.Visible = true;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void rgToDoMessengerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            hfIDX.Value = rgToDoMessengerList.SelectedValue.ToString();
            FillScreen();
        }

        private void FillScreen()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where idx = " + hfIDX.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if(clR.RowCount() > 0)
            {
                clR.GetRow();
                hfFromID.Value = clR.GetFields("fromid");
                lblToDisplay.Text = clsFunc.GetUserName(hfUserID.ToString());
                lblFromDisplay.Text = clsFunc.GetFromName(hfFromID.ToString());
                lblDate.Text = clR.GetFields("messagedate");
                txtHeader.Text = clR.GetFields("header");
                txtMessageDisplay.Text = clR.GetFields("message");
                if (Convert.ToBoolean(clR.GetFields("completedmessage")))
                    btnCompleted.Visible = false;
            }
        }

        protected void btnClosed_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgToDoMessengerList.Rebind();
        }

        protected void btnCompleted_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update usermessage ";
            SQL = SQL + "set completedmessage = 1 ";
            SQL = SQL + "where idx = " + hfIDX.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgToDoMessengerList.Rebind();
        }

        protected void btnRemoveMessage_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update usermessage ";
            SQL = SQL + "set deletemessage = 1 ";
            SQL = SQL + "where idx = " + hfIDX.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgToDoMessengerList.Rebind();
        }
    }
}