﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VeritasHomeC
{
    public class mGlobal
    {
        public static bool bJobRefresh;

        public static string rDirHome = "~/default.aspx?sid=";
        public static string rDirSalesReports = "~/reports/salesreports.aspx?sid=";
        public static string rDirClaimsReports = "~/reports/claimsreports.aspx?sid=";
        public static string rDirAccountingReports = "~/reports/accountingreports.aspx?sid=";
        public static string rDirAgents = "~/reports/agentssearch.aspx?sid=";
        public static string rDirReports = "~/reports/reports.aspx?sid=";
        public static string rDirDealer = "~/dealer/dealersearch.aspx?sid=";
        public static string rDirContract = "~/contract/contractsearch.aspx?=";
        public static string rDirClaim = "~/claim/claimsearch.aspx?sid=";
        public static string rDirAccounting = "~/accounting/accounting.aspx?sid=";
        public static string rDirUsers = "~/users/users.aspx?sid=";
        public static string rDirLogout = "~/default.aspx";
        public static string rDirMCSummation = "~/reports/accountingreports/MonthlyCommissionSummation.aspx?sid=";
        public static string rDirMCSold = "~/reports/accountingreports/MonthlyCommissionSold.aspx?sid=";
        public static string rDirMCCancels = "~/reports/accountingreports/MonthlyCommissionCancels.aspx?sid=";


    }
}