﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoxyCancellation.aspx.cs" Inherits="VeritasHomeC.MoxyCancellation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Moxy Cancellation</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel runat="server">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true" Font-Size="Large">
                                                        Contract No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblContractNo" runat="server" Font-Size="Large" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Sale Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblSaleDate" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Effective Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblEffDate" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Expire Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblExpDate" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Paid Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblPaidDate" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Sale Miles:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblSaleMiles" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Effective Mile:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblEffMile" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Expire Mile:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblExpMile" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Program:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblProgram" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Plan:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblPlanType" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Term:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Label ID="lblTerm" runat="server" Text=""></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                        Customer Info:
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                        Dealer Info:
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                        Agent Info:
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                        <asp:TextBox Font-Size="11" Font-Names="calibri" ID="txtCustomerInfo" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="5" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                        <asp:TextBox ID="txtDealer" Font-Size="11" Font-Names="calibri" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="6" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                        <asp:TextBox ID="txtAgent" Font-Size="11" Font-Names="calibri" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="6" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">    
                                                                    Cancel Eff. Date:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadDatePicker ID="rdpProcessDate" runat="server"></telerik:RadDatePicker>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Cancel Eff. Miles:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadNumericTextBox ID="txtMiles" NumberFormat-DecimalDigits="0" runat="server"></telerik:RadNumericTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    &nbsp
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    &nbsp
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    &nbsp
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:HyperLink ID="hlWorksheet" Target="_blank" runat="server">View Worksheet</asp:HyperLink>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Cancel Percent:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCancelPercent" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Term Percent:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtTermPercent" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Mile Percent:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtMilePercent" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Cancel Status:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCancelStatus" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Gross Customer:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtGrossCustomer" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Gross Dealer:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtGrossDealer" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Gross Admin:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtGrossAdmin" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Claim Amt:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtClaimAmt" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Cancel Fee:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCancelFee" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    &nbsp
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    &nbsp
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Lienholder:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtLienholder" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Quote Date:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtQuoteDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Request Date:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtRequestDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Cancel Date:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCancelDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Customer Refund:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCustomerRefund" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    From Dealer:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtFromDealer" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    From Admin:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtFromAdmin" ReadOnly="true" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Table runat="server">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Font-Bold="true">
                                                                                Payee:
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:RadioButton ID="rbDealer" Text="Dealer" AutoPostBack="true" GroupName="Payee" Font-Bold="true" runat="server" />
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:RadioButton ID="rbCustomer" Text="Customer" AutoPostBack="true" GroupName="Payee" Font-Bold="true" runat="server" />
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:RadioButton ID="rbOther" OnCheckedChanged="rbOther_CheckedChanged" Text="Other" AutoPostBack="true" GroupName="Payee" Font-Bold="true" runat="server" />
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                &nbsp
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                &nbsp
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                &nbsp
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:Button ID="btnVerify" OnClick="btnVerify_Click" runat="server" Text="Verify" CssClass="button1" />
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:Button ID="btnDeny" OnClick="btnDeny_Click" runat="server" Text="Deny" CssClass="button1" />
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:Button ID="btnAccept" OnClick="btnAccept_Click" runat="server" Text="Accept" CssClass="button1" />
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Panel runat="server" ID="pnlOther" Visible="false">
                                                                        <asp:Table runat="server">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Company Name:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    First Name:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Last Name:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Address 1:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    Address 2:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    City:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    State:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                                                                    <asp:SqlDataSource ID="dsStates"
                                                                                    ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>            </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    Zip:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Phone:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                    </asp:Panel>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Panel runat="server" ID="pnlVerify" Visible="false">
                                                                        <asp:Table runat="server">
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Cancel Percent:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCancelPercentV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Term Percent:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtTermPercentV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Mile Percent:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtMilePercentV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Cancel Status:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCancelStatusV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Gross Customer:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtGrossCustomerV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Gross Dealer:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtGrossDealerV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Gross Admin:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtGrossAdminV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Claim Amt:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtClaimAmtV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Cancel Fee:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCancelFeeV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    &nbsp
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    &nbsp
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Lienholder:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtLienholderV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Quote Date:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtQuoteDateV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Request Date:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtRequestDateV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Cancel Date:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCancelDateV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                            <asp:TableRow>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    Customer Refund:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtCustomerRefundV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    From Dealer:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtFromDealerV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                                <asp:TableCell Font-Bold="true">
                                                                                    From Admin:
                                                                                </asp:TableCell>
                                                                                <asp:TableCell>
                                                                                    <asp:TextBox ID="txtFromAdminV" ReadOnly="true" runat="server"></asp:TextBox>
                                                                                </asp:TableCell>
                                                                            </asp:TableRow>
                                                                        </asp:Table>
                                                                    </asp:Panel>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="RadWindowManager1"></telerik:RadWindowManager>
           </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" CssClass="button2" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfContractID" runat="server" />
    </form>
</body>
</html>
