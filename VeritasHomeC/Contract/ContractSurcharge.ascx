﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractSurcharge.ascx.cs" Inherits="VeritasHomeC.ContractSurcharge" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlSurcharge">
                <telerik:RadGrid ID="rgSurcharge" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                    AllowSorting="true" AllowPaging="false"  Width="500" ShowFooter="true">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ContractSurchargeID" PageSize="25" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractSurchargeID"  ReadOnly="true" Visible="false" UniqueName="ContractSurchargeID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Surcharge" UniqueName="Surcharge" HeaderText="Surcharge"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>


<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />