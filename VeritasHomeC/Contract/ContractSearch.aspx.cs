﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;

namespace VeritasHomeC
{
    public partial class ContractSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            CheckToDo();

            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }  
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
            }
        }


        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchContracts();
        }

        private void SearchContracts()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select contractid, contractno, dealername, fname, lname, c.city, c.state, saledate, a.agentname ";
            SQL = SQL + "from contract c ";
            SQL = SQL + "left join dealer d on c.dealerid = d.dealerid ";
            SQL = SQL + "left join agents a on c.agentsid = a.agentid ";
            SQL = SQL + "where contractid > 0 ";

            if (txtContractNo.Text.Length > 0)
                SQL = SQL + "and contractno like '%" + txtContractNo.Text.Trim() + "%' ";

            if (txtDealerNo.Text.Length > 0)
                SQL = SQL + "and dealerno like '%" + txtDealerNo.Text.Trim() + "%' ";

            if (txtDealername.Text.Length > 0)
                SQL = SQL + "and dealername like '%" + txtDealername.Text.Trim() + "%' ";

            if (txtAgentName.Text.Length > 0)
                SQL = SQL + "and agentname like '%" + txtAgentName.Text.Trim() + "%' ";

            if (txtFName.Text.Length > 0)
                SQL = SQL + "and fname like '%" + txtFName.Text.Trim() + "%' ";

            if (txtLName.Text.Length > 0)
                SQL = SQL + "and lname like '%" + txtLName.Text.Trim() + "%' ";

            if (txtCity.Text.Length > 0)
                SQL = SQL + "and c.city like '%" + txtCity.Text.Trim() + "%' ";

            if (cboState.Text.Length > 0)
                SQL = SQL + "and c.state like '%" + cboState.Text.Trim() + "%' ";

            if (txtPhone.Text.Length > 0)
                SQL = SQL + "and c.phone like '%" + txtPhone.Text.Trim() + "%' ";

            if (Information.IsDate(txtSaleDate.Text))
                SQL = SQL + "and saledate = '" + txtSaleDate.Text.Trim() + "' ";

            if (Information.IsDate(txtPayDate.Text))
                SQL = SQL + "and datepaid = '" + txtPayDate.Text.Trim() + "' ";

            if (txtCheckNo.Text.Length > 0)
                SQL = SQL + "and contractid in (select contractid from contractpayment where checkno like '%" + txtCheckNo.Text.Trim() + "%' ";

            if (String.IsNullOrEmpty(hfInsCarrierID.Value))
                hfInsCarrierID.Value = "0";

            if (Convert.ToInt64(hfInsCarrierID.Value) > 0)
                SQL = SQL + "and inscarrierid = " + hfInsCarrierID.Value + " ";

            if (hfVeroOnly.Value == "True")
                SQL = SQL + "and a.agentid = 54 ";

            SQL = SQL + "order by lname ";
            rgContractID.DataSource = clC.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgContractID.DataBind();
        }

        protected void rgContractID_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/contract.aspx?sid=" + hfID.Value + "&contractid=" + rgContractID.SelectedValue);
        }

        protected void txtAgentName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void txtContractNo_Load(object sender, EventArgs e)
        {

        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }
    }
}