﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractBreakdown.ascx.cs" Inherits="VeritasHomeC.ContractBreakdown" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Table runat="server" Font-Names="Calibri" Font-Size="11">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlAmounts">
                <telerik:RadGrid ID="rgAmount" OnSelectedIndexChanged="rgAmount_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                    AllowSorting="true" AllowPaging="false"  Width="500" ShowFooter="true">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ContractAmtID" PageSize="25" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractAmtID"  ReadOnly="true" Visible="false" UniqueName="ContractAmtID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateTypeName" HeaderText="Bucket"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CategoryName" UniqueName="CategoryName" HeaderText="Category"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Amount"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CancelAmt" UniqueName="CancelAmt" HeaderText="Cancel Amt"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="pnlModify">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bucket: 
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRateType" ReadOnly="true" runat="server"></asp:TextBox>
                            <asp:Button ID="btnSearchRateType" BackColor="#1eabe2" BorderColor="#1eabe2" runat="server" Text="Search Bucket" />
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Amount:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Cancel Amount:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCancelAmt" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnCancel" OnClick="btnCancel_Click" CssClass="button1" runat="server" Text="Cancel" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnSave" BackColor="#1eabe2" CssClass="button2" runat="server" Text="Save" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlSearchRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddRateType" CssClass="Button1" runat="server" Text="Add Rate Type" BorderColor="#1eabe2" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadGrid ID="rgRateType" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="dsRateType">
                                <GroupingSettings CaseSensitive="false" />
                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="RateTypeID" PageSize="10" ShowFooter="true">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="RateTypeID" ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="RateTypeName" UniqueName="RateType" HeaderText="Bucket" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CategoryName" UniqueName="RateCategory" HeaderText="Bucket Category" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings EnablePostBackOnRowClick="true">
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="dsRateType"
                            ProviderName="System.Data.SqlClient" SelectCommand="select ratetypeid, ratetypeName, categoryname from ratetype rt inner join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid where ratetypeid <> 29 " 
                            runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnCancelRateType" runat="server" Text="Cancel" CssClass="button1"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel ID="pnlAddRateType" runat="server">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Bucket Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRateTypeAdd" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Bucket Category
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadComboBox ID="cboRateCategoryAdd" DataSourceID="dsRateCategory" DataTextField="CategoryName" DataValueField="RateCategoryID" runat="server"></telerik:RadComboBox>
                            <asp:SqlDataSource ID="dsRateCategory"
                            ProviderName="System.Data.SqlClient" SelectCommand="select RateCategoryid, categoryname from ratecategory order by categoryname " runat="server"></asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancelCategory" runat="server" Text="Cancel" CssClass="button1"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSaveCategory" runat="server" Text="Save" CssClass="button2"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfRateTypeID" runat="server" />
<asp:HiddenField ID="hfContractAmtID" runat="server" />