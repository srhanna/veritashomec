﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cancellation.aspx.cs" Inherits="VeritasHomeC.Cancellation" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Cancellation</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel ID="pnlOut" CssClass="CancelOut" runat="server">
            <asp:Table runat="server" Font-Names="Calibri" Font-Size="11">
                <asp:TableRow>
                    <asp:TableCell>
                        &nbsp&nbsp&nbsp&nbsp&nbsp
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Panel runat="server" BackColor="#1eabe2" Height="100" Width="756" ID="pnlHeader">
                            <asp:Image ID="Image1" ImageUrl="~/images/Veritas_Final-Logo_550px.png" runat="server" />
                        </asp:Panel>
                    </asp:TableCell>
                    <asp:TableCell>
                        &nbsp&nbsp&nbsp&nbsp&nbsp
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        &nbsp&nbsp&nbsp&nbsp&nbsp
                    </asp:TableCell>
                    <asp:TableCell Font-Bold="true" HorizontalAlign="Center">
                        Customer Refund Worksheet
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        &nbsp&nbsp&nbsp&nbsp&nbsp
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table runat="server" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Contract Number
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="190" HorizontalAlign="Left">
                                     &nbsp <asp:Label ID="lblContractNo" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Selling Dealership
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="190" HorizontalAlign="Left">
                                     &nbsp <asp:Label ID="lblDealer" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp VIN:
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="190" HorizontalAlign="Left">
                                    &nbsp <asp:Label ID="lblVIN" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Lien Holder
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160">
                                     &nbsp<asp:Label ID="lblLienholder" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Customer Name
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160">
                                     &nbsp <asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Payee
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160">
                                     &nbsp <asp:Label ID="lblPayee" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Sale Date
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblSaleDate" runat="server" Text=""></asp:Label> &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell style="border-right-color: Black; border-left-style: solid; border-left-width:medium"  Width="160">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160">
                                     &nbsp <asp:Label ID="lblAddr1" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Dealer Cost
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    &nbsp  <asp:Label ID="lblDealerCostBase" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" style="border-right-color: Black; border-left-style: solid; border-left-width:medium"  Width="160">
                                    &nbsp Payee Address
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160">
                                     &nbsp <asp:Label ID="lblAddr2" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Customer Cost
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    &nbsp <asp:Label ID="lblCustomerCost" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell style="border-right-color: Black; border-left-style: solid; border-left-width:medium; border-bottom-style:solid;border-bottom-width:medium"  Width="160">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160">
                                     &nbsp <asp:Label ID="lblAddr3" runat="server" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp  Cancel Eff. Date
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    &nbsp <asp:Label ID="lblEffDate" runat="server" Text=""></asp:Label> &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp  
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                     &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp 
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                     &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp 
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                     &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Time Percentage
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblTimePer" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Expiration Date
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                     <asp:Label ID="lblTotalDays" runat="server" Text=""></asp:Label> &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Refund Percentage
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblRefundPer" runat="server" Text=""></asp:Label> &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp 
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Total Days Elapsed
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblDaysElapse" runat="server" Text=""></asp:Label> &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Regulated State
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblRegulated" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp 
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Dealer Cost to Customer
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblDealerCost" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Admin Cost to Customer
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblAdminCost" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Claims/Refunds Paid
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblClaims" runat="server" Text=""></asp:Label> &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Total Customer Refund
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblCustomerRefund" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Cancellation Fee
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblCancelFee" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                                <asp:TableCell Width="45">
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true" BorderWidth="2" BorderStyle="Solid" Width="160">
                                    &nbsp Cancellation Info
                                </asp:TableCell>
                                <asp:TableCell BorderStyle="Solid" BorderWidth="2" Width="160" HorizontalAlign="Right">
                                    <asp:Label ID="lblCancelInfo" runat="server" Text=""></asp:Label>  &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="rQuote">
                                <asp:TableCell ColumnSpan="5">
                                     &nbsp Please be aware that any changes to the cancellation date may result in a change to the refund shown in this quote
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="rCancel">
                                <asp:TableCell ColumnSpan="5">
                                     &nbsp Dealer needs to refund the customer within 10 business days of receiving the refund check. 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="rCancel2">
                                <asp:TableCell ColumnSpan="5">
                                    &nbsp  If no refund is due from Admin,the selling dealer is to refund the Dealer Cost to Customer
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="rCancel3">
                                <asp:TableCell ColumnSpan="5">
                                    &nbsp  within 10 days of receiving the cancellation breakdown worksheet.
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    &nbsp
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell>
                        &nbsp&nbsp&nbsp&nbsp&nbsp
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportPDF" Text="Export page to PDF" AutoPostBack="false" UseSubmitBehavior="false"></telerik:RadButton>
              <telerik:RadClientExportManager runat="server" ID="RadClientExportManager1">
                  <PdfSettings MarginRight="10mm" MarginBottom="10mm" MarginLeft="10mm" MarginTop="10mm" />
              </telerik:RadClientExportManager>

    <script>
        var $ = $telerik.$;
 
        function exportPDF() {

            $find('<%=RadClientExportManager1.ClientID%>').exportPDF($(".CancelOut"));
        }
 
    </script>
    </form>
</body>
</html>
