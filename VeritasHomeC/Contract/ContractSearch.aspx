﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractSearch.aspx.cs" Inherits="VeritasHomeC.ContractSearch" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Contract Search</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Contract No:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtContractNo" OnLoad="txtContractNo_Load" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Seller No:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtDealerNo" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Seller Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtDealername" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Agent Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtAgentName" OnTextChanged="txtAgentName_TextChanged" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    First Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtFName" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Last Name:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtLName" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    City:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtCity" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    State:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                                    <asp:SqlDataSource ID="dsStates"
                                                    ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Phone:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtPhone" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Sale Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtSaleDate" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Pay Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtPayDate" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Check No:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadTextBox ID="txtCheckNo" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Search Contract" CssClass="button1" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadGrid ID="rgContractID" OnSelectedIndexChanged="rgContractID_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                            AllowSorting="true" AllowPaging="true"  Width="1500" ShowFooter="true">
                                            <GroupingSettings CaseSensitive="false" />
                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ContractID" PageSize="10" ShowFooter="true">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="DealerName" UniqueName="DealerName" HeaderText="Seller Name"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="City" UniqueName="DealerCity" HeaderText="City"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="State" UniqueName="AgentState" HeaderText="State"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="AgentName" UniqueName="AgentName" HeaderText="Agent Name"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                <Selecting AllowRowSelect="true" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" CssClass="button2" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfInsCarrierID" runat="server" />
        <asp:HiddenField ID="hfVeroOnly" runat="server" />
    </form>
</body>
</html>
