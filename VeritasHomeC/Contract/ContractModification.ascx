﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractModification.ascx.cs" Inherits="VeritasHomeC.ContractModification" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell VerticalAlign="top">
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            New Sale Date:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtNewSaleDate" TextMode="Date" runat="server"></asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Button ID="btnUpdateSaleDate" OnClick="btnUpdateSaleDate_Click" runat="server" CssClass="button1" Text="Update Sale Date" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgHistory">
                <telerik:RadGrid ID="rgHistory" runat="server" AutoGenerateColumns="false" AllowSorting="true">
                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                        CommandItemDisplay="TopAndBottom">
                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractHistoryID" UniqueName="ContractHistoryID" Visible="false"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FieldName" UniqueName="FieldName" HeaderText="Field Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OldValue" UniqueName="OldValue" HeaderText="Old Value"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NewValue" UniqueName="NewValue" HeaderText="New Value"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="Credate" HeaderText="Mod Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UserName" UniqueName="UserName" HeaderText="Mod By"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </telerik:RadAjaxPanel>
        </asp:TableCell>
    </asp:TableRow>
    </asp:Table>
    </asp:TableCell>
        <asp:TableCell>&nbsp</asp:TableCell>
    <asp:TableCell VerticalAlign="top">
    <asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Lienholder: 
            </asp:TableCell>
            <asp:TableCell>
            <telerik:RadDropDownList AutoPostBack="true"
                ID="LienholderSelect" 
                runat="server"
                DataValueField="lienholder" 
                OnSelectedIndexChanged="LienholderSelect_SelectedIndexChanged"
                DataTextField="lienholder" 
                DataSourceID="dsLienholderSelect">
            </telerik:RadDropDownList>
            &nbsp
            <asp:Button ID="btnUpdateLienholder" Text="Update Lienholder" runat="server" CssClass="button1" OnClick="btnUpdateLienholder_Click" />
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Plan Type:
            </asp:TableCell>
            <asp:TableCell>
            <telerik:RadDropDownList AutoPostBack="true"
                ID="PlanTypeSelect"
                runat="server"
                DataValueField="plantypeid"
                OnSelectedIndexChanged="PlanTypeSelect_SelectedIndexChanged"
                DataTextField="PlanType">
                </telerik:RadDropDownList>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Term: 
            </asp:TableCell>
        <asp:TableCell>
            <telerik:RadDropDownList AutoPostBack="true"
                ID="TermMonthSelect"
                runat="server"
                DataValueField="termmonth"
                OnSelectedIndexChanged="TermMonthSelect_SelectedIndexChanged"
                DataTextField="termmonth"
                DataSourceID="dsTermMonthSelect">
            </telerik:RadDropDownList>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Dealer Cost:
            </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtDealerCost" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Customer Cost:
            </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtCustomerCost" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>&nbsp</asp:TableCell>
        <asp:TableCell>
            <asp:Button ID="btnUpdatePlan" runat="server" Text="Update Contract Plan" CssClass="button2" OnClick="btnUpdatePlan_Click" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="top" HorizontalAlign="center">
                 <asp:label runat="server" Text="Surcharges" Font-Bold="true" Font-Underline="true"></asp:label>
                <asp:Table runat="server" BorderWidth="1px">
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadCheckBoxList ID="chkSurcharges" runat="server">
                                <DataBindings DataTextField="Surcharge" DataValueField="SurchargeID" />
                            </telerik:RadCheckBoxList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="center">
                            <asp:Button ID="btnUpdateSurcharge" Text="Update Surcharges" runat="server" OnClick="btnUpdateSurcharge_Click" CssClass="button1" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

<telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="rwDealerError"></telerik:RadWindowManager>

<asp:SqlDataSource ID="dsLienholderSelect"
            ProviderName="System.Data.SqlClient" SelectCommand="select lienholder from contract where not LienHolder is null group by lienholder order by lienholder" runat="server">
</asp:SqlDataSource>

<asp:SqlDataSource ID="dsTermMonthSelect"
            ProviderName="System.Data.SqlClient" SelectCommand="select termmonth from contract where not termmonth is null group by termmonth order by termmonth" runat="server">
</asp:SqlDataSource>

 <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Title="Note Error" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text="You need to enter in new Dealer Cost"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" CssClass="button2" Width="75" Text="OK" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>

<asp:HiddenField ID="hfSurchargeID" runat="server" />
<asp:HiddenField ID="hfTermMonth" runat="server" />
<asp:HiddenField ID="hfPlanType" runat="server" />
<asp:HiddenField ID="hfPlanTypeID" runat="server" />
<asp:HiddenField ID="hfLienholder" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
