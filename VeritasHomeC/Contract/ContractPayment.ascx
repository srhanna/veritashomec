﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractPayment.ascx.cs" Inherits="VeritasHomeC.ContractPayment" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel ID="pnlPaymentList" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnAddPayment" OnClick="btnAddPayment_Click" runat="server" Text="Add Payment" CssClass="button1" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgContractPayment" OnSelectedIndexChanged="rgContractPayment_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                    AllowSorting="true" AllowPaging="true"  Width="1175" ShowFooter="true">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ContractPaymentID" PageSize="10" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractPaymentID"  ReadOnly="true" Visible="false" UniqueName="ContractPaymentID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Company"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FName" UniqueName="FName" HeaderText="First Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LName" UniqueName="LName" HeaderText="Last Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PaymentType" UniqueName="PaymentType" HeaderText="Payment Type"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CheckNo" UniqueName="CheckNo" HeaderText="Check No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="pnlPayment" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payee: 
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPayee" runat="server"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnPayeeSearch" OnClick="btnPayeeSearch_Click" runat="server" Text="Payee Search" CssClass="button1" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payment Type:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadComboBox ID="cboPaymentType" DataSourceID="SqlDataSource2" DataValueField="paymenttype" DataTextField="paymenttype" runat="server"></telerik:RadComboBox>
                <asp:SqlDataSource ID="SqlDataSource2"
                ProviderName="System.Data.SqlClient" SelectCommand="select paymenttype from contractpaymenttype" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Date Paid:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadDatePicker ID="rdpProcessDate" runat="server"></telerik:RadDatePicker>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Check No:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtCheckNo" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Paid Amt:
            </asp:TableCell>
            <asp:TableCell>
                <telerik:RadNumericTextBox ID="txtpaidAmt" runat="server"></telerik:RadNumericTextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnCancelPayment" OnClick="btnCancelPayment_Click" runat="server" Text="Cancel" CssClass="button1" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnSavePayment" OnClick="btnSavePayment_Click" runat="server" Text="Save" CssClass="button2" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlPayeeSearch" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnCancelSearch" OnClick="btnCancelSearch_Click" runat="server" Text="Cancel Search" CssClass="button1" />
            </asp:TableCell>
            <asp:TableCell>
                <asp:Button ID="btnAddPayee" OnClick="btnAddPayee_Click" runat="server" Text="Add Payee" CssClass="button1" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <telerik:RadGrid ID="rgPayee" OnSelectedIndexChanged="rgPayee_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                    AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                    <GroupingSettings CaseSensitive="false" />
                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ContractPayeeID" ShowFooter="true">
                        <Columns>
                            <telerik:GridBoundColumn DataField="ContractPayeeID"  ReadOnly="true" Visible="false" UniqueName="ContractPayeeID"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PayeeNo" FilterCheckListWebServiceMethod="LoadPayeeNo" UniqueName="PayeeNo" HeaderText="Payee Number" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PayeeName" FilterCheckListWebServiceMethod="LoadPayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>                                                   
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" />
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="SqlDataSource1"
                ProviderName="System.Data.SqlClient" SelectCommand="select contractpayeeid, payeeno, payeename 
                from contractpayee order by payeename" runat="server"></asp:SqlDataSource>
            </asp:TableCell>
        </asp:TableRow> 
    </asp:Table>
</asp:Panel>

<asp:Panel ID="pnlPayeeModify" runat="server">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payee Number:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPayeeNo" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Font-Bold="true">
                Payee Name: 
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPayeeName" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                &nbsp
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnCancelPayee" runat="server" Text="Cancel" OnClick="btnCancelPayee_Click" />
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnSavePayee" runat="server" Text="Save" OnClick="btnSavePayee_Click" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfPayeeID" runat="server" />
<asp:HiddenField ID="hfContractPaymentID" runat="server" />
