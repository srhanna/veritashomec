﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractBreakdown : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsRateCategory.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsRateType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                hfContractID.Value = Request.QueryString["contractid"];
                pnlAddRateType.Visible = false;
                pnlModify.Visible = false;
                pnlSearchRateType.Visible = false;
                FillRateTypeGrid();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillRateTypeGrid()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select contractamtid, ratetypename, categoryname, amt, cancelamt from contractamt ca ";
            SQL = SQL + "left join ratetype rt on rt.ratetypeid = ca.ratetypeid ";
            SQL = SQL + "left join ratecategory rc on rt.ratecategoryid = rc.ratecategoryid ";
            SQL = SQL + "where contractid = " + hfContractID.Value + " ";
            SQL = SQL + "order by orderid, ratetypename ";

            rgAmount.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgAmount.DataBind();
        }

        protected void rgAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfContractAmtID.Value = rgAmount.SelectedValue.ToString();
            if (CheckCommission())
                return;

            pnlAmounts.Visible = false;
            pnlModify.Visible = true;
            FillModify();
        }

        private void FillModify()
        {
            string SQL;
            clsDBO clCA = new clsDBO();
            SQL = "select * from contractamt ca ";
            SQL = SQL + "left join ratetype rt on rt.ratetypeid = ca.ratetypeid ";
            SQL = SQL + "where contractamtid = " + hfContractAmtID.Value;
            clCA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCA.RowCount() > 0)
            {
                clCA.GetRow();
                hfRateTypeID.Value = clCA.GetFields("ratetypeid");
                txtRateType.Text = clCA.GetFields("ratetypename");
                txtAmount.Text = clCA.GetFields("amt");
                txtCancelAmt.Text = clCA.GetFields("cancelamt");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlModify.Visible = false;
            pnlAmounts.Visible = true;
        }

        private bool CheckCommission()
        {
            clsDBO clCA = new clsDBO();
            string SQL;
            SQL = "select * from contractamt ca ";
            SQL = SQL + "left join ratetype rt on rt.ratetypeid = ca.ratetypeid ";
            SQL = SQL + "where contractamtid  = " + hfContractAmtID.Value;
            clCA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCA.RowCount() > 0)
            {
                clCA.GetRow();
                if (clCA.GetFields("ratecategoryid") == "2")
                {
                    return true;
                }
            }

            return false;
        }
    }
}