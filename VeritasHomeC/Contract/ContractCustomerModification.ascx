﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractCustomerModification.ascx.cs" Inherits="VeritasHomeC.ContractCustomerModification" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            First Name:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
        </asp:TableCell>
        <asp:TableCell Font-Bold="true">
            Last Name:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Address 1:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Address 2:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            City:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
        </asp:TableCell>
        <asp:TableCell Font-Bold="true">
            State:
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
            <asp:SqlDataSource ID="dsStates"
            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
        </asp:TableCell>
        <asp:TableCell Font-Bold="true">
            Zip:
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Font-Bold="true">
            Phone:
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table runat="server" Width="900" HorizontalAlign="Right">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="btnSave" OnClick="btnSave_Click"  runat="server" Width="100" Text="Save" CssClass="button2" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Width="100" Text="Cancel" CssClass="button1" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
