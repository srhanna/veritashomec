﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractPayment : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            SqlDataSource2.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            hfContractID.Value = Request.QueryString["contractid"];

            if (!IsPostBack)
            {
                ReadOnlyButtons();
                pnlPaymentList.Visible = true;
                pnlPayment.Visible = false;
                pnlPayeeSearch.Visible = false;
                pnlPayeeModify.Visible = false;
                FillPayment();
            }
        }

        private void FillPayment()
        {
            clsDBO clR = new clsDBO();
            string SQL = @"select contractpaymentid, cp.contractid, ce.payeename, c.FName, c.lname, cp.paymenttype, cp.DatePaid, cp.checkno, 
                    PaidAmt from ContractPayment cp 
                    left join contractpayee ce on cp.payeeid = ce.contractpayeeid
                    inner join contract c on cp.ContractID = c.ContractID ";
            SQL = SQL + "where cp.contractid = " + hfContractID.Value;
            rgContractPayment.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void ReadOnlyButtons()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnAddPayment.Enabled = false;
                    btnPayeeSearch.Enabled = false;
                    btnSavePayment.Enabled = false;
                }
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddPayment_Click(object sender, EventArgs e)
        {
            hfContractPaymentID.Value = "0";
            txtpaidAmt.Text = "";
            txtCheckNo.Text = "";
            txtPayee.Text = "";
            hfPayeeID.Value = "0";
            rdpProcessDate.Clear();
            pnlPaymentList.Visible = false;
            pnlPayment.Visible = true;
        }

        protected void btnSavePayment_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contractpayment where contractpaymentid = " + hfContractPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("contractid", hfContractID.Value);
            clR.SetFields("payeeid", hfPayeeID.Value);
            clR.SetFields("paymenttype", cboPaymentType.SelectedValue);
            clR.SetFields("datepaid", rdpProcessDate.SelectedDate.ToString());
            clR.SetFields("checkno", txtCheckNo.Text);
            clR.SetFields("paidamt", txtpaidAmt.Text);

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
            pnlPayment.Visible = false;
            pnlPaymentList.Visible = true;
            rgContractPayment.Rebind();
            CheckAmt();
            FillPayment();
            Response.Redirect("~/contract/contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        private void CheckAmt()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dPayment = 0; double dCost = 0;
            SQL = "select moxydealercost from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dCost = Convert.ToDouble(clR.GetFields("moxydealercost"));
            }

            SQL = "select sum(paidamt) as Amt from contractpayment where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dPayment = Convert.ToDouble(clR.GetFields("amt"));
            }

            if (dCost == dPayment)
            {
                SQL = "update contract set status = 'Paid' where contractid = " + hfContractID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                Response.Redirect("~/contract/contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }

        protected void btnCancelPayment_Click(object sender, EventArgs e)
        {
            pnlPayment.Visible = false;
            pnlPaymentList.Visible = true;
            FillPayment();
        }

        protected void btnPayeeSearch_Click(object sender, EventArgs e)
        {
            pnlPayment.Visible = false;
            pnlPayeeSearch.Visible = true;
        }

        protected void btnCancelSearch_Click(object sender, EventArgs e)
        {
            pnlPayment.Visible = true;
            pnlPayeeSearch.Visible = false;
        }

        protected void rgPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPayeeID.Value = rgPayee.SelectedValue.ToString();
            FillPayee();
            pnlPayeeSearch.Visible = false;
            pnlPayment.Visible = true; 
        }

        private void FillPayee()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contractpayee where contractpayeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayee.Text = clR.GetFields("payeeno") + " / " + clR.GetFields("payeename");
            }
        }

        protected void rgContractPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfContractPaymentID.Value = rgContractPayment.SelectedValue.ToString();
            pnlPaymentList.Visible = false;
            pnlPayment.Visible = true;
            clsDBO clR = new clsDBO();
            string SQL = "select * from contractpayment where contractpaymentid = " + hfContractPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCheckNo.Text = clR.GetFields("checkno");
                hfPayeeID.Value = clR.GetFields("payeeid");
                FillPayee();
                rdpProcessDate.SelectedDate = Convert.ToDateTime(clR.GetFields("datepaid"));
                txtpaidAmt.Text = clR.GetFields("paidamt");
            }
        }

        protected void btnAddPayee_Click(object sender, EventArgs e)
        {
            pnlPayeeSearch.Visible = false;
            pnlPayeeModify.Visible = true;

        
        }

        protected void btnCancelPayee_Click(object sender, EventArgs e)
        {
            pnlPayeeSearch.Visible = true;
            pnlPayeeModify.Visible = false;
        }

        protected void btnSavePayee_Click(object sender, EventArgs e)
        {
            pnlPayeeSearch.Visible = true;
            pnlPayeeModify.Visible = false;

            clsDBO clR = new clsDBO();
            string SQL = "select * from contractpayee where contractpayeeid = 0";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("payeeno", txtPayeeNo.Text);
                clR.SetFields("payeename", txtPayeeName.Text);
                clR.AddRow();
                clR.SaveDB();
                rgPayee.Rebind();
            }
        }
    }
}