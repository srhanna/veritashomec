﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractNotes.ascx.cs" Inherits="VeritasHomeC.ContractNotes" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Panel runat="server" ID="pnlNote">
        <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add" CssClass="button1" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <telerik:RadGrid ID="rgNote" OnSelectedIndexChanged="rgNote_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowSorting="true" DataSourceID="dsNotes" AllowPaging="true">
        <ClientSettings EnablePostBackOnRowClick="true">
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
        <MasterTableView AutoGenerateColumns="false" DataKeyNames="ContractNoteID" PageSize="10" ShowFooter="true">
            <Columns>
                <telerik:GridBoundColumn DataField="ContractNoteID" UniqueName="ContractsNoteID" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Note" UniqueName="Note" HeaderText="Note"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CreBy" UniqueName="CreBy" HeaderText="Created By"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" DataFormatString="{0:M/d/yyyy}" HeaderText="Create Date"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ModBy" UniqueName="ModBy" HeaderText="Modified By"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ModDate" UniqueName="ModDate" DataFormatString="{0:M/d/yyyy}" HeaderText="Modified Date"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:SqlDataSource ID="dsNotes" ProviderName="System.Data.SqlClient" 
        SelectCommand="select contractnoteid, note, cre.fname + ' ' + cre.lname as creby, credate, mod.fname + ' ' + mod.lname as modby, moddate  from contractnote cn 
        left join userinfo cre on cre.UserID = cn.CreBy
        left join userinfo mod on mod.UserID = cn.ModBy
        where contractid = @ContractID " 
        runat="server">
        <SelectParameters>
            <asp:ControlParameter ControlID="hfContractID" Name="ContractID" PropertyName="Value" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Panel>
<asp:Panel runat="server" ID="pnlChange">
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtNote" TextMode="MultiLine" Width="1000" Height="200"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Claim Always:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:CheckBox ID="chkClaimAlways" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Create By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCreBy" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Create Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtCreDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Modified By:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtModBy" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Modified Date:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtModDate" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button runat="server" ID="btnCancel" OnClick="btnCancel_Click" Text="Cancel" CssClass="button1"></asp:Button>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Click" Text="Save" CssClass="button2" style="border-radius:4px;"></asp:Button>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:HiddenField ID="hfUserID" runat="server" />
<asp:HiddenField ID="hfContractID" runat="server" />
<asp:HiddenField ID="hfID" runat="server" />
<asp:HiddenField ID="hfContractNoteID" runat="server" />
