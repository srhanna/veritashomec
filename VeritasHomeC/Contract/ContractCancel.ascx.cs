﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractCancel : System.Web.UI.UserControl
    {
        private double dCancelFee, dCancelPer, dTermFactor, dCancelFactor, dCancelAmt, dDealerAmt, dAdminAmt, dClaimAmt;
        private bool bOOB, bRegulatedState;


        protected void Page_Load(object sender, EventArgs e)
        {
            dsPayType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                hfContractID.Value = Request.QueryString["contractid"];
                tcSaveCancel.Visible = false;
                CalcContractStatus();
                rbDealer.Checked = true;
                pnlOther.Visible = false;
                if (CheckDealerOOB())
                {
                    rbOther.Checked = true;
                    pnlOther.Visible = true;
                }
                pnlPaylink.Visible = false;
                ShowActivateCancel();
                FillCancel();

                if (!CheckCancel())
                    CheckMoxy();
                else
                    trMoxyCancel.Visible = false;
            }
        }

        public void FillCancellation()
        {
            FillCancel();

            if (!CheckCancel())
                CheckMoxy();
            else
                trMoxyCancel.Visible = false;

            if (!IsPostBack)
            {
                ReadOnlyButtons();
                lblError.Visible = false;
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clsDBO clR = new clsDBO();
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "true;")
                {
                    btnActivate.Enabled = false;
                    btnCancel.Enabled = false;
                    btnInvalid.Enabled = false;
                    btnMoxy.Enabled = false;
                    btnPay.Enabled = false;
                    btnQuote.Enabled = false;
                    btnUnable.Enabled = false;
                    btnSaveCancel.Enabled = false;
                    btnUpdateNote.Enabled = false;
                }
            }
        }

        private bool CheckCancel()
        {
            string SQL = "select * from contract where contractid = " + hfContractID.Value + " " + "and status = 'Cancelled' ";
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
                return true;

            return false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private bool CheckDealerOOB()
        {
            string SQL = "select c.dealerid from dealer d inner join contract c on d.dealerid = c.dealerid ";
            SQL = SQL + "where c.contractid = " + hfContractID.Value + " " + "and dealerstatusid = 5 ";
            clsDBO clD = new clsDBO();
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
                return true;

            return false;
        }

        private void CheckMoxy()
        {
            string SQL = "select * from contractcancelmoxy where contractid = " + hfContractID.Value + " ";
            SQL = SQL + "and processedcancel = 0 and cancelstatus = 'cancelled' ";
            clsDBO clCCM = new clsDBO();
            clCCM.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCCM.RowCount() > 0)
                trMoxyCancel.Visible = true;
            else
                trMoxyCancel.Visible = false;
        }

        private void ShowActivateCancel()
        {
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clsDBO clC = new clsDBO();
            btnActivate.Visible = false;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("Status") == "Cancelled" || clC.GetFields("status") == "Cancelled Before Paid" || clC.GetFields("status") == "Expired")
                {
                    if (CheckActivateContract())
                        btnActivate.Visible = true;
                }
            }
        }

        private bool CheckActivateContract()
        {
            string SQL = "select activatecontract from usersecurityinfo where userid = " + hfUserID.Value;
            clsDBO clUSI = new clsDBO();
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                return Convert.ToBoolean(clUSI.GetFields("activatecontract"));
            }

            return false;
        }

        private void CalcContractStatus()
        {
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clsDBO clC = new clsDBO();
            tcCancel.Visible = true;
            tcQuote.Visible = true;
            tcRequest.Visible = true;
            tcInvalid.Visible = true;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("status") == "Paid") 
                {
                    pnlCalcCancellation.Visible = true;
                    trCancelCalc.Visible = true;
                    tcInvalid.Visible = false;
                }
                else
                    tcInvalid.Visible = true;

                if (clC.GetFields("status") == "Cancelled") 
                {
                    tcCancel.Visible = false;
                    tcQuote.Visible = false;
                    tcRequest.Visible = false;
                    tcInvalid.Visible = false;
                }

                if (clC.GetFields("state").ToLower() == "fl") {
                    trFL.Visible = true;
                    if (clC.GetFields("lienholder").ToLower() == "paylink")
                    {
                        rbFLCustomer.Checked = false;
                        rbFLAdmin.Checked = true;
                    }
                    else
                    {
                        rbFLCustomer.Checked = true;
                        rbFLAdmin.Checked = false;
                    }
                }  
                else
                    trFL.Visible = false;

                CheckSecurity();
                FillCancel();
                GetLienholder2();
                GetDealerCost();
            }
        }

        private void CheckSecurity()
        {
            clsDBO clC = new clsDBO();
            clsDBO clUSI = new clsDBO();
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clC.RowCount() > 0)
                clC.GetRow();
            else
                return;

            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            tcCancel.Visible = true;
            tcQuote.Visible = true;
            tcRequest.Visible = true;
            tcInvalid.Visible = true;
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (Convert.ToInt64(clC.GetFields("programid")) >= 47)
                {
                    if (Convert.ToInt64(clC.GetFields("programid")) > 61)
                    {
                        tcCancel.Visible = false;
                        tcQuote.Visible = false;
                        tcRequest.Visible = false;
                        tcInvalid.Visible = false;
                        if (Convert.ToBoolean(clUSI.GetFields("CancelGap")))
                        {
                            tcCancel.Visible = true;
                            tcQuote.Visible = false;
                            tcRequest.Visible = false;
                            tcInvalid.Visible = false;
                            txtRequestDate.ReadOnly = false;
                            txtTermPercent.ReadOnly = false;
                            txtCancelPercent.ReadOnly = false;
                            txtTermPercent.ReadOnly = false;
                            txtCancelStatus.ReadOnly = false;
                            txtClaimAmt.ReadOnly = false;
                            txtCancelFee.ReadOnly = false;
                            txtLienholder.ReadOnly = false;
                            txtQuoteDate.ReadOnly = false;
                            txtCancelDate.ReadOnly = false;
                            txtCustomerRefund.ReadOnly = false;
                            txtFromAdmin.ReadOnly = false;
                            txtFromDealer.ReadOnly = false;
                            return;
                        }
                    }
                }

                if (!Convert.ToBoolean(clUSI.GetFields("cancellation")))
                {
                    tcCancel.Visible = false;
                    tcQuote.Visible = false;
                    tcRequest.Visible = false;
                    tcInvalid.Visible = false;
                    tcSaveCancel.Visible = false;
                }
                else
                {
                    if (Convert.ToBoolean(clUSI.GetFields("cancelmodification"))) {
                        if (clC.GetFields("status") == "Cancelled")
                        {
                            tcCancel.Visible = false;
                            tcQuote.Visible = false;
                            tcRequest.Visible = false;
                            tcInvalid.Visible = false;
                            txtRequestDate.ReadOnly = false;
                            txtTermPercent.ReadOnly = false;
                            txtCancelPercent.ReadOnly = false;
                            txtTermPercent.ReadOnly = false;
                            txtCancelStatus.ReadOnly = false;
                            txtClaimAmt.ReadOnly = false;
                            txtCancelFee.ReadOnly = false;
                            txtLienholder.ReadOnly = false;
                            txtQuoteDate.ReadOnly = false;
                            txtCancelDate.ReadOnly = false;
                            txtCustomerRefund.ReadOnly = false;
                            txtFromAdmin.ReadOnly = false;
                            txtFromDealer.ReadOnly = false;
                            tcSaveCancel.Visible = true;
                        }
                    }
                    else
                    {
                        if (!Convert.ToBoolean(clUSI.GetFields("cancellation")))
                        {
                            tcCancel.Visible = false;
                            tcQuote.Visible = false;
                            tcRequest.Visible = false;
                            tcInvalid.Visible = false;
                        }
                    }
                }

                if (Convert.ToBoolean(clUSI.GetFields("cancelpay")))
                    tcPayment.Visible = true;
                else
                    tcPayment.Visible = false;
            }
        }

      

        private void FillCancel()
        {
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clsDBO clCC = new clsDBO();
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() > 0)
            {
                clCC.GetRow();
                txtCancelStatus.Text = clCC.GetFields("cancelstatus");
                if (clCC.GetFields("termfactor").Length > 0)
                    txtTermPercent.Text = Convert.ToDouble(clCC.GetFields("termfactor")).ToString("##.000%");

                txtCancelPercent.Text = Convert.ToDouble(clCC.GetFields("cancelfactor")).ToString("##.000%");

                if (clCC.GetFields("canceleffdate").Length > 0)
                    rdpProcessDate.SelectedDate = Convert.ToDateTime(clCC.GetFields("canceleffdate"));

                if (clCC.GetFields("quotedate").Length > 0)
                    txtQuoteDate.Text = Convert.ToDateTime(clCC.GetFields("quotedate")).ToString("d");

                if (clCC.GetFields("requestdate").Length > 0)
                    txtRequestDate.Text = Convert.ToDateTime(clCC.GetFields("requestdate")).ToString("d");

                if (clCC.GetFields("canceldate").Length > 0)
                    txtCancelDate.Text = Convert.ToDateTime(clCC.GetFields("Canceldate")).ToString("d");

                cboPayType.SelectedValue = "2";
                txtCancelFee.Text = Convert.ToDouble(clCC.GetFields("cancelfeeamt")).ToString("#,##0.00");
                txtClaimAmt.Text = Convert.ToDouble(clCC.GetFields("ClaimAmt")).ToString("#,##0.00");
                double dCustRefund;
                clsDBO clC = new clsDBO();
                SQL = "select * from contract ";
                SQL = SQL + "where contractid = " + clCC.GetFields("contractid");
                clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clC.RowCount() > 0) 
                {
                    clC.GetRow();
                    CheckRegulatedState(clC.GetFields("state"));
                    GetDealerInfo(Convert.ToInt64(clC.GetFields("dealerid")));
                }

                if (bOOB)
                {
                    if (bRegulatedState)
                    {
                        //if (Convert.ToDouble(clCC.GetFields("dealeramt")) > Convert.ToDouble(clCC.GetFields("adminamt")))
                        dCustRefund = Convert.ToDouble(clCC.GetFields("adminamt"));
                        dAdminAmt = Convert.ToDouble(clCC.GetFields("adminamt"));
                    }
                    else
                    {
                        dCustRefund = Convert.ToDouble(clCC.GetFields("adminamt"));
                        dAdminAmt = Convert.ToDouble(clCC.GetFields("adminamt"));
                    }                    
                }
                else 
                {
                    dCustRefund = Convert.ToDouble(clCC.GetFields("dealeramt")) + Convert.ToDouble(clCC.GetFields("adminamt"));
                    dAdminAmt = Convert.ToDouble(clCC.GetFields("adminamt"));
                }


                txtCustomerRefund.Text = dCustRefund.ToString("#,##0.00");
                txtFromAdmin.Text = dAdminAmt.ToString("#,##0.00");
                txtFromDealer.Text = Convert.ToDouble(clCC.GetFields("dealeramt")).ToString("#,##0.00");
                txtCancelStatus.Text = clCC.GetFields("cancelstatus");
                txtPaymentAmt.Text = clCC.GetFields("adminamt");
                if (clCC.GetFields("cancelpaiddate").Length > 0)
                    txtPayDate.SelectedDate = Convert.ToDateTime(clCC.GetFields("cancelpaiddate"));

                if (clCC.GetFields("cancelpaidamt").Length > 0)
                    txtPaymentAmt.Text = clCC.GetFields("cancelpaidamt");

                txtPaymentInfo.Text = clCC.GetFields("paymentinfo");

                if (clCC.GetFields("paytypeid").Length > 0)
                    cboPayType.SelectedValue = clCC.GetFields("paytypeid");

                txtNote.Text = clCC.GetFields("note");
                rbOther.Checked = Convert.ToBoolean(clCC.GetFields("Other"));
                rbCustomer.Checked = Convert.ToBoolean(clCC.GetFields("customer"));
                rbDealer.Checked = Convert.ToBoolean(clCC.GetFields("dealer"));
                txtGrossDealer.Text = Convert.ToDouble(clCC.GetFields("grossdealer")).ToString("#,##0.00");
                txtGrossCustomer.Text = Convert.ToDouble(clCC.GetFields("grosscustomer")).ToString("#,##0.00");
                txtGrossAdmin.Text = Convert.ToDouble(clCC.GetFields("grossadmin")).ToString("#,##0.00");
                chkDealerCancel.Checked = Convert.ToBoolean(clCC.GetFields("DealerCancel"));
                if (rbOther.Checked) 
                {
                    pnlOther.Visible = true;
                    VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();
                    clP.PayeeID = Convert.ToInt64(clCC.GetFields("payeeid"));
                    clP.OpenPayeeInfo();
                    txtAddr1.Text = clP.Addr1;
                    txtAddr2.Text = clP.Addr2;
                    txtCity.Text = clP.City;
                    cboState.SelectedValue = clP.State;
                    txtPhone.Text = clP.Phone;
                    txtZip.Text = clP.Zip;
                    txtCompanyName.Text = clP.CompanyName;
                    txtFName.Text = clP.FName;
                    txtLName.Text = clP.LName;
                }

                hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" + hfContractID.Value;
            }
        }

        private bool CheckCancel100()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("cancel100")))
                    return true;
            }

            return false;
        }

        protected void rbOther_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOther.Checked)
            {
                pnlOther.Visible = true;
                GetLienholder();
                GetDealerCost();
            }
            else
            {
                pnlOther.Visible = false;
                txtCompanyName.Text = "";
            }
        }

        protected void btnQuote_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clCC = new clsDBO();
            if (!CheckCancel100())
            {
                if (CheckClaim())
                {
                    lblError.Visible = true;
                    lblError.Text = "Claim is Open or Paid after the Cancel Effective Date.";
                    return;
                }
                else
                    lblError.Visible = false;
            }
            else
                lblError.Visible = false;

            CalcCancellation(true);
            GetLienholder2();
            GetDealerCost();
            SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("cancelstatus", "Quote");
            clCC.SetFields("termfactor", dTermFactor.ToString());
            clCC.SetFields("cancelfactor", dCancelFactor.ToString());
            clCC.SetFields("canceleffdate", rdpProcessDate.SelectedDate.ToString());
            clCC.SetFields("quotedate", DateTime.Today.ToString());
            clCC.SetFields("adminamt", dAdminAmt.ToString());
            clCC.SetFields("claimamt", txtClaimAmt.Text);
            clCC.SetFields("dealeramt", dDealerAmt.ToString());
            //clCC.SetFields("CancelPaidAmt".ToString("dCancelAmt");
            clCC.SetFields("cancelfeeamt", dCancelFee.ToString());
            clCC.SetFields("dealer", rbDealer.Checked.ToString());
            clCC.SetFields("customer", rbCustomer.Checked.ToString());
            clCC.SetFields("Other", rbOther.Checked.ToString());
            txtQuoteDate.Text = rdpProcessDate.SelectedDate.ToString();
            clCC.SetFields("cancelinfo", hfCancelInfo.Value);
            clCC.SetFields("Note", txtNote.Text);
            clCC.SetFields("GrossCustomer", txtGrossCustomer.Text);
            clCC.SetFields("grossadmin", txtGrossAdmin.Text);
            clCC.SetFields("grossdealer", txtGrossDealer.Text);
            clCC.SetFields("DealerCancel", chkDealerCancel.Checked.ToString());

            VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();

            if (rbCustomer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddCustomerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbDealer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddDealerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbOther.Checked == true)
            {
                clP.FName = txtFName.Text;
                clP.CompanyName = txtCompanyName.Text;
                clP.LName = txtLName.Text;
                clP.Addr1 = txtAddr1.Text;
                clP.Addr2 = txtAddr2.Text;
                clP.City = txtCity.Text;
                clP.State = cboState.Text;
                clP.Zip = txtZip.Text;
                clP.Phone = txtPhone.Text;
                clP.AddOtherToPayee(hfContractID.Value);
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (clCC.RowCount() == 0)
            {
                clCC.SetFields("credate", DateTime.Today.ToString());
                clCC.SetFields("creby", hfUserID.Value);
            }
            else
            {
                clCC.SetFields("moddate", DateTime.Today.ToString());
                clCC.SetFields("modby", hfUserID.Value);
            }

            clCC.SaveDB();
            hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" + hfContractID.Value;
        }

        private void CalcCancellation(bool xQuote)
        {
            VeritasGlobalToolsV2.clsCancellation clCancel = new VeritasGlobalToolsV2.clsCancellation();
            clCancel.CancelDate = rdpProcessDate.SelectedDate.ToString();
            clCancel.ContractID = Convert.ToInt64(hfContractID.Value);
            clCancel.Quote = xQuote;
            clCancel.CalculateCancellation();
            clCancel.FLAdmin = rbFLAdmin.Checked;
            clCancel.FLCustomer = rbFLCustomer.Checked;
            dTermFactor = clCancel.CancelTermPer;
            dCancelFactor = clCancel.CancelPer;
            dCancelFee = clCancel.CancelFee;
            dAdminAmt = clCancel.AdminAmt;
            dDealerAmt = clCancel.DealerAmt;
            dCancelAmt = clCancel.DealerAmt + clCancel.AdminAmt;

            txtCancelFee.Text = clCancel.CancelFee.ToString("#,##0.00");
            txtCancelPercent.Text = clCancel.CancelPer.ToString("0.###%");
            txtClaimAmt.Text = clCancel.Claims.ToString("#,##0.00");
            txtCustomerRefund.Text = clCancel.CustomerCost.ToString("#,##0.00");
            txtFromAdmin.Text = clCancel.AdminAmt.ToString("#,##0.00");
            txtFromDealer.Text = clCancel.DealerAmt.ToString("#,##0.00");
            txtTermPercent.Text = clCancel.CancelTermPer.ToString("0.###%");
            hfCancelInfo.Value = clCancel.CancelInfo;
            txtGrossCustomer.Text = clCancel.GrossCustomer.ToString("#,##0.00");
            txtGrossDealer.Text = clCancel.GrossDealer.ToString("#,##0.00");
            txtGrossAdmin.Text = clCancel.GrossAdmin.ToString("#,##0.00");
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clsDBO clCC = new clsDBO();
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() > 0)
            {
                clCC.GetRow();
                clCC.SetFields("cancelpaiddate", txtPayDate.SelectedDate.ToString());
                clCC.SetFields("cancelpaidamt", txtPaymentAmt.Text);
                clCC.SetFields("PaymentInfo", txtPaymentInfo.Text);
                clCC.SetFields("paytypeID", cboPayType.SelectedValue);
                clCC.SetFields("CancelStatus", "Paid");
                clCC.SaveDB();
            }

            SQL = "update veritasMoxy.dbo.moxycontractcancel ";
            SQL = SQL + "set refundstatus = 'Refund Paid' ";
            SQL = SQL + "from VeritasMoxy.dbo.MoxyContractCancel mcc ";
            SQL = SQL + "inner join VeritasMoxy.dbo.MoxyContract mc ";
            SQL = SQL + "on mc.DealID = mcc.DealID ";
            SQL = SQL + "where contractno = '" + GetContractNo() + "'";
            clCC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void CalcCancellation2()
        {
            VeritasGlobalToolsV2.clsCancellation clCancel = new VeritasGlobalToolsV2.clsCancellation();
            clCancel.CancelDate = rdpProcessDate.SelectedDate.ToString();
            clCancel.ContractID = Convert.ToInt64(hfContractID.Value);
            clCancel.CalculateCancellation();

            dTermFactor = clCancel.CancelTermPer;
            dCancelFactor = clCancel.CancelPer;
            dCancelFee = clCancel.CancelFee;
            dAdminAmt = clCancel.AdminAmt;
            dDealerAmt = clCancel.DealerAmt;
            dCancelAmt = clCancel.DealerAmt + clCancel.AdminAmt;

            txtCancelFee.Text = clCancel.CancelFee.ToString("#,##0.00");
            txtCancelPercent.Text = clCancel.CancelPer.ToString("0.###%");
            txtClaimAmt.Text = clCancel.Claims.ToString("#,##0.00");
            txtCustomerRefund.Text = clCancel.DealerAmt + clCancel.AdminAmt.ToString("#,##0.00");
            txtFromAdmin.Text = clCancel.AdminAmt.ToString("#,##0.00");
            txtFromDealer.Text = clCancel.DealerAmt.ToString("#,##0.00");
            txtTermPercent.Text = clCancel.CancelTermPer.ToString("0.###%");
            txtGrossAdmin.Text = clCancel.GrossAdmin.ToString("#,##0.00");
            txtGrossCustomer.Text = clCancel.GrossCustomer.ToString("#,##0.00");
            txtGrossDealer.Text = clCancel.GrossDealer.ToString("#,##0.00");
        }

        protected void btnUpdateNote_Click(object sender, EventArgs e)
        {
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clsDBO clCC = new clsDBO();
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() > 0)
            {
                clCC.GetRow();
                clCC.SetFields("note", txtNote.Text);
                clCC.SaveDB();
            }
        }

        private void GetDealerInfo(long xDealerID)
        {
            string SQL = "select * from dealer where dealerid = " + xDealerID;
            clsDBO clD = new clsDBO();
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                if (clD.GetFields("dealerstatusid") == "5")
                    bOOB = true;
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlCalcCancellation.Visible = true;
            pnlNote.Visible = true;
            if (rbOther.Checked)
                pnlOther.Visible = true;

            pnlPayment.Visible = true;
            pnlPaylink.Visible = false;
            Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        protected void rbDealer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbDealer.Checked)
                pnlOther.Visible = false;
        }

        protected void rbCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCustomer.Checked)
                pnlOther.Visible = false;
        }

        protected void btnInvalid_Click(object sender, EventArgs e)
        {
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clsDBO clCC = new clsDBO();
            clsDBO clC = new clsDBO();
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            GetLienholder2();
            GetDealerCost();
            dTermFactor = 1;
            dClaimAmt = 0;
            dCancelPer = 1;

            SQL = "select * From contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0) {
                clC.GetRow();
                if (clC.GetFields("moxydealercost").Length > 0) {
                    dAdminAmt = Convert.ToDouble(clC.GetFields("moxydealercost"));
                    dDealerAmt = Convert.ToDouble(clC.GetFields("customercost")) - Convert.ToDouble(clC.GetFields("moxydealercost"));
                }
                else 
                {
                    dAdminAmt = 0;
                    dDealerAmt = 0;
                }

            }
            else 
            {
                dAdminAmt = 0;
                dDealerAmt = 0;
            }

            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("cancelstatus","Invalid");
            clCC.SetFields("termfactor", dTermFactor.ToString());
            clCC.SetFields("claimamt", dClaimAmt.ToString());
            clCC.SetFields("cancelfactor", dCancelPer.ToString());
            clCC.SetFields("canceleffdate", rdpProcessDate.SelectedDate.ToString());
            clCC.SetFields("canceldate", DateTime.Today.ToString());
            clCC.SetFields("adminamt", dAdminAmt.ToString());
            clCC.SetFields("dealeramt", dDealerAmt.ToString());
            clCC.SetFields("dealer", rbDealer.Checked.ToString());
            clCC.SetFields("customer", rbDealer.Checked.ToString());
            clCC.SetFields("Other", rbOther.Checked.ToString());
            clCC.SetFields("Note", txtNote.Text);
            clCC.SetFields("DealerCancel", chkDealerCancel.Checked.ToString());

            VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();

            if (rbCustomer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddCustomerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbDealer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddDealerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbOther.Checked == true)
            {
                clP.FName = txtFName.Text;
                clP.CompanyName = txtCompanyName.Text;
                clP.LName = txtLName.Text;
                clP.Addr1 = txtAddr1.Text;
                clP.Addr2 = txtAddr2.Text;
                clP.City = txtCity.Text;
                clP.State = cboState.Text;
                clP.Zip = txtZip.Text;
                clP.Phone = txtPhone.Text;
                clP.AddOtherToPayee(hfContractID.Value);
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            clCC.SetFields("cancelfeeamt", dCancelFee.ToString());
            if (clCC.RowCount() == 0)
            {
                clCC.SetFields("credate", DateTime.Today.ToString());
                clCC.SetFields("creby", hfUserID.Value);
                clCC.AddRow();
            }
            else
            {
                clCC.SetFields("moddate", DateTime.Today.ToString());
                clCC.SetFields("modby", hfUserID.Value);
            }

            clCC.SaveDB();
            SQL = "select * from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0) 
            {
                clC.GetRow();
                clC.SetFields("status", "Invalid");
                clC.SaveDB();
            }

            hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" + hfContractID.Value;
            CheckPayLink();
        }

        protected void btnSaveCancel_Click(object sender, EventArgs e)
        {
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clsDBO clCC = new clsDBO();
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            GetLienholder2();
            GetDealerCost();
            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("cancelstatus", "Cancelled");
            clCC.SetFields("termfactor", (Convert.ToDouble(txtTermPercent.Text.Replace("%", "")) / 100).ToString());
            clCC.SetFields("claimamt", dClaimAmt.ToString());
            clCC.SetFields("cancelfactor", (Convert.ToDouble(txtCancelPercent.Text.Replace("%", "")) / 100).ToString());
            clCC.SetFields("canceleffdate", rdpProcessDate.SelectedDate.ToString());
            clCC.SetFields("canceldate", DateTime.Today.ToString());
            clCC.SetFields("adminamt", txtFromAdmin.Text);
            clCC.SetFields("dealeramt", txtFromDealer.Text);
            clCC.SetFields("dealer", rbDealer.Checked.ToString());
            clCC.SetFields("customer", rbDealer.Checked.ToString());
            clCC.SetFields("Other", rbOther.Checked.ToString());
            clCC.SetFields("Note", txtNote.Text);
            clCC.SetFields("DealerCancel", chkDealerCancel.Checked.ToString());

            VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();

            if (rbCustomer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddCustomerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbDealer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddDealerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbOther.Checked == true)
            {
                clP.FName = txtFName.Text;
                clP.CompanyName = txtCompanyName.Text;
                clP.LName = txtLName.Text;
                clP.Addr1 = txtAddr1.Text;
                clP.Addr2 = txtAddr2.Text;
                clP.City = txtCity.Text;
                clP.State = cboState.Text;
                clP.Zip = txtZip.Text;
                clP.Phone = txtPhone.Text;
                clP.AddOtherToPayee(hfContractID.Value);
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            clCC.SetFields("cancelfeeamt", txtCancelFee.Text);
            if (clCC.RowCount() == 0)
            {
                clCC.SetFields("credate", DateTime.Today.ToString());
                clCC.SetFields("creby", hfUserID.Value);
                clCC.AddRow();
            }
            else
            {
                clCC.SetFields("moddate", DateTime.Today.ToString());
                clCC.SetFields("modby", hfUserID.Value);
            }

            clCC.SaveDB();
            clsDBO clC = new clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clC.SetFields("status", "Cancelled");
                clC.SaveDB();
            }

            hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" + hfContractID.Value;
            CheckPayLink();
        }

        private void CheckRegulatedState(string xState)
        {
            string SQL = "select * from cancelstaterules where state = '" + xState + "' ";
            clsDBO clR = new clsDBO();
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("regulated")))
                    bRegulatedState = true;
            }
        }

        

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clCC = new clsDBO();
            if (!CheckCancel100())
            {
                if (CheckClaim())
                {
                    lblError.Visible = true;
                    lblError.Text = "Claim Is Open or Paid after the Cancel Effective Date";
                    return;
                }
                else
                    lblError.Visible = false;
            }
            else
                lblError.Visible = false;

            CalcCancellation(true);
            SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            GetLienholder2();
            GetDealerCost();
            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("cancelstatus", "Cancelled");
            clCC.SetFields("termfactor", (Convert.ToDouble(txtTermPercent.Text.Replace("%", "")) / 100).ToString());
            clCC.SetFields("claimamt", txtClaimAmt.Text);
            clCC.SetFields("cancelfactor", (Convert.ToDouble(txtCancelPercent.Text.Replace("%", "")) / 100).ToString());
            clCC.SetFields("canceleffdate", rdpProcessDate.SelectedDate.ToString());
            clCC.SetFields("canceldate", DateTime.Today.ToString());
            clCC.SetFields("adminamt", txtFromAdmin.Text);
            clCC.SetFields("dealeramt", txtFromDealer.Text);
            clCC.SetFields("dealer", rbDealer.Checked.ToString());
            clCC.SetFields("customer", rbCustomer.Checked.ToString());
            clCC.SetFields("Other", rbOther.Checked.ToString());
            clCC.SetFields("GrossCustomer", txtGrossCustomer.Text);
            clCC.SetFields("grossadmin", txtGrossAdmin.Text);
            clCC.SetFields("grossdealer", txtGrossDealer.Text);
            clCC.SetFields("DealerCancel", chkDealerCancel.Checked.ToString());

            VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();

            if (rbCustomer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddCustomerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbDealer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddDealerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbOther.Checked == true)
            {
                clP.FName = txtFName.Text;
                clP.CompanyName = txtCompanyName.Text;
                clP.LName = txtLName.Text;
                clP.Addr1 = txtAddr1.Text;
                clP.Addr2 = txtAddr2.Text;
                clP.City = txtCity.Text;
                clP.State = cboState.Text;
                clP.Zip = txtZip.Text;
                clP.Phone = txtPhone.Text;
                clP.AddOtherToPayee(hfContractID.Value);
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            clCC.SetFields("cancelfeeamt", txtCancelFee.Text);
            clCC.SetFields("cancelinfo", hfCancelInfo.Value);
            if (clCC.RowCount() == 0)
            {
                clCC.SetFields("credate", DateTime.Today.ToString());
                clCC.SetFields("creby", hfUserID.Value.ToString());
                clCC.AddRow();
            }
            else
            {
                clCC.SetFields("moddate", DateTime.Today.ToString());
                clCC.SetFields("modby", hfUserID.Value);
            }

            clCC.SaveDB();
            clsDBO clC = new clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("datepaid").Length > 0)
                    clC.SetFields("status", "Cancelled");
                else
                    clC.SetFields("status", "Cancelled Before Paid");

                clC.SaveDB();
            }

            hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" + hfContractID.Value;
            
            ProcessContractAmt();
            CheckPayLink();
        }

        protected void btnMoxy_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/MoxyCancellation.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        private void ProcessContractAmt()
        {
            string SQL = "select * from contractamt where contractid = " + hfContractID.Value;
            clsDBO clCA = new clsDBO();
            clsDBO clCA2 = new clsDBO();
            clCA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCA.RowCount() > 0)
            {
                for (int i=0; i <= clCA.RowCount() - 1; i++)
                {
                    clCA.GetRowNo(i);
                    SQL = "select * from contractamt where contractamtid = " + clCA.GetFields("contractamtid");
                    clCA2.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCA2.RowCount() > 0)
                    {
                        clCA2.GetRow();
                        clCA2.SetFields("cancelamt", (Convert.ToDouble(clCA2.GetFields("amt")) * Convert.ToDouble(txtCancelPercent.Text.Replace("%", "")) / 100).ToString());
                        clCA2.SaveDB();
                    }
                }
            }
        }

        private void GetDealerCost()
        {
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clsDBO clD = new clsDBO();
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerCost.Text = Convert.ToDouble(clD.GetFields("moxydealercost")).ToString("#,##0.00");
            }
        }

        private void GetLienholder()
        {
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtCompanyName.Text = clC.GetFields("lienholder");
                txtLienholder.Text = clC.GetFields("lienholder");
            }
        }

        private void GetLienholder2()
        {
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtLienholder.Text = clC.GetFields("lienholder");
            }
        }

        protected void btnUnable_Click(object sender, EventArgs e)
        {
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clsDBO clCC = new clsDBO();
            CalcCancellation(true);
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("cancelstatus", "Unable To Process");
            clCC.SetFields("termfactor", dTermFactor.ToString());
            clCC.SetFields("claimamt", dClaimAmt.ToString());
            clCC.SetFields("cancelfactor", dCancelPer.ToString());
            clCC.SetFields("canceleffdate", rdpProcessDate.SelectedDate.ToString());
            clCC.SetFields("Requestdate", DateTime.Today.ToString());
            clCC.SetFields("adminamt", dAdminAmt.ToString());
            clCC.SetFields("dealeramt", dDealerAmt.ToString());
            clCC.SetFields("dealer", rbDealer.Checked.ToString());
            clCC.SetFields("customer", rbCustomer.Checked.ToString());
            clCC.SetFields("Other", rbOther.Checked.ToString());
            clCC.SetFields("DealerCancel", chkDealerCancel.Checked.ToString());

            txtCancelStatus.Text = "Unable to Process";
            VeritasGlobalToolsV2.clsPayee clP = new VeritasGlobalToolsV2.clsPayee();
            clCC.SetFields("Note", txtNote.Text);

            if (rbCustomer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddCustomerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbDealer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddDealerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbOther.Checked == true)
            {
                clP.FName = txtFName.Text;
                clP.CompanyName = txtCompanyName.Text;
                clP.LName = txtLName.Text;
                clP.Addr1 = txtAddr1.Text;
                clP.Addr2 = txtAddr2.Text;
                clP.City = txtCity.Text;
                clP.State = cboState.Text;
                clP.Zip = txtZip.Text;
                clP.Phone = txtPhone.Text;
                clP.AddOtherToPayee(hfContractID.Value);

                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            clCC.SetFields("cancelfeeamt", dCancelFee.ToString());

            if (clCC.RowCount() == 0)
            {
                clCC.SetFields("credate", DateTime.Today.ToString());
                clCC.SetFields("creby", hfUserID.Value);
                clCC.AddRow();
            }
            else
            {
                clCC.SetFields("moddate", DateTime.Today.ToString());
                clCC.SetFields("modby", hfUserID.Value);
            }

            clCC.SaveDB();
        }

        private string GetContractNo()
        {
            string SQL = "select contractno from contract where contractno = '" + hfContractID.Value + "' ";
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                return clC.GetFields("contractno");
            }

            return "";
        }

        private void CheckPayLink()
        {
            string PayLinkText = "The contract has a Lien Holder of Mepco or Paylink." 
                + "Please supply Mepco or Paylink with required information to cancel contract."
                + "if (you have any questions contact your supervisor.";
            if (txtLienholder.Text.ToLower() == "mepco" || txtLienholder.Text.ToLower() == "paylink")
            {
                pnlCalcCancellation.Visible = false;
                pnlNote.Visible = false;
                pnlOther.Visible = false;
                pnlPayment.Visible = false;
                pnlPaylink.Visible = true;
                txtPaylink.Text = PayLinkText;
            }
            else
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        protected void btnActivate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clCC = new clsDBO();
            clsDBO clR = new clsDBO();
            long lDealID;
            string sDBType;
            var clMD = new VeritasGlobalToolsV2.clsMoxyDealID();

            SQL = "select status, effdate, expdate from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("status") == "Cancelled")
                {
                    SQL = "update contract ";
                    SQL = SQL + "set status = 'Paid' ";
                    SQL = SQL + "where contractid = " + hfContractID.Value;
                    clCC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                    SQL = "update contractcancel ";
                    SQL = SQL + "set cancelstatus = 'Quote', ";
                    SQL = SQL + "canceldate = null ";
                    SQL = SQL + "where contractid = " + hfContractID.Value;
                    clCC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                    SQL = "select * from contract ";
                    SQL = SQL + "where contractid = " + hfContractID.Value;
                    clCC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings["connstring"]);
                    if (clCC.RowCount() > 0)
                    {
                        clCC.GetRow();
                        if (clCC.GetFields("moxydealid").Length > 0)
                            lDealID = Convert.ToInt64(clCC.GetFields("moxydealid"));
                        else
                            lDealID = 0;
                    }
                    else
                        lDealID = 0;

                    clMD.ContractID = Convert.ToInt64(hfContractID.Value);
                    clMD.GetMoxyDealID();
                    sDBType = clMD.DBType;
                }

                if (clR.GetFields("status") == "Cancelled Before Paid")
                {
                    SQL = "update contract set status = 'Pending' where contractid = " + hfContractID.Value;
                    clCC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                }

                if (clR.GetFields("status") == "Expired")
                {
                    if (Convert.ToDateTime(clR.GetFields("effdate")) < DateTime.Today)
                    {
                        if (Convert.ToDateTime(clR.GetFields("expdate")) > DateTime.Today)
                        {
                            SQL = "update contract set status = 'Paid' where contract id " + hfContractID.Value;
                            clCC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                        }
                    }
                }
            }

            Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        private bool CheckClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (rdpProcessDate.SelectedDate == null)
                return false;

            SQL = "select * from claim ";
            SQL = SQL + "where contractid = " + hfContractID.Value + " ";
            SQL = SQL + "and credate > '" + rdpProcessDate.SelectedDate + "' ";
            SQL = SQL + "and (status = 'Open' ";
            SQL = SQL + "or status = 'Paid') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }









    }
}