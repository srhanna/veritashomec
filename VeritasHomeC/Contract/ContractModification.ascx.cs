﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class ContractModification : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            dsLienholderSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsTermMonthSelect.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            LienholderSelect.Items.Insert(0, "None");

            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                ReadOnlyButtons();
                FillHistory();
                GetPlanType(hfContractID.Value);
                DetermineSurchargeVisibility();


                GetLienholder();
                PlanTypeSelect.SelectedText = hfPlanType.Value;
                TermMonthSelect.SelectedText = hfTermMonth.Value;
            }
        }

        private void DetermineSurchargeVisibility()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select surchargeid, surcharge from surcharge where not surchargeid in (select surchargeid from contractsurcharge where contractid = " + hfContractID.Value + ")";
            chkSurcharges.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            chkSurcharges.DataBind();
        }

        private void GetLienholder()
        {
            string SQL = "select * from contract where contractid = '" + hfContractID.Value + "' ";
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                LienholderSelect.SelectedValue = clC.GetFields("lienholder");
            }
        }

        private void GetPlanType(string contractid)
        {
            string SQL = "select p.plantypeid, p.plantype from contract c" +
                " inner join plantype p on p.plantypeid = c.plantypeid where c.contractid = " + contractid;
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfPlanType.Value = clC.GetFields("plantype");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");
            }

            GetTermMonth();
            FillPlanTypeDropDown();
        }

        private void GetTermMonth()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfTermMonth.Value = clR.GetFields("TermMonth");
            }
        }

        private void FillPlanTypeDropDown()
        {
            clsDBO clP = new clsDBO();
            string SQL;
            if (hfPlanTypeID.Value == "4" || hfPlanTypeID.Value == "8" || hfPlanTypeID.Value == "12")
                SQL = "select plantypeid, plantype from plantype where plantypeid in (4,8,12)";
            else if (hfPlanTypeID.Value == "5" || hfPlanTypeID.Value == "9" || hfPlanTypeID.Value == "13")
                SQL = "select plantypeid, plantype from plantype where plantypeid in (5,9,13)";
            else if (hfPlanTypeID.Value == "6" || hfPlanTypeID.Value == "10" || hfPlanTypeID.Value == "14")
                SQL = "select plantypeid, plantype from plantype where plantypeid in (6,10,14)";
            else if (hfPlanTypeID.Value == "7" || hfPlanTypeID.Value == "11" || hfPlanTypeID.Value == "15")
                SQL = "select plantypeid, plantype from plantype where plantypeid in (7,11,15)";
            else
                return;

            PlanTypeSelect.DataSource = clP.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            PlanTypeSelect.DataBind();

        }

        private void FillHistory()
        {
            clsDBO clH = new clsDBO();
            string SQL;
            SQL = "select ContractHistoryID, ContractID, fieldname, oldvalue, newvalue, credate, username from contracthistory ch " +
                   "inner join UserInfo ui on ui.userid = ch.CreBy ";
            SQL = SQL + "where contractid = " + hfContractID.Value + " ";
            SQL = SQL + "order by credate desc ";
            rgHistory.DataSource = clH.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void ReadOnlyButtons()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (!Convert.ToBoolean(clR.GetFields("contractmodification")))
                    btnUpdateSaleDate.Enabled = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnUpdateSaleDate_Click(object sender, EventArgs e)
        {
            clsDBO clC = new clsDBO();
            string SQL;
            long lDayDiff;
            if (txtNewSaleDate.Text.Length == 0)
                return;

            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lDayDiff = clsFunc.DateDiffDays(Convert.ToDateTime(clC.GetFields("saledate")), Convert.ToDateTime(clC.GetFields("effdate")));
                UpdateHistory("SaleDate", clC.GetFields("saledate"), txtNewSaleDate.Text);
                clC.SetFields("saledate", txtNewSaleDate.Text);
                clC.SetFields("effdate", (Convert.ToDateTime(clC.GetFields("saledate")).AddDays(lDayDiff)).ToString());
                clC.SetFields("expdate", (Convert.ToDateTime(clC.GetFields("effdate")).AddMonths(Convert.ToInt32(clC.GetFields("termmonth")))).ToString());
                clC.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }
        
        private void UpdateHistory(string xFieldName, string xOldValue, string xNewValue)
        {
            clsDBO clC = new clsDBO();
            string SQL = "select * from contracthistory where contracthistoryid = 0 ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clC.NewRow();
            clC.SetFields("contractid", hfContractID.Value);
            clC.SetFields("fieldname", xFieldName);
            clC.SetFields("OldValue", xOldValue);
            clC.SetFields("newvalue", xNewValue);
            clC.SetFields("creby", hfUserID.Value);
            clC.SetFields("credate", DateTime.Today.ToString());
            clC.AddRow();
            clC.SaveDB();
        }

        protected void btnUpdateLienholder_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from contract c where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(LienholderSelect.SelectedValue) || LienholderSelect.SelectedValue == "None")
                {
                    clsDBO clT = new clsDBO();
                    string SQL2 = "update contract set lienholder = NULL where contractid = " + hfContractID.Value;
                    clT.RunSQL(SQL2, ConfigurationManager.AppSettings["connstring"]);

                }
                else
                    clR.SetFields("lienholder", LienholderSelect.SelectedValue);

                clR.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
            }
        }

        protected void PlanTypeSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfPlanType.Value = PlanTypeSelect.SelectedText;
            hfPlanTypeID.Value = PlanTypeSelect.SelectedValue;
        }

        protected void TermMonthSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfTermMonth.Value = TermMonthSelect.SelectedValue;
        }

        protected void btnUpdatePlan_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtDealerCost.Text) || !Information.IsNumeric(txtDealerCost.Text))
            {
                ShowError();
               
                return;              
            }


            clsDBO clR = new clsDBO();
            string SQL;

            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToDouble(txtDealerCost.Text) < (Convert.ToDouble(clR.GetFields("moxydealercost")) - 250)
                    || Convert.ToDouble(txtDealerCost.Text) == Convert.ToDouble(clR.GetFields("moxydealercost")))
                {
                    ShowError();
                    
                    return;
                }

                UpdateHistory("moxydealercost", clR.GetFields("moxydealercost"), txtDealerCost.Text);
                clR.SetFields("moxydealercost", txtDealerCost.Text);
                UpdateHistory("plantypeid", clR.GetFields("plantypeid"), hfPlanTypeID.Value);
                clR.SetFields("plantypeid", hfPlanTypeID.Value);
                if(txtCustomerCost.Text.Length > 0)
                {
                    UpdateHistory("customercost", clR.GetFields("customercost"), txtCustomerCost.Text);
                    clR.SetFields("customercost", txtCustomerCost.Text);
                }              
                UpdateHistory("termmonth", clR.GetFields("termmonth"), hfTermMonth.Value);

                if (clR.GetFields("termmonth") != hfTermMonth.Value)
                    clR.SetFields("expdate", Convert.ToDateTime(clR.GetFields("effdate")).AddMonths(Convert.ToInt32(hfTermMonth.Value)).ToString());

                clR.SetFields("termmonth", hfTermMonth.Value);

                
                clR.SaveDB();
                Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);

            }

        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }


        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void LienholderSelect_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            hfLienholder.Value = LienholderSelect.SelectedValue;
        }

        protected void btnUpdateSurcharge_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;

            if (String.IsNullOrEmpty(txtDealerCost.Text) || !Information.IsNumeric(txtDealerCost.Text))
            {
                
                ShowError();
                return;
            }

            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToDouble(txtDealerCost.Text) < (Convert.ToDouble(clR.GetFields("moxydealercost")) - 250)
                    || Convert.ToDouble(txtDealerCost.Text) == Convert.ToDouble(clR.GetFields("moxydealercost")))
                {
                    
                    ShowError();
                    return;
                }

                UpdateHistory("moxydealercost", clR.GetFields("moxydealercost"), txtDealerCost.Text);
                clR.SetFields("moxydealercost", txtDealerCost.Text);
                if (txtCustomerCost.Text.Length > 0)
                {
                    UpdateHistory("customercost", clR.GetFields("customercost"), txtCustomerCost.Text);
                    clR.SetFields("customercost", txtCustomerCost.Text);
                }
                
                clR.SaveDB();
            }

            foreach(ButtonListItem item in chkSurcharges.Items)
            {
                if (item.Selected)
                {
                    UpdateHistory(item.Text, "False", "True");
                    SQL = "insert into contractsurcharge " +
                        "(contractid, surchargeid, surchargeamt)" +
                        " values (" + hfContractID.Value + ", " + item.Value + ", 0) ";
                    clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                }
            }

            Response.Redirect("contract.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }
    }
}