﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractCost : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillScreen();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillScreen()
        {
            clsDBO clC = new clsDBO();
            string SQL = "select moxydealercost, discountamt, customercost, markup from contract "
             + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("customercost").Length == 0)
                    clC.SetFields("customercost", "0");

                if (clC.GetFields("markup").Length == 0)
                    clC.SetFields("markup", "0");

                if (clC.GetFields("moxydealercost").Length == 0)
                    clC.SetFields("moxydealercost", "0");

                if (clC.GetFields("discountamt").Length == 0)
                    clC.SetFields("discountamt", "0");

                txtCustomerCost.Text = Convert.ToDouble(clC.GetFields("customercost")).ToString("#,##0.00");
                txtMarkup.Text = Convert.ToDouble(clC.GetFields("markup")).ToString("#,##0.00");
                txtDealerCost.Text = Convert.ToDouble(clC.GetFields("moxydealercost")).ToString("#,##0.00");
                txtDiscountAmt.Text = Convert.ToDouble(clC.GetFields("discountamt")).ToString("#,##0.00");
                txtAddMarkup.Text = (Convert.ToDouble(clC.GetFields("customercost")) -
                    Convert.ToDouble(clC.GetFields("markup")) -
                    Convert.ToDouble(clC.GetFields("moxydealercost")) +
                    Convert.ToDouble(clC.GetFields("discountamt"))).ToString("#,##0.00");

                GetContractPayments();
            }
        }

        private void GetContractPayments()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select sum(paidamt) as amt from contractpayment where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (String.IsNullOrEmpty(clR.GetFields("amt")))
                    txtAmountPaid.Text = "0.00";
                else
                    txtAmountPaid.Text = Convert.ToDouble(clR.GetFields("amt")).ToString("#,##0.00");

                txtAmountOwed.Text = (Convert.ToDouble(txtDealerCost.Text) - Convert.ToDouble(txtAmountPaid.Text)).ToString();
            }

        }
    }
}