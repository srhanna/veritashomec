﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class Contract : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                hfContractID.Value = Request.QueryString["contractid"];
                FillContract();
                pvSurcharge.Selected = true;
                pnlContractConfirm.Visible = false;
            }

            if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }


        private void FillContract()
        {
            string SQL;
            string sTemp;
            string nl = System.Environment.NewLine;
            SQL = "select p.programname as PName, * from contract c ";
            SQL = SQL + "left join program p on c.programid = p.programid ";
            SQL = SQL + "left join plantype pt on pt.plantypeid = c.plantypeid ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lblContractNo.Text = clC.GetFields("contractno");
                lblStatus.Text = clC.GetFields("status");
                if (clC.GetFields("saledate").Length > 0)
                    lblSaleDate.Text = Convert.ToDateTime(clC.GetFields("saledate")).ToString("d");

                if (clC.GetFields("effdate").Length > 0)
                    lblEffDate.Text = Convert.ToDateTime(clC.GetFields("effdate")).ToString("d");

                if (clC.GetFields("expdate").Length > 0)
                    lblExpDate.Text = Convert.ToDateTime(clC.GetFields("expdate")).ToString("d");

                hfProgramID.Value = clC.GetFields("programid");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");
                lblProgram.Text = clC.GetFields("pname");
                lblPlanType.Text = clC.GetFields("plantype");
                lblTerm.Text = clC.GetFields("termmonth");
                lblLienholder.Text = clC.GetFields("lienholder");
                sTemp = clC.GetFields("fname") + " ";
                sTemp = sTemp + clC.GetFields("lname") + nl;
                sTemp = sTemp + clC.GetFields("addr1") + nl;

                if (clC.GetFields("addr2").Length > 0)
                    sTemp = sTemp + clC.GetFields("addr2") + nl;

                sTemp = sTemp + clC.GetFields("city") + " ";
                sTemp = sTemp + clC.GetFields("state") + " ";
                sTemp = sTemp + clC.GetFields("zip") + nl;
                sTemp = sTemp + clC.GetFields("phone");
                txtCustomerInfo.Text = sTemp;
                sTemp = clC.GetFields("coveredaddr1") + nl;
                
                if (clC.GetFields("coveredaddr2").Length > 0)
                    sTemp = sTemp + clC.GetFields("coveredaddr2") + nl;

                sTemp = sTemp + clC.GetFields("city") + " ";
                sTemp = sTemp + clC.GetFields("state") + " ";
                sTemp = sTemp + clC.GetFields("zip");
                txtCoveredInfo.Text = sTemp;

                if (clC.GetFields("datepaid").Length > 0)
                    lblPaidDate.Text = Convert.ToDateTime(clC.GetFields("datepaid")).ToString("d");

                if (clC.GetFields("decpage").Length == 0)
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                }
                else
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clC.GetFields("decpage");
                    hlDec.Text = clC.GetFields("decpage");
                }

                if (clC.GetFields("tcpage").Length == 0)
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                }
                else
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clC.GetFields("tcpage");
                    hlTC.Text = clC.GetFields("tcpage");
                }

                GetDealerInfo(Convert.ToInt64(clC.GetFields("dealerid")));
                GetAgentInfo(Convert.ToInt64(clC.GetFields("agentsid")));
            }
        }

        private void GetAgentInfo(long xAgentID)
        {
            clsDBO clA = new clsDBO();
            string SQL, sTemp;
            string nl = System.Environment.NewLine;
            SQL = "select * from agents where agentid = " + xAgentID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                sTemp = clA.GetFields("agentno") + nl;
                sTemp = sTemp + clA.GetFields("agentname") + nl;
                sTemp = sTemp + clA.GetFields("addr1") + nl;
                if (clA.GetFields("addr2").Length > 0)
                    sTemp = sTemp + clA.GetFields("addr2") + nl;

                sTemp = sTemp + clA.GetFields("city") + " " + clA.GetFields("state") + " " + clA.GetFields("zip") + nl;
                sTemp = sTemp + clA.GetFields("phone");
                txtAgent.Text = sTemp;
            }
        }

        private void GetDealerInfo(long xDealerID)
        {
            clsDBO clA = new clsDBO();
            string SQL, sTemp;
            string nl = System.Environment.NewLine;
            SQL = "select * from dealer where dealerid = " + xDealerID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                sTemp = clA.GetFields("dealerno") + nl;
                sTemp = sTemp + clA.GetFields("dealername") + nl;
                sTemp = sTemp + clA.GetFields("addr1") + nl;
                if (clA.GetFields("addr2").Length > 0)
                    sTemp = sTemp + clA.GetFields("addr2") + nl;

                sTemp = sTemp + clA.GetFields("city") + " " + clA.GetFields("state") + " " + clA.GetFields("zip") + nl;
                sTemp = sTemp + clA.GetFields("phone");
                txtDealer.Text = sTemp;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            if (hfReadOnly.Value == "True")
                btnAddClaim.Enabled = false;
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            tsContract.Tabs[10].Visible = false;
         
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contractmodification")))
                    tsContract.Tabs[10].Visible = true;
            }
        }

        private void ShowError(string xMessage)
        {
            hfError.Value = "Visible";
            lblError.Text = xMessage;
            string script = @"function f(){$find(""" + rwError.ClientID + @""").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowDupeClaim()
        {
            hfError.Value = "Visible";
            string script = @"function f(){$find(""" + rwDupeClaim.ClientID + @""").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = @"function f(){$find(""" + rwError.ClientID + @""").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void tsContract_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsContract.SelectedTab.Value == "Breakdown")
                pvBreakdown.Selected = true;

            if (tsContract.SelectedTab.Value == "Surcharge")
                pvSurcharge.Selected = true;

            if (tsContract.SelectedTab.Value == "Costs")
                pvCost.Selected = true;

            if (tsContract.SelectedTab.Value == "Commissions")
                pvCommissions.Selected = true;

            if (tsContract.SelectedTab.Value == "Payment")
                pvPayment.Selected = true;

            if (tsContract.SelectedTab.Value == "Document")
                pvContractDocument.Selected = true;

            if (tsContract.SelectedTab.Value == "Cancellation")
            {
               pvCancel.Selected = true;
                ucCancel.FillCancellation();
            }

            if (tsContract.SelectedTab.Value == "Transfer")
               pvTransfer.Selected = true;

            if (tsContract.SelectedTab.Value == "Notes")
                pvNotes.Selected = true;

            if (tsContract.SelectedTab.Value == "CustModify")
                pvCustomerMod.Selected = true;

            if (tsContract.SelectedTab.Value == "ContractModify")
                pvContractMod.Selected = true;
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAddClaim_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            string SQL;
            clsDBO clC = new clsDBO();
            if (hfReadOnly.Value == "True")
                return;

            pnlContractConfirm.Visible = false;
            tsContract.Visible = true;
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("status") == "Paid")
                {
                    if (clC.GetFields("plantypeid") != "118" && clC.GetFields("programid") != "103")
                    {
                        GetClaimNo();
                        AddClaimDetail();
                        Response.Redirect(@"~\claim\claim.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value);
                    }
                    else
                    {
                        GetClaimNoGAP();
                        Response.Redirect(@"~\claim\claimgap.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value);
                    }
                    return;
                }
                else
                {
                    if (!CheckCancelExpire())
                    {
                        ShowError("Contract is Expired or Cancelled. Can not start claim.");
                        return;
                    }

                    tsContract.Visible = false;
                    txtContractStatus2.Text = clC.GetFields("status");
                    if (clC.GetFields("plantypeid") != "118" && clC.GetFields("programid") != "103")
                    {
                        GetClaimNo();
                        AddClaimDetail();
                    }
                    else
                    {
                        GetClaimNoGAP();
                        hfPlanTypeID.Value = clC.GetFields("plantypeid");
                        hfProgramID.Value = clC.GetFields("programid");
                    }

                    pnlContractConfirm.Visible = true;
                    return;
                }
            }
        }

        private bool CheckCancelExpire()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clU = new clsDBO();
            SQL = "select * from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("status").ToLower() == "expired" || clR.GetFields("status").ToLower() == "pending expired"
                    || clR.GetFields("status").ToLower() == "cancelled" || clR.GetFields("status").ToLower() == "cancelled before paid")
                {
                    SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
                    clU.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clU.RowCount() > 0)
                    {
                        clU.GetRow();
                        if (Convert.ToBoolean(clU.GetFields("allowcancelexpire")))
                            return true;
                    }
                }
                else
                    return true;
            }

            return false;
        }

        private bool CheckClaimDuplicate()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (!CheckWithinDays())
            {
                if (hfContractID.Value.Length > 0)
                {
                    SQL = "select * from claim where contract id = " + hfContractID.Value + " ";
                    SQL = SQL + "and credate > '" + DateTime.Today.AddDays(-3) + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clR.RowCount() > 0)
                    {
                        ShowDupeClaim();
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CheckWithinDays()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("within3days")))
                    return true;
            }

            return false;
        }

        private void AddClaimDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            long lDeductID = 0, dDeduct = 0;
            SQL = "select deductid from contract where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lDeductID = Convert.ToInt64(clR.GetFields("deductid"));
            }

            SQL = "select * from deductable where deductid = " + lDeductID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dDeduct = Convert.ToInt64(clR.GetFields("deductamt"));
            }

            SQL = "select * from claimdetail where claimid = " + hfClaimID.Value + " " + "and jobno = 'A01'";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                clR.NewRow();
            else
                clR.GetRow();

            clR.SetFields("ratetypeid", "1");
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdetailtype", "Deductible");
            clR.SetFields("jobno", "A01");
            clR.SetFields("reqqty", "1");
            clR.SetFields("reqcost", (dDeduct * -1).ToString());
            clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqamt", (dDeduct * -1).ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Today.ToString());
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        private void GetClaimNo()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            string sClaimNo;
            long lClaimNo = 0;

        MoveHere:

            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claim ";
            SQL = SQL + "where claimno like 'C1%' ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sClaimNo = clC.GetFields("claimno");
                
                if (sClaimNo.Length == 0)
                    sClaimNo = "C100000000";

                sClaimNo = sClaimNo.Replace("C1", "");
                lClaimNo = Convert.ToInt64(sClaimNo);
                lClaimNo = lClaimNo + 1;
                sClaimNo = lClaimNo.ToString("100000000");
                sClaimNo = "C" + sClaimNo;
            }

            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claim ";
                SQL = SQL + "where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clC.RowCount() > 0)
                    goto MoveHere;
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", "Open");
                    if (hfContractID.Value.Length > 0)
                        clC.SetFields("contractid", hfContractID.Value);
                    else
                        clC.SetFields("contractid", "0");

                    clC.SetFields("lossdate", DateTime.Today.ToString());
                    clC.SetFields("claimactivityid", "1");
                    clC.SetFields("creby", hfUserID.Value);
                    clC.SetFields("assignedto", hfUserID.Value);
                    clC.SetFields("Modby", hfUserID.Value);
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("addnew", "True");
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claim ";
            SQL = SQL + "where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfClaimID.Value = clC.GetFields("claimid");
                SQL = "insert into claimopenhistory ";
                SQL = SQL + "(claimid, opendate, openby) ";
                SQL = SQL + "values (" + hfClaimID.Value + ",'";
                SQL = SQL + DateTime.Today + "',";
                SQL = SQL + hfUserID.Value + ")";
                clC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }
        }

        private void GetClaimNoGAP()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            string sClaimNo;
            long lClaimNo = 0;

        MoveHere:

            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claimgap ";
            SQL = SQL + "where claimno like 'G0%' ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (sClaimNo.Length > 0)
                {
                    sClaimNo = clC.GetFields("claimno");
                    sClaimNo = sClaimNo.Replace("G0", "");
                    lClaimNo = Convert.ToInt64(sClaimNo);
                    lClaimNo = lClaimNo + 1;
                    sClaimNo = lClaimNo.ToString("000000000");
                    sClaimNo = "G" + sClaimNo;
                }
                else
                    sClaimNo = "G000000000";
            }

            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claimgap ";
                SQL = SQL + "where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clC.RowCount() > 0)
                    goto MoveHere;
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", "1");
                    if (hfContractID.Value.Length != 0)
                        clC.SetFields("contractid", hfContractID.Value);

                    clC.SetFields("claimdate", DateTime.Today.ToString());
                    clC.SetFields("creby", hfUserID.Value.ToString());
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("modby", hfUserID.Value);
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claimgap ";
            SQL = SQL + "where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfClaimID.Value = clC.GetFields("claimgapid");
            }
        }

        protected void btnCancelClaim_Click(object sender, EventArgs e)
        {
            UpdateContractClaimCancel();
            UpdateClaimNote();
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        private void UpdateContractClaimCancel()
        {
            clsDBO clCL = new clsDBO();
            string SQL;
            if (hfReadOnly.Value == "True")
                return;

            SQL = "select * from claim where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                clCL.SetFields("status", "Void");
                clCL.SetFields("contractid", hfContractID.Value);
                clCL.SaveDB();
            }
        }

        protected void btnProceedClaim_Click(object sender, EventArgs e)
        {
            if (hfPlanTypeID.Value != "118" && hfProgramID.Value != "103")
            {
                UpdateClaimNote();
                pnlContractConfirm.Visible = false;
                tsContract.Visible = true;
                Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
            }
            else
            {
                UpdateClaimNote2();
                pnlContractConfirm.Visible = false;
                tsContract.Visible = true;
                Response.Redirect("~/claim/claimgap.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
            }
        }

        private void UpdateClaimNote()
        {
            if (txtNote.Text.Length == 0)
                return;

            clsDBO clCN = new clsDBO();
            string SQL = "select * from claimnote where claimid = " + hfClaimID.Value + " " + "and claimnoteid = 0 ";
            clCN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clCN.NewRow();
            clCN.SetFields("claimid", hfClaimID.Value);
            clCN.SetFields("claimnotetypeid", "5");
            clCN.SetFields("note", txtNote.Text);
            clCN.SetFields("credate", DateTime.Today.ToString());
            clCN.SetFields("creby", hfUserID.Value);
            clCN.AddRow();
            clCN.SaveDB();
        }

        private void UpdateClaimNote2()
        {
            if (txtNote.Text.Length == 0)
                return;

            clsDBO clCN = new clsDBO();
            string SQL = "select * from claimgapnote where claimgapid = " + hfClaimID.Value + " " + "and claimnoteid = 0 ";
            clCN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clCN.NewRow();
            clCN.SetFields("claimgapid", hfClaimID.Value);
            clCN.SetFields("claimgapnotetypeid", "3");
            clCN.SetFields("note", txtNote.Text);
            clCN.SetFields("credate", DateTime.Today.ToString());
            clCN.SetFields("creby", hfUserID.Value);
            clCN.AddRow();
            clCN.SaveDB();
        }
    }
}