﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contract.aspx.cs" Inherits="VeritasHomeC.Contract" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="ContractSurcharge.ascx" TagName="ContractSurcharge" TagPrefix="uc" %>
<%@ Register Src="ContractBreakdown.ascx" TagName="Breakdown" TagPrefix="uc" %>
<%@ Register Src="ContractCost.ascx" TagName="ContractCost" TagPrefix="uc" %>
<%@ Register Src="ContractCommission.ascx" TagName="Commission" TagPrefix="uc" %>
<%@ Register Src="ContractPayment.ascx" TagName="Payment" TagPrefix="uc" %>
<%@ Register Src="ContractDocuments.ascx" TagName="ContractDocument" TagPrefix="uc" %>
<%@ Register Src="ContractCancel.ascx" TagName="Cancel" TagPrefix="uc" %>
<%@ Register Src="ContractTransfer.ascx" TagName="Transfer" TagPrefix="uc" %>
<%@ Register Src="ContractModification.ascx" TagName="ContractMod" TagPrefix="uc" %>
<%@ Register Src="ContractNotes.ascx" TagName="Notes" TagPrefix="uc" %>
<%@ Register Src="ContractCustomerModification.ascx" TagName="CustomerMod" TagPrefix="uc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Contract</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server" Font-Names="calibri" Font-Size="11">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Button ID="btnAddClaim" OnClick="btnAddClaim_Click" runat="server" Text="Add Claim" CssClass="button1" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true" Font-Size="Large">
                                                    Contract No:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblContractNo" runat="server" Font-Size="Large" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    &nbsp
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Status:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Sale Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblSaleDate" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Effective Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblEffDate" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Expire Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblExpDate" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Paid Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblPaidDate" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Program:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblProgram" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Plan:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblPlanType" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Term:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblTerm" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Lienholder: 
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblLienholder" runat="server" Text=""></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                    Covered Info:
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                    Customer Info:
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                    Seller Info:
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true" ColumnSpan="2">
                                                    Agent Info:
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                    <asp:TextBox Font-Size="11" Font-Names="calibri" ID="txtCoveredInfo" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="5" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                    <asp:TextBox Font-Size="11" Font-Names="calibri" ID="txtCustomerInfo" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="5" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                    <asp:TextBox ID="txtDealer" Font-Size="11" Font-Names="calibri" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="6" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                                                    <asp:TextBox ID="txtAgent" Font-Size="11" Font-Names="calibri" ReadOnly="true" Width="225" BorderStyle="Solid" Rows="6" TextMode="MultiLine" CssClass="MultilineTextBox" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true" ID="tcDec">
                                                    Dec Page:
                                                </asp:TableCell>
                                                <asp:TableCell ID="tcDesc2">
                                                    <asp:HyperLink ID="hlDec" Target="_blank" runat="server"></asp:HyperLink>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true" ID="tcTC">
                                                    Terms and Conditions Page:
                                                </asp:TableCell>
                                                <asp:TableCell ID="tcTC2">
                                                    <asp:HyperLink ID="hlTC" Target="_blank" runat="server"></asp:HyperLink>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadTabStrip ID="tsContract" OnTabClick="tsContract_TabClick" runat="server">
                                            <Tabs>
                                                <telerik:RadTab Text="Surcharges" Value="Surcharge"></telerik:RadTab>
                                                <telerik:RadTab Text="Breakdown" Value="Breakdown"></telerik:RadTab>
                                                <telerik:RadTab Text="Costs" Value="Costs"></telerik:RadTab>
                                                <telerik:RadTab Text="Commissions" Value="Commissions"></telerik:RadTab>
                                                <telerik:RadTab Text="Payment" Value="Payment"></telerik:RadTab>
                                                <telerik:RadTab Text="Documents" Value="Document"></telerik:RadTab>
                                                <telerik:RadTab Text="Cancellation" Value="Cancellation"></telerik:RadTab>
                                                <telerik:RadTab Text="Transfer" Value="Transfer"></telerik:RadTab>
                                                <telerik:RadTab Text="Notes" Value="Notes"></telerik:RadTab>
                                                <telerik:RadTab Text="Customer Modification" Value="CustModify"></telerik:RadTab>
                                                <telerik:RadTab Text="Contract Modification" Value="ContractModify"></telerik:RadTab>
                                            </Tabs>
                                        </telerik:RadTabStrip>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadMultiPage ID="rmContract" runat="server">
                                            <telerik:RadPageView ID="pvSurcharge" runat="server">
                                                <uc:ContractSurcharge runat="server" />
                                            </telerik:RadPageView>
                                            
                                            <telerik:RadPageView ID="pvBreakdown" runat="server">
                                                <uc:Breakdown runat="server"></uc:Breakdown>
                                            </telerik:RadPageView>
                                           
                                             <telerik:RadPageView ID="pvCost" runat="server">
                                                <uc:ContractCost runat="server"></uc:ContractCost>
                                            </telerik:RadPageView>
                                            
                                            <telerik:RadPageView ID="pvCommissions" runat="server">
                                                <uc:Commission runat="server" />
                                            </telerik:RadPageView>

                                            <telerik:RadPageView ID="pvPayment" runat="server">
                                                <uc:Payment runat="server" />
                                            </telerik:RadPageView>
                                             
                                            <telerik:RadPageView ID="pvContractDocument" runat="server">
                                                <uc:ContractDocument runat="server" />
                                            </telerik:RadPageView>
                                            
                                            <telerik:RadPageView ID="pvCancel" runat="server">
                                                <uc:Cancel runat="server" ID="ucCancel" />
                                            </telerik:RadPageView>
                                            
                                            <telerik:RadPageView ID="pvTransfer" runat="server">
                                                <uc:Transfer runat="server" />
                                            </telerik:RadPageView>
                                            
                                            <telerik:RadPageView ID="pvNotes" runat="server">
                                                <uc:Notes runat="server" />
                                            </telerik:RadPageView>
                                            
                                            <telerik:RadPageView ID="pvCustomerMod" runat="server">
                                                <uc:CustomerMod runat="server" />
                                            </telerik:RadPageView>
                                               
                                            <telerik:RadPageView ID="pvContractMod" runat="server">
                                                <uc:ContractMod runat="server" />
                                            </telerik:RadPageView>
                                        </telerik:RadMultiPage>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:panel runat="server" ID="pnlContractConfirm">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Contract Status:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtContractStatus2" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Note:
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtNote" Height="200" Width="500" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnProceedClaim" OnClick="btnProceedClaim_Click" BackColor="#1eabe2" runat="server" Text="Proceed with Claim" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnCancelClaim" OnClick="btnCancelClaim_Click" BackColor="#1eabe2" runat="server" Text="Cancel Claim" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfReadOnly" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfProgramID" runat="server" />
                <asp:HiddenField ID="hfPlanTypeID" runat="server" />
                <asp:HiddenField ID="hfError" runat="server" />
                <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>
                <asp:HiddenField ID="hfDuplicateClaim" runat="server" />
                <telerik:RadWindow ID="rwDupeClaim"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label runat="server" ID="Label1" Text="There is a claim already entered within the last 3 days."></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="Button1" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
