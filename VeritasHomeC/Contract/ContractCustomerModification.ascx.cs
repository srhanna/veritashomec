﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractCustomerModification : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillCustomer();
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clsDBO clR = new clsDBO();
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    btnSave.Visible = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillCustomer()
        {
            clsDBO clC = new clsDBO();
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtAddr1.Text = clC.GetFields("addr1");
                txtAddr2.Text = clC.GetFields("addr2");
                txtCity.Text = clC.GetFields("city");
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                txtPhone.Text = clC.GetFields("phone");
                txtZip.Text = clC.GetFields("zip");
                cboState.SelectedValue = clC.GetFields("state");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            FillCustomer();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            clsDBO clC = new clsDBO();
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            var clMAC = new VeritasGlobalToolsV2.clsMoxyAddressChange();

            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clC.SetFields("addr1", txtAddr1.Text);
                clC.SetFields("addr2", txtAddr2.Text);
                clC.SetFields("city", txtCity.Text);
                clC.SetFields("fname", txtFName.Text);
                clC.SetFields("lname", txtLName.Text);
                clC.SetFields("phone", txtPhone.Text);
                clC.SetFields("zip", txtZip.Text);
                clC.SetFields("state", cboState.SelectedValue);
                clC.SaveDB();
                clMAC.UpdateMoxy(Convert.ToInt64(hfContractID.Value));
            }
        }
    }
}