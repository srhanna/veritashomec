﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractSurcharge : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetServerInfo();
                hfContractID.Value = Request.QueryString["contractid"];
                FillSurchargeGrid();
            }
        }

        private void FillSurchargeGrid()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select ContractSurchargeID, surcharge from contractSurcharge cs " +
                "inner join Surcharge s on s.SurchargeID = cs.surchargeid ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            rgSurcharge.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgSurcharge.DataBind();
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }
    }
}