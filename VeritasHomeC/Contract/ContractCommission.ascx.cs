﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractCommission : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsAgent.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsRateType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                FillTable();
                ReadOnlyButtons();
                pnlSearchRateType.Visible = false;
                pnlChange.Visible = false;
                pnlSearchAgent.Visible = false;
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clsDBO clR = new clsDBO();
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "true;")
                {
                    btnAdd.Enabled = false;
                    btnSave.Enabled = false;
                    btnRateTypeSearch.Enabled = false;
                }
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillTable()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select contractcommissionid, rt.ratetypename, amt, datepaid, p.CompanyName, cancelamt, canceldate from contractcommissions cc  ";
            SQL = SQL + "inner join ratetype rt on rt.ratetypeid = cc.ratetypeid ";
            SQL = SQL + "inner join Payee p on p.PayeeID = cc.payeeid ";
            SQL = SQL + "where cc.contractid = " + hfContractID.Value;
            rgCommission.DataSource = clC.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgCommission.DataBind();
        }

        protected void rgCommission_SelectedIndexChanged(object sender, EventArgs e)
        {
            clsDBO clC = new clsDBO();
            string SQL;
            pnlCommission.Visible = false;
            pnlChange.Visible = true;
            SQL = "select * from contractcommissions where contractcommissionid = " + rgCommission.SelectedValue;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfContractCommissionID.Value = rgCommission.SelectedValue.ToString();
                hfRateTypeID.Value = clC.GetFields("ratetypeid");
                if (clC.GetFields("datepaid").Length > 0)
                    rdpCycleDate.SelectedDate = Convert.ToDateTime(clC.GetFields("datepaid"));
                else
                    rdpCycleDate.Clear();

                hfAgentID.Value = clC.GetFields("payeeid");
                txtPayeeID.Text = clsFunc.GetPayee(Convert.ToInt64(hfAgentID.Value));
                txtAmt.Text = Convert.ToDouble(clC.GetFields("amt")).ToString("#,##0.00");

                if (clC.GetFields("canceldate").Length > 0)
                    rdpCancelCycleDate.SelectedDate = Convert.ToDateTime(clC.GetFields("canceldate"));
                else
                    rdpCancelCycleDate.Clear();

                txtCancelAmt.Text = clC.GetFields("cancelamt");
                GetRateType();
            }
        }

        private void GetRateType()
        {
            string SQL = "select * from ratetype where ratetypeid = " + hfRateTypeID.Value;
            clsDBO clRT = new clsDBO();
            clRT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clRT.RowCount() > 0)
            {
                clRT.GetRow();
                txtRateType.Text = clRT.GetFields("ratetypename");
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlChange.Visible = false;
            pnlCommission.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clCC = new clsDBO();
            SQL = "select * from contractcommissions where contractcommissionid = " + hfContractCommissionID.Value;
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            clCC.SetFields("contractid", hfContractID.Value);
            clCC.SetFields("ratetypeid", hfRateTypeID.Value);
            clCC.SetFields("payeeid", hfAgentID.Value);
            if (txtAmt.Text.Length > 0)
                clCC.SetFields("amt", txtAmt.Text);
            else
                clCC.SetFields("amt", "0");

            if (!String.IsNullOrEmpty(rdpCycleDate.SelectedDate.ToString()))
                clCC.SetFields("datepaid", rdpCycleDate.SelectedDate.ToString());

            if (txtCancelAmt.Text.Length > 0)
                clCC.SetFields("cancelamt", txtCancelAmt.Text);
            else
                clCC.SetFields("cancelamt", "0");

            if (!String.IsNullOrEmpty(rdpCancelCycleDate.SelectedDate.ToString()))
                clCC.SetFields("canceldate", rdpCancelCycleDate.SelectedDate.ToString());

            if (clCC.RowCount() == 0)
                clCC.AddRow();

            clCC.SaveDB();
            pnlChange.Visible = false;
            pnlCommission.Visible = true;
            FillTable();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlCommission.Visible = false;
            pnlChange.Visible = true;
            hfContractCommissionID.Value = "0";
            hfAgentID.Value = "0";
            hfRateTypeID.Value = "0";
            txtPayeeID.Text = "";
            txtRateType.Text = "";
            txtAmt.Text = "";
            rdpCycleDate.Clear();
            txtCancelAmt.Text = "";
            rdpCancelCycleDate.Clear();
        }

        protected void btnRateTypeSearch_Click(object sender, EventArgs e)
        {
            rgRateType.Rebind();
            pnlChange.Visible = false;
            pnlSearchRateType.Visible = true;
        }

        protected void btnCancelRateType_Click(object sender, EventArgs e)
        {
            pnlChange.Visible = true;
            pnlSearchRateType.Visible = false;
        }

        protected void rgRateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRateTypeID.Value = rgRateType.SelectedValue.ToString();
            pnlChange.Visible = true;
            pnlSearchRateType.Visible = false;
            GetRateType();
        }

        protected void btnAgentSearch_Click(object sender, EventArgs e)
        {
            pnlChange.Visible = false;
            pnlSearchAgent.Visible = true;
        }

        protected void btnClearAgentInfo_Click(object sender, EventArgs e)
        {
            hfAgentID.Value = "0";
            txtPayeeID.Text = "";
            pnlChange.Visible = true;
            pnlSearchAgent.Visible = false;
        }

        protected void rgAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAgentID.Value = rgAgent.SelectedValue.ToString();
            txtPayeeID.Text = clsFunc.GetAgentInfo(Convert.ToInt64(hfAgentID.Value));
            pnlChange.Visible = true;
            pnlSearchAgent.Visible = false;
        }
    }
}