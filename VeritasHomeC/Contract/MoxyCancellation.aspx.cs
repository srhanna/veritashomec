﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class MoxyCancellation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                FillContract();
                FillMoxyCancellation();
            }
        }

        private void CheckToDo()
        {
            clsDBO clTD = new clsDBO();
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL = "select * from usermessage where toid = " + hfUserID.Value + " " +
                "and completedmessage = 0 and deletedmessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void FillMoxyCancellation()
        {
            clsDBO clCCM = new clsDBO();
            string SQL = "select * from contractcancelmoxy where contractid = " + hfContractID.Value + " " + "and cancelstatus = 'Cancelled' ";
            clCCM.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCCM.RowCount() > 0)
            {
                clCCM.GetRow();
                if (clCCM.GetFields("canceleffdate").Length > 0)
                    rdpProcessDate.SelectedDate = Convert.ToDateTime(clCCM.GetFields("canceleffdate"));

                txtMiles.Text = Convert.ToInt64(clCCM.GetFields("cancelmile")).ToString("#,##0");
                txtCancelPercent.Text = Convert.ToDouble(clCCM.GetFields("cancelfactor")).ToString("p");
                txtTermPercent.Text = Convert.ToDouble(clCCM.GetFields("termfactor")).ToString("p");
                txtMilePercent.Text = Convert.ToDouble(clCCM.GetFields("milefactor")).ToString("p");
                txtCancelStatus.Text = clCCM.GetFields("cancelstatus");
                txtGrossCustomer.Text = Convert.ToDouble(clCCM.GetFields("grosscustomer")).ToString("c");
                txtGrossDealer.Text = Convert.ToDouble(clCCM.GetFields("grossdealer")).ToString("c");
                txtGrossAdmin.Text = Convert.ToDouble(clCCM.GetFields("grossadmin")).ToString("c");
                txtClaimAmt.Text = Convert.ToDouble(clCCM.GetFields("claimamt")).ToString("c");
                txtCancelFee.Text = Convert.ToDouble(clCCM.GetFields("cancelfeeamt")).ToString("c");
                txtQuoteDate.Text = Convert.ToDateTime(clCCM.GetFields("canceldate")).ToString("d");
                txtRequestDate.Text = Convert.ToDateTime(clCCM.GetFields("canceldate")).ToString("d");
                txtCancelDate.Text = Convert.ToDateTime(clCCM.GetFields("canceldate")).ToString("d");
                txtCustomerRefund.Text = Convert.ToDouble(clCCM.GetFields("adminamt")) + Convert.ToDouble(clCCM.GetFields("dealeramt")).ToString("c");
                txtFromDealer.Text = Convert.ToDouble(clCCM.GetFields("dealeramt")).ToString("c");
                txtFromAdmin.Text = Convert.ToDouble(clCCM.GetFields("adminamt")).ToString("c");
            }
        }

        private void FillContract()
        {
            string SQL, sTemp;
            string nl = System.Environment.NewLine;
            SQL = "select * from contract c ";
            SQL = SQL + "left join program p on c.programid = p.programid ";
            SQL = SQL + "left join plantype pt on pt.plantypeid = c.plantypeid ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clsDBO clC = new clsDBO();
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lblContractNo.Text = clC.GetFields("contractno");
                lblStatus.Text = clC.GetFields("status");
                if (clC.GetFields("saledate").Length > 0)
                    lblSaleDate.Text = Convert.ToDateTime(clC.GetFields("saledate")).ToString("d");

                if (clC.GetFields("effdate").Length > 0)
                    lblEffDate.Text = Convert.ToDateTime(clC.GetFields("effdate")).ToString("d");

                if (clC.GetFields("expdate").Length > 0)
                    lblExpDate.Text = Convert.ToDateTime(clC.GetFields("expdate")).ToString("d");

                if (clC.GetFields("salemile").Length > 0)
                    lblSaleMiles.Text = Convert.ToInt64(clC.GetFields("salemile")).ToString("#,##0");

                if (clC.GetFields("effmile").Length > 0)
                    lblEffMile.Text = Convert.ToInt64(clC.GetFields("effmile")).ToString("#,##0");

                if (clC.GetFields("expmile").Length > 0)
                    lblExpMile.Text = Convert.ToInt64(clC.GetFields("expmile")).ToString("#,##0");

                lblProgram.Text = clC.GetFields("programname");
                lblPlanType.Text = clC.GetFields("plantype");
                lblTerm.Text = clC.GetFields("termmonth") + "/" + Convert.ToInt64(clC.GetFields("termmile")).ToString("#,##0");
                sTemp = clC.GetFields("fname");
                sTemp = sTemp + clC.GetFields("lname") + nl;
                sTemp = sTemp + clC.GetFields("addr1") + nl;
                if (clC.GetFields("addr2").Length > 0)
                    sTemp = sTemp + clC.GetFields("addr2") + nl;

                sTemp = sTemp + clC.GetFields("city") + " ";
                sTemp = sTemp + clC.GetFields("state") + " ";
                sTemp = sTemp + clC.GetFields("zip") + nl;
                sTemp = sTemp + clC.GetFields("phone");
                txtCustomerInfo.Text = sTemp;
                txtLienholder.Text = clC.GetFields("lienholder");
                if (clC.GetFields("datepaid").Length > 0)
                    lblPaidDate.Text = Convert.ToDateTime(clC.GetFields("datepaid")).ToString("d");

                rbDealer.Checked = true;
                GetDealerInfo(Convert.ToInt64(clC.GetFields("dealerid")));
                GetAgentInfo(Convert.ToInt64(clC.GetFields("agentsid")));
            }

            hlWorksheet.NavigateUrl = "cancellation.aspx?contractid=" + hfContractID.Value;

        }

        private void GetAgentInfo(long xAgentID)
        {
            clsDBO clA = new clsDBO();
            string nl = System.Environment.NewLine;
            string SQL, sTemp;
            SQL = "select * from agent where agentid = " + xAgentID;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                sTemp = clA.GetFields("agentno") + nl;
                sTemp = sTemp + clA.GetFields("agentname") + nl;
                sTemp = sTemp + clA.GetFields("addr1") + nl;
                if (clA.GetFields("addr2").Length > 0)
                    sTemp = sTemp + clA.GetFields("addr2") + nl;

                sTemp = sTemp + clA.GetFields("city") + " " + clA.GetFields("state") + " " + clA.GetFields("zip") + nl;
                sTemp = sTemp + clA.GetFields("phone");
                txtAgent.Text = sTemp;
            }
        }

        private void GetDealerInfo(long xDealerID)
        {
            clsDBO clD = new clsDBO();
            string nl = System.Environment.NewLine;
            string SQL, sTemp;
            SQL = "select * from dealer where dealerid = " + xDealerID;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                sTemp = clD.GetFields("dealerno") + nl;
                sTemp = sTemp + clD.GetFields("dealername") + nl;
                sTemp = sTemp + clD.GetFields("addr1") + nl;
                if (clD.GetFields("addr2").Length > 0)
                    sTemp = sTemp + clD.GetFields("addr2") + nl;

                sTemp = sTemp + clD.GetFields("city") + " " + clD.GetFields("state") + " " + clD.GetFields("zip") + nl;
                sTemp = sTemp + clD.GetFields("phone");
                txtDealer.Text = sTemp;
                if (clD.GetFields("dealerstatusid") == "5")
                    rbOther.Checked = true;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
            }
        }
        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = @"function f(){$find(""" + rwError.ClientID + @""").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "Visible";
            string script = @"function f(){$find(""" + rwError.ClientID + @""").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            clsDBO clCC = new clsDBO();
            CalcCancellation(true);
            GetLienholder2();
            string SQL = "select * from contractcancel where contractid = " + hfContractID.Value;
            clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCC.RowCount() == 0)
                clCC.NewRow();
            else
                clCC.GetRow();

            var clP = new VeritasGlobalToolsV2.clsPayee();
            
            if (rbCustomer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddCustomerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbDealer.Checked == true)
            {
                clP.ContractID = Convert.ToInt64(hfContractID.Value);
                clP.AddDealerToPayee();
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (rbOther.Checked == true)
            {
                clP.FName = txtFName.Text;
                clP.CompanyName = txtCompanyName.Text;
                clP.LName = txtLName.Text;
                clP.Addr1 = txtAddr1.Text;
                clP.Addr2 = txtAddr2.Text;
                clP.City = txtCity.Text;
                clP.State = cboState.Text;
                clP.Zip = txtZip.Text;
                clP.Phone = txtPhone.Text;
                clP.AddOtherToPayee(hfContractID.Value);
                clCC.SetFields("payeeid", clP.PayeeID.ToString());
            }

            if (clCC.RowCount() == 0)
            {
                clCC.SetFields("credate", DateTime.Today.ToString());
                clCC.SetFields("creby", hfUserID.Value);
                clCC.AddRow();
            }
            else
            {
                clCC.SetFields("moddate", DateTime.Today.ToString());
                clCC.SetFields("modby", hfUserID.Value);
            }

            clCC.SaveDB();
        }

        private void GetLienholder2()
        {
            clsDBO clC = new clsDBO();
            string SQL = "select * from contract where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtLienholder.Text = clC.GetFields("lienholder");
            }
        }

        private void CalcCancellation(bool xQuote)
        {
            pnlVerify.Visible = true;
            var clCancel = new VeritasGlobalToolsV2.clsCancellation();
            clCancel.CancelDate = rdpProcessDate.SelectedDate.ToString();
            clCancel.ContractID = Convert.ToInt64(hfContractID.Value);
            clCancel.Quote = xQuote;
            clCancel.CalculateCancellation();
            txtCancelFeeV.Text = clCancel.CancelFee.ToString("#,##0.00");
            txtCancelPercentV.Text = clCancel.CancelPer.ToString("0.###%");
            txtClaimAmtV.Text = clCancel.Claims.ToString("#,##0.00");
            txtCustomerRefundV.Text = clCancel.DealerAmt + clCancel.AdminAmt.ToString("#,##0.00");
            txtFromAdminV.Text = clCancel.AdminAmt.ToString("#,##0.00");
            txtFromDealerV.Text = clCancel.DealerAmt.ToString("#,##0.00");
            txtTermPercentV.Text = clCancel.CancelTermPer.ToString("0.###%");
            txtGrossCustomerV.Text = clCancel.GrossCustomer.ToString("#,##0.00");
            txtGrossDealerV.Text = clCancel.GrossDealer.ToString("#,##0.00");
            txtGrossAdminV.Text = clCancel.GrossAdmin.ToString("#,##0.00");
        }

        protected void btnDeny_Click(object sender, EventArgs e)
        {
            UpdateProcess();
            DoPopup();
        }

        private void DoPopup()
        {
            RadWindowManager1.RadAlert("Contract in Moxy needs to be placed in Sold Status", 400, 100, "Deny", "");
        }

        private void UpdateProcess()
        {
            clsDBO clR = new clsDBO();
            string SQL = "update contractcancelmoxy set processedcancel = 1 where contractid = " + hfContractID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            MoveMOxyCancellation();
            UpdateProcess();
            RadWindowManager1.RadAlert("Accept is complate", 400, 100, "Accept", "");
        }

        private void MoveMOxyCancellation()
        {
            clsDBO clCCM = new clsDBO();
            clsDBO clCC = new clsDBO();
            var clP = new VeritasGlobalToolsV2.clsPayee();
            string SQL = "select * from contractcancelmoxy where contractid = " + Request.QueryString["contractid"];
            clCCM.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCCM.RowCount() > 0) {
                clCCM.GetRow();
                SQL = "select * from contractcancel ";
                SQL = SQL + "where contractid = " + Request.QueryString["contractid"];
                clCC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clCC.RowCount() > 0)
                    clCC.GetRow();
                else
                    clCC.NewRow();

                clCC.SetFields("contractid", Request.QueryString["contractid"]);
                clCC.SetFields("cancelstatus", clCCM.GetFields("cancelstatus"));
                clCC.SetFields("cancelmile", clCCM.GetFields("cancelmile"));
                clCC.SetFields("termfactor", clCCM.GetFields("termfactor"));
                clCC.SetFields("milefactor", clCCM.GetFields("milefactor"));
                clCC.SetFields("claimamt", clCCM.GetFields("claimamt"));
                clCC.SetFields("cancelfactor", clCCM.GetFields("cancelfactor"));
                clCC.SetFields("quotedate", clCCM.GetFields("canceldate"));
                clCC.SetFields("requestdate", clCCM.GetFields("canceldate"));
                clCC.SetFields("canceleffdate", clCCM.GetFields("canceleffdate"));
                clCC.SetFields("adminamt", clCCM.GetFields("adminamt"));
                clCC.SetFields("dealeramt", clCCM.GetFields("dealeramt"));
                clCC.SetFields("cancelfeeamt", clCCM.GetFields("cancelfeeamt"));
                clCC.SetFields("creby", clCCM.GetFields("creby"));
                clCC.SetFields("credate", clCCM.GetFields("credate"));

                if (rbCustomer.Checked == true) 
                {
                    clP.ContractID = Convert.ToInt64(hfContractID.Value);
                    clP.AddCustomerToPayee();
                    clCC.SetFields("payeeid", clP.PayeeID.ToString());
                }
                if (rbDealer.Checked == true) 
                {
                    clP.ContractID = Convert.ToInt64(hfContractID.Value);
                    clP.AddDealerToPayee();
                    clCC.SetFields("payeeid", clP.PayeeID.ToString());
                }
                if (rbOther.Checked == true) 
                {
                    clP.FName = txtFName.Text;
                    clP.CompanyName = txtCompanyName.Text;
                    clP.LName = txtLName.Text;
                    clP.Addr1 = txtAddr1.Text;
                    clP.Addr2 = txtAddr2.Text;
                    clP.City = txtCity.Text;
                    clP.State = cboState.Text;
                    clP.Zip = txtZip.Text;
                    clP.Phone = txtPhone.Text;
                    clP.AddOtherToPayee(hfContractID.Value);
                    clCC.SetFields("payeeid", clP.PayeeID.ToString());
                }

                clCC.SetFields("dealer", rbDealer.Checked.ToString());
                clCC.SetFields("Customer", rbCustomer.Checked.ToString());
                clCC.SetFields("Other", rbCustomer.Checked.ToString());
                clCC.SetFields("grosscustomer", clCCM.GetFields("grosscustomer"));
                clCC.SetFields("grossadmin", clCCM.GetFields("grossadmin"));
                clCC.SetFields("grossdealer", clCCM.GetFields("grossdealer"));
                if (clCC.RowCount() == 0)
                    clCC.AddRow();

                clCC.SaveDB();
            }

            SQL = "update contract ";
            SQL = SQL + "set status = 'Cancelled' ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clCC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            Response.Redirect("~/contract/MoxyCancellation.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value);
        }

        protected void rbOther_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOther.Checked)
                pnlOther.Visible = true;
            else
                pnlOther.Visible = false;
        }
    }
}