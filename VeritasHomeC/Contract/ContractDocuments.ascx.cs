﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractDocuments : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            hfContractID.Value = Request.QueryString["contractid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlList.Visible = true;
                pnlAdd.Visible = false;
                pnlDetail.Visible = false;
                FillGrid();
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            clsDBO clR = new clsDBO();
            string SQL = "select * from usersecurityinfo where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnSave.Enabled = false;
                    btnUpload.Enabled = false;
                    btnDelete.Enabled = false;
                }
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillGrid()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select contractdocumentid, documentname, documentdesc, documentlink ";
            SQL = SQL + "from contractdocument ";
            SQL = SQL + "where contractid = " + hfContractID.Value + " ";
            SQL = SQL + "and deleted = 0 ";
            rgContractDocument.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgContractDocument.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = true;
            pnlList.Visible = false;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string sDocLink, SQL;
            clsDBO clR = new clsDBO();
            GetContractNo();
            string folderpath = Server.MapPath("~") + @"\documents\contracts\" + hfContractNo.Value + "_" + FileUpload2.FileName;
            sDocLink = "~/documents/contracts/" + hfContractNo.Value + "_" + FileUpload2.FileName;
            FileUpload2.SaveAs(folderpath);
            SQL = "select * from contractdocument where contractid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            clR.SetFields("contractid", hfContractID.Value);
            clR.SetFields("documentname", txtDocName.Text);
            clR.SetFields("documentdesc", txtDocDesc.Text);
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.AddRow();
            clR.SaveDB();
            pnlAdd.Visible = false;
            pnlList.Visible = true;
            FillGrid();
        }

        private void GetContractNo()
        {
            string SQL = "select contractno from contract where contractid = " + hfContractID.Value;
            clsDBO clR = new clsDBO();
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractNo.Value = clR.GetFields("contractno");
            }
        }

        protected void rgContractDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            hfDocID.Value = rgContractDocument.SelectedValue.ToString();
            FillDetail();
        }

        private void FillDetail()
        {
            string SQL = "select * from contractdocument where contractdocumentid = " + hfDocID.Value;
            clsDBO clR = new clsDBO();
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCreBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clR.GetFields("creby")));
                txtCreDate.Text = clR.GetFields("credate");
                txtDescDetail.Text = clR.GetFields("documentdesc");
                txtModBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clR.GetFields("modby")));
                txtTitleDetail.Text = clR.GetFields("documentname");
                txtModDate.Text = clR.GetFields("moddate");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "update contractdocument ";
            SQL = SQL + "set deleted = 1 ";
            SQL = SQL + "where contractdocumentid = " + hfDocID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from contractdocument where contractdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("documentname", txtTitleDetail.Text);
                clR.SetFields("documentdesc", txtDescDetail.Text);
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("moddate", DateTime.Now.ToString());
                clR.SaveDB();
            }
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = false;
            pnlList.Visible = true;
        }
    }
}