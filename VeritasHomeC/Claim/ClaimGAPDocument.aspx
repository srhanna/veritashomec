﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimGAPDocument.aspx.cs" Inherits="VeritasHomeC.ClaimGAPDocument" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
     <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlList" runat="server" DefaultButton="btnHiddenList">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add Document" CssClass="button1" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimDocument" AutoGenerateColumns="false" OnSelectedIndexChanged="rgClaimDocument_SelectedIndexChanged" runat="server" AllowSorting="true" AllowPaging="true" 
                                    Width="1000" ShowFooter="true" DataSourceID="dsDocs">
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimGAPDocumentID" PageSize="10" DataSourceID="dsDocs">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimGapDocumentID"  ReadOnly="true" Visible="false" UniqueName="ClaimDocumentID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimGapDocType" UniqueName="ClaimGapDocType" HeaderText="Document Type"></telerik:GridBoundColumn>
                                            <telerik:GridHyperLinkColumn DataTextField="DocumentName" Target="_blank" DataNavigateUrlFields="DocumentLink" UniqueName="DocumentLink" HeaderText="Document Name"></telerik:GridHyperLinkColumn>
                                            <telerik:GridBoundColumn DataField="DocumentDesc" UniqueName="DocumentDesc" HeaderText="Description"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Create Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsDocs" ProviderName="System.Data.SqlClient" 
                                    SelectCommand="select claimgapdocumentid, ClaimGapDocType, documentdesc,documentname, documentlink, credate
                                    from claimgapdocument d
                                    inner join ClaimGapDocType dt on dt.ClaimGapDocTypeid = d.ClaimGAPDocTypeID
                                    where claimgapid =  @ClaimID and deleted = 0 " 
                                    runat="server">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenList" Visible="false" runat="server" Text="Button" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlDetail" runat="server">
                     <asp:Table runat="server">
                         <asp:TableRow>
                             <asp:TableCell>
                                 <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Document Type:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboDocType" DataSourceid="dsDocType" DataValueField="claimgapdoctypeid" DataTextField="claimgapdoctype" runat="server"></asp:DropDownList>
                                            <asp:SqlDataSource ID="dsDocType" ProviderName="System.Data.SqlClient"
                                                SelectCommand="select claimgapdoctypeid, claimgapdoctype from claimgapdoctype 
                                                order by claimgapdoctypeid" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Title:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtTitleDetail" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Description
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtDescDetail" Width="400" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                 </asp:Table>
                             </asp:TableCell>
                         </asp:TableRow>
                         <asp:TableRow>
                             <asp:TableCell>
                                 <asp:Table runat="server">
                                     <asp:TableRow>
                                         <asp:TableCell Font-Bold="true">
                                             Created By:
                                         </asp:TableCell>
                                         <asp:TableCell>
                                             <asp:TextBox ID="txtCreBy" runat="server"></asp:TextBox>
                                         </asp:TableCell>
                                         <asp:TableCell Font-Bold="true">
                                             Create Date:
                                         </asp:TableCell>
                                         <asp:TableCell>
                                             <asp:TextBox ID="txtCreDate" runat="server"></asp:TextBox>
                                         </asp:TableCell>
                                         <asp:TableCell Font-Bold="true">
                                             Modified By:
                                         </asp:TableCell>
                                         <asp:TableCell>
                                             <asp:TextBox ID="txtModBy" runat="server"></asp:TextBox>
                                         </asp:TableCell>
                                         <asp:TableCell Font-Bold="true">
                                             Modified Date:
                                         </asp:TableCell>
                                         <asp:TableCell>
                                             <asp:TextBox ID="txtModDate" runat="server"></asp:TextBox>
                                         </asp:TableCell>
                                     </asp:TableRow>
                                 </asp:Table>
                             </asp:TableCell>
                         </asp:TableRow>
                         <asp:TableRow>
                             <asp:TableCell HorizontalAlign="Right">
                                 <asp:Table runat="server">
                                     <asp:TableRow>
                                         <asp:TableCell>
                                               <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="Close" BackColor="#1eabe2"/>
                                         </asp:TableCell>
                                         <asp:TableCell>
                                               <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" Text="Delete" BackColor="#1eabe2"/>
                                         </asp:TableCell>
                                         <asp:TableCell>
                                               <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" BackColor="#1eabe2"/>
                                         </asp:TableCell>
                                     </asp:TableRow>
                                 </asp:Table>
                             </asp:TableCell>
                         </asp:TableRow>
                     </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlAdd" runat="server" DefaultButton="btnHiddenDetail">
                    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" Width="300px" PostBackControls="btnUpload">
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Document Type:
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="cboDocType2" DataSourceid="dsDocType2" DataValueField="claimgapdoctypeid" DataTextField="claimgapdoctype" runat="server"></asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDocType2" ProviderName="System.Data.SqlClient"
                                        SelectCommand="select claimgapdoctypeid, claimgapdoctype from claimgapdoctype 
                                        order by claimgapdoctypeid" runat="server"></asp:SqlDataSource>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Title:
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtDocName" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Description
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtDocDesc" Width="400" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" Height="200px" Width="300px" LoadingPanelID="RadAjaxLoadingPanel1">
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:FileUpload ID="FileUpload2" runat="server" />
                                                    <asp:Button ID="btnUpload" OnClick="btnUpload_Click" runat="server" Text="Upload" BackColor="#1eabe2"/>
                                                    <asp:Button ID="btnCloseAdd" OnClick="btnCloseAdd_Click" runat="server" Text="Close" BackColor="#1eabe2"/>
                                                    <asp:Button ID="btnHiddenDetail" Visible="false" runat="server" Text="Button" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </telerik:RadAjaxPanel>
                                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Transparency="30" >
                                        <asp:Label ID="Label1" runat="server" Text="Loading...."></asp:Label>
                                    </telerik:RadAjaxLoadingPanel>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </telerik:RadAjaxPanel>
                </asp:Panel>

                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfClaimNo" runat="server" />
                <asp:HiddenField ID="hfDocID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
