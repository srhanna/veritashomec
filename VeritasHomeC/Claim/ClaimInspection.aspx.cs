﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics;

namespace VeritasHomeC
{
    public partial class ClaimInspection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];

            if (!IsPostBack)
            {
                GetServerInfo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                FillGrid();
                if (CheckLock())
                {
                    btnSave.Visible = false;
                    btnAddInspection.Visible = false;
                }
                else
                {
                    btnSave.Visible = true;
                    btnAddInspection.Visible = true;
                }
                ReadOnlyButtons();
                lblError2.Visible = false;
                pnlTasaEmail.Visible = false;
                pnlTASAConfirm.Visible = false;
                pnlANReason.Visible = false;
            }
        }

        private void CheckView()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claiminspection ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and viewdate is null ";
            SQL = SQL + "and not resulturl is null ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                SQL = "update claiminspection ";
                SQL = SQL + "set viewdate = '" + DateTime.Today + "', ";
                SQL = SQL + "viewby = " + hfUserID.Value + " ";
                SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
                SQL = SQL + "and viewdate is null ";
                SQL = SQL + "and not resulturl is null ";
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnSave.Enabled = false;
                    btnAddInspection.Enabled = false;
                }
            }
        }

        private bool CheckLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "Select claimid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (lockuserid = " + hfUserID.Value + " ";
            SQL = SQL + "or lockuserid is null) ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                return true;
            else
                return false;
        }

        private void FillGrid()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select claiminspectionid, inspectno, inspectnote, fname + ' ' + lname as RequestBy, requestdate, resulturl, imagesurl, detailsurl ";
            SQL = SQL + "from claiminspection ci ";
            SQL = SQL + "left join userinfo ui on ci.requestby = ui.userid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            rgClaimInspect.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgClaimInspect.Rebind();
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void LockButtons()
        {
            btnAddCarFax.Enabled = false;
            btnAddInspection.Enabled = false;
            btnSave.Enabled = false;
        }

        private void UnlockButtons()
        {
            btnAddInspection.Enabled = true;
            btnAddCarFax.Enabled = true;
            btnSave.Enabled = true;
        }

        protected void btnAddInspection_Click(object sender, EventArgs e)
        {
            if (hfUserID.Value != "17" && hfUserID.Value != "18" && hfUserID.Value != "20" && hfUserID.Value != "12" && hfUserID.Value != "1241" && hfUserID.Value != "1227")
            {
                if (CheckANDealer() && CheckANRF() && CheckAgeUnder())
                {
                    pnlTASAConfirm.Visible = true;
                    pnlList.Visible = false;
                    return;
                }

                if (CheckANRF() && CheckClaimAmt() && CheckAge())
                {
                    pnlANReason.Visible = true;
                    pnlList.Visible = false;
                    txtInspectReasonNote.Content = "";
                    return;
                }

                if (CheckANDealer() && CheckANRF() && CheckClaimAmtOver())
                {
                    pnlTASAConfirm.Visible = true;
                    pnlList.Visible = false;
                    return;
                }
            }

            pnlDetail.Visible = true;
            pnlList.Visible = false;
            btnSave.Visible = true;
            hfAdd.Value = "True";
            FillInspectType();
            lblError.Visible = false;
            GetInspectNo();
            trInspectResult.Visible = false;
        }

        private bool CheckClaimAmtOver()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select sum(totalamt) as AMT from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'Authorized' ";
            SQL = SQL + "or claimdetailstatus = 'Approved' ";
            SQL = SQL + "or claimdetailstatus = 'Paid') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (Convert.ToInt64(clR.GetFields("amt")) > 1500)
                        return true;
                }
            }

            return false;
        }

        private bool CheckAgeUnder()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            long lDays;
            SQL = "select credate from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lDays = clsFunc.DateDiffDays(Convert.ToDateTime(clR.GetFields("credate")), DateTime.Today);
                if (lDays < 60)
                    return true;
            }

            return false;
        }

        private bool CheckAge()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            long lDays;
            SQL = "select credate from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lDays = clsFunc.DateDiffDays(Convert.ToDateTime(clR.GetFields("credate")), DateTime.Today);
                if (lDays > 60)
                {
                    return true;
                }
            }

            return false;
        }

        private bool CheckClaimAmt()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select sum(totalamt) as AMT from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'Authorized' ";
            SQL = SQL + "or claimdetailstatus = 'Approved' ";
            SQL = SQL + "or claimdetailstatus = 'Paid') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (Convert.ToInt64(clR.GetFields("amt")) < 1500)
                        return true;
                }
            }

            return false;
        }

        private bool CheckANRF()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            SQL = SQL + "and servicecenterid in (";
            SQL = SQL + "select servicecenterid from servicecenter ";
            SQL = SQL + "where dealerno like '2%') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private bool CheckANDealer()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            SQL = SQL + "and contractid in (";
            SQL = SQL + "select contractid from contract where dealerid in (";
            SQL = SQL + "select dealerid from dealer where dealerno like '2%')) ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private void GetInspectNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select count(*) as cnt from claiminspection ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtInspectNo.Text = (Convert.ToInt64(clR.GetFields("cnt")) + 1).ToString();
            }
            else
                txtInspectNo.Text = "1";
        }

        private void FillInspectType()
        {
            cboInspectType.Items.Add("");
            cboInspectType.Items.Add("Inspection Only");
            cboInspectType.Items.Add("Inspection and Oil");
            cboInspectType.Items.Add("No Inspection");
            cboInspectType.Items.Add("Oil Only");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            long lInspectTry = 0;
            lblError.Visible = false;
            UpdateClaimInspect();
            UpdateClaimDetail();
            UpdateClaim();
            if (hfAdd.Value == "True")
            {
                if (!chkTruePic.Checked)
                {
                    if (cboInspectType.Text != "") 
                    {
                        if (cboInspectType.Text != "No Inspect")
                        {
                            if (!ConfigurationManager.AppSettings["connstring"].ToLower().Contains("test"))
                            {
                            MoveHere:;

                                lInspectTry = lInspectTry + 1;
                                ProcessStartInfo p = new ProcessStartInfo();
                                p.FileName = @"C:\ProcessProgram\WIS\WISInspect.exe";
                                p.Arguments = "-claimid " + hfClaimID.Value + " -inspectno " + txtInspectNo.Text + " -inspecttype Inspection";
                                p.WindowStyle = ProcessWindowStyle.Hidden;
                                Process pr = new Process();
                                pr.StartInfo = p;
                                pr.Start();
                                pr.WaitForExit();

                                if (!CheckInspect())
                                {
                                    if (lInspectTry == 3)
                                    {
                                        PlaceError();
                                        return;
                                    }
                                    goto MoveHere;
                                }
                            } 
                        }
                    }
                }
            }

            FillGrid();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        private void PlaceError()
        {
            lblError.Visible = true;
            lblError.Text = "Could not transmit to WIS for Inspection. Please try again.";
        }

        private bool CheckInspect()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claiminspection ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and inspectno = " + txtInspectNo.Text + " ";
            SQL = SQL + "and truepic = 0 ";
            SQL = SQL + "and not inspectionid is null ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private void UpdateClaimDetail()
        {
            string SQL;
            clsDBO clCD = new clsDBO();
            double sAmt = 0;

            if (cboInspectType.Text == "No Inspection")
                return;

            if (cboInspectType.Text == "Inspection Only")
                sAmt = 86;

            if (cboInspectType.Text == "Inspection and Oil")
                sAmt = 136;

            if (cboInspectType.Text == "Oil Only")
                sAmt = 50;

            if (chkTruePic.Checked)
                sAmt = 25;

            if (sAmt > 0) {
                SQL = "select * from claimdetail ";
                SQL = SQL + "where jobno = 'A02' ";
                SQL = SQL + "and qty = " + txtInspectNo.Text + " ";
                SQL = SQL + "and claimid = " + hfClaimID.Value;
                clCD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

                if (clCD.RowCount() > 0)
                    clCD.GetRow();
                else
                    clCD.NewRow();


                clCD.SetFields("claimid", hfClaimID.Value);
                clCD.SetFields("ClaimDetailType", "Inspect");
                clCD.SetFields("jobno", "A02");
                clCD.SetFields("qty", txtInspectNo.Text);
                clCD.SetFields("ratetypeid", "1");
                clCD.SetFields("reqqty", "1");
                clCD.SetFields("reqamt", sAmt.ToString());
                clCD.SetFields("reqcost", sAmt.ToString());
                clCD.SetFields("LossCode", "IFPend");
                clCD.SetFields("AuthAmt", "0");
                clCD.SetFields("taxamt", "0");
                clCD.SetFields("paidamt", "0");
                clCD.SetFields("claimdetailstatus", "Requested");
                if (chkTruePic.Checked)
                    clCD.SetFields("claimpayeeid", "10459");
                else
                    clCD.SetFields("claimpayeeid", "5771");


                if (clCD.GetFields("authamt").Length > 0)
                    if (Convert.ToInt64(clCD.GetFields("authamt")) > 0)
                        clCD.SetFields("authamt", sAmt.ToString());



                clCD.SetFields("moddate", DateTime.Today.ToString());
                clCD.SetFields("modby", hfUserID.Value);
                if (clCD.RowCount() == 0) 
                {
                    clCD.SetFields("credate", DateTime.Today.ToString());
                    clCD.SetFields("creby", hfUserID.Value);
                    clCD.AddRow();
                }

                clCD.SaveDB();
            }
        }

        private void UpdateClaimInspect()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sInspectType = "";

            if (cboInspectType.Text == "") {
                lblError.Text = "Please select an Inspection Type.";
                lblError.Visible = true;
                return;
            }

            if (cboInspectType.Text == "Inspection Only")
                sInspectType = "Inspection";

            if (cboInspectType.Text == "Inspection and Oil")
                sInspectType = "InspectionOil";

            if (cboInspectType.Text == "Oil Only")
                sInspectType = "Oil";

            if (cboInspectType.Text == "No Inspection")
                sInspectType = "NoInspect";

            SQL = "select * from claiminspection ";
            SQL = SQL + "where inspectno = " + txtInspectNo.Text + " ";
            SQL = SQL + "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("truepic", chkTruePic.Checked.ToString());
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("inspectno", txtInspectNo.Text);
            clR.SetFields("InspectType", sInspectType);
            clR.SetFields("InspectNote", txtInspectNote.Text);
            clR.SetFields("resulturl", txtInspectResult.Text);
            clR.SetFields("truepic", chkTruePic.Checked.ToString());
            if (clR.RowCount() == 0) {
                clR.SetFields("requestby", hfUserID.Value);
                clR.SetFields("requestdate", DateTime.Now.ToString());
                clR.AddRow();
            }

            clR.SaveDB();
        }

        private void UpdateClaim()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "update claim ";
            SQL = SQL + "set inspect = 1 ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnAddNoInspect_Click(object sender, EventArgs e)
        {
            hfAdd.Value = "False";
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            btnSave.Visible = true;

            cboInspectType.Items.Clear();
            cboInspectType.Items.Add("No Inspection");

            lblError.Visible = false;
            txtInspectNo.Text = "0";
            trInspectResult.Visible = false;
        }

        protected void btnAddCarFax_Click(object sender, EventArgs e)
        {
            hfAdd.Value = "False";
            AddClaimDetail();
            AddInspection();
            FillGrid();
        }

        private void AddInspection()
        {
            string SQL;
            clsDBO clCI = new clsDBO();
            SQL = "select * from claiminspection ";
            SQL = SQL + "where claiminspectionid = 0 ";
            clCI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCI.RowCount() == 0)
            {
                clCI.NewRow();
                clCI.SetFields("claimid", hfClaimID.Value);
                clCI.SetFields("inspectno", "0");
                clCI.SetFields("inspectionid", "0");
                clCI.SetFields("inspectnote", "CarFax");
                clCI.SetFields("requestby", hfUserID.Value);
                clCI.SetFields("requestdate", DateTime.Today.ToString());
                clCI.AddRow();
                clCI.SaveDB();
            }
        }

        private void AddClaimDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string sContractNo;

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = 'a03' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0) {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimdetailtype", "Other");
                clR.SetFields("JobNo", "A03");
                clR.SetFields("ratetypeid", "1");
                clR.SetFields("qty", "1");
                clR.SetFields("cost", "13");
                clR.SetFields("losscode", "ZCARFAX");
                clR.SetFields("reqamt", "13");
                clR.SetFields("authamt", "13");
                clR.SetFields("taxamt", "0");
                clR.SetFields("totalamt", "13");
                clR.SetFields("dateauth", DateTime.Now.ToString());
                clR.SetFields("authby", hfUserID.Value);
                clR.SetFields("credate", DateTime.Now.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("moddate", DateTime.Now.ToString());
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("claimpayeeid", "4882");
                clR.SetFields("taxamt", "0");
                clR.SetFields("paidamt", "0");
                clR.SetFields("claimdetailstatus", "Authorized");
                clR.AddRow();
                clR.SaveDB();
                SQL = "select * from usermessage ";
                SQL = SQL + "where idx = 0 ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() == 0) {
                    sContractNo = GetContractNo();
                    clR.NewRow();
                    clR.SetFields("toid", hfUserID.Value);
                    clR.SetFields("fromid", "1187");
                    clR.SetFields("header", "Carfax - " + sContractNo);
                    clR.SetFields("message", "Attach carfax to the contract documents for " + sContractNo);
                    clR.SetFields("messagedate", DateTime.Now.ToString());
                    clR.AddRow();
                    clR.SaveDB();
                }
            }
        }

        protected void rgClaimInspect_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAdd.Value = "False";
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            btnSave.Visible = true;
            FillDetail();
        }

        private void FillDetail()
        {
            string SQL;
            clsDBO clCI = new clsDBO();
            SQL = "select * from claiminspection ";
            SQL = SQL + "where claiminspectionid = " + rgClaimInspect.SelectedValue;
            clCI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCI.RowCount() > 0)
            {
                clCI.GetRow();
                if (clCI.GetFields("inspecttype") == "NoInspect")
                    cboInspectType.Text = "No Inspection";

                if (clCI.GetFields("inspecttype") == "Inspection")
                    cboInspectType.Text = "Inspection Only";

                txtInspectNo.Text = clCI.GetFields("inspectno");
                txtInspectNote.Text = clCI.GetFields("inspectnote");
                txtInspectResult.Text = clCI.GetFields("resulturl");
                chkTruePic.Checked = Convert.ToBoolean(clCI.GetFields("truepic"));
            }
        }

        private string GetContractNo()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select contractno from contract c ";
            SQL = SQL + "inner join claim cl on cl.contractid = c.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                return clC.GetFields("contractno");
            }

            return "";
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnReasonNoteSave_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            if (txtInspectReasonNote.Text.Length == 0)
                return;

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnoteid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", "5");
                clR.SetFields("note", txtInspectReasonNote.Content);
                clR.SetFields("NoteText", txtInspectReasonNote.Text);
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
                clR.AddRow();
                clR.SaveDB();
            }

            pnlDetail.Visible = true;
            pnlList.Visible = false;
            pnlANReason.Visible = false;
            btnSave.Visible = true;
            hfAdd.Value = "True";
            FillInspectType();
            GetInspectNo();
            trInspectResult.Visible = false;
            lblError2.Visible = false;
        }

        protected void btnTASANo_Click(object sender, EventArgs e)
        {
            pnlTASAConfirm.Visible = false;
            pnlList.Visible = true;
            lblError2.Text = "Management Override required";
            lblError2.Visible = true;
        }

        protected void btnTASAYes_Click(object sender, EventArgs e)
        {
            lblError2.Visible = false;
            pnlTasaEmail.Visible = true;
            txtNote.Content = "";
            pnlTASAConfirm.Visible = false;
        }

        protected void btnSaveNote_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            if (txtNote.Text.Length == 0)
                return;
            SQL = "select * from claimnote";
            SQL = SQL + "where claimnoteid = 0";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid","5");
                clR.SetFields("note", txtNote.Content);
                clR.SetFields("NoteText", txtNote.Text);
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
                clR.AddRow();
                clR.SaveDB();
            }

            pnlDetail.Visible = true;
            pnlList.Visible = false;
            pnlTasaEmail.Visible = false;
            btnSave.Visible = true;
            hfAdd.Value = "True";
            FillInspectType();
            GetInspectNo();
            trInspectResult.Visible = false;
        }

        protected void btnCancelNote_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            pnlTasaEmail.Visible = false;
            btnSave.Visible = true;
            hfAdd.Value = "True";
            FillInspectType();
            GetInspectNo();
            trInspectResult.Visible = false;
        }
    }
}