﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGAPNote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNote.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsNoteType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                rgNote.Rebind();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            txtNote.Content = "";
            txtCreBy.Text = "";
            txtCreDate.Text = "";
            hfClaimNoteID.Value = "0";
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            hfClaimNoteID.Value = rgNote.SelectedValue.ToString();
            SQL = "select * from claimgapnote cn ";
            SQL = SQL + "inner join userinfo ui on ui.userid = cn.creby ";
            SQL = SQL + "where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboNoteType.SelectedValue = clR.GetFields("claimgapnotetypeid");
                txtNote.Content = clR.GetFields("note");
                txtCreBy.Text = clR.GetFields("username");
                txtCreDate.Text = clR.GetFields("credate");
                btnSave.Visible = true;
                if (hfUserID.Value != clR.GetFields("creby"))
                    btnSave.Visible = false;

                if (clsFunc.DateDiffHours(Convert.ToDateTime(txtCreDate.Text), DateTime.Now) > 24)
                {
                    btnSave.Visible = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            if (hfClaimNoteID.Value.Length == 0)
                hfClaimNoteID.Value = "0";

            SQL = "select * from claimgapnote cn ";
            SQL = SQL + "where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("claimgapid", hfClaimID.Value);
            clR.SetFields("claimgapnotetypeid", cboNoteType.SelectedValue);
            string sTemp;
            sTemp = txtNote.Text;
            clR.SetFields("note", txtNote.Content);
            clR.SetFields("notetext", sTemp);
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.RowCount() == 0) {
                clR.SetFields("credate", DateTime.Now.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }
            clR.SaveDB();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.Rebind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.SelectedIndexes.Clear();
        }
    }
}