﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class HCC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hfClaimID.Value = Request.QueryString["claimid"];
                FillScreen();
            }
        }

        private void FillScreen()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimhcc ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                lblDateSubmitted.Text = Convert.ToDateTime(clR.GetFields("datesubmitted")).ToString("d");
                if (clR.GetFields("adjusterid").Length > 0)
                    lblAdjusterName.Text = clsFunc.GetUserName(clR.GetFields("adjusterid"));

                CalcClaimInfo();
                CalcContractInfo();
                CalcDealer();
                CalcAgent();
                CalcServiceCenter();
                CalcInsCarrier();
                lblDaysIntoContract.Text = clR.GetFields("daysintocontract");
                lblMilesIntoContract.Text = clR.GetFields("milesintocontract");
                lblLimitofLiability.Text = clR.GetFields("LimitOfLiability");
                if (clR.GetFields("nadavalue").Length > 0)
                    lblNADA.Text = Convert.ToDouble(clR.GetFields("NADAValue")).ToString("#,##0.00");

                txtDescribeFailure.Text = clR.GetFields("DescFailure");

                if (clR.GetFields("claimcost").Length > 0)
                    lblClaimCost.Text = Convert.ToDouble(clR.GetFields("claimcost")).ToString("#,##0.00");

                if (clR.GetFields("LaborCostPerHour").Length > 0)
                    lblLaborCostPerHour.Text = Convert.ToDouble(clR.GetFields("laborcostperhour")).ToString("#,##0.00");

                if (clR.GetFields("averagecost").Length > 0)
                    lblAverageCost.Text = Convert.ToDouble(clR.GetFields("averagecost")).ToString("#,###.00");
            }
        }

        private void CalcInsCarrier()
        {
            clsDBO clI = new clsDBO();
            string SQL;
            SQL = "select * from inscarrier ";
            SQL = SQL + "where inscarrierid = " + hfInsCarrierID.Value;
            clI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clI.RowCount() > 0)
            {
                clI.GetRow();
                lblInsCarrier.Text = clI.GetFields("inscarriername");
            }
        }

        private void CalcServiceCenter()
        {
            clsDBO clSC = new clsDBO();
            string SQL;
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = " + hfInsCarrierID.Value;
            clSC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSC.RowCount() > 0)
            {
                clSC.GetRow();
                lblServiceCenterName.Text = clSC.GetFields("servicecentername");
            }
        }

        private void CalcAgent()
        {
            clsDBO clA = new clsDBO();
            string SQL;
            SQL = "select * from agents ";
            SQL = SQL + "where agendid = " + hfInsCarrierID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                lblAgentName.Text = clA.GetFields("agentname");
            }
        }

        private void CalcDealer()
        {
            clsDBO clD = new clsDBO();
            string SQL;
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfInsCarrierID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                lblDealerName.Text = clD.GetFields("dealername");
            }
        }

        private void CalcClaimInfo()
        {
            clsDBO clCl = new clsDBO();
            string SQL;
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfInsCarrierID.Value;
            clCl.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCl.RowCount() > 0)
            {
                clCl.GetRow();
                hfContractID.Value = clCl.GetFields("contractid");
                lblClaimNo.Text = clCl.GetFields("claimno");
                hfServiceCenterID.Value = clCl.GetFields("servicecenterid");

                if (clCl.GetFields("lossdate").Length > 0)
                    lblClaimDate.Text = Convert.ToDateTime(clCl.GetFields("lossdate")).ToString("d");

                lblCurrentMile.Text = clCl.GetFields("lossmile");
            }
            else
            {
                hfContractID.Value = "0";
                hfServiceCenterID.Value = "0";
            }
        }

        private void CalcContractInfo()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from contract ";
            SQL = SQL + "where contractid = " + hfInsCarrierID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lblContractNo.Text = clR.GetFields("contractno");
                lblCustomerName.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
                hfDealerID.Value = clR.GetFields("dealerid");
                hfAgentID.Value = clR.GetFields("agentsid");
                hfInsCarrierID.Value = clR.GetFields("inscarrierid");

                if (clR.GetFields("saledate").Length > 0)
                    lblSaleDate.Text = Convert.ToDateTime(clR.GetFields("saledate")).ToString("d");

                lblMakeModel.Text = clR.GetFields("Make") + "/" + clR.GetFields("model");
            }
            else
            {
                hfAgentID.Value = "0";
                hfDealerID.Value = "0";
                hfInsCarrierID.Value = "0";
            }
        }
    }
}