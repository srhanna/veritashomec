﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            hfClaimID.Value = Request.QueryString["claimid"];
            dsDocs.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            if (!IsPostBack)
            {
                GetServerInfo();
                pnlList.Visible = true;
                pnlAdd.Visible = false;
                pnlDetail.Visible = false;
                FillGrid();
                if (CheckLock())
                {
                    btnAdd.Visible = false;
                    btnSave.Visible = false;
                    btnUpload.Visible = false;
                }
                else
                {
                    btnAdd.Visible = true;
                    btnSave.Visible = true;
                    btnUpload.Visible = true;
                }
                ReadOnlyButtons();
                rgClaimDocument.Rebind();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnAdd.Enabled = false;
                    btnSave.Enabled = false;
                    btnUpload.Enabled = false;
                }
            }
        }

        private bool CheckLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "Select claimid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                return false;
            else
                return true;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillGrid()
        {
            //clsDBO clR = new clsDBO();
            //string SQL;

            //SQL = "select claimdocumentid, documentname, documentdesc, documentlink, credate ";
            //SQL = SQL + "from claimdocument ";
            //SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            //SQL = SQL + "and deleted = 0 ";
            //rgClaimDocument.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgClaimDocument.Rebind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = true;
            pnlList.Visible = false;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string sDocLink;
            string SQL;
            clsDBO clR = new clsDBO();
            GetClaimNo();
            string folderPath = Server.MapPath("~") + @"\documents\claims\" + hfClaimNo.Value + "_" + FileUpload2.FileName;
            sDocLink = "~/documents/claims/" + hfClaimNo.Value + "_" + FileUpload2.FileName;
            FileUpload2.SaveAs(folderPath);

            SQL = "select * from claimdocument ";
            SQL = SQL + "where claimid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("documentname", txtDocName.Text);
            clR.SetFields("documentdesc", txtDocDesc.Text);
            clR.SetFields("documentlink", sDocLink);
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.AddRow();
            clR.SaveDB();
            pnlAdd.Visible = false;
            pnlList.Visible = true;
            FillGrid();
        }

        private void GetClaimNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select claimno from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimNo.Value = clR.GetFields("claimno");
            }
        }

        protected void rgClaimDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlList.Visible = true;
            pnlDetail.Visible = true;
            hfDocID.Value = rgClaimDocument.SelectedValue.ToString();
            FillDetail();
        }

        private void FillDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimdocument ";
            SQL = SQL + "where claimdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtCreBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clR.GetFields("creby")));
                txtCreDate.Text = clR.GetFields("credate");
                txtDescDetail.Text = clR.GetFields("documentdesc");
                txtModBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clR.GetFields("modby")));
                txtTitleDetail.Text = clR.GetFields("documentname");
                txtModDate.Text = clR.GetFields("moddate");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "update claimdocument ";
            SQL = SQL + "set deleted = 1 ";
            SQL = SQL + "where claimdocumentid = " + hfDocID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimdocument ";
            SQL = SQL + "where claimdocumentid = " + hfDocID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("documentname", txtTitleDetail.Text);
                clR.SetFields("documentdesc", txtDescDetail.Text);
                clR.SetFields("modby", hfUserID.Value);
                clR.SetFields("moddate", DateTime.Now.ToString());
                clR.SaveDB();
            }

            pnlAdd.Visible = false;
            pnlList.Visible = true;
            rgClaimDocument.Rebind();
        }

        protected void btnCloseAdd_Click(object sender, EventArgs e)
        {
            pnlAdd.Visible = false;
            pnlList.Visible = true;
        }
    }
}