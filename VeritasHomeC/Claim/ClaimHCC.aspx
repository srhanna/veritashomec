﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimHCC.aspx.cs" Inherits="VeritasHomeC.ClaimHCC" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblError" ForeColor="Red" Visible="false" Font-Size="Large" runat="server" Text="Label"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Date Submitted:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtDateSubmitted" ReadOnly="true" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Adjuster Name:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtAdjuster" ReadOnly="true" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Days Into Contract:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtDaysIntoContract" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Miles Into Contract:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtMilesIntoContract" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Contract Limit of Liability:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtLimitofLiability" Width="150" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        NADA Value:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtNADA" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="button1" />
                                    </asp:TableCell>
                                    <asp:TableCell Visible="false">
                                        <asp:HyperLink ID="hlWorksheet" Target="_blank" runat="server">View Worksheet</asp:HyperLink>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Describe Failure:
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDescFailure" TextMode="MultiLine" Height="75" Width="750" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Claims Cost:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtClaimCost" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Labor Cost Per Hour:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtLaborCostPerHour" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Average Labor Cost:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtAverageCost" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Recommend Correction:
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtRecommendCorrect" TextMode="MultiLine" Height="75" Width="750" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Approve Correction:
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtApprovedCorrect" TextMode="MultiLine" Height="75" Width="750" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Original RO Amount:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtOriginalROAmount" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        MCE/Approval Amount:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtMCEApproveAmount" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Inspection Done:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkInspectionDone" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Teardown Done:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkTeardownDone" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Records Requested:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkRecordsRequestd" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Customer Statement Given:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkCustomerStatementGiven" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Parts/Labor MCE:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkPartsLaborMCE" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Insurer Approval:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkInsurerApproval" OnCheckedChanged="chkInsurerApproval_CheckedChanged" AutoPostBack="true" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="trInsurerApproval">
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Insurer Approval E-Mail:
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadEditor ID="txtInsurerApproval" EditModes="Design" ToolbarMode="ShowOnFocus" Width="1000" Height="400" runat="server"></telerik:RadEditor>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Explain the claim and why you recommending approval:
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtExplainClaim" TextMode="MultiLine" Height="75" Width="750" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnManagerApproval" OnClick="btnManagerApproval_Click" runat="server" Text="Manager Approval" CssClass="button1" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trManagerApp1">
                                    <asp:TableCell Font-Bold="true">
                                        Manager Approval:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtManagerSign" Font-Names="Barcelony" Font-Size="Large" Width="200" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtManagerApprovalDate" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trManagerApp2">
                                    <asp:TableCell Font-Bold="true">
                                        Print Name:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtManagerApprovalPrint" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCEOApproval" OnClick="btnCEOApproval_Click" runat="server" Text="CEO Approval" CssClass="button1" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trCEOApp1">
                                    <asp:TableCell Font-Bold="true">
                                        CEO Approval:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtCEOApprovalSign" Font-Names="Barcelony" Font-Size="Large" Width="200" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtCEOApprovalDate" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trCEOApp2">
                                    <asp:TableCell Font-Bold="true">
                                        Print Name:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtCEOApprovalPrint" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
