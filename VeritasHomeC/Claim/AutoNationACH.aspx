﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutoNationACH.aspx.cs" Inherits="VeritasHomeC.AutoNationACH" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>AutoNation ACH</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~/images/Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" OnClick="btnLossCode_Click" runat="server" Text="Loss Code"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" onclick="btnUnlockClaim_Click" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    Period Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpEndWeek" OnSelectedDateChanged="rdpEndWeek_SelectedDateChanged" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="rgClaim">
                                            <telerik:RadGrid ID="rgClaim" OnItemCommand="rgClaim_ItemCommand" runat="server" AutoGenerateColumns="false"
                                                AllowSorting="true" AllowPaging="true"  Width="1500" ShowFooter="true" DataSourceID="SQLDataSource1" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimID" PageSize="10" ShowFooter="true" CommandItemDisplay="TopAndBottom">
                                                <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimID"  ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DateApprove" UniqueName="DateApproved" HeaderText="Approved Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" UniqueName="Dealer" HeaderText="Seller Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Seller No" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="RONumber" UniqueName="RONumber" HeaderText="RONumber" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="VIN" UniqueName="VIN" HeaderText="VIN" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="LossDate" UniqueName="LossDate" HeaderText="Loss Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" HeaderText="Customer" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ProductCode" UniqueName="ProductCode" HeaderText="Product Code" ItemStyle-Width="150" HeaderStyle-Width="150"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="PaidAmt" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridClientSelectColumn UniqueName="ClaimSelect"></telerik:GridClientSelectColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings >
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource1"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select cl.claimid, cd.DateApprove, DealerName, d.DealerNo, c.FName + ' ' + c.LName as Customer, 
                                                    cl.RONumber, right(c.vin, 8) as VIN, cl.lossdate, cl.claimno, '' as ProductCode, sum(cd.totalAmt) as PaidAmt 
                                                    from claim cl 
                                                    inner join claimdetail cd on cd.ClaimID = cl.ClaimID 
                                                    inner join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID
                                                    inner join ServiceCenter sc on cp.PayeeNo = sc.ServiceCenterNo
                                                    inner join dealer d on d.DealerNo = sc.DealerNo
                                                    inner join contract c on c.ContractID = cl.ContractID
                                                    where d.dealerno like '2%'
                                                    and cl.status = 'Open' and ClaimDetailStatus='Approved'
                                                    and not cl.claimno in (select claimno from claimautonationach) 
                                                    and dateapprove > '9/15/2019' group by cl.claimid, cd.DateApprove, DealerName, d.DealerNo, cl.RONumber, right(c.vin, 8), cl.lossdate, cl.claimno,c.FName + ' ' + c.LName order by dateapprove" runat="server"></asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="1000" HorizontalAlign="Right">
                                        <asp:Button ID="btnMoveToPayment" OnClick="btnMoveToPayment_Click" runat="server" Text="Move To Payment"  BackColor="#1eabe2" BorderColor="#1eabe2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblError2" ForeColor="Red" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPayment">
                                            <telerik:RadGrid ID="rgPayment" OnItemCommand="rgPayment_ItemCommand" runat="server" AutoGenerateColumns="false"
                                                AllowSorting="true" AllowPaging="true" ShowFooter="true"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500" DataKeyNames="ClaimID">
                                                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimID"  ReadOnly="true" Visible="false" UniqueName="ClaimID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DateApprove" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="DateApproved" AllowFiltering="True" HeaderText="Approved Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerName" UniqueName="Dealer" HeaderText="Seller Name" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Seller No" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="RONumber" UniqueName="RONumber" HeaderText="RONumber" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="VIN" UniqueName="VIN" HeaderText="VIN" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="LossDate" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true" UniqueName="LossDate" AllowFiltering="True" HeaderText="Loss Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" AllowFiltering="true" HeaderText="Customer" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" AllowFiltering="true" HeaderText="Claim No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ItemStyle-Width="125" HeaderStyle-Width="125"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ProductCode" UniqueName="ProductCode" HeaderText="Product Code" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="150" HeaderStyle-Width="150"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="PaidAmt" AllowFiltering="false" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridButtonColumn CommandName="DeleteRow" CommandArgument="ClaimID" ButtonType="ImageButton" ImageUrl="~/images/delete.png"></telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </telerik:RadAjaxPanel>

                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfError" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfErrorText" runat="server" />
                <asp:HiddenField ID="hfSelectedValues" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
