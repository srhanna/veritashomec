﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimInspection.aspx.cs" Inherits="VeritasHomeC.ClaimInspection" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlList">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblError2" runat="server" Text="" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddInspection" OnClick="btnAddInspection_Click" runat="server" Text="Add Inspection" CssClass="button1"/>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            &nbsp
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddNoInspect" OnClick="btnAddNoInspect_Click" runat="server" Text="No Inspection" CssClass="button1"/>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            &nbsp
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddCarFax" OnClick="btnAddCarFax_Click" runat="server" Text="Add Carfax" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimInspect" OnSelectedIndexChanged="rgClaimInspect_SelectedIndexChanged" AutoGenerateColumns="false" runat="server" AllowSorting="false"
                                    Width="1750" ShowFooter="true">
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                        <Scrolling AllowScroll="true" />
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimInspectionID">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimInspectionID"  ReadOnly="true" Visible="false" UniqueName="ClaimInspectionID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="InspectNo" UniqueName="InspectNo" HeaderText="Inspect No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="InspectNote" UniqueName="InspectNote" HeaderText="Inspect Note"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RequestBy" UniqueName="RequestBy" HeaderText="Request By"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RequestDate" UniqueName="RequestDate" HeaderText="Request Date"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TruePic" UniqueName="TruePic" HeaderText="TruePic"></telerik:GridBoundColumn>
                                            <telerik:GridHyperLinkColumn DataTextField="ResultURL" Target="_blank" DataNavigateUrlFields="ResultUrl" UniqueName="ResultURL" HeaderText="Result"></telerik:GridHyperLinkColumn>
                                            <telerik:GridHyperLinkColumn DataTextField="ImagesURL" Target="_blank" DataNavigateUrlFields="ImagesUrl" UniqueName="ImagesURL" HeaderText="Images"></telerik:GridHyperLinkColumn>
                                            <telerik:GridHyperLinkColumn DataTextField="DetailsURL" Target="_blank" DataNavigateUrlFields="DetailsUrl" UniqueName="DetailsURL" HeaderText="Details"></telerik:GridHyperLinkColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Inspection No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtInspectNo" ReadOnly="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Inspection Type:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboInspectType" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Inspection Note:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            &nbsp
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            &nbsp
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="chkTruePic" runat="server" Font-Bold="true" Text="TruePic" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtInspectNote" TextMode="MultiLine" Width="500" Height="250" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="trInspectResult">
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Inspection Result:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtInspectResult" Width="200" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                   </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="button1"/>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="Close" CssClass="button1"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlTASAConfirm">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Have you received approval from TASA to order an inspection?
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnTASAYes" OnClick="btnTASAYes_Click" runat="server" Text="Yes" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnTASANo" OnClick="btnTASANo_Click" runat="server" Text="No" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:panel runat="server" ID="pnlANReason">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                THIS CLAIM SHOULD BE APPROVED. PLEASE STATE REASON FOR INSPECTION ORDER:
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadEditor ID="txtInspectReasonNote" EditModes="Design" Width="1000" ToolbarMode="ShowOnFocus" runat="server"></telerik:RadEditor>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnReasonNoteSave" OnClick="btnReasonNoteSave_Click" runat="server" Text="Save" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                           </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:panel>
                <asp:Panel ID="pnlTasaEmail" runat="server">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblNote" runat="server" Font-Bold="true" Text="Please copy TASA E-Mail here. It will get added to the Notes from here."></asp:Label>  
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadEditor ID="txtNote" EditModes="Design" Width="1000" ToolbarMode="ShowOnFocus" runat="server"></telerik:RadEditor>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSaveNote" OnClick="btnSaveNote_Click" runat="server" Text="Save" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCancelNote" OnClick="btnCancelNote_Click" runat="server" Text="Cancel" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfAdd" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
