﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HCC.aspx.cs" Inherits="VeritasHomeC.HCC" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
        .clsTxt {
            width: 200px;
            min-height: 25px;
            max-height: 200px;
            resize: none;
        }
    </style>
    <script>
        function resizeTextBox(txt) {
            txt.style.height = "1px";
            txt.style.height = (1 + txt.scrollHeight) + "px";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel ID="pnlOut" runat="server" CssClass="HCCOut">
            <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Width="756" Height="100">
                <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
                <asp:Table runat="server" HorizontalAlign="Right"> 
                    <asp:TableRow>
                        <asp:TableCell>
                            Corporate Office
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            T (888) 740-6170
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            F (888) 570-6737
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
            <asp:Table runat="server" Width="756">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Font-Bold="true" Font-Size="Large">
                        Large Claim Approval Form
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        To be filled out on ALL claims over $5,000
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Date Submitted:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblDateSubmitted" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    Adjuster Name:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblAdjusterName" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Contract Number:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblContractNo" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Claim Number:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblClaimNo" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Customer Name:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Selling Dealer:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblDealerName" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Selling Agent:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblAgentName" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Repair Facility:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblServiceCenterName" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Insurance Carrier:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblInsCarrier" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        &nbsp
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Font-Bold="true">
                        CONTRACT/VEHICLE INFORMATION
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Sale Date:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblSaleDate" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Claim Date:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblClaimDate" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Vehicle Make/Model:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblMakeModel" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Current Mileage:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblCurrentMile" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Days Into Contract:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblDaysIntoContract" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Miles Into Contract:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblMilesIntoContract" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Contract Limit of Liability:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblLimitofLiability" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Vehicle NADA:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblNADA" runat="server"></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        &nbsp
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Font-Bold="true" HorizontalAlign="Center">
                        CLAIM INFORMATION
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Describe Failure:
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="756">
                                    <asp:TextBox ID="txtDescribeFailure" CssClass="clsTxt" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table runat="server">
                            <asp:TableRow>
                                <asp:TableCell Font-Bold="true">
                                    Claim Cost:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblClaimCost" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Labor cost per hour charged:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblLaborCostPerHour" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Font-Bold="true">
                                    Average labor cost:
                                </asp:TableCell>
                                <asp:TableCell style="border-bottom-color: Black; border-bottom-style: solid; border-bottom-width:thin;">
                                    <asp:Label ID="lblAverageCost" runat="server"></asp:Label>
                                </asp:TableCell>
                                
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>


        <telerik:RadButton RenderMode="Lightweight" runat="server" OnClientClicked="exportPDF" Text="Export page to PDF" AutoPostBack="false" UseSubmitBehavior="false"></telerik:RadButton>
              <telerik:RadClientExportManager runat="server" ID="RadClientExportManager1">
                  <PdfSettings MarginRight="10mm" MarginBottom="10mm" MarginLeft="10mm" MarginTop="10mm" />
              </telerik:RadClientExportManager>

    <script>
        var $ = $telerik.$;

        function exportPDF() {

            $find('<%=RadClientExportManager1.ClientID%>').exportPDF($(".HCCOut"));
        }

    </script>
        <asp:HiddenField ID="hfClaimID" runat="server" />
        <asp:HiddenField ID="hfContractID" runat="server" />
        <asp:HiddenField ID="hfAgentID" runat="server" />
        <asp:HiddenField ID="hfDealerID" runat="server" />
        <asp:HiddenField ID="hfServiceCenterID" runat="server" />
        <asp:HiddenField ID="hfInsCarrierID" runat="server" />
    </form>
</body>
</html>
