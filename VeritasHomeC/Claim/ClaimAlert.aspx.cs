﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimAlert : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];


            if (!IsPostBack)
                FillPage();
        }

        private  void FillPage()
        {
            string SQL;
            string nl = System.Environment.NewLine;
            clsDBO clR = new clsDBO();
            clsDBO clA = new clsDBO();

            SQL = "select * from dealernote dn ";
            SQL = SQL + "inner join contract c on c.dealerid = dn.dealerid ";
            SQL = SQL + "inner join claim cl on c.contractid = cl.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (dn.claimalways <> 0 ";
            SQL = SQL + "or claimnewentry <> 0) ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount()-1; i++)
                {
                    clR.GetRowNo(i);
                    txtDealerNote.Text = txtDealerNote.Text + clR.GetFields("note") + nl + nl; 
                }
            }

            SQL = "Select * from agentsnote an ";
            SQL = SQL + "inner join Dealer d On d.agentsid = an.agentid ";
            SQL = SQL + "inner join contract c On c.dealerid = d.dealerid ";
            SQL = SQL + "inner join claim cl on c.contractid = cl.contractid ";
            SQL = SQL + "where cl.claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (an.claimalways <> 0 ";
            SQL = SQL + "or an.claimnewentry <> 0) ";
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clA.RowCount() > 0)
            {
                for (int i = 0; i <= clA.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    txtDealerNote.Text = txtDealerNote.Text + clA.GetFields("note") + nl + nl;
                }
            }
        }
    }
}