﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGapPayment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            dsClaimPayment.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsPaymentType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsPayee.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlError.Visible = false;
                pnlDetail.Visible = false;
                pnlSeekPayee.Visible = false;
                pnlAddClaimPayee.Visible = false;
                rgClaimPayment.Rebind();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddPayment_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            txtPayeeNo.Text = "";
            txtPayeeName.Text = "";
            txtPaymentAuth.Text = "0";
            rdpAuthDate.Clear();
            rdpPaidDate.Clear();
            hfClaimPaymentID.Value = "0";
        }

        protected void rgClaimPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            hfClaimPaymentID.Value = rgClaimPayment.SelectedValue.ToString();
            pnlList.Visible = false;
            pnlDetail.Visible = true;

            SQL = "select * from claimgappayment ";
            SQL = SQL + "where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfPayeeID.Value = clR.GetFields("claimgappayeeid");
                cboPaymentType.SelectedValue = clR.GetFields("ClaimGAPPaymentTypeID");
                txtPaymentAuth.Text = clR.GetFields("paymentauth");
                if (clR.GetFields("authdate").Length > 0)
                    rdpAuthDate.SelectedDate = Convert.ToDateTime(clR.GetFields("authdate"));
                else
                    rdpAuthDate.Clear();

                if (clR.GetFields("paiddate").Length > 0)
                    rdpPaidDate.SelectedDate = Convert.ToDateTime(clR.GetFields("paiddate"));
                else
                    rdpPaidDate.Clear();
            }

            hfClaimPaymentID.Value = clR.GetFields("claimpaymentid");

            GetPayeeInfo();
            GetBankVIN();
        }

        private void GetBankVIN()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clC = new clsDBO();
            SQL = "select * from claimgap ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select right(vin, 6) as Right6 from contract ";
                SQL = SQL + "where contractid = " + clR.GetFields("contractid");
                clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clC.RowCount() > 0)
                {
                    clC.GetRow();
                    txtLast6.Text = clC.GetFields("right6");
                }

                SQL = "select * from claimgaploaninfo ";
                SQL = SQL + "where claimgapid = " + hfClaimID.Value;
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtBankLoad.Text = clR.GetFields("AccountNo");
                }
            }
        }

        private void GetPayeeInfo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgappayee ";
            SQL = SQL + "where claimgappayeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeNo.Text = clR.GetFields("payeeno");
                txtPayeeName.Text = clR.GetFields("payeename");
            }
            else
            {
                txtPayeeNo.Text = "";
                txtPayeeName.Text = "";
            }
        }

        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekPayee.Visible = true;
            rgClaimPayee.Rebind();
        }

        protected void btnDetailClose_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgClaimPayment.Rebind();
        }

        protected void btnDetailUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgappayment ";
            SQL = SQL + "where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
            }
            else
            {
                clR.NewRow();
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                if (hfAuthDate.Value.Length > 0)
                {
                    clR.SetFields("authdate", hfAuthDate.Value);
                    clR.SetFields("authby", hfUserID.Value);
                }
            }

            clR.SetFields("claimgapid", hfClaimID.Value);
            clR.SetFields("claimgappaymenttypeid", cboPaymentType.SelectedValue);
            clR.SetFields("claimgappayeeid", hfPayeeID.Value);
            clR.SetFields("paymentauth", txtPaymentAuth.Text);
            
            if (!String.IsNullOrEmpty(rdpAuthDate.SelectedDate.ToString()))
                clR.SetFields("authdate", rdpAuthDate.SelectedDate.ToString());

            if (!String.IsNullOrEmpty(rdpPaidDate.SelectedDate.ToString()))
                clR.SetFields("paiddate", rdpPaidDate.SelectedDate.ToString());

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        protected void rdpAuthDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            hfAuthDate.Value = rdpAuthDate.SelectedDate.ToString();
            if (hfClaimPaymentID.Value.Length == 0)
                return;

            if (hfClaimPaymentID.Value == "0")
                return;

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update claimpayment ";
            SQL = SQL + "set authdate = '" + rdpAuthDate.SelectedDate + "', ";
            SQL = SQL + "authby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnAddClaimPayee_Click(object sender, EventArgs e)
        {
            pnlSeekPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            hfPayeeID.Value = "0";
            GetNextPayeeNo();
        }

        protected void rdpPaidDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            hfAuthDate.Value = rdpAuthDate.SelectedDate.ToString();
            if (hfClaimPaymentID.Value.Length == 0)
                return;

            if (hfClaimPaymentID.Value == "0")
                return;

            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "update claimpayment ";
            SQL = SQL + "set paiddate = '" + rdpAuthDate.SelectedDate + "', ";
            SQL = SQL + "paidby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimpaymentid = " + hfClaimPaymentID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnPayeeCancel_Click(object sender, EventArgs e)
        {
            pnlAddClaimPayee.Visible = false;
            pnlSeekPayee.Visible = true;
            rgClaimPayee.Rebind();
        }

        protected void btnPayeeSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgappayee ";
            SQL = SQL + "where claimgappayeeid = " + hfPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                clR.NewRow();
            else
                clR.GetRow();

            clR.SetFields("payeeno", txtGAPPayeeNo.Text);
            clR.SetFields("payeename", txtGapPayeeName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("email", txtEMail.Text);

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
            pnlAddClaimPayee.Visible = false;
            pnlSeekPayee.Visible = true;
            rgClaimPayee.Rebind();
        }

        private void GetNextPayeeNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(payeeno) as mSCN from claimgappayee ";
            SQL = SQL + "where payeeno like 'GP0%' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            if (clR.GetFields("mscn").Length == 0)
                txtGAPPayeeNo.Text = "GP0000000";
            else
                txtGAPPayeeNo.Text = "GP" + (Convert.ToInt64(clsFunc.Right(clR.GetFields("mscn"), 6)) + 1).ToString("0000000");
        }

        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            GetPayeeInfo();
            pnlSeekPayee.Visible = false;
            pnlDetail.Visible = true;
        }
    }
}