﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimNotes.aspx.cs" Inherits="VeritasHomeC.ClaimNotes" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
     <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim Notes</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlList" DefaultButton="btnHiddenList">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Button ID="btnAddNote" OnClick="btnAddNote_Click" runat="server" Text="Add Note" CssClass="button1" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgNote">
                                <telerik:RadGrid ID="rgNote" OnItemCommand="rgNote_ItemCommand" OnSelectedIndexChanged="rgNote_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsNote"
                                    AllowSorting="true" Width="1500" ShowFooter="true">
                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimNoteID" ShowFooter="true" CommandItemDisplay="TopAndBottom">
                                        <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimNoteID"  ReadOnly="true" Visible="false" UniqueName="ClaimNoteID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimNoteTypeDesc" UniqueName="ClaimNoteTypeDesc" HeaderText="Note Type Desc"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NoteText" HtmlEncode="true" ItemStyle-Width="400" UniqueName="Note" HeaderText="Note"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CreDate" UniqueName="CreDate" HeaderText="Note Date"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="UserName" UniqueName="UserName" HeaderText="User"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsNote" SelectCommand="select claimnoteid, ClaimNoteTypeDesc, notetext, cn.CreDate, username from claimnote cn
                                    left join userinfo ui on ui.userid = cn.creby 
                                    left join claimnotetype nt on nt.claimnotetypeid = cn.claimnotetypeid
                                    where claimid = @claimid
                                    and (cn.claimnotetypeid = 4 or cn.claimnotetypeid = 5 or cn.claimnotetypeid = 6 or cn.claimnotetypeid = 7 or cn.claimnotetypeid = 8 )
                                    order by credate desc" runat="server">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </telerik:RadAjaxPanel>
                        </asp:TableCell>
                        <asp:TableCell Visible="false">
                            <asp:Button ID="btnHiddenList" Visible="false" runat="server" Text="Button" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDetail">
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Note Type:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadComboBox ID="cboNoteType" DataSourceID="dsNoteType" DataTextField="ClaimNoteTypeDesc" DataValueField="ClaimNoteTypeID" runat="server"></telerik:RadComboBox>
                                        <asp:SqlDataSource ID="dsNoteType" 
                                        ProviderName="System.Data.SqlClient" SelectCommand="select claimnotetypeid, claimnotetypedesc from claimnotetype where claimnotetypeid = 4 or claimnotetypeid = 5 or claimnotetypeid = 6 or claimnotetypeid = 7 or claimnotetypeid = 8 order by claimnotetypedesc " runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Note Header:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="cboHeader" runat="server"></asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkReminder" Text="Use as a reminder" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Note:
                                    </asp:TableCell>
                                    <asp:TableCell ID="tcAddNote">
                                        <telerik:RadEditor ID="txtNote" EditModes="Design" Width="1000" ToolbarMode="ShowOnFocus" runat="server"></telerik:RadEditor>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        &nbsp
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Note Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtCreDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Note By:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txtCreBy" ReadOnly="true" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CssClass="button1" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="button2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
            <asp:HiddenField ID="hfID" runat="server" />
            <asp:HiddenField ID="hfUserID" runat="server" />
            <asp:HiddenField ID="hfClaimID" runat="server" />
            <asp:HiddenField ID="hfClaimNoteID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="upPanel" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <img src="~/images/wait.gif" alt="Loading" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
