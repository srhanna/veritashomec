﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Drawing;

namespace VeritasHomeC
{
    public partial class ClaimSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsUserInfo.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            if (!IsPostBack)
            {
                GetServerInfo();
                trUserInfo.Visible = false;
                trUserInfoClose.Visible = false;
                CheckToDo();
                
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                trClaimResult.Visible = false;
                trServiceCenter.Visible = false;
                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else 
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                if (hfUserID.Value == "1")
                    btnLossCode.Visible = true;
                else
                    btnLossCode.Visible = false;

                ReadOnlyButtons();
                PostMessage();
            }

        }

        private void PostMessage()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clUI = new clsDBO();
            lblMessage.Text = "";
            SQL = "select * from userinfo ui ";
            SQL = SQL + "inner join UserSecurityInfo usi on ui.UserID = usi.UserID ";
            SQL = SQL + "where ui.userid = " + hfUserID.Value + " ";
            SQL = SQL + "and teamid = 2 ";
            clUI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                if (clUI.GetFields("teamid") == "2")
                {
                    SQL = "select * from veritasclaimticket.dbo.ticket ";
                    SQL = SQL + "where statusid = 3 ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    
                    if (clR.RowCount() > 0)
                        lblMessage.Text = "You have received a message for a Ticket.";
                    
                    SQL = "select * from veritasclaimticket.dbo.ticket ";
                    SQL = SQL + "where statusid = 2 ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

                    if (clR.RowCount() > 0)
                        lblMessage.Text = "You have received a response from ticket.";
                }
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnAddClaim.Enabled = false;
                    btnServiceCenters.Enabled = false;
                    btnClaimsOpen.Enabled = false;
                }

                hfVeroOnly.Value = (Convert.ToBoolean(clR.GetFields("veroonly"))).ToString();
                hfAgentID.Value = clR.GetFields("agentid");
                hfSubAgentID.Value = clR.GetFields("subagentid");
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfInsCarrierID.Value = clSI.GetFields("inscarrierid");
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clC = new clsDBO();

            SQL = "select claimid, c.contractid, contractno, claimno, cl.status, servicecenterno, servicecentername, sc.city, sc.state, lossdate  from claim cl ";
            SQL = SQL + "left join contract c on cl.contractid = c.contractid ";
            SQL = SQL + "left join servicecenter sc on cl.servicecenterid = sc.servicecenterid ";
            SQL = SQL + "left join dealer d on c.dealerid = d.dealerid ";
            SQL = SQL + "left join agents a on a.agentid = d.agentsid ";
            SQL = SQL + "where claimid > 0 ";
            if (txtContractNo.Text.Trim().Length > 0)
                SQL = SQL + "and contractno like '%" + txtContractNo.Text.Trim() + "%' ";
            if (txtRONumber.Text.Trim().Length > 0)
                SQL = SQL + "and ronumber like '%" + txtRONumber.Text.Trim() + "%' ";
            if (txtClaimNo.Text.Trim().Length > 0)
                SQL = SQL + "and claimno like '%" + txtClaimNo.Text.Trim() + "%' ";
            if (txtFName.Text.Trim().Length > 0)
                SQL = SQL + "and fname like '%" + txtFName.Text.Trim() + "%' ";

            if (txtLName.Text.Trim().Length > 0)
                SQL = SQL + "and lname like '%" + txtLName.Text.Trim() + "%' ";

            if (txtServiceCenterNo.Text.Trim().Length > 0)
                SQL = SQL + "and cl.servicecenterid = " + hfServiceCenterID.Value;

            if (txtCity.Text.Trim().Length > 0)
                SQL = SQL + "and sc.city like '%" + txtCity.Text.Trim() + "%' ";

            if (cboState.SelectedValue.Length > 0)
                SQL = SQL + "and sc.state = '" + cboState.Text + "%' ";

            if (cboStatus.Text.Length > 0)
                SQL = SQL + "and cl.status = '" + cboStatus.SelectedValue + "' ";

            if (hfAssignedTo.Value.Length > 0)
                SQL = SQL + "and assignedto = " + hfAssignedTo.Value + " ";

            if (chkAutoNation.Checked)
                SQL = SQL + "and sc.dealerno like '2%' ";

            if (Convert.ToInt64(hfInsCarrierID.Value) > 0)
                SQL = SQL + "and c.inscarrierid = " + hfInsCarrierID.Value + " ";

            if (CheckANOnly())
            {
                SQL = SQL + "and (sc.dealerno like '2%' ";
                SQL = SQL + "or d.dealerno like '2%') ";
            }

            if (hfVeroOnly.Value == "True")
                SQL = SQL + "and a.agentid = 54 ";

            if (hfAgentID.Value.Length > 0) {
                if (hfAgentID.Value != "0")
                    SQL = SQL + "and d.agentsid = " + hfAgentID.Value + " ";
            }

            if (hfSubAgentID.Value.Length > 0)
            {
                if (hfSubAgentID.Value != "0")
                    SQL = SQL + "and d.subagentid = " + hfSubAgentID.Value + " ";
            }

            SQL = SQL + "Union ";
            SQL = SQL + "select claimgapid, c.contractid, contractno, claimno, s.ClaimGAPStatusdesc as status, '' as servicecenterno,  '' as servicecentername, c.city, c.state, cl.ClaimDate as lossdate ";
            SQL = SQL + "from claimgap cl ";
            SQL = SQL + "left join contract c on cl.contractid = c.contractid ";
            SQL = SQL + "left join dealer d on c.dealerid = d.dealerid ";
            SQL = SQL + "left join agents a on a.agentid = d.agentsid ";
            SQL = SQL + "left join ClaimGapStatus s on cl.Status = s.ClaimGAPStatusID ";
            SQL = SQL + "where cl.claimgapid > 0 ";

            if (txtContractNo.Text.Trim().Length > 0)
                SQL = SQL + "and contractno like '%" + txtContractNo.Text.Trim() + "%' ";

            if (txtClaimNo.Text.Trim().Length > 0)
                SQL = SQL + "and claimno like '%" + txtClaimNo.Text.Trim() + "%' ";

            if (txtFName.Text.Trim().Length > 0)
                SQL = SQL + "and c.fname like '%" + txtFName.Text.Trim() + "%' ";

            if (txtLName.Text.Trim().Length > 0)
                SQL = SQL + "and c.lname like '%" + txtLName.Text.Trim() + "%' ";

            if (txtCity.Text.Trim().Length > 0)
                SQL = SQL + "and c.city like '%" + txtCity.Text.Trim() + "%' ";

            if (cboState.SelectedValue.Length > 0)
                SQL = SQL + "and c.state = '" + cboState.Text + "%' ";

            if (cboStatus.Text.Length > 0)
                SQL = SQL + "and s.claimgapstatusdesc = '" + cboStatus.SelectedValue + "' ";

            if (hfAssignedTo.Value.Length > 0)
                SQL = SQL + "and assignedto = " + hfAssignedTo.Value + " ";

            if (chkAutoNation.Checked)
                SQL = SQL + "and c.dealerno like '2%' ";

            if (Convert.ToInt64(hfInsCarrierID.Value) > 0)
                SQL = SQL + "and c.inscarrierid = " + hfInsCarrierID.Value + " ";

            if (CheckANOnly())
                SQL = SQL + "or d.dealerno like '2%' ";

            if (hfVeroOnly.Value == "True")
                SQL = SQL + "and a.agentid = 54 ";

            if (hfAgentID.Value.Length > 0)
            {
                if (hfAgentID.Value != "0")
                    SQL = SQL + "and d.agentsid = " + hfAgentID.Value + " ";
            }

            if (hfSubAgentID.Value.Length > 0)
            {
                if (hfSubAgentID.Value != "0")
                    SQL = SQL + "and d.subagentid = " + hfSubAgentID.Value + " ";
            }

            rgClaim.DataSource = clC.GetData(SQL, ConfigurationManager.AppSettings["connstring"]).Tables[0];
            rgClaim.Rebind();
            trClaimResult.Visible = true;
        }

        private bool CheckANOnly()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and autonationonly <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        protected void btnSCSeek_Click(object sender, EventArgs e)
        {
            trServiceCenter.Visible = true;
            rgServiceCenter.DataSourceID = "SQLDataSource1";
            rgServiceCenter.Rebind();
        }

        protected void rgServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtServiceCenterNo.Text = rgServiceCenter.SelectedValue.ToString();
            GetServiceCenterID();
            trServiceCenter.Visible = false;
        }

        private void GetServiceCenterID()
        {
            hfServiceCenterID.Value = "0";
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterno = '" + txtServiceCenterNo.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfServiceCenterID.Value = clR.GetFields("servicecenterid");
            }
        }

        protected void rgClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select PlanTypeID, ProgramID from contract c ";
            SQL = SQL + "where contractid = " + rgClaim.SelectedValues["ContractID"];
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if(clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("plantypeid") != "118" && clR.GetFields("programid") != "103")
                    Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + rgClaim.SelectedValues["ClaimID"]);
                else
                    Response.Redirect("~/claim/claimgap.aspx?sid=" + hfID.Value + "&ClaimID=" + rgClaim.SelectedValues["ClaimID"]);
            }
        }

        protected void btnAddClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=0");
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnSeekUser_Click(object sender, EventArgs e)
        {
            trClaimResult.Visible = false;
            trUserInfo.Visible = true;
            trUserInfoClose.Visible = true;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            trClaimResult.Visible = true;
            trUserInfo.Visible = false;
            trUserInfoClose.Visible = false;
            hfAssignedTo.Value = "";
            txtAssignedTo.Text = "";
        }

        protected void rgUserInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            trClaimResult.Visible = true;
            trUserInfo.Visible = false;
            trUserInfoClose.Visible = false;
            hfAssignedTo.Value = rgUserInfo.SelectedValue.ToString();
            GetUserInfo();
        }

        public void GetUserInfo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfAssignedTo.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAssignedTo.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }
    }
}