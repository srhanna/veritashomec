﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using Telerik.Web.UI.Calendar;


namespace VeritasHomeC
{
    public partial class AutoNationACH : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            
            if(!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (hfUserID.Value == "1")
                    btnLossCode.Visible = true;
                else
                    btnLossCode.Visible = false;

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                FillPaymentTable();
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    Response.Redirect("~/claim/claimssearch.aspx?sid=" + hfID.Value);
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }


        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))      
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void ShowError() {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx");
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/servicecenters.aspx?sid=" + hfID.Value);
        }

        private void FillClaimTable()
        {
            rgClaim.Rebind();
        }

        private void FillPaymentTable()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimautonationach "; 
            SQL = SQL + "where canaid > 0 ";

            if (!String.IsNullOrEmpty(rdpEndWeek.SelectedDate.ToString()))
                SQL = SQL + "and perioddate = '" + rdpEndWeek.SelectedDate + "' ";

            SQL = SQL + "order by dateapprove ";
            rgPayment.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgPayment.Rebind();
        }

        protected void rgPayment_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv") {
                rgPayment.ExportSettings.ExportOnlyData = false;
                rgPayment.ExportSettings.IgnorePaging = true;
                rgPayment.ExportSettings.OpenInNewWindow = true;
                rgPayment.ExportSettings.UseItemStyles = true;
                // rgDealer.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgPayment.MasterTableView.ExportToCSV();
            }
            if (e.CommandName == "DeleteRow")
            {
                DeletePayment(Convert.ToInt64(rgPayment.Items[e.Item.ItemIndex].GetDataKeyValue("ClaimID")));
                FillPaymentTable();
                rgClaim.Rebind();
            }
        }

        private void DeletePayment(long xID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "delete claimautonationach ";
            SQL = SQL + "where claimid = " + xID;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnMoveToPayment_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(rdpEndWeek.SelectedDate.ToString())){
                lblError2.Text = "No Period date is given";
                lblError2.Visible = true;
                return;
            }
            else
                lblError2.Visible = false;

            foreach (GridDataItem gdi in rgClaim.SelectedItems)
                AddPayment(Convert.ToInt64(gdi.GetDataKeyValue("ClaimID")));

            rgClaim.Rebind();
            FillPaymentTable();
        }

        private void AddPayment(long xID)
        {
            string SQL;
            clsDBO clP = new clsDBO();
            clsDBO clR = new clsDBO();
            SQL = "Select cl.claimid, cd.DateApprove, DealerName, d.DealerNo, c.FName + ' ' + c.LName as Customer, cl.RONumber, right(c.vin, 8) as VIN, cl.lossdate, cl.claimno, '' as ProductCode, sum(cd.TotalAmt) as PaidAmt ";
            SQL = SQL + "from claim cl ";
            SQL = SQL + "inner join claimdetail cd on cd.ClaimID = cl.ClaimID ";
            SQL = SQL + "inner join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID ";
            SQL = SQL + "inner join ServiceCenter sc on cp.PayeeNo = sc.ServiceCenterNo ";
            SQL = SQL + "inner join dealer d on d.DealerNo = sc.DealerNo ";
            SQL = SQL + "inner join contract c on c.ContractID = cl.ContractID ";
            SQL = SQL + "where d.dealerno like '2%' ";
            SQL = SQL + "and cl.status = 'Open' and ClaimDetailStatus='Approved' ";
            SQL = SQL + "and dateapprove > '9/15/2019' ";
            SQL = SQL + "and cl.claimid = " + xID + " ";
            SQL = SQL + "group by cl.claimid, cd.DateApprove, DealerName, d.DealerNo, cl.RONumber, right(c.vin, 8), cl.lossdate, cl.claimno,c.FName + ' ' + c.LName ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                SQL = "select * from claimautonationach ";
                SQL = SQL + "where claimid = " + xID + " ";
                clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clP.RowCount() == 0)
                    clP.NewRow();
                else
                    clP.GetRow();
            }

            clP.SetFields("perioddate", rdpEndWeek.SelectedDate.ToString());
            clP.SetFields("claimid", xID.ToString());
            clP.SetFields("dateapprove", clR.GetFields("dateapprove"));
            clP.SetFields("dealername", clR.GetFields("dealername"));
            clP.SetFields("dealerno", clR.GetFields("dealerno"));
            clP.SetFields("ronumber", clR.GetFields("ronumber"));
            clP.SetFields("customer", clR.GetFields("customer"));
            clP.SetFields("vin", clR.GetFields("vin"));
            clP.SetFields("lossdate", clR.GetFields("lossdate"));
            clP.SetFields("claimno", clR.GetFields("claimno"));
            clP.SetFields("Paidamt", clR.GetFields("paidamt"));

            if (clP.RowCount() == 0)
                clP.AddRow();
            
            clP.SaveDB();
        }

        protected void rdpEndWeek_SelectedDateChanged(object sender, SelectedDateChangedEventArgs e)
        {
            FillPaymentTable();
        }

        protected void rgClaim_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToCsv") {
                rgClaim.ExportSettings.ExportOnlyData = false;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.MasterTableView.ExportToCSV();
            }
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }
    }
}