﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimPayment.aspx.cs" Inherits="VeritasHomeC.ClaimPayment" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
   <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlError">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell Width="500" BorderColor="Red" BorderStyle="Solid" BorderWidth="1">
                                <asp:Label ID="lblError" runat="server" Text="Label" ForeColor="Red"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlList" runat="server">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label1" runat="server" Text="Paid"></asp:Label>
                                            <telerik:RadGrid ID="rgPaid" OnSelectedIndexChanged="rgPaid_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsPaid" AllowSorting="true">
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimPayeeID" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimPayeeID" ReadOnly="true" UniqueName="ClaimPayeeID" Visible="false"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" UniqueName="ClaimPayeeID" Visible="false"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeNo" UniqueName="PayeeNo" HeaderText="Payee No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsPaid" ProviderName="System.Data.SqlClient" SelectCommand="select claimid, cd.ClaimPayeeID, cp.PayeeNo, cp.PayeeName, sum(totalamt) as PaidAmt 
                                                from claimdetail cd left join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID 
                                                where cd.claimdetailstatus = 'Paid' 
                                                and claimid = @ClaimID 
                                                group by  claimid, cd.ClaimPayeeID, cp.PayeeNo, cp.PayeeName " runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label2" runat="server" Text="To Be Paid"></asp:Label>
                                            <telerik:RadGrid ID="rgToBePaid" OnSelectedIndexChanged="rgToBePaid_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsToBePaid" AllowSorting="true" AllowPaging="true">
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimPayeeID" PageSize="10" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimPayeeID" ReadOnly="true" UniqueName="ClaimPayeeID" Visible="false"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimID" ReadOnly="true" UniqueName="ClaimPayeeID" Visible="false"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeNo" UniqueName="PayeeNo" HeaderText="Payee No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsToBePaid" ProviderName="System.Data.SqlClient" SelectCommand="select claimid, cd.ClaimPayeeID, cp.PayeeNo, cp.PayeeName, sum(totalamt) as PaidAmt 
                                                from claimdetail cd left join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID 
                                                where (cd.claimdetailstatus = 'Approved' 
                                                or cd.claimdetailstatus = 'Authorized')
                                                and claimid = @ClaimID 
                                                group by  claimid, cd.ClaimPayeeID, cp.PayeeNo, cp.PayeeName " runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Table runat="server">
                                    <asp:TableRow runat="server">
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddPayment" OnClick="btnAddPayment_Click" runat="server" Text="Add Payment" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgPayment" OnSelectedIndexChanged="rgPayment_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsPayment" AllowSorting="true" AllowPaging="true">
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimPaymentID" PageSize="10" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimPaymentID" UniqueName="ClaimPaymentID" Visible="false"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeNo" UniqueName="PayeeNo" HeaderText="Payee No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PaymentAmt" UniqueName="PaidAmt" HeaderText="Paid Amt"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DateTransmitted" UniqueName="DateTransmitted" HeaderText="Date Transmitted"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsPayment" ProviderName="System.Data.SqlClient" SelectCommand="select cp.claimpaymentid, payeeno, payeename, cp.paymentamt, cp.datetransmitted, cp.datepaid, cp.status from claimpayment cp 
                                                inner join claimpayee cpe on cp.claimpayeeid = cpe.claimpayeeid 
                                                where claimpaymentid in (select claimpaymentid from claimpaymentlink cpl 
                                                inner join claimdetail cd on cd.claimdetailid = cpl.claimdetailid 
                                                where cd.claimid = @ClaimID) " runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                         </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlPaidDetail" runat="server">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgPaidDetail" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true">
                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimDetailID" PageSize="10" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimDetailID" UniqueName="ClaimDetailID" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimPayeeID" UniqueName="ClaimPayeeID" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeNo" UniqueName="PayeeNo" HeaderText="Payee"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCode" UniqueName="LossCode" HeaderText="Loss Code"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DatePaid" UniqueName="DatePaid" HeaderText="Paid Date"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Button ID="btnPaidDetailClose" OnClick="btnPaidDetailClose_Click" runat="server" Text="Close"  CssClass="button1" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlToBePaidDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Invoice No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtInvoiceNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayAmt" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPaymentMethod" OnTextChanged="cboPaymentMethod_TextChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow ID="trACH">
                                        <asp:TableCell Font-Bold="true">
                                            Confirmation No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtConfirmNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessACH" OnClick="btnProcessACH_Click" runat="server" Text="Process ACH" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trCheck">
                                        <asp:TableCell Font-Bold="true">
                                            Check No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCheckNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessCheck" OnClick="btnProcessCheck_Click" runat="server" Text="Process Check" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trCC">
                                        <asp:TableCell Font-Bold="true">
                                            Credit Card No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCCNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessCC" OnClick="btnProcessCC_Click" runat="server" Text="Process Credit Card" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trWex">
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDropDownList ID="cboWexMethod" OnSelectedIndexChanged="cboWexMethod_SelectedIndexChanged" AutoPostBack="true" DataSourceID="dsWexMethod" DataTextField="wexmethod" DataValueField="wexmethod" runat="server"></telerik:RadDropDownList>
                                            <asp:SqlDataSource ID="dsWexMethod" ProviderName="System.Data.SqlClient"
                                                 SelectCommand="select wexmethod from wexmethod" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Addess:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexAddress" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessWex" OnClick="btnProcessWex_Click" runat="server" Text="Process Wex" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCloseToBePaidDetail" OnClick="btnCloseToBePaidDetail_Click" runat="server" Text="Close"  CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlPaymentDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPaymentAmt" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            Payment Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayMethod" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Date Transmitted:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtDateTransmitted" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Date Paid:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtDatePaid" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            ACH Info:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtACHInfo" BackColor="LightYellow" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Check No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCheckNoView" BackColor="LightYellow" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            CC No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCCNoView" BackColor="LightYellow" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexMethod" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            Wex Delivery Address:
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            <asp:TextBox ID="txtWexAddressView" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexCode" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Status:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPaymentStatus" BackColor="LightYellow" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Company No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCompanyNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Button ID="btnPaymentViewClose" OnClick="btnPaymentViewClose_Click" runat="server" Text="Close" CssClass="button1" />
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Button ID="btnPaymentViewSave" OnClick="btnPaymentViewSave_Click" runat="server" Text="Save" CssClass="button1" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlPaymentAdd">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeNoAdd" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekPayee" OnClick="btnSeekPayee_Click" runat="server" Text="Seek"  CssClass="button1"/>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeNameAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPaymentAmtAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            Payment Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPayMethodAdd" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Date Transmitted:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpDateTransmittedAdd" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Date Paid:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpDatePaidAdd" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            ACH Info:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtACHInfoAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Check No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCheckNoAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            CC No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCCNoAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexDeliveryMethodAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            Wex Delivery Address:
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            <asp:TextBox ID="txtWexDeliveryAddressAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexCodeAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Status:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboStatusAdd" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Company No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCompanyNoAdd" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCloseAdd" OnClick="btnCloseAdd_Click" runat="server" Text="Close"  CssClass="button1"/>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSaveAdd" OnClick="btnSaveAdd_Click" runat="server" Text="Save"  CssClass="button1"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlSearchClaimPayee" runat="server" DefaultButton="btnHiddenSCP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimPayee" OnSelectedIndexChanged="rgClaimPayee_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                    AllowSorting="true" AllowPaging="true" Width="1000" ShowFooter="true" DataSourceID="dsClaimPayee">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ClaimPayeeID" PageSize="10" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimPayeeID"  ReadOnly="true" Visible="false" UniqueName="ClaimPayeeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeNo" FilterCheckListWebServiceMethod="LoadPayeeNo" UniqueName="PayeeNo" HeaderText="Payee No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName" FilterCheckListWebServiceMethod="LoadPayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Zip" FilterCheckListWebServiceMethod="LoadZip" UniqueName="Zip" HeaderText="Zip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsClaimPayee"
                                ProviderName="System.Data.SqlClient" SelectCommand="select claimpayeeid, payeeno, payeename, city, state, zip from claimpayee" runat="server"></asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenSCP" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>



                <asp:HiddenField ID="hfClaimID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfUserID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfClaimNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRONo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfContractID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfContractNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCustomerName" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfVIN" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfPayeeContact" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfClaimPayeeID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfClaimPaymentID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfWexCCno" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfWexCode" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCompanyNo" runat="server"></asp:HiddenField>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
