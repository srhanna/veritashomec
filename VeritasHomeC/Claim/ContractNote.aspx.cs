﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ContractNote : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNotes.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if(!IsPostBack)
            {
                pnlNote.Visible = true;
                pnlChange.Visible = false;
                hfContractID.Value = Request.QueryString["contractid"];
                GetServerInfo();
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnSave.Enabled = false;
                    btnAdd.Enabled = false;
                }
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillTable()
        {
            //string SQL;
            //clsDBO clC = new clsDBO();
            //SQL = "select contractnoteid, note, cre.fname + ' ' + cre.lname as creby, credate, mod.fname + ' ' + mod.lname as modby, moddate  from contractnote cn ";
            //SQL = SQL + "left join userinfo cre on cre.UserID = cn.CreBy ";
            //SQL = SQL + "left join userinfo mod on mod.UserID = cn.ModBy ";
            //SQL = SQL + "where contractid = " + hfContractID.Value;
            //rgNote.DataSource = clC.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings["connstring"]);
            rgNote.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            txtNote.ReadOnly = false;
            pnlChange.Visible = true;
            pnlNote.Visible = false;
            hfContractNoteID.Value = "0";
            FillNote();
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlChange.Visible = true;
            pnlNote.Visible = false;
            hfContractNoteID.Value = rgNote.SelectedValue.ToString();
            FillNote();
        }

        private void FillNote()
        {
            string SQL;
            clsDBO clN = new clsDBO();
            txtNote.ReadOnly = false;
            btnSave.Visible = true;
            SQL = "select * from contractnote ";
            SQL = SQL + "where contractnoteid = " + hfContractNoteID.Value;
            clN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clN.RowCount() > 0) {
                clN.GetRow();
                txtNote.Text = clN.GetFields("note");
                txtModDate.Text = clN.GetFields("moddate");
                txtCreDate.Text = clN.GetFields("credate");
                if (clN.GetFields("creby") != hfUserID.Value) {
                    txtNote.ReadOnly = true;
                    btnSave.Visible = false;
                }
                if (clsFunc.DateDiffHours(Convert.ToDateTime(clN.GetFields("moddate")), DateTime.Now) > 24) {
                    txtNote.ReadOnly = true;
                    btnSave.Visible = false;
                }

                txtCreBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clN.GetFields("creby")));

                if (clN.GetFields("modby").Length > 0)
                    txtModBy.Text = clsFunc.GetUserInfo(Convert.ToInt64(clN.GetFields("modby")));
               
            }
            else 
            {
                txtNote.Text = "";
                txtModDate.Text = "";
                txtCreDate.Text = "";
                txtCreBy.Text = "";
                txtModBy.Text = "";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlNote.Visible = true;
            pnlChange.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clN = new clsDBO();
            btnSave.Visible = true;
            SQL = "select * from contractnote ";
            SQL = SQL + "where contractnoteid = " + hfContractNoteID.Value;
            clN.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings["connstring"]);
            if (clN.RowCount() == 0)
                clN.NewRow();
            else
                clN.GetRow();

            clN.SetFields("contractid", hfContractID.Value);
            clN.SetFields("note", txtNote.Text);
            if (clN.RowCount() == 0)
            {
                clN.SetFields("creby", hfUserID.Value);
                clN.SetFields("credate", DateTime.Now.ToString());
                clN.SetFields("modby", hfUserID.Value);
                clN.SetFields("moddate", DateTime.Now.ToString());
                clN.AddRow();
            }
            else 
            {
                clN.SetFields("modby", hfUserID.Value);
                clN.SetFields("moddate", DateTime.Now.ToString());
            }

            clN.SaveDB();
            FillTable();
            pnlChange.Visible = false;
            pnlNote.Visible = true;
        }
    }
}