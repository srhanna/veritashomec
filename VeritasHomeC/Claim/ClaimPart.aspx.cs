﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics;

namespace VeritasHomeC
{
    public partial class ClaimPart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimPayee.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsLossCode.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsParts.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsLabor.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlPartList.Visible = true;
                pnlPartDetail.Visible = false;
                pnlAddClaimPayee.Visible = false;
                pnlSearchClaimPayee.Visible = false;
                pnlSeekLossCode.Visible = false;
                pnlLaborDetail.Visible = false;
                GetShopRate();

                if (CheckLock())
                    LockButtons();
                else
                    UnlockButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    LockButtons();
            }
        }


        private void GetShopRate()
        {
            hfShopLabor.Value = "0";
            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select shoprate from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfShopLabor.Value = clC.GetFields("shoprate");
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private bool CheckLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "Select claimid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                return false;
            else
                return true;
        }

        private void LockButtons()
        {
            btnAddClaimPayee.Enabled = false;
            btnAddLabor.Enabled = false;
            btnAddLossCode.Enabled = false;
            btnAddPart.Enabled = false;
            btnSCSave.Enabled = false;
            btnSeekClaimPayeeLabor.Enabled = false;
            btnSeekLossCodeLabor.Enabled = false;
            btnSeekLossCodePart.Enabled = false;
            btnSeekPayee.Enabled = false;
            btnUpdateLabor.Enabled = false;
            btnUpdatePart.Enabled = false;
        }

        private void UnlockButtons()
        {
            btnAddClaimPayee.Enabled = true;
            btnAddLabor.Enabled = true;
            btnAddLossCode.Enabled = true;
            btnAddPart.Enabled = true;
            btnSCSave.Enabled = true;
            btnSeekClaimPayeeLabor.Enabled = true;
            btnSeekLossCodeLabor.Enabled = true;
            btnSeekLossCodePart.Enabled = true;
            btnSeekPayee.Enabled = true;
            btnUpdateLabor.Enabled = true;
            btnUpdatePart.Enabled = true;
        }

        private void FillLossCode()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select losscodeid, LossCodeDesc, losscode from claimlosscode ";
            SQL = "where losscode id > 0";
            SQL = SQL + GetLossCodeFilter();
            dsLossCode.SelectCommand = SQL;
            rgLossCode.Rebind();
        }

        public string GetLossCodeFilter()
        {
            long lcontractID = GetContractID();
            clsDBO clR = new clsDBO();
            string sTemp = "";
            string SQL;
            if (lcontractID == 0)
                return "";

            SQL = "select * from contractsurcharge ";
            SQL = SQL + "where contract = " + lcontractID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                return "";
            else
            {
                clR.GetRow();
                if (clR.GetFields("surchargeid") == "1")
                    sTemp = sTemp + "or losscode like 'ac101%' ";

                if (clR.GetFields("surchargeid") == "2")
                    sTemp = sTemp + "or losscode like 'CL101%' ";

                if (clR.GetFields("surchargeid") == "3")
                    sTemp = sTemp + "or losscode like 'AC101%' ";

                if (clR.GetFields("surchargeid") == "4")
                    sTemp = sTemp + "or losscode like 'PL101%' ";

                if (clR.GetFields("surchargeid") == "5")
                    sTemp = sTemp + "or losscode like 'RF103%' ";

                if (clR.GetFields("surchargeid") == "6")
                    sTemp = sTemp + "or losscode like 'EL101%' ";

                if (clR.GetFields("surchargeid") == "7")
                    sTemp = sTemp + "or losscode like 'SP101%' ";

                if (clR.GetFields("surchargeid") == "8")
                    sTemp = sTemp + "or losscode like 'SP102%' ";

                if (clR.GetFields("surchargeid") == "9")
                    sTemp = sTemp + "or losscode like 'PL102%' ";

                if (clR.GetFields("surchargeid") == "10")
                    sTemp = sTemp + "or losscode like 'AC103%' ";

                if (clR.GetFields("surchargeid") == "11")
                    sTemp = sTemp + "or losscode like 'RF101%' ";

                if (clR.GetFields("surchargeid") == "12")
                    sTemp = sTemp + "or losscode like 'RF102%' ";
            }

            if (sTemp.Length > 0)
            {
                sTemp.Substring(2, sTemp.Length - 2);
                sTemp = "and (" + sTemp + ") ";
                return sTemp;
            }

            return "";
        }

        private long GetContractID()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("contractid").Length > 0)
                    return Convert.ToInt64(clR.GetFields("contractid"));
            }

            return 0;
        }

        protected void rgParts_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlPartList.Visible = false;
            pnlPartDetail.Visible = true;
            hfAddPart.Value = "False";
            FillPartDetail();
            //FillPartQuotes();
            LockAuthPartText();
        }

        private void FillPartDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            trPartLabor.Visible = false;
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + rgParts.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                hfClaimDetailID.Value = clR.GetFields("claimdetailid");
                txtPartNo.Text = clR.GetFields("partno");
                txtPartDesc.Text = clR.GetFields("Claimdesc");
                txtReqCost.Text = clR.GetFields("reqcost");
                txtReqQty.Text = clR.GetFields("reqqty");
                hfJobNo.Value = clR.GetFields("jobno");
                txtAuthQty.Text = clR.GetFields("AuthQty");
                txtAuthCost.Text = clR.GetFields("Authcost");
                txtPartTax.Text = (Convert.ToDouble(clR.GetFields("taxper")) * 100).ToString();
                
                if (Convert.ToDouble(clR.GetFields("taxper")) == 0)
                    txtPartTax.Text = hfPartTax.Value;

                txtShipping.Text = clR.GetFields("shippingcost");
                txtLossCodePart.Text = clR.GetFields("losscode");
                txtLossCodePartDesc.Text = GetLossCodeDesc(clR.GetFields("losscode"));
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                hfJobNo.Value = clR.GetFields("jobno");
                hfLossCodeSeek.Value = "Part";
                GetPartLabor();
                GetPayee();
            }
        }

        private void GetPartLabor()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = '" + hfClaimID.Value + "' ";
            SQL = SQL + "and claimdetailtype = 'Labor' ";
            SQL = SQL + "and jobno = '" + hfJobNo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLaborReqAmtPart.Text = clR.GetFields("reqqty");
                txtLaborReqRatePart.Text = hfShopLabor.Value;
                txtLaborAuthAmtPart.Text = clR.GetFields("authqty");
                txtLaborAuthRatePart.Text = clR.GetFields("authcost");
            }
        }

        private string GetLossCodeDesc(string xLossCode)
        {
            if (xLossCode.Length == 0)
                return "";

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimlosscode ";
            SQL = SQL = "where losscoe = '" + xLossCode + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("losscodedesc");
            }

            return "";
        }

        private void GetPayee()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfClaimPayeeID.Value == "")
                return;

            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (hfLossCodeSeek.Value == "Labor")
                {
                    txtClaimPayeeNameLabor.Text = clR.GetFields("payeename");
                    txtClaimPayeeNoLabor.Text = clR.GetFields("payeeno");
                }
                else
                {
                    txtClaimPayee.Text = clR.GetFields("payeename");
                    txtPayeeNo.Text = clR.GetFields("payeeno");
                }
            }
        }

        protected void btnAddPart_Click(object sender, EventArgs e)
        {
            hfJobNo.Value = "";
            txtLossCodePart.Text = "";
            CreClaimDetail();
            pnlPartList.Visible = false;
            pnlPartDetail.Visible = true;
            txtPayeeNo.Text = "";
            txtPartNo.Text = "";
            txtPartDesc.Text = "";
            txtAuthCost.Text = "0";
            txtReqQty.Text = "0";
            txtReqCost.Text = "0";
            txtAuthQty.Text = "0";
            txtAuthQty.Text = "0";
            txtPayeeNo.Text = "";
            txtClaimPayee.Text = "";
            txtLossCodePart.Text = "";
            txtLossCodePartDesc.Text = "";
            txtShipping.Text = "0";
            trPartLabor.Visible = true;
            GetClaimPayeeID();
            txtLaborReqAmtPart.Text = hfShopLabor.Value;
            SetLossCodePart();
        }

        private void CreClaimDetail()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimdetailtype", "Part");
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("claimdetailstatus", "Requested");
                clR.SetFields("creby", hfUserID.Value);
                GetNextJobNo();
                clR.SetFields("jobno", hfJobNo.Value);
                hfAddPart.Value = "True";
                clR.AddRow();
                clR.SaveDB();
            }

            SQL = "update claim ";
            SQL = SQL + "set moddate = '" + DateTime.Today + "', ";
            SQL = SQL + "modby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "select max(claimdetailid) as mcd from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and creby = " + hfUserID.Value + " ";
            SQL = SQL + "and jobno = '" + hfJobNo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimDetailID.Value = clR.GetFields("mcd");
            }
        }

        private void SetLossCodePart()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select losscode from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodePart.Text = clR.GetFields("losscode");
            }

            if (txtLossCodePart.Text.Length > 0)
            {
                SQL = "select * from claimlosscode ";
                SQL = SQL + "where losscode = '" + txtLossCodePart.Text + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtLossCodePartDesc.Text = clR.GetFields("losscodedesc");
                }
            }
        }

        private void GetClaimPayeeID()
        {
            string SQL;
            clsDBO clCP = new clsDBO();
            clsDBO clR = new clsDBO();
            SQL = "select * from claim c ";
            SQL = SQL + "inner join servicecenter sc on sc.servicecenterid = c.servicecenterid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clCP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCP.RowCount() > 0)
            {
                clCP.GetRow();
                SQL = "select * from claimpayee ";
                SQL = SQL + "where payeeno = '" + clCP.GetFields("servicecenterno") + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                }
                else
                {
                    clR.NewRow();
                    clR.SetFields("payeeno", clCP.GetFields("servicecenterno"));
                    clR.SetFields("payeename", clCP.GetFields("servicecentername"));
                    clR.SetFields("addr1", clCP.GetFields("addr1"));
                    clR.SetFields("addr2", clCP.GetFields("addr2"));
                    clR.SetFields("city", clCP.GetFields("city"));
                    clR.SetFields("zip", clCP.GetFields("zip"));
                    clR.SetFields("phone", clCP.GetFields("phone"));
                    clR.SetFields("email", clCP.GetFields("email"));
                    clR.AddRow();
                    clR.SaveDB();
                    SQL = "select * from claimpayee ";
                    SQL = SQL + "where payeeno = '" + clCP.GetFields("servicecenterno") + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clR.RowCount() > 0)
                    {
                        clR.GetRow();
                        hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                    }
                }
            }
        }

        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "True";
            pnlPartDetail.Visible = false;
            pnlSearchClaimPayee.Visible = true;
        }

        protected void btnCancelPart_Click(object sender, EventArgs e)
        {
            pnlPartDetail.Visible = false;
            pnlPartList.Visible = true;
            if (hfAddPart.Value == "True")
                DeletePart();

            rgParts.Rebind();
        }

        private void DeletePart()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select max(claimdetailid) as ClaimDetailID from claimdetail ";
            SQL = SQL + "where creby = " + hfUserID.Value + " ";
            SQL = SQL + "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                SQL = "delete claimdetail ";
                SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }
        }

        protected void btnUpdatePart_Click(object sender, EventArgs e)
        {
            UpdatePart();
            hfAddPart.Value = "False";
            rgParts.Rebind();
            pnlPartDetail.Visible = false;
            pnlPartList.Visible = true;
        }

        private void UpdatePart()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            bool? bAdd = null;
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                bAdd = false;
                GetNextJobNo();
            }
            else
            {
                clR.NewRow();
                GetNextJobNo();
            }

            if (hfAddPart.Value == "True")
                bAdd = true;

            clR.SetFields("jobno", hfJobNo.Value);
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdetailtype", "Part");
            clR.SetFields("reqqty", txtReqQty.Text);
            clR.SetFields("partno", txtPartNo.Text);
            clR.SetFields("Claimdesc", txtPartDesc.Text);
            clR.SetFields("ReqCost", txtReqCost.Text);

            if (txtAuthCost.Text.Length == 0)
                txtAuthCost.Text = "0";

            clR.SetFields("Authcost", txtAuthCost.Text);
            clR.SetFields("AuthQty", txtAuthQty.Text);

            if (txtPartTax.Text.Length == 0)
                txtPartTax.Text = "0";

            clR.SetFields("taxPer", (Convert.ToDouble(txtPartTax.Text) / 100).ToString());
            if (clR.GetFields("claimdetailstatus") == "")
                clR.SetFields("claimdetailstatus", "Requested");

            if (txtShipping.Text.Length > 0)
                clR.SetFields("shippingcost", txtShipping.Text);

            GetTax();

            clR.SetFields("losscode", txtLossCodePart.Text);
  
            if (txtReqQty.Text.Length == 0)
                txtReqQty.Text = "0";

            if (txtReqCost.Text.Length == 0)
                txtReqCost.Text = "0";

            clR.SetFields("reqamt", ((Convert.ToDouble(txtReqCost.Text) * Convert.ToDouble(txtReqQty.Text)) + Convert.ToDouble(txtShipping.Text)).ToString());
            if (txtAuthQty.Text.Length == 0)
                txtAuthQty.Text = "0";

            if (txtAuthCost.Text.Length == 0)
                txtAuthCost.Text = "0";

            if (txtPartTax.Text.Length == 0)
                txtPartTax.Text = "0";

            clR.SetFields("ratetypeid", "1");
            clR.SetFields("taxamt", ((Convert.ToDouble(txtAuthCost.Text) * Convert.ToDouble(txtAuthQty.Text)) * (Convert.ToDouble(txtPartTax.Text) / 100)).ToString());
            clR.SetFields("taxamt", (Math.Round(Convert.ToDouble(clR.GetFields("taxamt")) * 100) / 100).ToString());
            clR.SetFields("authamt", ((Convert.ToDouble(txtAuthCost.Text) * Convert.ToDouble(txtAuthQty.Text)) + Convert.ToDouble(txtShipping.Text)).ToString());
            clR.SetFields("TotalAmt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);

        MoveHere:;

            if (clR.RowCount() > 0)
            {
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }

            clR.SaveDB();

            if (bAdd == true)
                SaveLaborPart();

            rgParts.Rebind();
            rgLabor.Rebind();
            SQL = "update claim ";
            SQL = SQL + "set moddate = '" + DateTime.Today + "', ";
            SQL = SQL + "modby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private bool CheckInspect()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select inspect from claim ";
            SQL = SQL + "WHERE CLAIMID = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("inspect") == "True")
                    return true;
            }

            return false;
        }

        private void SaveLaborPart()
        {
            if (txtLaborReqRatePart.Text.Length == 0)
                return;
            
            if (txtLaborReqAmtPart.Text.Length == 0)
                return;
            
            if (Convert.ToDouble(txtLaborReqAmtPart.Text) == 0)
                return;
            
            if (Convert.ToDouble(txtLaborReqRatePart.Text) == 0)
                return;

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = '" + hfJobNo.Value + "' ";
            SQL = SQL + "and claimdetailtype = 'Labor' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            if (txtLaborAuthAmtPart.Text.Length == 0)
                txtLaborAuthAmtPart.Text = "0";

            if (txtLaborAuthRatePart.Text.Length == 0)
                txtLaborAuthRatePart.Text = "0";

            if (txtLaborReqAmtPart.Text.Length == 0)
                txtLaborReqAmtPart.Text = "0";

            if (txtLaborReqRatePart.Text.Length == 0)
                txtLaborReqRatePart.Text = "0";

            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("jobno", hfJobNo.Value);
            clR.SetFields("claimdetailtype", "Labor");
            clR.SetFields("reqqty", txtLaborReqAmtPart.Text);
            clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqcost", txtLaborReqRatePart.Text);
            clR.SetFields("ratetypeid", "1");
            clR.SetFields("authcost", txtLaborAuthRatePart.Text);
            clR.SetFields("authqty", txtLaborAuthAmtPart.Text);
            GetTax();
            clR.SetFields("losscode", txtLossCodePart.Text);
            clR.SetFields("reqamt", (Convert.ToDouble(clR.GetFields("reqqty")) * Convert.ToDouble(clR.GetFields("reqcost"))).ToString());
            if (txtLaborTax.Text.Length == 0)
                txtLaborTax.Text = "0";

            clR.SetFields("taxamt", (Math.Round((Convert.ToDouble(txtLaborAuthRatePart.Text)
                * Convert.ToDouble(txtLaborAuthAmtPart.Text))
                * (Convert.ToDouble(txtLaborTax.Text))) / 100).ToString());
            clR.SetFields("authamt", (Convert.ToDouble(txtLaborAuthRatePart.Text) * Convert.ToDouble(txtLaborAuthAmtPart.Text)).ToString());
            clR.SetFields("totalamt", (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("claimdesc", txtPartDesc.Text);

            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.RowCount() == 0) 
            {
                clR.SetFields("ratetypeid", "1");
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }

            clR.SaveDB();
        }

        private void GetTax()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfPartTax.Value = clR.GetFields("parttax");
                hfLaborTax.Value = clR.GetFields("labortax");
            }

            if (hfPartTax.Value.Length == 0)
                hfPartTax.Value = "0";

            if (hfLaborTax.Value.Length == 0)
                hfLaborTax.Value = "0";
        }

        private void GetClaimDetailID()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = '" + hfJobNo.Value + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimDetailID.Value = clR.GetFields("claimdetailid");
            }
        }

        private void GetNextJobNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfJobNo.Value.Length == 0)
            {
                SQL = "select max(jobno) as mJob from claimdetail ";
                SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
                SQL = SQL + "and jobno like 'j%' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() == 0)
                    hfJobNo.Value = "J01";
                else
                {
                    clR.GetRow();
                    if (clR.GetFields("mjob").Length == 0)
                        hfJobNo.Value = "J01";
                    else
                        hfJobNo.Value = "J" + (Convert.ToInt64(clR.GetFields("mjob").Substring(clR.GetFields("mjob").Length - 2, 2)) + 1).ToString("00");
                }
            }

            SQL = "select min(jobno) as mJob from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno like 'j%' ";
            if (txtLossCodePart.Text.Length > 0)
                SQL = SQL + "and losscode = '" + txtLossCodePart.Text.Trim() + "' ";
            else
                SQL = SQL + "and losscode is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                if (clR.GetFields("mjob").Length > 0)
                    hfJobNo.Value = clR.GetFields("mJob");
            }
        }

        private void GetNextJobNoLabor()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfJobNo.Value.Length == 0) {
                SQL = "select max(jobno) as mJob from claimdetail ";
                SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
                SQL = SQL + "and jobno like 'j%' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() == 0)
                    hfJobNo.Value = "J01";
                else 
                {
                    clR.GetRow();
                    if (clR.GetFields("mjob").Length == 0)
                        hfJobNo.Value = "J01";
                    else
                        hfJobNo.Value = "J" + (Convert.ToInt64(clR.GetFields("mjob").Substring(clR.GetFields("mjob").Length - 2, 2)) + 1).ToString("00");
                }
             }

            SQL = "select min(jobno) as mJob from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno like 'j%' ";
            if (txtLossCodeLabor.Text.Length > 0)
                SQL = SQL + "and losscode = '" + txtLossCodeLabor.Text.Trim() + "' ";
            else
                SQL = SQL + "and losscode is null ";

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            if (clR.GetFields("mjob").Length > 0)
                hfJobNo.Value = clR.GetFields("mJob");   
        }

        protected void btnAddClaimPayee_Click(object sender, EventArgs e)
        {
            pnlSearchClaimPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            txtServiceCenterName.Text = "";
            txtZip.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            cboState.ClearSelection();
            txtPhone.Text = "";
        }

        protected void btnSCCancel_Click(object sender, EventArgs e)
        {
            pnlSearchClaimPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            txtServiceCenterName.Text = "";
            txtZip.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            cboState.ClearSelection();
            txtPhone.Text = "";
        }

        protected void btnSCSave_Click(object sender, EventArgs e)
        {
            AddServiceCenter();
            AddClaimPayee();
            rgClaimPayee.Rebind();
            pnlSearchClaimPayee.Visible = true;
            pnlAddClaimPayee.Visible = false;
        }

        private void AddClaimPayee() {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            clR.SetFields("payeeno", hfServiceCenterNo.Value);
            clR.SetFields("payeename", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("fax", txtFax.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.AddRow();
            clR.SaveDB();
        }

        private void AddServiceCenter()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            GetNextServiceCenterNo();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentername", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("fax", txtFax.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.AddRow();
            clR.SaveDB();
        }

        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter ";
            SQL = SQL + "where servicecenterno like 'RF0%' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            hfServiceCenterNo.Value = "RF" + Convert.ToInt64(clR.GetFields("mscn").Substring(clR.GetFields("mscn").Length - 6, 6) + 1).ToString("0000000");
        }

        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            string SQL;
            clsDBO clCP = new clsDBO();
            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = " + rgClaimPayee.SelectedValue;
            clCP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCP.RowCount() > 0)
            {
                clCP.GetRow();
                if (hfLossCodeSeek.Value == "Labor")
                {
                    txtClaimPayeeNameLabor.Text = clCP.GetFields("payeename");
                    txtClaimPayeeNoLabor.Text = clCP.GetFields("payeeno");
                    pnlLaborDetail.Visible = true;
                    pnlSearchClaimPayee.Visible = false;
                }
                else
                {
                    txtClaimPayee.Text = clCP.GetFields("payeename");
                    txtPayeeNo.Text = clCP.GetFields("payeeno");
                    pnlPartDetail.Visible = true;
                    pnlSearchClaimPayee.Visible = false;
                }
            }

        }

        protected void btnSeekLossCodePart_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "Part";
            pnlPartDetail.Visible = false;
            pnlSeekLossCode.Visible = true;
            rgLossCode.Rebind();
            ColorLossCode();
        }

        protected void rgLossCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfLossCodeSeek.Value == "Part") 
            {
                txtLossCodePart.Text = rgLossCode.SelectedValue.ToString();
                SQL = "select * from claimlosscode ";
                SQL = SQL + "where losscode = '" + rgLossCode.SelectedValue + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                {
                    clR.GetRow();
                    txtLossCodePartDesc.Text = clR.GetFields("losscodedesc");
                }

                pnlPartDetail.Visible = true;
                pnlSeekLossCode.Visible = false;
                GetNextJobNo();
                LockAuthPartText();
            }

            if (hfLossCodeSeek.Value == "Labor") 
            {
                txtLossCodeLabor.Text = rgLossCode.SelectedValue.ToString();
                SQL = "select * from claimlosscode ";
                SQL = SQL + "where losscode = '" + rgLossCode.SelectedValue + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0) {
                    clR.GetRow();
                    txtLossCodeDescLabor.Text = clR.GetFields("losscodedesc");
                }

                pnlLaborDetail.Visible = true;
                pnlSeekLossCode.Visible = false;
                GetNextJobNoLabor();
                LockAuthPartText();
            }
            
        }

        private void LockAuthLaborText()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllCoverage")))
                    return;
            }

            if (GetSaleDate() < Convert.ToDateTime("1/1/2019"))
                return;


            SQL = "select * from claimlosscode clc ";
            SQL = SQL + "inner join contractcoverage cc ";
            SQL = SQL + "on cc.CoverageCode = clc.LossCodePrefix ";
            SQL = SQL + "inner join contract c ";
            SQL = SQL + "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID ";
            SQL = SQL + "inner join claim cl ";
            SQL = SQL + "on cl.ContractID = c.ContractID ";
            SQL = SQL + "where cl.claimid = " + hfClaimID.Value + " ";
            if (txtLossCodeLabor.Text.Length == 0)
                return;

            SQL = SQL + "and cc.coveragecode = '" + txtLossCodeLabor.Text.Substring(0, 4) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() == 0)
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set losscodelock = 1 ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                txtLaborAuthHours.Enabled = false;
                txtLaborAuthRate.Enabled = false;
                txtLaborTax.Enabled = false;
                txtLossCodeLabor.BackColor = System.Drawing.Color.Red;
            }
            else 
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set losscodelock = 0 ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                txtLaborAuthHours.Enabled = true;
                txtLaborAuthRate.Enabled = true;
                txtLaborTax.Enabled = true;
                txtAuthCost.Enabled = true;
                txtLossCodeLabor.BackColor = System.Drawing.Color.White;
            }
        }

        private DateTime GetSaleDate()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select c.saledate from contract c ";
            SQL = SQL + "inner join claim cl on cl.contractid = c.contractid ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                return Convert.ToDateTime(clR.GetFields("saledate"));
            }

            return Convert.ToDateTime("1/1/1950");
        }

        private void LockAuthPartText()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllCoverage")))
                    return;
                
            
            if (GetSaleDate() < Convert.ToDateTime("1/1/2019"))
                return;

            SQL = "select * from claimlosscode clc ";
            SQL = SQL + "inner join contractcoverage cc ";
            SQL = SQL + "on cc.CoverageCode = clc.LossCodePrefix ";
            SQL = SQL + "inner join contract c ";
            SQL = SQL + "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID ";
            SQL = SQL + "inner join claim cl ";
            SQL = SQL + "on cl.ContractID = c.ContractID ";
            SQL = SQL + "where cl.claimid = " + hfClaimID.Value + " ";
            if (txtLossCodePart.Text.Length == 0)
                return;

            SQL = SQL + "and cc.coveragecode = '" + txtLossCodePart.Text.Substring(0, 4) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set losscodelock = 1 ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                txtAuthCost.Enabled = false;
                txtAuthQty.Enabled = false;
                txtPartTax.Enabled = false;
                txtLaborAuthAmtPart.Enabled = false;
                txtLaborAuthRatePart.Enabled = false;
                txtLossCodePart.BackColor = System.Drawing.Color.Red;
            }
            else 
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set losscodelock = 0 ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                txtAuthCost.Enabled = true;
                txtAuthQty.Enabled = true;
                txtPartTax.Enabled = true;
                txtLaborAuthAmtPart.Enabled = true;
                txtLaborAuthRatePart.Enabled = true;
                txtLossCodePart.BackColor = System.Drawing.Color.White;
            }
        }

        protected void btnAddLabor_Click(object sender, EventArgs e)
        {
            pnlPartList.Visible = false;
            pnlLaborDetail.Visible = true;
            hfClaimDetailID.Value = "0";
            ClearLabor();
            txtLaborReqRate.Text = hfShopLabor.Value;
            hfJobNo.Value = "";
            txtLossCodePart.Text = "";
            SetLaborLossCode();
        }

        private void SetLaborLossCode()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select losscode from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                txtLossCodeLabor.Text = clR.GetFields("losscode");
            }
            if (txtLossCodePart.Text.Length > 0) {
                SQL = "select * from claimlosscode ";
                SQL = SQL + "where losscode = '" + txtLossCodePart.Text + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0) {
                    clR.GetRow();
                    txtLossCodeDescLabor.Text = clR.GetFields("losscodedesc");
                }
            }
        }

        private void ClearLabor()
        {
            txtClaimPayee.Text = "";
            txtPayeeNo.Text = "";
            txtLaborReqHours.Text = "1";
            txtLaborReqRate.Text = "";
            txtLaborAuthHours.Text = "1";
            txtLaborAuthRate.Text = "";
            hfClaimDetailID.Value = "0";
            GetClaimPayeeID();
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                txtClaimPayeeNoLabor.Text = clR.GetFields("payeeno");
                txtClaimPayeeNameLabor.Text = clR.GetFields("payeename");
            }
        }

        protected void btnSeekLossCodeLabor_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "Labor";
            pnlLaborDetail.Visible = false;
            pnlSeekLossCode.Visible = true;
            rgLossCode.Rebind();
            ColorLossCode();

        }

        protected void btnSeekClaimPayeeLabor_Click(object sender, EventArgs e)
        {
            hfLossCodeSeek.Value = "Labor";
            pnlLaborDetail.Visible = false;
            pnlSearchClaimPayee.Visible = true;
        }

        protected void btnCancelLabor_Click(object sender, EventArgs e)
        {
            pnlLaborDetail.Visible = false;
            pnlPartList.Visible = true;
            rgLabor.Rebind();
        }

        protected void btnUpdateLabor_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                GetNextJobNoLabor();
            }
            else 
            {
                clR.NewRow();
                GetNextJobNoLabor();
                clR.SetFields("claimid", hfClaimID.Value);
            }

            clR.SetFields("claimdetailtype", "Labor");

            if (txtLaborReqRate.Text == "")
                txtLaborReqRate.Text = "0";
            
            if (txtLaborReqHours.Text == "")
                txtLaborReqHours.Text = "0";

            if (txtLaborAuthRate.Text == "")
                txtLaborAuthRate.Text = "0";

            if (txtLaborAuthHours.Text == "")
                txtLaborAuthHours.Text = "0";

            if (txtLaborTaxRate.Text.Length == 0)
                txtLaborTaxRate.Text = "0";

            clR.SetFields("jobno", hfJobNo.Value);
            clR.SetFields("taxper", (Convert.ToDouble(txtLaborTaxRate.Text) / 100).ToString());
            clR.SetFields("Reqqty", txtLaborReqHours.Text);
            clR.SetFields("reqcost", txtLaborReqRate.Text);
            clR.SetFields("Authqty", txtLaborAuthHours.Text);
            clR.SetFields("Authcost", txtLaborAuthRate.Text);
            clR.SetFields("ratetypeid", "1");
            clR.SetFields("losscode", txtLossCodeLabor.Text);
            clR.SetFields("reqamt", (Convert.ToDouble(txtLaborReqHours.Text) * Convert.ToDouble(txtLaborReqRate.Text)).ToString());
            if (hfLaborTax.Value == "")
                hfLaborTax.Value = "0";

            if (txtLaborTaxRate.Text.Length == 0)
                txtLaborTaxRate.Text = "0";

            clR.SetFields("taxper", (Convert.ToDouble(txtLaborTaxRate.Text) / 100).ToString());
            clR.SetFields("taxamt", ((Convert.ToDouble(txtLaborAuthHours.Text)
                * Convert.ToDouble(txtLaborAuthRate.Text))
                * (Convert.ToDouble(txtLaborTaxRate.Text) / 100)).ToString());
            clR.SetFields("taxamt", (Math.Round(Convert.ToDouble(clR.GetFields("taxamt")) * 100) / 100).ToString());
            clR.SetFields("authamt", (Convert.ToDouble(txtLaborAuthHours.Text) * Convert.ToDouble(txtLaborAuthRate.Text)).ToString());
            clR.SetFields("totalamt", (clR.GetFields("authamt") + Convert.ToDouble(clR.GetFields("taxamt"))).ToString());
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("claimdesc", txtLaborDesc.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.GetFields("claimdetailstatus") == "Authorized" && !CheckInspect()) {
                lblAuthorizedError.Text = "Inspection has not been completed.";
                pnlPartList.Visible = true;
                pnlPartDetail.Visible = false;
                pnlLaborDetail.Visible = false;
                clR.SetFields("claimdetailstatus", "Requested");
            }

            if (clR.GetFields("claimdetailstatus") == "Approved" && !CheckInspect()) {
                lblAuthorizedError.Text = "Inspection has not been completed.";
                pnlPartList.Visible = true;
                pnlPartDetail.Visible = false;
                pnlLaborDetail.Visible = false;
                clR.SetFields("claimdetailstatus", "Requested");
            }

            if (clR.GetFields("claimdetailstatus") == "")
                clR.SetFields("claimdetailstatus", "Requested");

            if (clR.RowCount() == 0)
                clR.SetFields("credate", DateTime.Today.ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.AddRow();


            clR.SaveDB();
            pnlPartList.Visible = true;
            pnlLaborDetail.Visible = false;
            rgLabor.Rebind();
            SQL = "update claim ";
            SQL = SQL + "set moddate = '" + DateTime.Today.ToString() + "', ";
            SQL = SQL + "modby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private bool CheckLimitApproved(double xAmt)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dLimit;

            SQL = "select claimapprove from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimapprove"));
            }
            else
                dLimit = 0;

            SQL = "select sum(authamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'approved' ";
            SQL = SQL + "or claimdetailstatus = 'Authorized') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0) {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")))
                        return true;
                }
                else 
                {
                    if (dLimit > xAmt)
                        return true;
               }
            }

            return false;
        }

        private bool CheckLimitAuthorized(double xAmt)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dLimit;

            SQL = "select claimauth from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimauth"));
            }
            else
                dLimit = 0;

            SQL = "select sum(authamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'approved' ";
            SQL = SQL + "or claimdetailstatus = 'Authorized') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")))
                        return true;
                }
                else
                {
                    if (dLimit > xAmt)
                        return true;
                }
            }

            return false;
        }

        protected void rgLabor_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlPartList.Visible = false;
            pnlLaborDetail.Visible = true;
            hfClaimDetailID.Value = rgLabor.SelectedValue.ToString();
            FillLaborDetail();
            LockAuthLaborText();
        }

        private void FillLaborDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                if (clR.GetFields("qty") == "")
                    clR.SetFields("qty", "0");

                if (clR.GetFields("cost") == "")
                    clR.SetFields("cost", "0");


                txtLaborReqHours.Text = clR.GetFields("reqqty");
                txtLaborReqRate.Text = clR.GetFields("reqcost");
                txtLaborAuthRate.Text = clR.GetFields("authcost");
                txtLaborAuthHours.Text = clR.GetFields("authqty");
                txtLaborTaxRate.Text = (Convert.ToDouble(clR.GetFields("taxper")) / 100).ToString();
                if (Convert.ToDouble(clR.GetFields("taxper")) == 0)
                    txtLaborTaxRate.Text = hfLaborTax.Value;


                txtLaborTax.Text = clR.GetFields("taxamt");

                if (txtLaborAuthHours.Text.Length == 0)
                    txtLaborAuthHours.Text = "0";


                if (txtLaborAuthRate.Text.Length == 0)
                    txtLaborAuthRate.Text = "0";


                if (txtLaborTax.Text.Length == 0)
                    txtLaborTax.Text = "0";


                txtLossCodeLabor.Text = clR.GetFields("losscode");
                txtLossCodeDescLabor.Text = GetLossCodeDesc(clR.GetFields("losscode"));
                hfLossCodeSeek.Value = "Labor";
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                txtLaborDesc.Text = clR.GetFields("claimdesc");
                GetPayee();
            }
        }

        protected void txtPartNo_TextChanged(object sender, EventArgs e)
        {
            GetPartInfo();
            txtPartDesc.Focus();
        }

        private void GetPartInfo()
        {
            string sVIN, sMake, sPartDesc;
            long lQty;

            if (txtReqQty.Text.Length == 0)
                lQty = 1;
            else
                lQty = Convert.ToInt64(txtReqQty.Text);

            sVIN = hfClaimDetailID.Value;
            sVIN = GetVIN();
            sMake = GetMake(sVIN);
            Process.Start(@"C:\ProcessProgram\CKParts\CKParts.exe", hfClaimDetailID.Value).WaitForExit();
            sPartDesc = GetPartDesc();
            if (sPartDesc.Length > 0)
                txtPartDesc.Text = sPartDesc;
        }

        private string GetPartDesc()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select claimdesc from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("claimdesc");
            }

            return "";
        }

        private string GetMake(string xVIN)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from vin.dbo.vin v ";
            SQL = SQL + "inner join vin.dbo.basicdata bd on v.vinid = bd.vinid ";
            SQL = SQL + "where v.vin = '" + xVIN.Substring(0, 11) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("make");
            }

            return "";
        }

        private string GetVIN()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select vin from contract c ";
            SQL = SQL + "inner join claim cl on c.contractid = cl.contractid ";
            SQL = SQL + "where cl.claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("vin");
            }

            return "";
        }

        protected void btnSendQuote_Click(object sender, EventArgs e)
        {
            string sVIN;
            VeritasGlobalToolsV2.clsParts clPart = new VeritasGlobalToolsV2.clsParts();
            sVIN = GetVIN();
            clPart.PartNo = txtPartNo.Text;
            clPart.PartName = txtPartDesc.Text;
            clPart.ClaimID = Convert.ToInt64(hfClaimID.Value);
            clPart.VIN = sVIN;
            clPart.SendPartRequest();
        }

        private void ProcessPurchase()
        {
            string SQL, sJobNo = "", sLossCode = "";
            clsDBO clR = new clsDBO();
            double dPartCost = 0;
            lblApprovedError.Text = "";

            SQL = "select * from claimdetailquote ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) { 
                clR.GetRow();
                if (clR.GetFields("orderid") != "") {
                    lblApprovedError.Text = "This part has been ordered all ready";
                    return;
                }
                if (Convert.ToDouble(clR.GetFields("youcost")) < 50) {
                    lblApprovedError.Text = "This part is to low of cost for purchase.";
                    return;
                }

               if (!CheckLimitAuthorized(Convert.ToDouble(clR.GetFields("yourcost")) + 42.5)) {
                    lblApprovedError.Text = "You are not authorized to make purchase.";
                    return;
                }

                Process.Start(@"C:\ProcessProgram\CKPartPurchase\CKPartPurchase.exe", clR.GetFields("claimdetailquoteid") + " " + hfUserID.Value).WaitForExit();
               //FillPartQuotes();

            }

            SQL = "select * from claimdetailquote ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                if (clR.GetFields("orderid") == "")
                    return;

                dPartCost = Convert.ToDouble(clR.GetFields("partcost"));
            }

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                clR.SetFields("authamt", dPartCost.ToString());
                clR.SetFields("taxper", "0");
                clR.SetFields("taxamt", "0");
                clR.SetFields("totalamt", dPartCost.ToString());
                clR.SetFields("claimdetailstatus", "Authorized");
                clR.SetFields("claimpayeeid", "7351");
                clR.SetFields("dateauth", DateTime.Today.ToString());
                clR.SetFields("authby", hfUserID.Value);
                sJobNo = clR.GetFields("jobno");
                sLossCode = clR.GetFields("losscode");
                clR.SaveDB();
            }
            SQL = "insert into claimdetail ";
            SQL = SQL + @"(claimid,claimdetailtype, JobNo; reqqty,reqcost,authqty, quthcost,reqamt,losscode,authamt,taxper,
                        taxamt,claimdetailstatus, dateauth, authby, credate, creby, moddate, modby,ClaimDesc, claimpayeeid)";
            SQL = SQL + "values ";
            SQL = SQL + "(" + hfClaimID.Value + ",'Part','" + sJobNo + "', 1, 42.50, 1, 42.50, 42.50,'" + sLossCode +
                "',42.50,0,0, 'Authorized','" + DateTime.Today + "'," + hfUserID.Value + ",'" + DateTime.Today + "'," +
                hfUserID.Value + ",'" + DateTime.Today + "'," + hfUserID.Value + ",'Shipping',7351";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void ColorLossCode()
        {
            if (GetSaleDate() < Convert.ToDateTime("1/1/2019"))
                return;

            rgLossCode.Columns[3].Visible = true;

            for (int i = 0; i <= rgLossCode.Items.Count - 1; i++)
            {
                if (rgLossCode.Items[i].Cells[3].Text == "1")
                    rgLossCode.Items[i].Cells[1].BackColor = System.Drawing.Color.Red;
            }
            rgLossCode.Columns[3].Visible = false;
        }
    }
}