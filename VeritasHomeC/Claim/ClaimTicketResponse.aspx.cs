﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class ClaimTicketResponse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsCDT.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimDetailStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimReason.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsJobs.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack) {
                GetServerInfo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                FillClaimCombo();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
                FillClaimCombo();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
                if (!Convert.ToBoolean(clSI.GetFields("claimpayment")))
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }

        private void FillClaimCombo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select claimid, claimno from claim ";
            SQL = SQL + "where claimid in (select claimid from veritasclaimticket.dbo.ticket where responseid <> 1 and statusid = 2) ";
            cboClaim.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            cboClaim.DataBind();
            hfClaimID.Value = cboClaim.SelectedValue;
            FillList();
        }

        private void FillList()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = @"select cd.claimdetailid, claimdetailtype,cd.partno, cd.claimdesc, claimdetailstatus, jobno, cd.losscode, lc.losscodedesc,
                payeename, reqamt, authamt, taxamt, paidamt, totalamt, r.response From claimdetail cd
                inner join veritasclaimticket.dbo.ticket T on t.claimdetailid = cd.claimdetailid
                inner join veritasclaimticket.dbo.response R on t.responseid = r.responseid
                left join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid
                Left join claimlosscode lc on lc.losscode = cd.losscode ";
            SQL = SQL + "where t.claimid = " + cboClaim.SelectedValue + " ";
            SQL = SQL + "and (t.responseid = 2 or t.responseid = 3) and statusid = 2 ";
            SQL = SQL + "order by jobno ";
            rgJobs.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgJobs.Rebind();
        }

        protected void cboClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimID.Value = cboClaim.SelectedValue;
        }

        protected void rgJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimDetailID.Value = rgJobs.SelectedValue.ToString();
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clT = new clsDBO();
            SQL = "select * from claimdetail where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                SQL = "select * from veritasclaimticket.dbo.ticket t ";
                SQL = SQL + "inner join veritasclaimticket.dbo.response r on r.responseid = t.responseid ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value + " ";
                clT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clT.RowCount() > 0) {
                    clT.GetRow();
                    cboClaimDetailType.SelectedValue = clR.GetFields("claimdetailtype");
                    txtJobNo.Text = clR.GetFields("jobno");
                    cboClaimDetailStatus.SelectedValue = clR.GetFields("claimdetailstatus");
                    txtPartNo.Text = clR.GetFields("partno");
                    txtClaimDesc.Text = clR.GetFields("claimdesc");
                    txtLossCode.Text = clR.GetFields("losscode");
                    cboReason.SelectedValue = clR.GetFields("claimreasonid");
                    txtLossCodeDesc.Text = GetLossCode(clR.GetFields("losscode"));
                    CalcPayee(Convert.ToInt64(clR.GetFields("claimpayeeid")));
                    txtReqQty.Text = clR.GetFields("reqqty");
                    txtReqCost.Text = clR.GetFields("reqcost");
                    txtReqAmt.Text = clR.GetFields("reqamt");
                    txtAuthQty.Text = clR.GetFields("authqty");
                    txtAuthAmt.Text = clR.GetFields("authcost");
                    txtTaxRate.Text = clR.GetFields("taxper");
                    txtTaxAmt.Text = clR.GetFields("taxamt");
                    txtTotalAmt.Text = clR.GetFields("totalamt");
                    txtAuthAmt.Text = clR.GetFields("authamt");

                    if (clR.GetFields("dateauth").Length > 0)
                        rdpAuthorizedDate.SelectedDate = Convert.ToDateTime(clR.GetFields("dateauth"));
                    else
                        rdpAuthorizedDate.Clear();

                    if (clR.GetFields("dateapprove").Length > 0)
                        rdpDateApprove.SelectedDate = Convert.ToDateTime(clR.GetFields("dateapprove"));
                    else
                        rdpDateApprove.Clear();

                    txtResponse.Text = clT.GetFields("response");
                    cboRateType.SelectedValue = clR.GetFields("ratetypeid");

                    if (clT.GetFields("responseid") == "2")
                    {
                        btnCloseTicket.Visible = true;
                        btnCancelClaimDetail.Visible = true;
                        btnSaveClaimDetail.Visible = false;
                    }
                    else
                    {
                        btnCloseTicket.Visible = false;
                        btnCancelClaimDetail.Visible = true;
                        btnSaveClaimDetail.Visible = true;
                    }


                    pnlList.Visible = false;
                    pnlDetail.Visible = true;

                }
            }
        }

        private void GetResponse(long xResponseID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from response ";
            SQL = SQL + "where responseid = " + xResponseID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtResponse.Text = clR.GetFields("response");
            }
        }

        private void CalcPayee(long xClaimPayeeID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = " + xClaimPayeeID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeNo.Text = clR.GetFields("payeeno");
                txtPayeeName.Text = clR.GetFields("payeename");
            }
        }

        private string GetLossCode(string xLossCode)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimlosscode ";
            SQL = SQL + "where losscode = '" + xLossCode + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("losscodedesc");
            }

            return "";
        }

        protected void txtAuthQty_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtAuthCost.Focus();
        }

        protected void txtAuthCost_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            dTax = (Math.Round(dTax * 100) / 100);
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtAuthCost.Focus();
        }

        protected void txtTaxRate_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            dTax = (Math.Round(dTax * 100) / 100);
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtAuthCost.Focus();
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        private void FillRateType()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select ratetypeid, RateTypeName from ratetype ";
            SQL = SQL + "where ratetypeid in (1,2834, 1101) ";
            SQL = SQL + "or ratetypeid in (select RateTypeID from ratetype ";
            SQL = SQL + "where ratecategoryid = 5 ";
            SQL = SQL + "and ratetypeid in (select ratetypeid ";
            SQL = SQL + "from contractcommissions ";
            SQL = SQL + "where agentid in (select agentsid from contract ";
            SQL = SQL + "where contractid in (select contractid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + ")))) ";
            SQL = SQL + "or RateTypeID in (select 10 from contract ";
            SQL = SQL + "where contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) ";
            SQL = SQL + "and (contractno like 'r%' or contractno like 'vel%')) ";
            SQL = SQL + "or RateTypeID in (select 2868 from contract ";
            SQL = SQL + "where contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) ";
            SQL = SQL + "and (contractno like 'va%' or contractno like 'vg%' or contractno like 'vep%')) ";
            SQL = SQL + "or RateTypeID in (select 24 from contract c ";
            SQL = SQL + "inner join contractamt ca on c.contractid = ca.contractid ";
            SQL = SQL + "where c.contractid in (select contractid from claim where claimid =  " + hfClaimID.Value + " ) ";
            SQL = SQL + "and ca.RateTypeID = 24) ";
            SQL = SQL + "or RateTypeID in (select 25 from contract c ";
            SQL = SQL + "inner join contractamt ca on c.contractid = ca.contractid ";
            SQL = SQL + "where c.contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) ";
            SQL = SQL + "and ca.RateTypeID = 25) ";
            SQL = SQL + "order by ratetypeid ";
            cboRateType.Items.Clear();
            long a = cboRateType.Items.Count;

            RadComboBoxItem i1 = new RadComboBoxItem();
            i1.Value = "0";
            i1.Text = "";
            cboRateType.Items.Add(i1);
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                for (int i = 0; i <= clR.RowCount() - 1; i++) {
                    clR.GetRowNo(i);
                    i1 = new RadComboBoxItem();
                    i1.Value = clR.GetFields("ratetypeid");
                    i1.Text = clR.GetFields("ratetypename");
                    cboRateType.Items.Add(i1);
                }
            }
        }

        protected void btnSaveClaimDetail_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimdesc", txtClaimDesc.Text);
                clR.SetFields("claimdetailtype", cboClaimDetailType.SelectedValue);
                
                if (txtJobNo.Text.Length > 0)
                    clR.SetFields("jobno", txtJobNo.Text);

                clR.SetFields("losscode", txtLossCode.Text);
                clR.SetFields("reqamt", txtReqAmt.Text);
                clR.SetFields("authcost", txtAuthCost.Text);

                if (txtTaxRate.Text.Length > 0)
                    clR.SetFields("taxper", (Convert.ToDouble(txtTaxRate.Text) / 100).ToString());

                if (txtTaxAmt.Text.Length == 0)
                    clR.SetFields("taxamt", "0");
                else
                    if (clR.GetFields("authqty").Length == 0)
                    clR.SetFields("authqty", "0");

                clR.SetFields("taxamt", txtTaxAmt.Text);

                clR.SetFields("totalamt", txtTotalAmt.Text);
                clR.SetFields("Authamt", txtAuthAmt.Text);
                clR.SetFields("claimdetailstatus", cboClaimDetailStatus.SelectedValue);
                clR.SetFields("claimreasonid", cboReason.SelectedValue);

                if (!String.IsNullOrEmpty(rdpAuthorizedDate.SelectedDate.ToString()))
                    clR.SetFields("dateauth", rdpAuthorizedDate.SelectedDate.ToString());
                else
                    clR.SetFields("dateauth", "");


                if (!String.IsNullOrEmpty(rdpDateApprove.SelectedDate.ToString()))
                    clR.SetFields("dateapprove", rdpDateApprove.SelectedDate.ToString());
                else
                    clR.SetFields("dateapprove", "");


                if (clR.GetFields("ratetypeid") == "1") 
                {
                    if (cboRateType.SelectedValue != "1") 
                    {
                        clR.SetFields("slushbyid", hfUserID.Value);
                        clR.SetFields("slushdate", DateTime.Today.ToString());
                    }
                }

                clR.SetFields("ratetypeid", cboRateType.SelectedValue);
                clR.SetFields("reqqty", txtReqQty.Text);
                clR.SetFields("reqcost", txtReqCost.Text);
                clR.SetFields("authqty", txtAuthQty.Text);
                clR.SetFields("authcost", txtAuthCost.Text);
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);

                if (clR.GetFields("claimdetailstatus") != "Paid") 
                {
                    if (cboClaimDetailStatus.SelectedValue == "Approved") {
                        if (txtAuthAmt.Text.Length == 0)
                            txtAuthAmt.Text = "0";

                        if (clR.GetFields("claimdetailid").Length > 0)
                        {
                            if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), Convert.ToInt64(clR.GetFields("claimdetailid"))))
                                lblApprovedError.Text = "Send to Manager for Approval";
                                goto MoveHere;
                        }
                        else 
                        {
                            if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), 0)) 
                            {
                                lblApprovedError.Text = "Send to Manager for Approval";
                                goto MoveHere;
                            }
                        }
                    }

                    if (cboClaimDetailStatus.SelectedValue == "Authorized")
                    {
                        if (txtAuthAmt.Text.Length == 0)
                            txtAuthAmt.Text = "0";

                        if (!CheckLimitAuthorized(Convert.ToDouble(txtAuthAmt.Text), Convert.ToInt64(clR.GetFields("claimdetailid")))) 
                        {
                            lblApprovedError.Text = "Send to Manager for Approval";
                            clR.SetFields("claimdetailstatus", "Requested");
                        }
                    }
                }

            //MoveToSave:;

                if (clR.RowCount() == 0) {
                    clR.SetFields("credate", DateTime.Today.ToString());
                    clR.SetFields("creby", hfUserID.Value);
                    clR.AddRow();
                }
                clR.SaveDB();

            MoveHere:;

                pnlDetail.Visible = false;
                pnlList.Visible = true;
                FillList();
                rgJobs.Rebind();
                SQL = "update claim ";
                SQL = SQL + "set moddate = '" + DateTime.Today + "', ";
                SQL = SQL + "modby = " + hfUserID.Value + " ";
                SQL = SQL + "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }

            CloseTicket();
        }

        private bool CheckLimitApproved(double xAmt, long xClaimDetailID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dLimit;

            SQL = "select claimapprove from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimapprove"));
            }
            else
                dLimit = 0;

            SQL = "select sum(authamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'approved' ";
            SQL = SQL + "or claimdetailstatus = 'Authorized') ";
            SQL = SQL + "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0) {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")) + xAmt)
                        return true;
                }
                else 
                {
                    if (dLimit > xAmt)
                        return true;
                }
            }

            return false;
        }

        private bool CheckLimitAuthorized(double xAmt, long xClaimDetailID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dLimit;

            SQL = "select claimauth from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimauth"));
            }
            else
                dLimit = 0;

            SQL = "select sum(authamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'approved' ";
            SQL = SQL + "or claimdetailstatus = 'Authorized') ";
            SQL = SQL + "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt")) + xAmt)
                        return true;
                }
                else
                {
                    if (dLimit > xAmt)
                        return true;
                }
            }

            return false;
        }

        protected void btnCancelClaimDetail_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            FillList();
        }

        protected void btnCloseTicket_Click(object sender, EventArgs e)
        {
            CloseTicket();
            FillList();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        private void CloseTicket()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update veritasclaimticket.dbo.ticket ";
            SQL = SQL + "set statusid = 4 ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }
    }
}