﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ServiceCenters : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tbTodo.Width = pnlHeader.Width;
            SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            GetServerInfo();
            CheckToDo();
            if (!IsPostBack) 
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (System.Configuration.ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                if (hfUserID.Value == "1")
                    btnLossCode.Visible = true;
                else
                    btnLossCode.Visible = false;

                ReadOnlyButtons();
                lblSCError.Visible = false;
                btnRFClaimSubmit.Enabled = false;
                CheckRFClaimSubmit();
                CheckSecurity();
            }
        }

        private void CheckSecurity()
        {
            string SQL;
            clsDBO clUSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (Convert.ToBoolean(clUSI.GetFields("AllowClaimAudit")))
                    btnClaimAudit.Enabled = true;
                else
                    btnClaimAudit.Enabled = false;
            }
        }

        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim ";
                SQL = SQL + "where not sendclaim is null ";
                SQL = SQL + "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                    btnRFClaimSubmit.Enabled = true;
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    Response.Redirect("~/users/claimssearch.aspx?sid=" + hfID.Value);

                if (Convert.ToBoolean(clR.GetFields("teamlead")))
                    btnClaimTeamOpen.Enabled = true;
                else
                    btnClaimTeamOpen.Enabled = false;
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void rgServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfServiceCenterID.Value = rgServiceCenter.SelectedValue.ToString();
            pnlSC.Visible = true;
            pnlGrid.Visible = false;
            ClearData();
            FillData();
        }

        private void ClearData()
        {
            txtServiceCenterNo.Text = "";
            txtServiceCenterName.Text = "";
            txtDealerNo.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            txtCity.Text = "";
            txtZip.Text = "";
            cboState.SelectedValue = "";
            txtPhone.Text = "";
            txtEMail.Text = "";
            txtFax.Text = "";
            txtContact.Text = "";
        }

        private void FillData()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = " + hfServiceCenterID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) 
            {
                clR.GetRow();
                txtServiceCenterNo.Text = clR.GetFields("servicecenterno");
                txtServiceCenterName.Text = clR.GetFields("servicecentername");
                txtDealerNo.Text = clR.GetFields("dealerno");
                txtAddr1.Text = clR.GetFields("addr1");
                txtAddr2.Text = clR.GetFields("addr2");
                txtCity.Text = clR.GetFields("city");
                cboState.SelectedValue = clR.GetFields("state");
                txtZip.Text = clR.GetFields("zip");
                txtPhone.Text = clR.GetFields("phone");
                txtContact.Text = clR.GetFields("contact");
                txtEMail.Text = clR.GetFields("email");
                txtFax.Text = clR.GetFields("fax");
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            hfServiceCenterID.Value = "0";
            pnlSC.Visible = true;
            pnlGrid.Visible = false;
            ClearData();
            GetNextServiceCenterNo();
        }

        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter ";
            SQL = SQL + "where servicecenterno like 'RF0%' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            if (clR.GetFields("mscn").Length > 0)
                txtServiceCenterNo.Text = "RF" + (Convert.ToInt64(clsFunc.Right(clR.GetFields("mscn"), 6)) + 1).ToString("0000000");
            else
                txtServiceCenterNo.Text = "RF0000000";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlSC.Visible = false;
            pnlGrid.Visible = true;
            rgServiceCenter.Rebind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clCP = new clsDBO();

            lblSCError.Visible = false;

            if (txtAddr1.Text.Length == 0)
            {
                lblSCError.Text = "No Address 1 given.";
                lblSCError.Visible = true;
                return;
            }

            if (txtServiceCenterName.Text.Length == 0)
            {
                lblSCError.Text = "No Service Center Name given.";
                lblSCError.Visible = true;
                return;
            }

            if (txtServiceCenterNo.Text.Length == 0)
            {
                lblSCError.Text = "No Service Center Number given.";
                lblSCError.Visible = true;
                return;
            }

            if (txtCity.Text.Length == 0)
            {
                lblSCError.Text = "No City given.";
                lblSCError.Visible = true;
                return;
            }

            if (cboState.Text.Length == 0)
            {
                lblSCError.Text = "No State given.";
                lblSCError.Visible = true;
                return;
            }

            if (txtZip.Text.Length == 0)
            {
                lblSCError.Text = "No Zip Code give.";
                lblSCError.Visible = true;
                return;
            }

            if (txtPhone.Text.Length == 0)
            {
                lblSCError.Text = "No Phone Number given.";
                lblSCError.Visible = true;
                return;
            }

            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = " + hfServiceCenterID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("servicecenterno", txtServiceCenterNo.Text);
            clR.SetFields("servicecentername", txtServiceCenterName.Text);
            clR.SetFields("dealerno", txtDealerNo.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("contact", txtContact.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.SetFields("fax", txtFax.Text);
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
            pnlSC.Visible = false;
            pnlGrid.Visible = true;
            rgServiceCenter.Rebind();
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach");
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnANAdd_Click(object sender, EventArgs e)
        {
            hfServiceCenterID.Value = "0";
            pnlSC.Visible = true;
            pnlGrid.Visible = false;
            ClearData();
            GetNextANServiceCenterNo();
        }

        private void GetNextANServiceCenterNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter ";
            SQL = SQL + "where servicecenterno like 'AN0%' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            if (clR.GetFields("mscn").Length > 0)
                txtServiceCenterNo.Text = "AN" + (Convert.ToInt64(clsFunc.Right(clR.GetFields("mscn"), 6)) + 1).ToString("0000000");
            else
                txtServiceCenterNo.Text = "AN0000000";
        }

        protected void btnPrimeAdd_Click(object sender, EventArgs e)
        {
            hfServiceCenterID.Value = "0";
            pnlSC.Visible = true;
            pnlGrid.Visible = false;
            ClearData();
            GetNextPrimeServiceCenterNo();
        }

        private void GetNextPrimeServiceCenterNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter ";
            SQL = SQL + "where servicecenterno like 'PR0%' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            if (clR.GetFields("mscn").Length > 0)
                txtServiceCenterNo.Text = "PR" + (Convert.ToInt64(clsFunc.Right(clR.GetFields("mscn"), 6)) + 1).ToString("0000000");
            else
                txtServiceCenterNo.Text = "PR0000000";
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }
    }
}