﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimGap.aspx.cs" Inherits="VeritasHomeC.ClaimGap" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim GAP</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsOpen" OnClick="btnClaimsOpen_Click" runat="server" Text="My Open Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" OnClick="btnUnlockClaim_Click" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimAudit" OnClick="btnClaimAudit_Click" runat="server" Text="Claim Audit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" OnClick="btnLossCode_Click" runat="server" Text="Loss Code"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlClaim" runat="server">
                                            <asp:Label ID="lblClaimHeader" runat="server" Width="1500" Text="Claim Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboClaimStatus" DataSourceID="dsClaimStatus" DataTextField="ClaimGAPStatusDesc" AutoPostBack="true" DataValueField="ClaimGAPStatusID" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsClaimStatus" 
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select ClaimGAPStatusID, ClaimGAPStatusDesc from claimgapstatus order by ClaimGapStatusID" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        <asp:TextBox ID="txtClaimDate" ReadOnly="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Loss Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="rdpLossDate" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Loss Mile:
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        <asp:TextBox ID="txtLossMile" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claimant Phone:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtClaimantPhone" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claimant E-Mail:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtClaimantEmail" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Authorized Person:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtClaimAuthorPerson" Width="150" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        NADA at Purchase
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtNADAatPurchase" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        NADA at Loss
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtNADAatLoss" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Deduct For Salvage
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtDeductForSalvage" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Rideshare or Commercial
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:CheckBox ID="chkRideshareCommercial" runat="server" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        &nbsp
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" CssClass="button1" Text="Update" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlContract" runat="server">
                                            <asp:Label ID="lblContractInfo" runat="server" Width="1500" Text="Contract Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Contract No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtContractNo" ReadOnly="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtStatus" Font-Bold="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Sale Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtSaleDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Paid Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPaidDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        First Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Last Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Program:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtProgram" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Plan:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPlan" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Term:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtTerm" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Effective Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtEffDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Expired Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtExpDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true" ID="tcDec">
                                                        Dec Page:
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="tcDesc2" ColumnSpan="2">
                                                        <asp:HyperLink ID="hlDec" Target="_blank" runat="server"></asp:HyperLink>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true" ID="tcTC">
                                                        Terms and Conditions Page:
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="tcTC2" ColumnSpan="2">
                                                        <asp:HyperLink ID="hlTC" Target="_blank" runat="server"></asp:HyperLink>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server">
                                            <asp:Label ID="lblDealer" runat="server" Width="1500" Text="Dealer Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Seller No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Seller Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerName" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 1:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerAddr1" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 2:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerAddr2" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        City, State, Zip:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerAddr3" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Phone:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadMaskedTextBox ID="txtDealerPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Agent No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAgentNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Agent Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAgentName" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell ColumnSpan="6">
                                                        <asp:Label ID="lblAtt" runat="server" Text="Attorney has been entered in this claim." ForeColor="Red"></asp:Label>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlDetail" runat="server">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadTabStrip ID="tsClaim" OnTabClick="tsClaim_TabClick" runat="server" Width="1700">
                                                            <Tabs>
                                                                <telerik:RadTab Text="Customer Info" Value="Customer" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Vehicle Info" Value="Vehicle" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Document Status" Value="Status" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Documents" Value="Documents" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Loan Info" Value="LoanInfo" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="POA" Value="POA" Width="120"></telerik:RadTab> 
                                                                <telerik:RadTab Text="CoBuyer" Value="CoBuyer" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Attorney" Value="Attorney" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Notes" Value="Notes" Width="120"></telerik:RadTab> 
                                                                <telerik:RadTab Text="Finance" Value="Finance" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Calc" Value="Calc" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Payments" Value="Payment" Width="120"></telerik:RadTab>
                                                            </Tabs>
                                                        </telerik:RadTabStrip>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:radmultipage ID="rmClaim" runat="server" >
                                                            <telerik:RadPageView ID="pvCustomer" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvVehicle" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvStatus" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvDocuments" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvLoanInfo" Height="500" runat="server">
                                                            </telerik:RadPageView>
															<telerik:RadPageView ID="pvPOA" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvCoBuyer" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                             <telerik:RadPageView ID="pvAttorney" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvNotes" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvFinance" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvCalc" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvPayment" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                       </telerik:radmultipage>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfAssignedTo" runat="server" />
                <asp:HiddenField ID="hfServiceCenterID" runat="server" />
                <asp:HiddenField ID="hfInsCarrierID" runat="server" />
                <asp:HiddenField ID="hfVeroOnly" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />
                <asp:HiddenField ID="hfSubAgentID" runat="server" />
                <asp:HiddenField ID="hfProgram" runat="server" />
                <asp:HiddenField ID="hfPlanTypeID" runat="server" />
                <asp:HiddenField ID="hfDealerID" runat="server" />

            </ContentTemplate>
        </asp:UpdatePanel>

        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    `   <asp:HiddenField ID="hfError" runat="server" />
    </form>
</body>
</html>
