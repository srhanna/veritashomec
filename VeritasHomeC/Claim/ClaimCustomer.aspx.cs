﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContractID.Value = Request.QueryString["contractid"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            if (!IsPostBack)
            {
                GetServerInfo();
                FillCustomer();
                btnUpdateCustomer.Visible = true;
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    btnUpdateCustomer.Enabled = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillCustomer()
        {
            GetContract();
        }

        private void GetContract()
        {
            string SQL;
            clsDBO clC = new clsDBO();

            if (hfContractID.Value.Length == 0)
                return;

            SQL = "select fname, lname, addr1, addr2, city, state, zip, phone ";
            SQL = SQL + "from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                txtAddr1.Text = clC.GetFields("addr1");
                txtAddr2.Text = clC.GetFields("addr2");
                txtCity.Text = clC.GetFields("city");
                cboState.SelectedValue = clC.GetFields("state");
                txtZip.Text = clC.GetFields("zip");
                txtPhone.Text = clC.GetFields("phone");
            }
        }

        protected void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clC = new clsDBO();
            VeritasGlobalToolsV2.clsMoxyAddressChange clMAC = new VeritasGlobalToolsV2.clsMoxyAddressChange();
            SQL = "select * from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                clC.SetFields("fname", txtFName.Text);
                clC.SetFields("lname", txtLName.Text);
                clC.SetFields("addr1", txtAddr1.Text);
                clC.SetFields("addr2", txtAddr2.Text);
                clC.SetFields("city", txtCity.Text);
                clC.SetFields("state", cboState.SelectedValue);
                clC.SetFields("zip", txtZip.Text);
                clC.SetFields("phone", txtPhone.Text);
                clC.SaveDB();
                clMAC.UpdateMoxy(Convert.ToInt64(hfContractID.Value));
                }
        }
    }
}