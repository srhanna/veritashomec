﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class RFClaimSubmit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsClaim.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsAssignedTo.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsJobs.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            tbTodo.Width = pnlHeader.Width;
            GetServerInfo();
            CheckToDo();

            if (!IsPostBack)
            {
                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                }
                else
                {
                    pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                    Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                }

                if (hfUserID.Value == "1")
                    btnLossCode.Visible = true;
                else
                    btnLossCode.Visible = false;

                btnRFClaimSubmit.Enabled = false;

                CheckRFClaimSubmit();
                hfClaimID.Value = "0";
                pnlDetail.Visible = false;
                pnlAssignedToChange.Visible = false;
            }
        }

        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim ";
                SQL = SQL + "where not sendclaim is null ";
                SQL = SQL + "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                    btnRFClaimSubmit.Enabled = true;
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
                if (!Convert.ToBoolean(clSI.GetFields("claimpayment")))
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);

        }

        protected void rgClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimID.Value = rgClaim.SelectedValue.ToString();
            FillClaimDetail();
        }

        private void FillClaimDetail()
        {
            hfContractID.Value = "0";
            hfServiceCenterID.Value = "0";
            txtContractNo.Focus();
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            OpenClaim();
            OpenContract();
            rgJobs.Rebind();
        }

        private void OpenClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from veritasclaims.dbo.claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
                hfServiceCenterID.Value = clR.GetFields("servicecenterid");
            }
        }

        private void OpenContract()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from contract c ";
            SQL = SQL + "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) 
            { 
                clR.GetRow();
                txtContractNo.Text = clR.GetFields("contractno");
                txtStatus.Text = clR.GetFields("status");
                txtSaleDate.Text = clR.GetFields("saledate");
                txtPaidDate.Text = clR.GetFields("datepaid");
                txtFName.Text = clR.GetFields("fname");
                txtLName.Text = clR.GetFields("lname");
                txtInspCarrier.Text = clR.GetFields("inscarriername");
                hfPlanTypeID.Value = clR.GetFields("plantypeid");
                hfProgram.Value = clR.GetFields("programid");
                GetProgram();
                GetPlanType();
                txtTerm.Text = clR.GetFields("termmonth") + "/" + Convert.ToInt64(clR.GetFields("termmile")).ToString("#,##0");
                if (clR.GetFields("effdate").Length > 0)
                    txtEffDate.Text = Convert.ToDateTime(clR.GetFields("effdate")).ToString("d");


                if (clR.GetFields("expdate").Length > 0)
                    txtExpDate.Text = Convert.ToDateTime(clR.GetFields("expdate")).ToString("d");

                txtEffMile.Text = Convert.ToInt64(clR.GetFields("effmile")).ToString("#,##0");

                if (clR.GetFields("expmile").Length > 0)
                    txtExpMile.Text = Convert.ToInt64(clR.GetFields("expmile")).ToString("#,##0");

                if (clR.GetFields("decpage").Length == 0)
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                }
                else 
                {
                    tcDec.Visible = true;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clR.GetFields("decpage");
                    hlDec.Text = clR.GetFields("decpage");
                }
                if (clR.GetFields("tcpage").Length == 0)
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                }
                else 
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clR.GetFields("tcpage");
                    hlTC.Text = clR.GetFields("tcpage");
                }
            }
        }

        private void GetPlanType()
        {
            string SQL;
            clsDBO clPT = new clsDBO();
            SQL = "select * from plantype ";
            SQL = SQL + "where plantypeid = " + hfPlanTypeID.Value;
            clPT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clPT.RowCount() > 0)
            {
                clPT.GetRow();
                txtPlan.Text = clPT.GetFields("plantype");
            }
        }

        private void GetProgram()
        {
            string SQL;
            clsDBO clP = new clsDBO();
            SQL = "select * from program ";
            SQL = SQL + "where programid = " + hfProgram.Value;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                txtProgram.Text = clP.GetFields("programname");
            }
        }

        protected void btnSendClaim_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsImportClaim clIC = new VeritasGlobalToolsV2.clsImportClaim();
            if (hfAssignedTo.Value == "")
                hfAssignedTo.Value = hfUserID.Value;

            if (hfAssignedTo.Value == "0")
                hfAssignedTo.Value = hfUserID.Value;

            clIC.VeritasClaimID = Convert.ToInt64(hfClaimID.Value);
            clIC.AssignedToID = Convert.ToInt64(hfAssignedTo.Value);
            clIC.UserID = Convert.ToInt64(hfUserID.Value);

            clIC.ProcessClaim();

            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgClaim.Rebind();

        }

        protected void btnChangeAssignedTo_Click(object sender, EventArgs e)
        {
            pnlAssignedToChange.Visible = true;
            pnlDetail.Visible = false;
        }

        protected void rgAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfAssignedTo.Value = "0";
            hfAssignedTo.Visible = Convert.ToBoolean(rgAssignedTo.SelectedValue);
            GetAssignedTo();
        }

        private void GetAssignedTo()
        {
            txtAssignedTo.Text = "";
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfAssignedTo.Value == "")
                hfAssignedTo.Value = hfUserID.Value;

            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfAssignedTo.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAssignedTo.Text = clR.GetFields("fname" + " " + clR.GetFields("lname"));
            }

            pnlDetail.Visible = true;
            pnlAssignedToChange.Visible = false;
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }
    }
}