﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.VisualBasic;

namespace VeritasHomeC
{
    public partial class ClaimTeamOpen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetServerInfo();
                CheckToDo();
                FillScreen();
                btnRFClaimSubmit.Enabled = false;
                CheckRFClaimSubmit();
                CheckSecurity();
            }

            if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }
        }

        private void CheckSecurity()
        {
            string SQL;
            clsDBO clUSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (Convert.ToBoolean(clUSI.GetFields("AllowClaimAudit")))
                    btnClaimAudit.Enabled = true;
                else
                    btnClaimAudit.Enabled = false;
            }
        }

        private void CheckRFClaimSubmit()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and teamlead <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                SQL = "select * from veritasclaims.dbo.claim ";
                SQL = SQL + "where not sendclaim is null ";
                SQL = SQL + "and processclaimdate is null ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                    btnRFClaimSubmit.Enabled = true;
            }
        }

        private void FillScreen()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clP = new clsDBO();
            string lTeamID = "0";
            SQL = "select * from userinfo ui inner join usersecurityinfo usi on ui.UserID = usi.Userid where ui.userid = " + hfUserID.Value;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                lTeamID = clP.GetFields("teamid");
            }

            SQL = @"select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, claimno, cl.CreDate as Reportdate, 
                cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			    case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
                case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			    sc.ServiceCenterName, cl.OpenDate, 
			    case when DC.cnt is null then 0 else dc.cnt end as opendays,
			    max(ci.RequestDate) as InspectionRequest,
			    max(um.MessageDate) as InspectionComplete,
                max(case when ci.RequestDate is null then 0 
			    else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			    end) as InspectionDays
			    from claim cl
                inner join contract c on c.contractid = cl.ContractID
                inner join dealer d on c.DealerID = d.DealerID
                left join agents a on d.agentsid = a.agentid
                left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			    left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			    left join vwNoActivity vna on cl.claimid = vna.claimid
			    left join vwclaimopen vco on cl.claimid = vco.claimid 
                left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
                left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
                left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			    left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			    left join UserMessage um on ci.InspectionID = um.InspectionID
                left join userinfo ui on ui.userid = cl.assignedto
			    left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
                group by claimid) DC on cl.claimid = dc.claimid
                where ((cl.CloseDate is null
                and not cl.ClaimActivityID in (11,14,15,17,18,21,22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,40,42,43,45,46,47,48)
                and cl.Status = 'Open')
                or cl.claimid in (select claimid from vwClaimDetailRequested)) ";

            SQL = SQL + "and AssignedTo in (select ui.userid from userinfo ui inner join usersecurityinfo usi on ui.UserID = usi.Userid where TeamID = ";
            SQL = SQL + lTeamID + ") ";
            SQL = SQL + @"group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, claimno, cl.CreDate, 
                cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			    case when DC.cnt is null then 0 else dc.cnt end,
                case when vna.cnt is null then 0 else vna.cnt end,
			    sc.ServiceCenterName, cl.OpenDate, 
			    case when vco.cnt is null then 0 else vco.cnt end,
			    case when vcd.cnt is null then 0 else vcd.cnt end ";
            SQL = SQL + "order by ClaimAge desc ";

            rgClaimOpen.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgClaimOpen.Rebind();
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void CheckToDo()
        {
            hlToDo.Visible = false;
            hlToDo.NavigateUrl = @"~\users\todoreader.aspx?sid=" + hfID.Value;
            string SQL;
            clsDBO clTD = new clsDBO();
            SQL = "select * from usermessage ";
            SQL = SQL + "where toid = " + hfUserID.Value + " ";
            SQL = SQL + "and completedmessage = 0 ";
            SQL = SQL + "and deletemessage = 0 ";
            clTD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clTD.RowCount() > 0)
                hlToDo.Visible = true;
            else
                hlToDo.Visible = false;
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
                if (!Convert.ToBoolean(clSI.GetFields("claimpayment")))
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowAlert()
        {
            hfAlert.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwAlert.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void rgClaimOpen_DataBound(object sender, EventArgs e)
        {
            int claimAge = 15;
            for (int i = 0; i <= rgClaimOpen.Items.Count - 1; i++)
            {
                if (Information.IsNumeric(rgClaimOpen.Items[i].Cells[claimAge].Text == "3"))
                    rgClaimOpen.Items[i].Cells[claimAge].BackColor = System.Drawing.Color.Yellow;

                if (Information.IsNumeric(rgClaimOpen.Items[i].Cells[claimAge].Text == "4"))
                    rgClaimOpen.Items[i].Cells[claimAge].BackColor = System.Drawing.Color.Yellow;

                if (Information.IsNumeric(rgClaimOpen.Items[i].Cells[claimAge].Text == "5"))
                    rgClaimOpen.Items[i].Cells[claimAge].BackColor = System.Drawing.Color.Orange;

                if (Information.IsNumeric(rgClaimOpen.Items[i].Cells[claimAge].Text == "6"))
                    rgClaimOpen.Items[i].Cells[claimAge].BackColor = System.Drawing.Color.Orange;

                if (Information.IsNumeric(rgClaimOpen.Items[i].Cells[claimAge].Text == "6"))
                    rgClaimOpen.Items[i].Cells[claimAge].BackColor = System.Drawing.Color.Yellow;
            }
        }

        protected void rgClaimOpen_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + rgClaimOpen.SelectedValue);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }
    }
}