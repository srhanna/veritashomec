﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGAPPOA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                FillPOA();
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillPOA()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgappoa ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtFName.Text = clR.GetFields("poaname");
                txtAddr1.Text = clR.GetFields("poaaddr1");
                txtAddr2.Text = clR.GetFields("poaaddr2");
                txtCity.Text = clR.GetFields("poacity");
                cboState.SelectedValue = clR.GetFields("poastate");
                txtZip.Text = clR.GetFields("poazip");
                txtPhone.Text = clR.GetFields("poaphone");
                txtEMail.Text = clR.GetFields("poaemail");
            }
        }

        protected void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgappoa ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimgapid", hfClaimID.Value);
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("credate", DateTime.Today.ToString());
            }
            else
            {
                clR.GetRow();
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
            }


            clR.SetFields("poaname", txtFName.Text);
            clR.SetFields("poaaddr1", txtAddr1.Text);
            clR.SetFields("poaaddr2", txtAddr2.Text);
            clR.SetFields("poacity", txtCity.Text);
            clR.SetFields("poastate", cboState.SelectedValue);
            clR.SetFields("poazip", txtZip.Text);
            clR.SetFields("poaphone", txtPhone.Text);
            clR.SetFields("poaemail", txtEMail.Text);

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }
    }
}