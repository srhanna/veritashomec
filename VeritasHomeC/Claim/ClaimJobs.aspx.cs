﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;
using System.Diagnostics;

namespace VeritasHomeC
{
    public partial class ClaimJobs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsCDT.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimDetailStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimReason.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsLossCode.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsWexMethod.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimPayee.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsJobs.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            if (mGlobal.bJobRefresh)
            {
                rgJobs.Rebind();
                mGlobal.bJobRefresh = false;
            }

            if (!IsPostBack)
            {
                GetServerInfo();
                CheckPaymentSecurity();
                if (CheckLock())
                    LockButtons();
                else
                    UnlockButtons();

                pnlDetail.Visible = false;
                pnlList.Visible = true;
                pnlAddClaimPayee.Visible = false;
                pnlLossCode.Visible = false;
                pnlSeekPayee.Visible = false;
                pnlToBePaidDetail.Visible = false;
                trACH.Visible = false;
                trCC.Visible = false;
                trCheck.Visible = false;
                trWex.Visible = false;
                FillPayeeTotal();
                FillRateType();
                FillJobsTotal();
                FillTotal();
                ReadOnlyButtons();
                FillJobNo();
                FillJobNo2();
                FillRateType2();

                if (hfSlushNote.Value == "Visible")
                    ShowSlushNote();
                else
                    HideSlushNote();

                if (hfFieldCheck.Value == "Visible")
                    ShowFieldCheck();
                else
                    HideFieldCheck();
            }
        }

        private void FillRateType2()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select ratetypeid, RateTypeName from ratetype ";
            SQL = SQL + "where ratetypeid in (1) ";

            ListItem i1 = new ListItem();
            i1.Value = "0";
            cboSlushRateType.Items.Add(i1);
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <=clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    i1 = new ListItem();
                    i1.Value = clR.GetFields("ratetypeid");
                    i1.Text = clR.GetFields("ratetypename");
                    cboSlushRateType.Items.Add(i1);
                }
            }
        }

        private void FillJobNo2()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "";
            cboSlushJobNo.Items.Add(li);
            li.Value = "All";
            li.Text = "All";
            cboSlushJobNo.Items.Add(li);
            SQL = "select JobNo from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            SQL = SQL + "group by Jobno ";
            SQL = SQL + "order by JobNo ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("jobno");
                    li.Text = clR.GetFields("jobno");
                    cboSlushJobNo.Items.Add(li);
                }
            }
        }

        private void FillJobNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "";
            cboJobNo.Items.Add(li);
            SQL = "select JobNo from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            SQL = SQL + "group by Jobno ";
            SQL = SQL + "order by JobNo ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i < clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("jobno");
                    li.Text = clR.GetFields("jobno");
                    cboJobNo.Items.Add(li);
                }
            }
        }

        private void FillJobsTotal()
        {
            clsDBO clCD = new clsDBO();
            string SQL;
            ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "All";
            cboJobsTotal.Items.Add(li);
            SQL = "select jobno from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "group by jobno ";
            SQL = SQL + "order by jobno ";
            clCD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCD.RowCount() > 0)
            {
                for (int i=0; i < clCD.RowCount()-1; i++)
                {
                    clCD.GetRowNo(i);
                    li = new ListItem();
                    li.Text = clCD.GetFields("jobno");
                    li.Value = (i + 1).ToString();
                    cboJobsTotal.Items.Add(li);
                }
            }
            cboJobsTotal.SelectedValue = "0";
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    LockButtons();

                if (Convert.ToBoolean(clR.GetFields("AllowSlush")))
                    tcManageSlush.Visible = true;
                else
                    tcManageSlush.Visible = false;

                tcManagerAuth.Visible = true;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private bool CheckLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "Select claimid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                return true;

            return false;
        }

        private void CheckPaymentSecurity()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and claimpayment <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                btnMakePayment.Visible = true;
            else
                btnMakePayment.Visible = false;
        }

        private void LockButtons()
        {
            btnAddClaimPayee.Enabled = false;
            btnAddDetail.Enabled = false;
            btnSaveClaimDetail.Enabled = false;
            btnSCSave.Enabled = false;
            btnSeekLossCode.Enabled = false;
            btnSeekPayee.Enabled = false;
            btnApproveIt.Enabled = false;
            btnAuthIt.Enabled = false;
            btnDenyIt.Enabled = false;
        }

        private void UnlockButtons()
        {
            btnAddClaimPayee.Enabled = true;
            btnAddDetail.Enabled = true;
            btnCancelClaimDetail.Enabled = true;
            btnSaveClaimDetail.Enabled = true;
            btnSCCancel.Enabled = true;
            btnSCSave.Enabled = true;
            btnSeekLossCode.Enabled = true;
            btnSeekPayee.Enabled = true;
        }

        protected void btnAddDetail_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            cboClaimDetailType.ClearSelection();
            GetNextJobNo();
            cboClaimDetailStatus.ClearSelection();
            txtLossCode.Text = "";
            txtLossCodeDesc.Text = "";
            txtPayeeNo.Text = "";
            txtPayeeName.Text = "";
            txtReqAmt.Text = "0";
            txtAuthAmt.Text = "0";
            txtTaxAmt.Text = "0";
            txtPaidAmt.Text = "0";
            cboRateType.SelectedValue = "1";
            rdpAuthorizedDate.SelectedDate = null;
            rdpDateApprove.SelectedDate = null;
            rdpDatePaid.SelectedDate = null;
            hfClaimDetailID.Value = "0";
        }

        private void GetNextJobNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfJobNo.Value.Length == 0)
            {
                SQL = "select max(jobno) as mJob from claimdetail ";
                SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
                SQL = SQL + "and jobno like 'j%' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() == 0)
                    hfJobNo.Value = "J01";
                else
                {
                    clR.GetRow();
                    if (clR.GetFields("mjob").Length == 0)
                        hfJobNo.Value = "J01";
                    else
                        hfJobNo.Value = "J" + Convert.ToInt64(clR.GetFields("mjob").Substring(clR.GetFields("mjob").Length - 2, 2) + 1).ToString("00");
                }
            }

            SQL = "select max(jobno) as mJob from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno like 'j%' ";
            SQL = SQL + "and jobno <> '" + hfJobNo.Value + "' ";
            SQL = SQL + "and losscode = '" + txtLossCode.Text.Trim() + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("mjob").Length > 0)
                    hfJobNo.Value = clR.GetFields("mJob");
            }

            txtJobNo.Text = hfJobNo.Value;
        }

        private void FillRateType()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select ratetypeid, RateTypeName from ratetype ";
            SQL = SQL + "where ratetypeid in (1,2834, 1101) ";

            SQL = SQL + "or ratetypeid in (select RateTypeID from ratetype ";
            SQL = SQL + "where ratecategoryid = 5 ";
            SQL = SQL + "and ratetypeid in (select ratetypeid ";
            SQL = SQL + "from contractcommissions ";
            SQL = SQL + "where agentid in (select agentsid from contract ";
            SQL = SQL + "where contractid in (select contractid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + ")))) ";
            SQL = SQL + "or RateTypeID in (select 10 from contract ";
            SQL = SQL + "where contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) ";
            SQL = SQL + "and (contractno like 'r%' or contractno like 'vel%')) ";
            SQL = SQL + "or RateTypeID in (select 2868 from contract ";
            SQL = SQL + "where contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) ";
            SQL = SQL + "and (contractno like 'va%' or contractno like 'vg%' or contractno like 'vep%')) ";
            SQL = SQL + "or RateTypeID in (select 24 from contract c ";
            SQL = SQL + "inner join contractamt ca on c.contractid = ca.contractid ";
            SQL = SQL + "where c.contractid in (select contractid from claim where claimid =  " + hfClaimID.Value + " ) ";
            SQL = SQL + "and ca.RateTypeID = 24) ";
            SQL = SQL + "or RateTypeID in (select 25 from contract c ";
            SQL = SQL + "inner join contractamt ca on c.contractid = ca.contractid ";
            SQL = SQL + "where c.contractid in (select contractid from claim where claimid = " + hfClaimID.Value + " ) ";
            SQL = SQL + "and ca.RateTypeID = 25) ";
            SQL = SQL + "order by ratetypeid ";

            cboRateType.Items.Clear();
            long a = cboRateType.Items.Count();
            RadComboBoxItem i1 = new RadComboBoxItem();
            i1.Value = "0";
            i1.Text = "";
            cboRateType.Items.Add(i1);
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount()-1; i++)
                {
                    clR.GetRowNo(i);
                    i1 = new RadComboBoxItem();
                    i1.Value = clR.GetFields("ratetypeid");
                    i1.Text = clR.GetFields("ratetypename");
                    cboRateType.Items.Add(i1);
                }
            }

            
        }

        protected void rgJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("allowlush")))
                    cboRateType.Enabled = true;
                else
                    cboRateType.Enabled = false;

                if (Convert.ToBoolean(clR.GetFields("AutoNationOnly")))
                    return;

                if (Convert.ToBoolean(clR.GetFields("VeroOnly")))
                    return;
            }

            pnlList.Visible = false;
            pnlDetail.Visible = true;
            cboClaimDetailType.ClearSelection();
            txtJobNo.Text = "";
            cboClaimDetailStatus.ClearSelection();
            txtLossCode.Text = "";
            txtLossCodeDesc.Text = "";
            txtPayeeNo.Text = "";
            txtPayeeName.Text = "";
            txtReqAmt.Text = "0";
            txtAuthAmt.Text = "0";
            txtTaxAmt.Text = "0";
            txtPaidAmt.Text = "0";
            rdpAuthorizedDate.SelectedDate = null;
            rdpDateApprove.SelectedDate = null;
            rdpDatePaid.SelectedDate = null;
            cboRateType.ClearSelection();
            hfClaimDetailID.Value = "0";
            hfClaimDetailID.Value = rgJobs.SelectedValue.ToString();

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + rgJobs.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboClaimDetailType.SelectedValue = clR.GetFields("claimdetailtype");
                txtJobNo.Text = clR.GetFields("jobno");
                cboClaimDetailStatus.SelectedValue = clR.GetFields("claimdetailstatus");
                txtLossCode.Text = clR.GetFields("losscode");
                GetLossCodeDesc();
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                GetPayee();

                txtReqAmt.Text = clR.GetFields("reqamt");
                txtAuthAmt.Text = clR.GetFields("authamt");
                txtTaxAmt.Text = clR.GetFields("taxamt");
                txtPaidAmt.Text = clR.GetFields("paidamt");
                txtReqQty.Text = clR.GetFields("reqqty");
                txtReqCost.Text = clR.GetFields("reqcost");
                txtAuthQty.Text = clR.GetFields("authqty");
                txtAuthCost.Text = clR.GetFields("authcost");
                txtPartNo.Text = clR.GetFields("partno");
                txtClaimDesc.Text = clR.GetFields("ClaimDesc");
                txtTotalAmt.Text = clR.GetFields("totalamt");
                cboReason.SelectedValue = clR.GetFields("claimreasonid");
                txtTaxRate.Text = (Convert.ToInt64(clR.GetFields("taxper")) * 100).ToString();

                if (clR.GetFields("dateauth").Length > 0)
                    rdpAuthorizedDate.SelectedDate = Convert.ToDateTime(clR.GetFields("dateauth"));
                else
                    rdpAuthorizedDate.Clear();

                if (clR.GetFields("dateapprove").Length > 0)
                    rdpDateApprove.SelectedDate = Convert.ToDateTime(clR.GetFields("dateapprove"));
                else
                    rdpDateApprove.Clear();

                if (clR.GetFields("datepaid").Length > 0)
                    rdpDatePaid.SelectedDate = Convert.ToDateTime(clR.GetFields("datepaid"));
                else
                    rdpDatePaid.Clear();

                cboRateType.SelectedValue = clR.GetFields("ratetypeid");

                if (txtReqQty.Text.Length == 0)
                    txtReqQty.Text = "0";

                if (txtReqCost.Text.Length == 0)
                    txtReqCost.Text = "0";

                if (txtAuthCost.Text.Length == 0)
                    txtAuthCost.Text = "0";

                if (txtAuthQty.Text.Length == 0)
                    txtAuthQty.Text = "0";

                if (txtTaxRate.Text.Length == 0)
                    txtTaxRate.Text = "0";

                if (txtReqAmt.Text.Length == 0)
                    txtReqAmt.Text = "0";

                if (txtTotalAmt.Text.Length == 0)
                    txtTotalAmt.Text = "0";

                LockAuthText();
                CheckSecurity();

            }
        }

        private void CheckSecurity()
        {
            string SQL;
            clsDBO clUSI = new clsDBO();
            cboClaimDetailStatus.Enabled = true;
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (cboClaimDetailStatus.SelectedValue == "Denied")
                {
                    if (Convert.ToBoolean(clUSI.GetFields("AllowUndenyClaim")))
                        cboClaimDetailStatus.Enabled = true;
                    else
                        cboClaimDetailStatus.Enabled = false;
                }
            }
        }

        private void LockAuthText()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("AllCoverage")))
                    return;
            }

            if (GetSaleDate() < Convert.ToDateTime("1/1/2019"))
                return;

            SQL = "select * from claimlosscode clc ";
            SQL = SQL + "inner join contractcoverage cc ";
            SQL = SQL + "on cc.CoverageCode = clc.LossCodePrefix ";
            SQL = SQL + "inner join contract c ";
            SQL = SQL + "on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID ";
            SQL = SQL + "inner join claim cl ";
            SQL = SQL + "on cl.ContractID = c.ContractID ";
            SQL = SQL + "where cl.claimid = " + hfClaimID.Value + " ";

            if (txtLossCode.Text.Length == 0)
                return;

            SQL = SQL + "and cc.coveragecode = '" + txtLossCode.Text.Substring(0, 4) + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set losscodelock = 1 ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                txtAuthCost.Enabled = false;
                txtAuthQty.Enabled = false;
                txtTaxRate.Enabled = false;
                cboClaimDetailStatus.Enabled = false;
                txtAuthAmt.Enabled = false;
                txtPaidAmt.Enabled = false;
                txtLossCode.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set losscodelock = 0 ";
                SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                txtAuthCost.Enabled = true;
                txtAuthQty.Enabled = true;
                txtTaxRate.Enabled = true;
                cboClaimDetailStatus.Enabled = true;
                txtAuthAmt.Enabled = true;
                txtPaidAmt.Enabled = true;
                txtLossCode.BackColor = System.Drawing.Color.White;
            }
        }

        private DateTime GetSaleDate()
        {
            clsDBO clR = new clsDBO();
            string SQL, GetSaleDate;
            GetSaleDate = "1/1/1950";
            SQL = "select c.saledate from contract c ";
            SQL = SQL + "inner join claim cl on cl.contractid = c.contractid";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                GetSaleDate = clR.GetFields("saledate");
                return Convert.ToDateTime(GetSaleDate);
            }

            return Convert.ToDateTime(GetSaleDate);
        }

        private void GetPayee()
        {
            if (hfClaimPayeeID.Value == "")
                hfClaimPayeeID.Value = "0";

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtPayeeName.Text = clR.GetFields("payeename");
                txtPayeeNo.Text = clR.GetFields("payeeno");
            }
        }

        private void GetLossCodeDesc()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimlosscode";
            SQL = SQL + "where losscode = '" + txtLossCode.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeDesc.Text = clR.GetFields("losscodedesc");
            }
        }

        protected void btnSeekLossCode_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlLossCode.Visible = true;
            rgLossCode.Rebind();
            ColorLossCode();
        }

        private void ColorLossCode()
        {

            if (GetSaleDate() < Convert.ToDateTime("1/1/2019"))
                return;

            rgLossCode.Columns[3].Visible = true;

            for (int i=0; i<= rgLossCode.Items.Count - 1; i++)
            {
                if (rgLossCode.Items[i].Cells[3].Text == "1")
                    rgLossCode.Items[i].Cells[1].BackColor = System.Drawing.Color.Red;
            }

            rgLossCode.Columns[3].Visible = false;
        }

        protected void btnSeekPayee_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekPayee.Visible = true;
        }

        protected void btnCancelClaimDetail_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgJobs.Rebind();
        }

        protected void btnSaveClaimDetail_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            SQL = "select * from claimdetail ";
            SQL = SQL + "where cliamdetailid = " + hfClaimDetailID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            if (cboClaimDetailStatus.SelectedValue == "Authorized" || cboClaimDetailStatus.SelectedValue == "Approved")
            {
                if (clR.GetFields("claimdetailstatus") != "Authorized")
                {
                    if (cboClaimDetailStatus.SelectedValue != clR.GetFields("claimdetailstatus"))
                    {
                        if (CheckMissingInfo())
                            return;
                    }
                }
            }

            if (cboClaimDetailStatus.Text == "Authorized" && !CheckInspect())
            {
                lblAuthorizedError.Text = "Inspection has not been complated.";
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                return;
            }
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdesc", txtClaimDesc.Text);
            clR.SetFields("claimdetailtype", cboClaimDetailStatus.SelectedValue);
            if (txtJobNo.Text.Length > 0)
                clR.SetFields("jobno", txtJobNo.Text);
            clR.SetFields("losscode", txtLossCode.Text);
            clR.SetFields("reqamt", txtReqAmt.Text);
            clR.SetFields("claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("authcost", txtAuthCost.Text);
            
            if (txtTaxAmt.Text.Length > 0)
                clR.SetFields("taxper", (Convert.ToDouble(txtTaxRate.Text) / 100).ToString());

            if (txtTaxAmt.Text.Length == 0)
                clR.SetFields("taxamt", "0");
            else
            {
                if (clR.GetFields("authqty").Length == 0)
                    clR.SetFields("authqty", "0");
                clR.SetFields("taxamt", txtTaxAmt.Text);
            }

            clR.SetFields("totalamt", txtTotalAmt.Text);
            clR.SetFields("Authamt", txtAuthAmt.Text);
            clR.SetFields("paidamt", txtPaidAmt.Text);
            clR.SetFields("claimdetailstatus", cboClaimDetailStatus.SelectedValue);
            clR.SetFields("claimreasonid", cboReason.SelectedValue);
            if (!String.IsNullOrEmpty(rdpAuthorizedDate.SelectedDate.ToString()))
                clR.SetFields("dateauth", rdpAuthorizedDate.SelectedDate.ToString());
            else
                clR.SetFields("dateauth", "");

            if (!String.IsNullOrEmpty(rdpDateApprove.SelectedDate.ToString()))
                clR.SetFields("dateapprove", rdpDateApprove.SelectedDate.ToString());
            else
                clR.SetFields("dateapprove", "");

            if (!String.IsNullOrEmpty(rdpDatePaid.SelectedDate.ToString()))
                clR.SetFields("datepaid", rdpDatePaid.SelectedDate.ToString());
            else
                clR.SetFields("datepaid", "");

            if (clR.GetFields("ratetypeid") == "1")
            {
                if (cboRateType.SelectedValue != "1")
                {
                    clR.SetFields("slushbyid", hfUserID.Value);
                    clR.SetFields("slushdate", DateTime.Today.ToString());
                }
            }

            clR.SetFields("ratetypeid", cboRateType.SelectedValue);
            clR.SetFields("reqqty", txtReqQty.Text);
            clR.SetFields("reqcost", txtReqCost.Text);
            clR.SetFields("authqty", txtAuthQty.Text);
            clR.SetFields("authcost", txtAuthCost.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);

            lblApprovedError.Text = "";

            if (cboClaimDetailStatus.SelectedValue == "Denied")
            {
                CheckClose();
                goto MoveToSave;
            }

            if (cboClaimDetailStatus.SelectedValue == "Cancelled")
            {
                CheckClose();
                goto MoveToSave;
            }

            if (cboClaimDetailStatus.SelectedValue == "Requested")
            {
                CheckClose();
                goto MoveToSave;
            }

            if (clR.GetFields("claimdetailstatus") != "Paid")
            {
                if (cboClaimDetailStatus.SelectedValue == "Approved")
                {
                    if (txtAuthAmt.Text.Length == 0)
                        txtAuthAmt.Text = "0";

                    if (clR.GetFields("claimdetailid").Length > 0)
                    {
                        if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), Convert.ToInt64(clR.GetFields("claimdetailid"))))
                        {
                            lblApprovedError.Text = "Send to Manager for Approval";
                            goto MoveHere;
                        }
                    }
                    else 
                    {
                        if (!CheckLimitApproved(Convert.ToDouble(txtAuthAmt.Text), 0))
                        {
                            lblApprovedError.Text = "Send to Manager for Approval";
                            goto MoveHere;
                        }
                    }
                }
                if (cboClaimDetailStatus.SelectedValue == "Authorized")
                {
                    if (txtAuthAmt.Text.Length == 0)
                        txtAuthAmt.Text = "0";

                    if (!CheckLimitAuthorized(Convert.ToDouble(txtAuthAmt.Text), Convert.ToInt64(clR.GetFields("claimdetailid"))))
                    {
                        lblApprovedError.Text = "Send to Manager for Approval";
                        clR.SetFields("claimdetailstatus", "Requested");
                    }
                }
            }

        MoveToSave:;

            if (hfTicket.Value == "True")
            {
                hfTicket.Value = "False";
                PlaceTicket();
            }

            if (clR.RowCount() == 0)
            {
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }

            clR.SaveDB();

        MoveHere:;

            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgJobs.Rebind();
            FillTotal();
            SQL = "update claim ";
            SQL = SQL + "set moddate = '" + DateTime.Today + "', ";
            SQL = SQL + "modby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private bool CheckMissingInfo()
        {
            if (CheckByPassMissingInfo())
                return false;

            string sMessage, SQL, sServiceCenter;
            clsDBO clR = new clsDBO();
            long l;
            sServiceCenter = "0";

            sMessage = "Check your funding for claim and you are missing the following fields. \r\n";
            l = sMessage.Length;
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("lossmile").Length == 0)
                    sMessage = sMessage + "Loss Mile \r\n";
                else
                {
                    if (clR.GetFields("lossmile") == "0")
                        sMessage = sMessage + "Loss Mile \r\n";
                }

                if (clR.GetFields("servicecenterid").Length == 0)
                    sMessage = sMessage + "Service Center \r\n";
                else
                {
                    if (clR.GetFields("servicecenterid") == "0")
                        sMessage = sMessage + "Service Center \r\n";
                }

                if (clR.GetFields("claimactivityid").Length == 0)
                    sMessage = sMessage + "Claim Activity \r\n";
                else
                {
                    if (clR.GetFields("claimactivityid") == "0")
                        sMessage = sMessage + "Claim Activity \r\n";
                }

                if (clR.GetFields("sccontactinfo").Length == 0)
                    sMessage = sMessage + "Service Center Contact Info \r\n";

                sServiceCenter = clR.GetFields("servicecenterid");
            }

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 1 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0 )
                sMessage = sMessage + "Complaint \r\n";

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 2 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                sMessage = sMessage + "Corrective Action \r\n";

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 3 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                sMessage = sMessage + "Cause \r\n";

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and not claimnotetypeid in (1,2,3,8) ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                sMessage = sMessage + "Notes \r\n";

            if (!CheckAN())
            {
                SQL = "select * from claimnote ";
                SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
                SQL = SQL + "and claimnotetypeid = 8 ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() == 0)
                    sMessage = sMessage + "Customer Statement \r\n";
            }

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailtype in ('Part', 'Labor') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                for (int i = 0; i <= clR.RowCount() - 1; i++) {
                    clR.GetRowNo(i);
                    if (clR.GetFields("ReqQty").Length == 0)  
                        sMessage = sMessage + clR.GetFields("jobno") + " Request Qty \r\n";
                    else 
                    {
                        if (clR.GetFields("ReqCost") == "0")
                            sMessage = sMessage + clR.GetFields("jobno") + " Request Cost \r\n";
                    }

                    if (clR.GetFields("AuthQty").Length == 0)
                        sMessage = sMessage + clR.GetFields("jobno") + " Authorized Qty \r\n";
                    else
                    {
                        if (clR.GetFields("AuthQty") == "0")
                            sMessage = sMessage + clR.GetFields("jobno") + " Authorized Qty \r\n";
                    }

                    if (clR.GetFields("AuthCost") == "0")
                        sMessage = sMessage + clR.GetFields("jobno") + " Authorized Cost \r\n";


                    if (clR.GetFields("losscode").Length == 0)
                        sMessage = sMessage + clR.GetFields("jobno") + " Loss Code \r\n";
                }
            }

            if (sMessage.Length > 72)
            {
                txtFieldChk.Text = sMessage;
                ShowFieldCheck();
                return true;
            }
            else
                txtFieldChk.Text = "Check your finding for claim!";

            return false;
        }

        private bool CheckByPassMissingInfo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and BypassMissingInfo <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private bool CheckAN()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from claim cl ";
            SQL = SQL + "inner join contract c on c.contractid = cl.contractid ";
            SQL = SQL + "inner join dealer d on d.dealerid = c.dealerid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and dealerno like '2%' ";
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private void CheckClose()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select claimdetailid from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                CloseClaim();

                if (cboClaimDetailStatus.SelectedValue == "Authorized")
                    UpdateAuthorizedDate();

                if (cboClaimDetailStatus.SelectedValue == "Approved")
                    UpdateApproveDate();
            }
        }

        private void UpdateApproveDate()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update claim ";
            SQL = SQL + "set approvedate = '" + DateTime.Today + "', ";
            SQL = SQL + "approveby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void UpdateAuthorizedDate()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update claim ";
            SQL = SQL + "set authdate = '" + DateTime.Today + "', ";
            SQL = SQL + "authby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void CloseClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update claim ";
            SQL = SQL + "set closedate = '" + DateTime.Today + "', ";
            SQL = SQL + "closeby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and closedate is null ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private bool CheckLimitApproved(double xAmt, long xClaimDetailID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dLimit;
            SQL = "select claimapprove from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimapprove"));
            }
            else
                dLimit = 0;

            SQL = "select sum(authamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'approved' ";
            SQL = SQL + "or claimdetailstatus = 'Authorized') ";
            SQL = SQL + "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt") + xAmt))
                        return true;
                }
                else
                {
                    if (dLimit > xAmt)
                        return true;
                }
            }

            return false;
        }

        private bool CheckLimitAuthorized(double xAmt, long xClaimDetailID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            double dLimit;
            SQL = "select claimauth from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dLimit = Convert.ToDouble(clR.GetFields("claimauth"));
            }
            else
                dLimit = 0;

            SQL = "select sum(authamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (claimdetailstatus = 'approved' ";
            SQL = SQL + "or claimdetailstatus = 'Authorized') ";
            SQL = SQL + "and claimdetailid <> " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    if (dLimit > Convert.ToDouble(clR.GetFields("amt") + xAmt))
                        return true;
                }
                else
                {
                    if (dLimit > xAmt)
                        return true;
                }
            }

            return false;
        }

        protected void rgLossCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtLossCode.Text = rgLossCode.SelectedValue.ToString();
            GetLossCodeDesc();
            GetNextJobNo();
            pnlLossCode.Visible = false;
            pnlDetail.Visible = true;
            LockAuthText();
        }

        protected void btnAddClaimPayee_Click(object sender, EventArgs e)
        {
            pnlSeekPayee.Visible = false;
            pnlAddClaimPayee.Visible = true;
            txtServiceCenterName.Text = "";
            txtZip.Text = "";
            txtAddr1.Text = "";
            txtAddr2.Text = "";
            cboState.ClearSelection();
            txtPhone.Text = "";
        }

        protected void rgClaimPayee_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimPayeeID.Value = rgClaimPayee.SelectedValue.ToString();
            GetPayee();
            pnlDetail.Visible = true;
            pnlSeekPayee.Visible = false;
        }
        protected void btnSCCancel_Click(object sender, EventArgs e)
        {
            pnlSeekPayee.Visible = true;
            pnlAddClaimPayee.Visible = true;
        }

        protected void btnSCSave_Click(object sender, EventArgs e)
        {
            AddServiceCenter();
            AddClaimPayee();
            FillClaimPayee();
            pnlSeekPayee.Visible = true;
            pnlAddClaimPayee.Visible = false;
        }

        private void FillClaimPayee()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select claimpayeeid, payeeno, payeename, city, state from claimpayee ";
            rgClaimPayee.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void AddClaimPayee()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentname", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.AddRow();
            clR.SaveDB();
        }

        private void AddServiceCenter()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            GetNextServiceCenterNo();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentname", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.AddRow();
            clR.SaveDB();
        }

        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            hfServiceCenterNo.Value = "RF" + (Convert.ToInt64(clsFunc.Right(clR.GetFields("mscn"), 6)) + 1).ToString("0000000");
        }

        private bool CheckInspect()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select inspect from claim ";
            SQL = SQL + "wehre claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("inspect") == "True")
                    return true;
            }

            return false;
        }

        private void FillTotal()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select 'Requested' as ClaimProcess, (select sum(reqamt) as Part from claimdetail where ClaimDetailType = 'Part' ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, ";
            SQL = SQL + "(select sum(reqamt) as Labor from claimdetail where ClaimDetailType = 'Labor' ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + " and claimid = " + hfClaimID.Value + ") as Labor, ";
            SQL = SQL + "(select sum(reqamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + " and claimid = " + hfClaimID.Value + ") as Other, ";
            SQL = SQL + "(select sum(reqamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + " and claimdetailtype <> 'Deduct') as SubTotal, ";
            SQL = SQL + "0 as taxAmt, ";
            SQL = SQL + "(select sum(reqamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.Text + "' ";

            SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, ";
            SQL = SQL + "(select sum(reqamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + ")  as Total, 1 as orderby ";
            SQL = SQL + "union ";
            SQL = SQL + "select 'Adjusted' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' ";
            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, ";
            SQL = SQL + "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Labor, ";
            SQL = SQL + "(select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') ";
            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied'";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Other, ";
            SQL = SQL + "(select sum(authamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied' ";
            SQL = SQL + "and claimdetailtype <> 'Deduct') as SubTotal, ";
            SQL = SQL + "(select sum(taxamt) as TaxAmt from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied') as TaxAmt, ";
            SQL = SQL + "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied'";
            SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, ";
            SQL = SQL + "(select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and ClaimDetailStatus <> 'cancelled' and ClaimDetailStatus <> 'Denied')  as Total, 2 as orderby  ";
            SQL = SQL + "union ";
            SQL = SQL + "select 'To Pay' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' ";
            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, ";
            SQL = SQL + "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and (ClaimDetailStatus = 'Approved' ";
            SQL = SQL + " or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Labor, ";
            SQL = SQL + "(select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' or ClaimDetailType = 'Other') ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') and claimid = " + hfClaimID.Value + ") as Other, ";
            SQL = SQL + "(select sum(authamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' ";
            SQL = SQL + "or ClaimDetailStatus = 'Paid') and claimdetailtype <> 'Deduct') as SubTotal, ";
            SQL = SQL + "(select sum(taxamt) as TaxAmt from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' ";
            SQL = SQL + "or ClaimDetailStatus = 'Paid')) as TaxAmt, ";
            SQL = SQL + "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid') ";
            SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, (select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Approved' or ClaimDetailStatus = 'Authorized' or ClaimDetailStatus = 'Paid'))  as Total, 3 as orderby ";
            SQL = SQL + "union ";
            SQL = SQL + "select 'Paid' as ClaimProcess, (select sum(authamt) as Part from claimdetail where ClaimDetailType = 'Part' ";
            SQL = SQL + "and ClaimDetailStatus = 'Paid' ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Part, ";
            SQL = SQL + "(select sum(authamt) as Labor from claimdetail where ClaimDetailType = 'Labor' and (ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Labor, (select sum(authamt) as Other from claimdetail where (ClaimDetailType = 'Inspect' ";
            SQL = SQL + "or ClaimDetailType = 'Other') and (ClaimDetailStatus = 'Paid') ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and claimid = " + hfClaimID.Value + ") as Other, ";
            SQL = SQL + "(select sum(authamt) as SubTotal from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Paid') ";
            SQL = SQL + "and claimdetailtype <> 'Deduct') as SubTotal, (select sum(taxamt) as TaxAmt from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Paid')) as TaxAmt, ";
            SQL = SQL + "(select sum(authamt) as 'Deduct' from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Paid') ";
            SQL = SQL + "and claimdetailtype = 'Deductible') AS Deduct, (select sum(authamt) + sum(taxamt) as Total from claimdetail where claimid = " + hfClaimID.Value + " ";
            if (Convert.ToInt64(cboPayeeTotal.SelectedValue) > 0)
                SQL = SQL + "and claimpayeeid = " + cboPayeeTotal.SelectedValue + " ";

            if (Convert.ToInt64(cboJobsTotal.SelectedValue) > 0)
                SQL = SQL + "and jobno = '" + cboJobsTotal.SelectedItem.Text + "' ";

            SQL = SQL + "and (ClaimDetailStatus = 'Paid'))  as Total, 4 as orderby ";
            SQL = SQL + "order by orderby ";
            rgClaimTotal.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgClaimTotal.DataBind();

        }

        private void FillPayeeTotal()
        {
            ListItem li = new ListItem();
            string SQL;
            clsDBO clR = new clsDBO();
            li.Value = "0";
            li.Text = "All";
            cboPayeeTotal.Items.Add(li);
            SQL = "select cp.claimpayeeid, payeeno, payeename from claimdetail cd ";
            SQL = SQL + "inner join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid ";
            SQL = SQL + "where cd.claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "group by cp.claimpayeeid, payeeno, payeename ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount()-1; i++)
                {
                    clR.GetRowNo(i);
                    li = new ListItem();
                    li.Value = clR.GetFields("claimpayeeid");
                    li.Text = clR.GetFields("payeeno") + "_" + clR.GetFields("payeename");
                    cboPayeeTotal.Items.Add(li);
                }
            }
        }

        protected void cboPayeeTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTotal();
        }

        private void FillPaymentMethod()
        {
            cboPaymentMethod.Items.Clear();
            cboPaymentMethod.Items.Add("");
            cboPaymentMethod.Items.Add("ACH");
            cboPaymentMethod.Items.Add("Check");
            cboPaymentMethod.Items.Add("Credit Card");
            cboPaymentMethod.Items.Add("Wex");
        }

        protected void btnMakePayment_Click(object sender, EventArgs e)
        {
            pnlToBePaidDetail.Visible = true;
            FillToBePaidDetail();
        }

        private void FillToBePaidDetail()
        {
            GetPayeeInfo();
            txtPaidAmt.Text = txtPayAmt.Text;
            FillPaymentMethod();
            txtConfirmNo.Text = "";
            txtCheckNo.Text = "";
        }

        private void GetPayeeInfo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimpayee ";
            SQL = SQL + "where claimpayeeid = " + hfClaimPayeeID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtClaimPayeeNo.Text = clR.GetFields("payeeno");
                txtClaimPayeeName.Text = clR.GetFields("payeename");
            }
        }

        protected void btnProcessACH_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trACH.Visible = false;
            rgJobs.Rebind();
        }

        private void ProcessPayment()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clCPL = new clsDBO();
            AddClaimPayment();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + hfClaimDetailID.Value + " ";
            SQL = SQL + "and claimpayeeid = " + hfClaimPayeeID.Value + " ";
            SQL = SQL + "and claimdetailstatus = 'approved' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount()-1; i++)
                {
                    clR.GetRowNo(i);
                    SQL = "select * from claimpaymentlink ";
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid") + " ";
                    SQL = SQL + "and claimpaymentid = " + hfClaimPaymentID.Value + " ";
                    clCPL.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCPL.RowCount() == 0) 
                    {
                        clCPL.NewRow();
                        clCPL.SetFields("claimdetailid", clR.GetFields("claimdetailid"));
                        clCPL.SetFields("claimpaymentid", hfClaimPaymentID.Value);
                        clCPL.AddRow();
                        clCPL.SaveDB();
                    }
                    SQL = "update claimdetail ";
                    if (cboPaymentMethod.Text != "Wex") 
                    {
                        SQL = SQL + "set claimdetailstatus = 'Paid', ";
                        SQL = SQL + "datepaid = '" + DateTime.Today + "', ";
                        SQL = SQL + "modby = " + hfUserID.Value + ", ";
                        SQL = SQL + "moddate = '" + DateTime.Today + "' ";
                    }
                    else 
                    {
                        SQL = SQL + "set claimdetailstatus = 'Transmitted To Wex', ";
                        SQL = SQL + "modby = " + hfUserID.Value + ", ";
                        SQL = SQL + "moddate = '" + DateTime.Today + "' ";
                    }
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCPL.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                }
            }
        }

        private void AddClaimPayment()
        {
            string SQL;
            clsDBO clCP = new clsDBO();
            SQL = "select * from clampayment ";
            SQL = SQL + "where claimpaymentid = 0 ";
            clCP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCP.RowCount() == 0)
            {
                clCP.NewRow();
                clCP.SetFields("claimpayeeid", hfClaimPayeeID.Value);
                clCP.SetFields("paymentamt", txtPayAmt.Text);
                if (cboPaymentMethod.Text == "ACH")
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("achinfo", txtConfirmNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Check") 
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("checkno", txtCheckNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Credit Card") 
                {
                    clCP.SetFields("datepaid", DateTime.Today.ToString());
                    clCP.SetFields("checkno", txtCCNo.Text);
                    clCP.SetFields("status", "Paid");
                }
                if (cboPaymentMethod.Text == "Wex") {
                    clCP.SetFields("datetransmitted", DateTime.Today.ToString());
                    clCP.SetFields("status", "Transmitted To Wex");
                    clCP.SetFields("ccno", hfWexCCno.Value);
                    clCP.SetFields("wexcode", hfWexCode.Value);
                    clCP.SetFields("wexdeliverymethod", cboWexMethod.SelectedText);
                    clCP.SetFields("wexdeliveryaddress", txtWexAddress.Text);
                    clCP.SetFields("companyno", hfCompanyNo.Value);
                }
                clCP.AddRow();
                clCP.SaveDB();
            }

            SQL = "select max(claimpaymentid) as mCPI from claimpayment ";
            SQL = SQL + "where claimpayeeid = " + hfClaimPayeeID.Value;
            clCP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCP.RowCount() > 0)
            {
                clCP.GetRow();
                hfClaimPaymentID.Value = clCP.GetFields("mcpi");
            }
        }

        protected void btnProcessCC_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trCheck.Visible = false;
            rgJobs.Rebind();
        }

        protected void cboPaymentMethod_TextChanged(object sender, EventArgs e)
        {
            trACH.Visible = false;
            trWex.Visible = false;
            trCheck.Visible = false;
            trCC.Visible = false;
            
            if (cboPaymentMethod.Text == "ACH")
                trACH.Visible = true;

            if (cboPaymentMethod.Text == "Check")
                trCheck.Visible = true;

            if (cboPaymentMethod.Text == "Credit Card")
                trCC.Visible = true;

            if (cboPaymentMethod.Text == "Wex")
                trWex.Visible = true;
        }

        protected void btnProcessWex_Click(object sender, EventArgs e)
        {
            ProcessWexDLL();
            pnlToBePaidDetail.Visible = false;
            rgJobs.Rebind();
            trWex.Visible = false;
        }

        protected void btnProcessCheck_Click(object sender, EventArgs e)
        {
            ProcessPayment();
            pnlToBePaidDetail.Visible = false;
            trCheck.Visible = false;
            rgJobs.Rebind();
        }

        private void ProcessWexDLL()
        {
            clsDBO clCWP = new clsDBO();
            clsDBO clR = new clsDBO();

            GetClaimNo();

            if (ConfigurationManager.AppSettings["connstring"] == "server=104.168.205.130;database=veritashometest;User Id=sa;Password=NCC1701E")
                return;
            else
            {
                GetCompanyNo();
                string SQL;
                clsDBO clWA = new clsDBO();
                SQL = "select * from wexapi ";
                SQL = SQL + "where companyno = '" + hfCompanyNo.Value + "' ";
                clWA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clWA.RowCount() > 0)
                {
                    clWA.GetRow();
                    GetClaimNo();
                    SQL = "select * from claimwexpayment ";
                    SQL = SQL + "where claimwexpaymentid = 0 ";
                    clCWP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCWP.RowCount() == 0)
                    {
                        clCWP.NewRow();
                        clCWP.SetFields("claimno", hfClaimNo.Value);
                        clCWP.SetFields("claimid", hfClaimID.Value);
                        clCWP.SetFields("rono",hfRONo.Value);
                        clCWP.SetFields("orggroup", clWA.GetFields("OrgGroup"));
                        clCWP.SetFields("username", clWA.GetFields("UserName"));
                        clCWP.SetFields("password", clWA.GetFields("Password"));
                        clCWP.SetFields("currency", clWA.GetFields("currency"));
                        clCWP.SetFields("wexaddress", txtWexAddress.Text);
                        clCWP.SetFields("wexmethod", cboWexMethod.SelectedValue);
                        clCWP.SetFields("bankno", clWA.GetFields("bankno"));
                        clCWP.SetFields("companyno", hfCompanyNo.Value);
                        clCWP.SetFields("paymentamt", txtPayAmt.Text);
                        clCWP.SetFields("contractno", hfContractNo.Value);
                        clCWP.SetFields("customername", hfCustomerName.Value);
                        clCWP.SetFields("Claimpayeename", txtClaimPayeeName.Text);
                        clCWP.SetFields("payeecontact", hfPayeeContact.Value);
                        clCWP.SetFields("vin", hfVIN.Value);
                        clCWP.AddRow();
                        clCWP.SaveDB();
                        SQL = "select max(claimwexpaymentid) as MCWP from claimwexpayment ";
                        SQL = SQL + "where claimid = " + hfClaimID.Value;
                        clCWP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                        if (clCWP.RowCount() > 0)
                        {
                            clCWP.GetRow();
                            if (!ConfigurationManager.AppSettings["connstring"].ToLower().Contains("test"))
                            {
                                Process pr = new Process();
                                pr.StartInfo.FileName = @"C:\ProcessProgram\WexPayment\WexPayment.exe";
                                pr.StartInfo.Arguments = clCWP.GetFields("mcwp");
                                pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                pr.Start();
                                pr.WaitForExit();
                                pr.Close();
                                SQL = "select * from claimwexpayment ";
                                SQL = SQL + "where claimwexpaymentid = " + clCWP.GetFields("mcwp");
                                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                                if (clR.RowCount() > 0)
                                {
                                    clR.GetRow();
                                    if (clR.GetFields("ReasonCode").ToLower() == "sucess")
                                    {
                                        hfWexCCno.Value = clR.GetFields("wexccno").Substring(0, 4);
                                        hfWexCode.Value = clR.GetFields("wexcode");
                                        ProcessPayment();
                                        pnlToBePaidDetail.Visible = false;
                                    }
                                }
                            }
                        }
                    } 
                }
            }
        }

        private void GetClaimNo()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from claim ";
            SQL = SQL + "where claim = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfClaimNo.Value = clR.GetFields("claimno");
                hfRONo.Value = clR.GetFields("RONumber");
                hfContractID.Value = clR.GetFields("contractid");
                OpenContract();
                hfPayeeContact.Value = clR.GetFields("sccontactinfo");
            }
        }

        private void GetCompanyNo()
        {
            clsDBO clCL = new clsDBO();
            string SQL;
            SQL = "select * from claim cl ";
            SQL = SQL + "inner join contract c on c.contractid = cl.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                if (clCL.GetFields("clipid") == "1" || clCL.GetFields("clipid") == "2")
                    hfCompanyNo.Value = "0000978";

                if (clCL.GetFields("clipid") == "3" || clCL.GetFields("clipid") == "4")
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "CHJ")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "DRV")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAC")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAD")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAN")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RDI")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "REP")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSA")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSD")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSW")
                        hfCompanyNo.Value = "0000978";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "VEL")
                        hfCompanyNo.Value = "0000978";
                    else
                        hfCompanyNo.Value = "0000976";
                }

                if (clCL.GetFields("clipid") == "5" || clCL.GetFields("clipid") == "6")
                    hfCompanyNo.Value = "0000976";

                if (clCL.GetFields("clipid") == "7" || clCL.GetFields("clipid") == "8")
                    hfCompanyNo.Value = "0000976";

                if (clCL.GetFields("clipid") == "9" || clCL.GetFields("clipid") == "10")
                    hfCompanyNo.Value = "0000977";

                if (clCL.GetFields("clipid") == "11")
                    hfCompanyNo.Value = "0000978";
            } 
        }

        private void OpenContract()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractNo.Value = clR.GetFields("contractno");
                hfCustomerName.Value = clR.GetFields("fname") + " " + clR.GetFields("lname");
                hfVIN.Value = clR.GetFields("vin");
            }
        }

        protected void txtReqQty_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost;

            if (txtReqQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtReqQty.Text);
            else
                dQty = 0;

            if (txtReqCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtReqCost.Text);
            else
                dCost = 0;

            txtReqAmt.Text = (dQty * dCost).ToString();
            txtReqCost.Focus();
        }

        protected void txtReqCost_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost;

            if (txtReqQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtReqQty.Text);
            else
                dQty = 0;

            if (txtReqCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtReqCost.Text);
            else
                dCost = 0;

            txtReqAmt.Text = (dQty * dCost).ToString();
            txtReqCost.Focus();
        }

        protected void txtAuthQty_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtAuthCost.Focus();
        }

        protected void txtAuthCost_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            dTax = (Math.Round(dTax * 100) / 100);
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtTaxRate.Focus();
        }

        protected void txtTaxRate_TextChanged(object sender, EventArgs e)
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            dTax = (Math.Round(dTax * 100) / 100);
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtTaxRate.Focus();
        }

        protected void cboClaimDetailStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (cboClaimDetailStatus.SelectedValue == "Approved")
            { 
                if (rdpDateApprove.SelectedDate == null) {
                    rdpDateApprove.SelectedDate = DateTime.Today;
                    CalcAuth();
                }
                
                if (rdpAuthorizedDate.SelectedDate == null)
                    rdpAuthorizedDate.SelectedDate = DateTime.Today;
                
            }

            if (cboClaimDetailStatus.SelectedValue == "Authorized") {
                if (!String.IsNullOrEmpty(rdpDateApprove.SelectedDate.ToString()))
                    rdpDateApprove.Clear();


                if (rdpAuthorizedDate.SelectedDate == null)
                    rdpAuthorizedDate.SelectedDate = DateTime.Today;
            }

            if (cboClaimDetailStatus.SelectedValue == "Cancelled") 
            {
                rdpDateApprove.Clear();
                rdpAuthorizedDate.Clear();
            }

            if (cboClaimDetailStatus.SelectedValue == "Denied")
            {
                rdpDateApprove.Clear();
                rdpAuthorizedDate.Clear();
                if (txtJobNo.Text.Substring(0, 1) == "J")
                    hfTicket.Value = "True";
            }

            if (cboClaimDetailStatus.SelectedValue == "Paid") 
            {
                rdpDatePaid.SelectedDate = DateTime.Today;
                txtPaidAmt.Text = txtTotalAmt.Text;
            }
            CheckClose();
        }

        private void PlaceTicket()
        {
            string SQL;
            clsDBO clT = new clsDBO();

            GetContractID();

            SQL = "select * from veritasclaimticket.dbo.ticket ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailid = " + hfClaimDetailID.Value + " ";
            clT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clT.RowCount() == 0)
            {
                if (!CheckContract())
                    return;

                clT.NewRow();
                clT.SetFields("ticketno", CalcTicketNo());
                clT.SetFields("contractid", hfContractID.Value);
                clT.SetFields("claimid", hfClaimID.Value);
                clT.SetFields("claimdetailid", hfClaimDetailID.Value);
                clT.SetFields("credate", DateTime.Today.ToString());
                clT.SetFields("creby", CalcUserName());
                clT.AddRow();
                clT.SaveDB();
            }
            else
            {
                clT.GetRow();
                clT.SetFields("statusid", "1");
                clT.SaveDB();
            }
        }

        private string CalcUserName()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("username");
            }

            return "";
        }

        private void GetContractID() {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
            }
        }

        private string CalcTicketNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            long lTicketNo;

            SQL = "select * from veritasclaimticket.dbo.ticket ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("ticketno");
            }

            SQL = "select * from veritasclaimticket.dbo.ticket ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                return "T000000";
            }

            SQL = "select max(ticketno) as TicketNo from veritasclaimticket.dbo.ticket ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lTicketNo = Convert.ToInt64(clR.GetFields("ticketno").Replace("T", ""));
                lTicketNo = lTicketNo + 1;
                return "T" + lTicketNo.ToString("000000");
            }

            return "";
        }

        private bool CheckContract()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from contract ";
            SQL = SQL + @"where contractid in (select contractid from contract
                where dealerid in (select dealerid from VeritasClaimTicket.dbo.Dealer)) ";
            SQL = SQL + "and contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private void CalcAuth()
        {
            double dQty, dCost, dTax, dTaxRate;

            if (txtAuthQty.Text.Length > 0)
                dQty = Convert.ToDouble(txtAuthQty.Text);
            else
                dQty = 0;

            if (txtAuthCost.Text.Length > 0)
                dCost = Convert.ToDouble(txtAuthCost.Text);
            else
                dCost = 0;

            if (txtTaxRate.Text.Length > 0)
                dTaxRate = Convert.ToDouble(txtTaxRate.Text) / 100;
            else
                dTaxRate = 0;

            dTax = (dQty * dCost) * dTaxRate;
            dTax = (Math.Round(dTax * 100) / 100);
            txtTaxAmt.Text = dTax.ToString();
            txtAuthAmt.Text = (dQty * dCost).ToString();
            txtTotalAmt.Text = ((dQty * dCost) + dTax).ToString();
            txtTaxRate.Focus();
        }

        protected void cboJobsTotal_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillTotal();
        }

        protected void cboClaimDetailType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (cboClaimDetailType.SelectedItem.Text == "Deductible")
                txtJobNo.Text = "A01";
            if (cboClaimDetailType.SelectedItem.Text == "Other")
                txtJobNo.Text = "A04";
            if (cboClaimDetailType.SelectedItem.Text == "Inspect")
                txtJobNo.Text = "A02";
        }

        protected void btnAuthIt_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            clsDBO clCD = new clsDBO();
            string SQL;
            double dQty, dCost, dTax, dTaxRate;
            bool bCopyReq;

            if (cboJobNo.SelectedValue == "0")
                return;

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = '" + cboJobNo.SelectedValue + "' ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i<clR.RowCount()-1;i++)
                {
                    clR.GetRowNo(i);
                    SQL = "select * from claimdetail ";
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCD.RowCount() > 0)
                    {
                        clCD.GetRow();
                        bCopyReq = false;
                        if (clCD.GetFields("authqty").Length > 0) {
                            if (clCD.GetFields("authqty") == "0") {
                                clCD.SetFields("authqty", clCD.GetFields("reqqty"));
                                bCopyReq = true;
                            }
                        }
                        else 
                        {
                            clCD.SetFields("authqty", clCD.GetFields("reqqty"));
                            bCopyReq = true;
                        }

                        if (clCD.GetFields("authcost").Length > 0) {
                            if (clCD.GetFields("authcost") == "0")
                            {
                                clCD.SetFields("authcost", clCD.GetFields("reqcost"));
                                bCopyReq = true;
                            }
                        }
                        else 
                        {
                            clCD.SetFields("authcost", clCD.GetFields("reqcost"));
                            bCopyReq = true;
                        }


                        if (clCD.GetFields("authqty").Length > 0)
                            dQty = Convert.ToDouble(clCD.GetFields("authqty"));
                        else
                            dQty = 0;

                        if (clCD.GetFields("authcost").Length > 0)
                            dCost = Convert.ToDouble(clCD.GetFields("authcost"));
                        else
                            dCost = 0;

                        if (clCD.GetFields("taxper").Length > 0)
                        {
                            dTaxRate = Convert.ToDouble(clCD.GetFields("taxper"));
                            if (clCD.GetFields("taxper") == "0")
                            {
                                dTaxRate = GetTaxRate(clCD.GetFields("claimdetailtype"));
                                clCD.SetFields("taxper", dTaxRate.ToString());
                            }
                        }

                        else 
                        {
                            dTaxRate = GetTaxRate(clCD.GetFields("claimdetailtype"));
                            clCD.SetFields("taxper", dTaxRate.ToString());
                        }

                        dTax = (dQty * dCost) * dTaxRate;
                        dTax = (Math.Round(dTax * 100) / 100);
                        clCD.SetFields("taxamt", dTax.ToString());
                        clCD.SetFields("authamt", (dQty * dCost).ToString());
                        clCD.SetFields("totalamt", ((dCost * dQty) + dTax).ToString());
                        if (!CheckLimitAuthorized(Convert.ToDouble(clCD.GetFields("totalamt")), Convert.ToInt64(clCD.GetFields("claimdetailid")))) {
                            lblAuthorizedError.Text = "Send to Manager for Authorization";
                            return;
                        }

                        clCD.SetFields("claimdetailstatus", "Authorized");
                        clCD.SetFields("authby", hfUserID.Value);
                        clCD.SetFields("moddate", DateTime.Today.ToString());
                        clCD.SetFields("modby", hfUserID.Value);
                        clCD.SetFields("dateauth", DateTime.Today.ToString());
                        clCD.SaveDB();
                    }
                }
            }

            SQL = "select claimdetailid from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0) 
            {
                CloseClaim();
                UpdateAuthorizedDate();
            }

            rgJobs.Rebind();
        }

        protected void btnApproveIt_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            clsDBO clCD = new clsDBO();
            string SQL;
            double dQty, dCost, dTax, dTaxRate;
            bool bCopyReq;

            if (cboJobNo.SelectedValue == "0")
                return;

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = '" + cboJobNo.SelectedValue + "' ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i < clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    SQL = "select * from claimdetail ";
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCD.RowCount() > 0)
                    {
                        clCD.GetRow();
                        bCopyReq = false;
                        if (clCD.GetFields("authqty").Length > 0)
                        {
                            if (clCD.GetFields("authqty") == "0")
                            {
                                clCD.SetFields("authqty", clCD.GetFields("reqqty"));
                                bCopyReq = true;
                            }
                        }
                        else
                        {
                            clCD.SetFields("authqty", clCD.GetFields("reqqty"));
                            bCopyReq = true;
                        }

                        if (clCD.GetFields("authcost").Length > 0)
                        {
                            if (clCD.GetFields("authcost") == "0")
                            {
                                clCD.SetFields("authcost", clCD.GetFields("reqcost"));
                                bCopyReq = true;
                            }
                        }
                        else
                        {
                            clCD.SetFields("authcost", clCD.GetFields("reqcost"));
                            bCopyReq = true;
                        }


                        if (clCD.GetFields("authqty").Length > 0)
                            dQty = Convert.ToDouble(clCD.GetFields("authqty"));
                        else
                            dQty = 0;

                        if (clCD.GetFields("authcost").Length > 0)
                            dCost = Convert.ToDouble(clCD.GetFields("authcost"));
                        else
                            dCost = 0;

                        if (clCD.GetFields("taxper").Length > 0)
                        {
                            dTaxRate = Convert.ToDouble(clCD.GetFields("taxper"));
                            if (clCD.GetFields("taxper") == "0")
                            {
                                dTaxRate = GetTaxRate(clCD.GetFields("claimdetailtype"));
                                clCD.SetFields("taxper", dTaxRate.ToString());
                            }
                        }

                        else
                        {
                            dTaxRate = GetTaxRate(clCD.GetFields("claimdetailtype"));
                            clCD.SetFields("taxrate", dTaxRate.ToString());
                        }

                        dTax = (dQty * dCost) * dTaxRate;
                        dTax = (Math.Round(dTax * 100) / 100);
                        clCD.SetFields("taxamt", dTax.ToString());
                        clCD.SetFields("authamt", (dQty * dCost).ToString());
                        clCD.SetFields("totalamt", ((dCost * dQty) + dTax).ToString());
                        if (!CheckLimitApproved(Convert.ToDouble(clCD.GetFields("totalamt")), Convert.ToInt64(clCD.GetFields("claimdetailid"))))
                        {
                            lblAuthorizedError.Text = "Send to Manager for Authorization";
                            return;
                        }

                        clCD.SetFields("claimdetailstatus", "Approved");
                        clCD.SetFields("authby", hfUserID.Value);
                        clCD.SetFields("dateauth", DateTime.Today.ToString());
                        clCD.SetFields("approveby", hfUserID.Value);
                        clCD.SetFields("dateapprove", DateTime.Today.ToString());
                        clCD.SetFields("moddate", DateTime.Today.ToString());
                        clCD.SetFields("modby", hfUserID.Value); 
                        clCD.SaveDB();
                    }
                }
            }

            SQL = "select claimdetailid from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                CloseClaim();
                UpdateApproveDate();
            }

            rgJobs.Rebind();
        }

        private double GetTaxRate(string xClaimDetailType)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            if (xClaimDetailType == "Deductible")
                return 0;

            if (xClaimDetailType == "Inspect")
                return 0;

            if (xClaimDetailType == "Other")
                return 0;

            if (xClaimDetailType == "")
                return 0;

            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (xClaimDetailType == "Part")
                {
                    if (clR.GetFields("parttax").Length > 0)
                        return Convert.ToDouble(clR.GetFields("parttax"));
                }

                if (xClaimDetailType == "Labor")
                {
                    if (clR.GetFields("labortax").Length > 0)
                        return Convert.ToDouble(clR.GetFields("parttax"));
                }
            }

            return 0;
        }

        protected void btnDenyIt_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            clsDBO clCD = new clsDBO();
            string SQL;
            if (cboJobNo.SelectedValue == "0")
                return;

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = '" + cboJobNo.SelectedValue + "' ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                for (int i = 0; i <= clR.RowCount() - 1; i++) {
                    clR.GetRowNo(i);
                    SQL = "select * from claimdetail ";
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCD.RowCount() > 0) {
                        clCD.GetRow();
                        clCD.SetFields("claimdetailstatus", "Denied");
                        clCD.SetFields("modby", hfUserID.Value);
                        clCD.SetFields("moddate", DateTime.Today.ToString());
                        clCD.SaveDB();
                    }
               }
            }
            SQL = "select claimdetailid from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                CloseClaim();

            rgJobs.Rebind();
        }


        private void ShowSlushNote()
        {
            hfSlushNote.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwSlushNote.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void HideSlushNote()
        {
            hfSlushNote.Value = "";
            string script = "function f(){$find(\"\"" + rwSlushNote.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void ShowFieldCheck()
        {
            hfFieldCheck.Value = "Visible";
            string script = "function f(){$find(\"\"" + rwFieldCheck.ClientID + "\"\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency2", script, true);
        }

        private void HideFieldCheck()
        {
            hfFieldCheck.Value = "";
            string script = "function f(){$find(\"\"" + rwFieldCheck.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUpdateSlush_Click(object sender, EventArgs e)
        {
            if (cboSlushJobNo.SelectedValue == "0")
                return;

            if (cboSlushRateType.SelectedValue == "0")
                return;

            hfSlushNote.Value = "Visible";
            ShowSlushNote();
        }

        private void UpdateSlushClaimDetail()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            if (cboSlushJobNo.SelectedValue == "0")
                return;

            if (cboSlushJobNo.SelectedValue == "All")
            {
                SQL = "update claimdetail ";
                SQL = SQL + "set ratetypeid = " + cboSlushRateType.SelectedValue + ", ";
                SQL = SQL + "moddate = '" + DateTime.Today + "', ";
                SQL = SQL + "modby = " + hfUserID.Value + ", ";

                if (Convert.ToInt64(cboSlushRateType.SelectedValue) > 1)
                {
                    SQL = SQL + "slushbyid = " + hfUserID.Value + ", ";
                    SQL = SQL + "slushdate = '" + DateTime.Today + "' ";
                }
                else
                {
                    SQL = SQL + "slushbyid = null, ";
                    SQL = SQL + "slushdate = null ";
                }
                SQL = SQL + "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }

            SQL = "update claimdetail ";
            SQL = SQL + "set ratetypeid = " + cboSlushRateType.SelectedValue + ", ";
            SQL = SQL + "moddate = '" + DateTime.Today + "', ";
            SQL = SQL + "modby = " + hfUserID.Value + ", ";
            if (Convert.ToInt64(cboSlushRateType.SelectedValue) > 1) {
                SQL = SQL + "slushbyid = " + hfUserID.Value + ", ";
                SQL = SQL + "slushdate = '" + DateTime.Today + "' ";
            }
            else 
            {
                SQL = SQL + "slushbyid = null, ";
                SQL = SQL + "slushdate = null ";
            }

            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = '" + cboJobNo.SelectedValue + "' ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnCloseSlushNote_Click(object sender, EventArgs e)
        {
            hfSlushNote.Value = "";
            HideSlushNote();
        }

        protected void btnSaveSlushNote_Click(object sender, EventArgs e)
        {
            lblAuthorizedError.Text = "";
            if (txtSlushNote.Text.Trim().Length < 25)
            {
                lblAuthorizedError.Text = "You need to put in a proper Note. Nothing was saved.";
                HideSlushNote();
                return;
            }

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnoteid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", "5");
                clR.SetFields("note", txtSlushNote.Text);
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("NoteText", txtSlushNote.Text);
                clR.AddRow();
                clR.SaveDB();
            }

            UpdateSlushClaimDetail();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            HideFieldCheck();
        }
    }
}