﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimSearch.aspx.cs" Inherits="VeritasHomeC.ClaimSearch" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
        <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim Search</title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsOpen" OnClick="btnClaimsOpen_Click" runat="server" Text="My Open Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" OnClick="btnUnlockClaim_Click" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" onclick="btnLossCode_Click" runat="server" Text="Loss Code"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label runat="server" ID="lblMessage" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="btnAddClaim" OnClick="btnAddClaim_Click" runat="server" Text="Add Claim" CssClass="button1" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" DefaultButton="btnSearch">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtClaimNo" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Service Center No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtServiceCenterNo" runat="server"></telerik:RadTextBox>
                                                        <asp:Button ID="btnSCSeek" OnClick="btnSCSeek_Click" runat="server" Text="Seek" CssClass="button1" />
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Contract No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtContractNo" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        RO Number:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtRONumber" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        First Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtFName" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Last Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtLName" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        City:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtCity" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        State:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsStates"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                   <%-- <asp:TableCell Font-Bold="true">
                                                        VIN:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtVIN" AutoPostBack="true" runat="server"></telerik:RadTextBox>
                                                    </asp:TableCell> --%>
                                                    <asp:TableCell>
                                                        Assigned To:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadTextBox ID="txtAssignedTo" runat="server"></telerik:RadTextBox>
                                                        <asp:Button ID="btnSeekUser" OnClick="btnSeekUser_Click" runat="server" Text="Seek User" CssClass="button1" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="cboStatus" runat="server" DataTextField="claimstatus" DataValueField="claimstatus" DataSourceID="dsStatus"></asp:DropDownList>
                                                        <asp:SqlDataSource ID="dsStatus" SelectCommand="select claimstatus, OrderNo from claimstatus 
                                                        union select ClaimGAPStatusDesc as ClaimStatus, orderno
                                                        from claimgapstatus order by OrderNo" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:CheckBox ID="chkAutoNation" Text="Auto Nation Service Center" runat="server" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Search Claim" CssClass="button1" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trClaimResult">
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" Width="300px" PostBackControls="rgClaim">
                                            <telerik:RadGrid ID="rgClaim" runat="server" OnSelectedIndexChanged="rgClaim_SelectedIndexChanged" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                                AllowSorting="true"  Width="1500" ShowFooter="true" AllowPaging="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ClaimID, ContractID" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ServiceCenterNo" UniqueName="ServiceCenterNo" HeaderText="Service Center No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ServiceCenterName" UniqueName="ServiceCenterName" HeaderText="Service Center Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="City" UniqueName="DealerCity" HeaderText="City"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="State" UniqueName="AgentState" HeaderText="State"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="VIN" UniqueName="VIN" HeaderText="VIN"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Make" UniqueName="Make" HeaderText="Make"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Model" UniqueName="Model" HeaderText="Model"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="LossDate" UniqueName="LossDate" HeaderText="Loss date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trServiceCenter">
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" Height="200px" Width="300px" PostBackControls="rgServiceCenter">
                                            <telerik:RadGrid ID="rgServiceCenter" runat="server" OnSelectedIndexChanged="rgServiceCenter_SelectedIndexChanged" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ServiceCenterNo" PageSize="10" ShowFooter="true">
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ServiceCenterID"  ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ServiceCenterNo" FilterCheckListWebServiceMethod="LoadDealerNo" UniqueName="DealerNo" HeaderText="Seller No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ServiceCenterName" FilterCheckListWebServiceMethod="LoadDealerName" UniqueName="DealerName" HeaderText="Seller Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="DealerCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="AgentState" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource1"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select ServiceCenterID, ServiceCenterNo, ServiceCenterName, City, State 
                                            from ServiceCenter order by ServiceCenterName" runat="server"></asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ID="trUserInfo">
                                        <telerik:RadGrid ID="rgUserInfo" runat="server" OnSelectedIndexChanged="rgUserInfo_SelectedIndexChanged" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                            AllowSorting="true" AllowPaging="true" Width="1000" ShowFooter="true" DataSourceID="dsUserInfo">
                                            <GroupingSettings CaseSensitive="false" />
                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="UserID" PageSize="20" ShowFooter="true">
                                                <Columns>
                                                    <telerik:GridBoundColumn DataField="UserID"  ReadOnly="true" Visible="false" UniqueName="UserID"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="UserName" FilterCheckListWebServiceMethod="LoadUserName" UniqueName="UserName" HeaderText="User Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="FName" FilterCheckListWebServiceMethod="LoadFName" UniqueName="FName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="LName" FilterCheckListWebServiceMethod="LoadLName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="EMail" FilterCheckListWebServiceMethod="LoadEMail" UniqueName="EMail" HeaderText="E-Mail" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                <Selecting AllowRowSelect="true" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                        <asp:SqlDataSource ID="dsUserInfo"
                                        ProviderName="System.Data.SqlClient" SelectCommand="select userid, username, fname, lname, email from userinfo order by username" runat="server"></asp:SqlDataSource>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="trUserInfoClose">
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="Close" CssClass="button1" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfAssignedTo" runat="server" />
                <asp:HiddenField ID="hfServiceCenterID" runat="server" />
                <asp:HiddenField ID="hfInsCarrierID" runat="server" />
                <asp:HiddenField ID="hfVeroOnly" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />
                <asp:HiddenField ID="hfSubAgentID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    `   <asp:HiddenField ID="hfError" runat="server" />
    </form>
</body>
</html>
