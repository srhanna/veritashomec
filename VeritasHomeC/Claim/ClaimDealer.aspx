﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimDealer.aspx.cs" Inherits="VeritasHomeC.ClaimDealer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
   <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Seller No:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDealerNo" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Seller Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDealerName" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 1:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAddr1" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Address 2:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAddr2" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            City, State Zip:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAddr3" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Phone:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtPhone" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Agent No:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAgentNo" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell Font-Bold="true">
                            Agent Name:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAgentName" ReadOnly="true" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfDealerID" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
