﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimGAPLoanInfo.aspx.cs" Inherits="VeritasHomeC.ClaimGAPLoanInfo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel runat="server" ID="pnlCustomer" DefaultButton="btnHiddenCustomer">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Bank Name:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtFName" Width="150" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Address 1:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Address 2:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    City:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    State:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadComboBox ID="cboState" Width="50" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                                                    <asp:SqlDataSource ID="dsStates"
                                                                    ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    Zip:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtZip" Width="75" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Phone:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    E-Mail:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtEMail" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Account Number:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtAccountNo" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" CssClass="button1" Text="Update" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenCustomer" Visible="false" runat="server" Text="Update" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
