﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class ClaimHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsHistory.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsHistory2.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimOpenHistory.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
                GetContractNo();
        }

        private void GetContractNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
                GetTotClaimsPaid();
            }
        }

        private void GetTotClaimsPaid()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select sum(paidamt) as Amt from claimdetail ";
            SQL = SQL + "where claimid in (select claimid from claim where contractid = " + hfContractID.Value + ") ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                    txtTotalClaimsPaid.Text = String.Format(Convert.ToDouble(clR.GetFields("amt")).ToString("#,##0.00"));
                else
                    txtTotalClaimsPaid.Text = "0.00";
            }
        }

        private void SetHierarchyLoadMode (GridTableView tableview)
        {
            tableview.HierarchyLoadMode = GridChildLoadMode.ServerBind;

            foreach (GridTableView detailView in tableview.DetailTables)
                SetHierarchyLoadMode(detailView);
        }

        protected void rgHistory_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetHierarchyLoadMode(rgHistory.MasterTableView);
            rgHistory.Rebind();
        }
    }
}