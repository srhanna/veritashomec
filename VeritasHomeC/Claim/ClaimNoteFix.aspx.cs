﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimNoteFix : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnClean_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnotetypeid = 5 ";
            SQL = SQL + "and (notetext like '%<p%>' ";
            SQL = SQL + "or notetext like '%<div%') ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i<=clR.RowCount()-1;i++)
                {
                    clR.GetRowNo(i);
                    ProcessNote(Convert.ToInt64(clR.GetFields("claimnoteid")));
                }
            }
        }

        private void ProcessNote(long xClaimNoteID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnoteid = " + xClaimNoteID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtNote.Content = clR.GetFields("note");
                clR.SetFields("notetext", txtNote.Text);
                clR.SaveDB();
            }
        }
    }
}