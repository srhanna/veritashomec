﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Claim.aspx.cs" Inherits="VeritasHomeC.Claim1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim</title>
</head>
<body>
    <form id="form1" runat="server">
       <style type="text/css">
        .RadWindow .rwTitleRow em
        {
            background-color: Red !important;
        }
        .RadWindow  .rwCorner em
        {
           background-color: Red !important;
         }
        </style>

         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsOpen" OnClick="btnClaimsOpen_Click" runat="server" Text="My Open Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>                 
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" OnClick="btnUnlockClaim_Click" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" onclick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" onclick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table ID="tbLock" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="500" BorderStyle="Solid" BorderColor="Red">
                                        <asp:Label ID="lblLocked" ForeColor="Red" Font-Bold="true" runat="server" Text="Label"></asp:Label> 
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlClaim" runat="server">
                                            <asp:Label ID="lblClaimHeader" runat="server" Width="1500" Text="Claim Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboClaimStatus" OnSelectedIndexChanged="cboClaimStatus_SelectedIndexChanged" DataSourceID="dsClaimStatus" DataTextField="ClaimStatus" AutoPostBack="true" DataValueField="ClaimStatus" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsClaimStatus" 
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select ClaimStatus from claimstatus order by orderno" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Activity:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboActivity" OnTextChanged="cboActivity_TextChanged" DropDownWidth="250" DataSourceID="dsActivity" DataTextField="ActivityDesc" AutoPostBack="true" DataValueField="ClaimActivityID" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsActivity"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select claimactivityid, activitydesc from claimactivity order by ActivityDesc" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Assigned To:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAssignedTo" runat="server"></asp:TextBox>
                                                        <asp:Button ID="btnChangeAssignedTo" OnClick="btnChangeAssignedTo_Click" runat="server" Text="Change" CssClass="button1" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Open Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtLossDate" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Days Since Sale:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDaysSale" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Service Center:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtServiceCenter" runat="server" ReadOnly="true"></asp:TextBox>
                                                        <asp:Button ID="btnSeekServiceCenter" OnClick="btnSeekServiceCenter_Click" runat="server" Text="Seek" CssClass="button1" />
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Service Center Info:
                                                    </asp:TableCell>
                                                    <asp:TableCell ColumnSpan="4">
                                                        <asp:TextBox ID="txtServiceInfo" ReadOnly="true" Width="600" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Contact Info:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtServiceCenterContact" OnTextChanged="txtServiceCenterContact_TextChanged" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        E-Mail:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtSCEmail" OnTextChanged="txtSCEmail_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        FAX:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadMaskedTextBox ID="txtSCFax" OnTextChanged="txtSCFax_TextChanged" Mask="(###) ###-####" AutoPostBack="true" runat="server"></telerik:RadMaskedTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Labor Rate Avg:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAvgRate" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Shop Rate:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtShopRate" ontextchanged="txtShopRate_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Default Loss Code:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtLossCodeClaim" runat="server"></asp:TextBox>
                                                        <asp:Button ID="btnSeekLossCode" onclick="btnSeekLossCode_Click" runat="server" Text="Seek" CssClass="button1"  />
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Loss Code Desc:
                                                    </asp:TableCell>
                                                    <asp:TableCell ColumnSpan="2">
                                                        <asp:TextBox ID="txtLossCodeDescClaim" Width="200" Enabled="false" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        RO Number
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtRONumber" OnTextChanged="txtRONumber_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        RO Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="rdpRODate" OnSelectedDateChanged="rdpRODate_SelectedDateChanged" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Part Tax:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtPartTax" OnTextChanged="txtPartTax_TextChanged" Type="Percent" MinValue="0" MaxValue="100" NumberFormat-DecimalDigits="3" AutoPostBack="true" runat="server"></telerik:RadNumericTextBox>
                                                        <asp:Button ID="btnCalcPart" OnClick="btnCalcPart_Click" runat="server" Text="Calc" CssClass="button1" />
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Labor Tax:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtLaborTax" OnTextChanged="txtLaborTax_TextChanged" Type="Percent" MinValue="0" MaxValue="100" NumberFormat-DecimalDigits="3" AutoPostBack="true" runat="server"></telerik:RadNumericTextBox>
                                                        <asp:Button ID="btnCalcLabor" OnClick="btnCalcLabor_Click" runat="server" Text="Calc" CssClass="button1" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:CheckBox ID="chkHCC" OnCheckedChanged="chkHCC_CheckedChanged" Text="High Cost Claims" Font-Bold="true" AutoPostBack="true" runat="server" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlContract" runat="server">
                                            <asp:Label ID="lblContractInfo" runat="server" Width="1500" Text="Contract Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Contract No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtContractNo" ReadOnly="true" runat="server"></asp:TextBox>
                                                        <asp:Button ID="btnSeekContract" OnClick="btnSeekContract_Click" runat="server" Text="Seek" CssClass="button1" />
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtStatus" Font-Bold="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Sale Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtSaleDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Paid Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPaidDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        First Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtFName" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Last Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtLName" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Insurance Carrier:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtInspCarrier" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Program:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtProgram" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Plan:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPlan" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Term:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtTerm" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Company Info:
                                                    </asp:TableCell>
                                                    <asp:TableCell ColumnSpan="3">
                                                        <asp:TextBox ID="txtCompanyInfo" runat="server"  Width="300"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Effective Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtEffDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Expired Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtExpDate" Width="75" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true" ID="tcDec">
                                                        Dec Page:
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="tcDesc2" ColumnSpan="2">
                                                        <asp:HyperLink ID="hlDec" Target="_blank" runat="server"></asp:HyperLink>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true" ID="tcTC">
                                                        Terms and Conditions Page:
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="tcTC2" ColumnSpan="2">
                                                        <asp:HyperLink ID="hlTC" Target="_blank" runat="server"></asp:HyperLink>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server">
                                            <asp:Label ID="lblVehInfo" runat="server" Width="1500" Text="Covered Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 1:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtCoveredAddr1" Width="150" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 2:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtCoveredAddr2" Width="50" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        City:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtCoveredCity" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        State:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtCoveredState" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Zip:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtCoveredZip" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server">
                                            <asp:Label ID="lblDealer" runat="server" Width="1500" Text="Dealer Information" ForeColor="White" BackColor="DarkBlue"></asp:Label>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Seller No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Seller Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerName" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true" ID="tcLR1">
                                                        Loss Ratio:
                                                    </asp:TableCell>
                                                    <asp:TableCell ID="tcLR2">
                                                        <asp:TextBox ID="txtLossRatio" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 1:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerAddr1" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Address 2:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerAddr2" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        City, State, Zip:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtDealerAddr3" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Phone:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadMaskedTextBox ID="txtDealerPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Agent No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAgentNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Agent Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtAgentName" Width="200" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Has Slush:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:CheckBox ID="chkHasSlush" runat="server" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel ID="pnlDetail" runat="server">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadTabStrip ID="tsClaim" OnTabClick="tsClaim_TabClick" runat="server" Width="1700">
                                                            <Tabs>
                                                                <telerik:RadTab Text="Surcharges" Value="Surcharges" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Customer Info" Value="Customer" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="3 C's" Value="3C" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Documents" Value="Documents" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Parts/Labor" Value="Part" Width="120"></telerik:RadTab> 
                                                                <telerik:RadTab Text="Alert" BackColor="Red" ForeColor="Red" Value="Alert" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Notes" Value="Notes" Width="120"></telerik:RadTab> 
                                                                <telerik:RadTab Text="Jobs" Value="Jobs" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Payments" Value="Payment" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="History" Value="History" Width="120"></telerik:RadTab>
                                                                <telerik:RadTab Text="Contract Notes" Value="ContractNote" Width="120"></telerik:RadTab>
                                                            </Tabs>
                                                        </telerik:RadTabStrip>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:radmultipage ID="rmClaim" runat="server" >
                                                            <telerik:RadPageView ID="pvSurcharges" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvCustomer" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pv3C" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvDocuments" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvInspection" Height="500" runat="server">
                                                            </telerik:RadPageView>
															<telerik:RadPageView ID="pvParts" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                             <telerik:RadPageView ID="pvJob" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvNotes" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvPayment" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvAlert" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvHistory" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvHCC" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                            <telerik:RadPageView ID="pvContractNote" Height="500" runat="server">
                                                            </telerik:RadPageView>
                                                       </telerik:radmultipage>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlSeekSC">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnAddSC" OnClick="btnAddSC_Click" runat="server" Text="Add Service Center" CssClass="button1" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgServiceCenter" OnSelectedIndexChanged="rgServiceCenter_SelectedIndexChanged" runat="server" AutoGenerateColumns="false"
                                                            AllowSorting="true" AllowPaging="true" Width="1000" ShowFooter="true">
                                                            <GroupingSettings CaseSensitive="false" />
                                                            <MasterTableView AllowFilteringByColumn="true" DataKeyNames="ServiceCenterID" PageSize="10">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="ServiceCenterID"  ReadOnly="true" Visible="false" UniqueName="ServiceCenterID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ServiceCenterNo" FilterCheckListWebServiceMethod="LoadServiceCenterNo" UniqueName="ServiceCenterNo" HeaderText="Service Center No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ServiceCenterName" FilterCheckListWebServiceMethod="LoadServiceCenterName" UniqueName="ServiceCenterName" HeaderText="Service Center Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="ServiceCenterCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Zip" FilterCheckListWebServiceMethod="LoadZip" UniqueName="Zip" HeaderText="Zip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="Phone" FilterCheckListWebServiceMethod="LoadPhone" UniqueName="Phone" HeaderText="Phone" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsServiceCenter" runat="server"
                                                            ProviderName="System.Data.SqlClient" 
                                                            SelectCommand="select servicecenterid, servicecenterno, servicecentername,city,state, zip,phone from servicecenter order by ServiceCenterName"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlAddSC" DefaultButton="btnHiddenSC">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Service Center Name:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtServiceCenterName" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    Address 1:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    Address 2:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    City:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    State:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                                                    <asp:SqlDataSource ID="dsStates"
                                                                    ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Zip Code:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Phone:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Fax:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <telerik:RadMaskedTextBox ID="txtFax" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    E-Mail:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtEMail" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Visible="false">
                                                                    <asp:Button ID="btnHiddenSC" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnSCCancel" OnClick="btnSCCancel_Click"  runat="server" Text="Cancel" CssClass="button1" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnSCSave" OnClick="btnSCSave_Click" runat="server" Text="Save" CssClass="button1" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Panel runat="server" ID="pnlSeekContract">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgContract" runat="server" AutoGenerateColumns="false" OnSelectedIndexChanged="rgContract_SelectedIndexChanged"
                                                            AllowSorting="true" AllowPaging="true" Width="1000" ShowFooter="true" >
                                                            <GroupingSettings CaseSensitive="false" />
                                                            <MasterTableView AllowFilteringByColumn="true" DataKeyNames="ContractID" PageSize="10">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="ContractID"  ReadOnly="true" Visible="false" UniqueName="ServiceCenterID"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ContractNo" FilterCheckListWebServiceMethod="LoadContractNo" UniqueName="ContractNo" HeaderText="Contract No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="FName" FilterCheckListWebServiceMethod="LoadFName" UniqueName="FName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="LName" FilterCheckListWebServiceMethod="LoadLName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="VIN" FilterCheckListWebServiceMethod="LoadVIN" UniqueName="VIN" HeaderText="VIN" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="DealerNo" FilterCheckListWebServiceMethod="LoadDealerNo" UniqueName="DealerNo" HeaderText="Seller No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="DealerName" FilterCheckListWebServiceMethod="LoadDealerName" UniqueName="DealerName" HeaderText="Seller Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsContract" runat="server"
                                                            ProviderName="System.Data.SqlClient" 
                                                            SelectCommand ="select contractid, contractno, fname, lname,c.city,c.state,dealerno,dealername from contract c 
                                                            left join dealer d on d.dealerid = c.dealerid 
                                                            where c.status <> 'Expired' and c.status <> 'Pending Expired' and c.status <> 'Cancelled' and c.status <> 'Cancelled Before Paid' "
                                                            ></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlSeekLossCode">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnAddLossCode" onclick="btnAddLossCode_Click" runat="server" Text="Add Loss Code" CssClass="button1" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgLossCode" onselectedindexchanged="rgLossCode_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" DataSourceID="dsLossCode"
                                                            AllowSorting="true" Width="1000" ShowFooter="true">
                                                            <GroupingSettings CaseSensitive="false" />
                                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="LossCode" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="losscodeid" Visible="false"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="LossCode" FilterCheckListWebServiceMethod="LoadLossCode" UniqueName="LossCode" HeaderText="Loss Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="LossCodeDesc" FilterCheckListWebServiceMethod="LoadLossCodeDesc" UniqueName="LossCodeDesc" HeaderText="Loss Code Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="ACP" UniqueName="ACP" HeaderText="ACP"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsLossCode" ProviderName="System.Data.SqlClient" 
                                                        SelectCommand="(select clc.*, 0 as ACP from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid 
									and not clc.losscode in ('ZDIAGTME','ZGOODWLL','ZGWCLAIM','ZLABRTME','ZLBRDIFF', 'ZPARTCST','ZANGDWLL','ZMCEDIFF')
									union select clc.*, 1 as ACP from ClaimLossCode ClC left join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    left join contract c on c.ProgramID <> cc.ProgramID and c.PlanTypeID <> cc.PlanTypeID left join claim cl on c.contractid = cl.contractid and  cl.claimid = @claimid  where cc.idx is null and not clc.losscode in ('ZDIAGTME', 'ZGOODWLL','ZGWCLAIM','ZLABRTME','ZLBRDIFF','ZPARTCST','ZANGDWLL','ZMCEDIFF')) order by losscode" runat="server">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlAssignedToChange">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <telerik:RadGrid ID="rgAssignedTo" OnSelectedIndexChanged="rgAssignedTo_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" DataSourceID="dsAssignedTo"
                                                            AllowSorting="true" Width="1000" ShowFooter="true">
                                                            <GroupingSettings CaseSensitive="false" />
                                                            <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="UserID" ShowFooter="true">
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="Userid" Visible="false"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="UserName" FilterCheckListWebServiceMethod="LoadUserName" UniqueName="UserName" HeaderText="User Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="FName" FilterCheckListWebServiceMethod="FName" UniqueName="LossFName" HeaderText="First Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    <telerik:GridBoundColumn DataField="LName" FilterCheckListWebServiceMethod="LoadLName" UniqueName="LName" HeaderText="Last Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                </Columns>
                                                            </MasterTableView>
                                                            <ClientSettings EnablePostBackOnRowClick="true">
                                                                <Selecting AllowRowSelect="true" />
                                                            </ClientSettings>
                                                        </telerik:RadGrid>
                                                        <asp:SqlDataSource ID="dsAssignedTo" ProviderName="System.Data.SqlClient" SelectCommand="select userid, username, fname, lname from userinfo where active > 0" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>

                                        <asp:Panel runat="server" ID="pnlLossCode">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Loss Code:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtLossCode" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Loss Code Desc:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtLossCodeDesc" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right"> 
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnCancelLossCode" OnClick="btnCancelLossCode_Click" runat="server" Text="Cancel" CssClass="button1" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnSaveLossCode" OnClick="btnSaveLossCode_Click" runat="server" Text="Save" CssClass="button1" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:Panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:panel runat="server" ID="pnlContractConfirm">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell Font-Bold="true">
                                                                    Contract Status:
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:TextBox ID="txtContractStatus2" runat="server"></asp:TextBox>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Note:
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtNote" Height="200" Width="500" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell HorizontalAlign="Right">
                                                        <asp:Table runat="server">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnProceedClaim" OnClick="btnProceedClaim_Click" CssClass="button1" runat="server" Text="Proceed with Claim" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Button ID="btnCancelClaim" OnClick="btnCancelClaim_Click" CssClass="button1" runat="server" Text="Cancel Claim" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:panel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfProgram" runat="server" />
                <asp:HiddenField ID="hfPlanTypeID" runat="server" />
                <asp:HiddenField ID="hfServiceCenterID" runat="server" />
                <asp:HiddenField ID="hfClaimLocked" runat="server" />
                <asp:HiddenField ID="hfServiceCenterNo" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfAssignedTo" runat="server" />
                <asp:HiddenField ID="hfClaimPayeeID" runat="server" />
                <asp:HiddenField ID="hfDealerID" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />
                <asp:HiddenField ID="hfReadOnly" runat="server" />
                <asp:HiddenField ID="hfClaimClose" runat="server" />


        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Title="Note Error" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text="You need to enter in Note."></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" CssClass="button2" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>

        <asp:HiddenField ID="hfDenied" runat="server" />
        <telerik:RadWindow ID="rwDenied"  runat="server" Width="500" Title="Note Error" Height="300" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblDenied" Text="You need to enter in Note."></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtDeniedNote" TextMode="MultiLine" Width="450" Height="175" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnDeniedClose" OnClick="btnDeniedClose_Click" runat="server" CssClass="button2" Width="75" Text="Close" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnDeniedSave" OnClick="btnDeniedSave_Click" runat="server" CssClass="button2" Width="75" Text="Save" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>

        <asp:HiddenField ID="hfOpen" runat="server" />
        <telerik:RadWindow ID="rwOpen"  runat="server" Width="500" Title="Note Error" Height="300" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblOpen" Text="You need to enter in Note."></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtOpenNote" TextMode="MultiLine" Width="450" Height="175" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnOpenClose" OnClick="btnOpenClose_Click" runat="server" CssClass="button2" Width="75" Text="Close" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnOpenSave" OnClick="btnOpenSave_Click" runat="server" CssClass="button2" Width="75" Text="Save" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>

            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfAlert" runat="server" />
        <telerik:RadWindow ID="rwAlert"  runat="server" Width="750" Height="750"  Behaviors="Close" BackColor ="Red" BorderColor="Red" EnableViewState="false" ReloadOnShow="false" Title="Alert">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAlertPopup" Width="730" Height="600" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="730"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnAlertOK" OnClick="btnAlertOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
