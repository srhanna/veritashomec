﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimGapPayment.aspx.cs" Inherits="VeritasHomeC.ClaimGapPayment" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlError">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell Width="500" BorderColor="Red" BorderStyle="Solid" BorderWidth="1">
                                <asp:Label ID="lblError" runat="server" Text="Label" ForeColor="Red"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlList" DefaultButton="btnHiddenCP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAddPayment" OnClick="btnAddPayment_Click" CssClass="button1" runat="server" Text="Add Payment" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimPayment" OnSelectedIndexChanged="rgClaimPayment_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsClaimPayment"
                                    AllowSorting="true" Width="1000" ShowFooter="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimPaymentID" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimPaymentID"  ReadOnly="true" Visible="false" UniqueName="ClaimPayeeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PaymentType"  UniqueName="PayeeType" HeaderText="Payee Type"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeNo"  UniqueName="PayeeNo" HeaderText="Payee No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AuthDate" UniqueName="AuthDate" HeaderText="Auth Date" DataFormatString="0:M/d/yyyy"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PaidDate" UniqueName="PaidDate" HeaderText="Paid Date" DataFormatString="0:M/d/yyyy"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsClaimPayment"
                                ProviderName="System.Data.SqlClient" 
                                    SelectCommand="select claimpaymentid, paymenttype, payeeno, payeename, AuthDate, PaidDate from claimgappayment cp 
                                    inner join claimgappayee cgp on cp.claimgappayeeid = cgp.claimgappayeeid
                                    inner join claimgappaymenttype cpt on cp.claimgappaymenttypeid = cpt.claimgappaymenttypeid " 
                                    runat="server">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenCP" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Payment Type:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:DropDownList ID="cboPaymentType" DataSourceID="dsPaymentType" DataTextField="PaymentType" DataValueField="ClaimGapPaymentTypeID" runat="server"></asp:DropDownList>
                                <asp:SqlDataSource ID="dsPaymentType"
                                ProviderName="System.Data.SqlClient" 
                                    SelectCommand="select claimgappaymenttypeid, PaymentType from claimgappaymentType " 
                                    runat="server">
                                </asp:SqlDataSource>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Payee No: 
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtPayeeNo" ReadOnly="true"  runat="server"></asp:TextBox>
                                <asp:Button ID="btnSeekPayee" OnClick="btnSeekPayee_Click" runat="server" CssClass="button1" Text="Seek" />
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Payee Name:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtPayeeName" ReadOnly="true"  runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Payment Amt:
                            </asp:TableCell>
                            <asp:TableCell>
                                <telerik:RadNumericTextBox ID="txtPaymentAuth" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Authorize Date:
                            </asp:TableCell>
                            <asp:TableCell>
                                <telerik:RadDatePicker ID="rdpAuthDate" OnSelectedDateChanged="rdpAuthDate_SelectedDateChanged" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Paid Date:
                            </asp:TableCell>
                            <asp:TableCell>
                                <telerik:RadDatePicker ID="rdpPaidDate" OnSelectedDateChanged="rdpPaidDate_SelectedDateChanged" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Bank Loan:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtBankLoad" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Last 6 of VIN:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtLast6" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="6" HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnDetailClose" OnClick="btnDetailClose_Click" CssClass="button1" runat="server" Text="Close" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnDetailUpdate" OnClick="btnDetailUpdate_Click" CssClass="button1" runat="server" Text="Update" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSeekPayee" DefaultButton="btnHiddenCP2">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAddClaimPayee" OnClick="btnAddClaimPayee_Click" runat="server" Text="Add Claim Payee" CssClass="button1" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimPayee" OnSelectedIndexChanged="rgClaimPayee_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsPayee"
                                    AllowSorting="true" Width="1000" ShowFooter="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ClaimGAPPayeeID" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimGapPayeeID" ReadOnly="true" Visible="false" UniqueName="ClaimPayeeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="payeeno"  FilterCheckListWebServiceMethod="LoadPayeeNo" UniqueName="payeeno" HeaderText="Payee No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName"  FilterCheckListWebServiceMethod="LoadPayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsPayee"
                                ProviderName="System.Data.SqlClient" SelectCommand="select claimgappayeeid, payeeno, payeename, city, state from claimgappayee" runat="server"></asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenCP2" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel ID="pnlAddClaimPayee" runat="server" DefaultButton="btnHiddenACP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtGAPPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtGapPayeeName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 1:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 2:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            City:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            State:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsStates"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Zip Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Phone:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            E-Mail:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtEMail" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenACP" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnPayeeCancel" OnClick="btnPayeeCancel_Click" runat="server" Text="Cancel" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnPayeeSave" OnClick="btnPayeeSave_Click" runat="server" Text="Save" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfClaimPaymentID" runat="server" />
                <asp:HiddenField ID="hfPayeeID" runat="server" />
                <asp:HiddenField ID="hfAuthDate" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
