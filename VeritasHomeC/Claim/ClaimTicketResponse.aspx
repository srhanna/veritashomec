﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimTicketResponse.aspx.cs" Inherits="VeritasHomeC.ClaimTicketResponse" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsOpen" OnClick="btnClaimsOpen_Click" runat="server" Text="My Open Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimTeamOpen" OnClick="btnClaimTeamOpen_Click" runat="server" Text="Claims Team Open"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketMessage" OnClick="btnTicketMessage_Click" runat="server" Text="Ticket Message"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketResponse" OnClick="btnTicketResponse_Click" runat="server" Text="Ticket Response"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimAudit" OnClick="btnClaimAudit_Click" runat="server" Text="Claim Audit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnRFClaimSubmit" OnClick="btnRFClaimSubmit_Click" runat="server" Text="RF Claim Submit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" runat="server" Text="Loss Code"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAutonationACH" OnClick="btnAutonationACH_Click" runat="server" Text="AutoNation ACH"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnInspectionWex" runat="server" Text="Wex Inspection"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCarfaxPayment" runat="server" Text="CarFax Payment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel runat="server" ID="pnlList">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:DropDownList ID="cboClaim" OnSelectedIndexChanged="cboClaim_SelectedIndexChanged" AutoPostBack="true" DataTextField="ClaimNo" DataValueField="ClaimID" runat="server"></asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgJobs" Height="200px" Width="300px">
                                                <telerik:RadGrid ID="rgJobs" OnSelectedIndexChanged="rgJobs_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" 
                                                    AllowSorting="true" Width="1000" ShowFooter="true">
                                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimDetailID" ShowFooter="true">
                                                        <GroupByExpressions>
                                                            <telerik:GridGroupByExpression>
                                                                <SelectFields>
                                                                    <telerik:GridGroupByField FieldAlias="JobNo" FieldName="JobNo" HeaderText="Job No" HeaderValueSeparator=": " />
                                                                    <telerik:GridGroupByField FieldAlias="LossCodDesc" FieldName="LossCodeDesc" HeaderValueSeparator=": " />
                                                                </SelectFields>
                                                                <GroupByFields>
                                                                    <telerik:GridGroupByField FieldName="JobNo" SortOrder="Ascending" />
                                                                    <telerik:GridGroupByField FieldName="LossCodeDesc" SortOrder="Ascending" />
                                                                </GroupByFields>
                                                            </telerik:GridGroupByExpression>
                                                        </GroupByExpressions>
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimPartID"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailType" UniqueName="ClaimDetailType" HeaderText="Detail Type"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDetailStatus" UniqueName="ClaimDetailStatus" HeaderText="Status"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ClaimDesc" UniqueName="ClaimDesc" HeaderText="Claim Desc"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="LossCode" UniqueName="LossCode" HeaderText="Loss Code"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="ReqAmt" UniqueName="ReqAmt" HeaderText="Req Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="AuthAmt" UniqueName="AuthAmt" HeaderText="Auth Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TaxAmt" UniqueName="TaxAmt" HeaderText="Tax Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="TotalAmt" UniqueName="TotalAmt" HeaderText="Total Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Response" UniqueName="Response" HeaderText="Response"></telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnablePostBackOnRowClick="true">
                                                        <Selecting AllowRowSelect="true" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </telerik:RadAjaxPanel>
                                            <asp:SqlDataSource ID="dsJobs" ProviderName="System.Data.SqlClient" 
                                                SelectCommand="select cd.claimdetailid, claimdetailtype,cd.partno, cd.claimdesc, claimdetailstatus, jobno, cd.losscode, lc.losscodedesc,
                                                payeename, reqamt, authamt, taxamt, paidamt, totalamt From claimdetail cd 
                                                inner join veritasclaimticket.dbo.ticket T on t.claimdetailid = cd.claimdetailid
                                                left join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid
                                                Left join claimlosscode lc on lc.losscode = cd.losscode 
                                                where t.claimid = @claimid and t.responseid = 2 
                                                order by jobno "
                                                runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlDetail">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="lblAuthorizedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="lblApprovedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell runat="server">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Detail Type:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboClaimDetailType" DataSourceID="dsCDT" AutoPostBack="true" DataTextField="ClaimDetailType" DataValueField="ClaimDetailType" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsCDT"
                                                        ProviderName="System.Data.SqlClient" SelectCommand="select ClaimDetailType from claimdetail group by ClaimDetailType order by ClaimDetailType" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Job No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtJobNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Detail Status:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboClaimDetailStatus" DataValueField="ClaimDetailStatus" AutoPostBack="true" DataSourceID="dsClaimDetailStatus" DataTextField="ClaimDetailStatusDesc" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsClaimDetailStatus" ProviderName="System.Data.SqlClient" SelectCommand="select ClaimDetailStatus, ClaimDetailStatusDesc from ClaimDetailStatus" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Reason Code:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboReason" DataValueField="ClaimReasonID" DataSourceID="dsClaimReason" DataTextField="ReasonDesc" runat="server"></telerik:RadComboBox>
                                                        <asp:SqlDataSource ID="dsClaimReason" ProviderName="System.Data.SqlClient" SelectCommand="select ClaimReasonID, ReasonDesc from ClaimReason order by reasondesc" runat="server"></asp:SqlDataSource>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Part No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPartNo" Enabled="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Claim Desc:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtClaimDesc" Enabled="true" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Loss Code:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtLossCode" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Loss Code Desc:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtLossCodeDesc" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Payee No:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPayeeNo" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Payee Name:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtPayeeName" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Req Qty:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtReqQty" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Req Cost:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtReqCost" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Req Amt:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtReqAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Auth Qty:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtAuthQty" OnTextChanged="txtAuthQty_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Auth Cost:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtAuthCost" OnTextChanged="txtAuthCost_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Tax Rate:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtTaxRate" OnTextChanged="txtTaxRate_TextChanged" AutoPostBack="true" Type="Percent" NumberFormat-DecimalDigits="4" runat="server" ></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Tax Amt:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtTaxAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Total Amt:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtTotalAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Authorized Amt:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadNumericTextBox ID="txtAuthAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Authorized Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="rdpAuthorizedDate" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Approved Date:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadDatePicker ID="rdpDateApprove" runat="server"></telerik:RadDatePicker>
                                                    </asp:TableCell>
                                                    <asp:TableCell Font-Bold="true">
                                                        Payment Account:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <telerik:RadComboBox ID="cboRateType" runat="server"></telerik:RadComboBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Font-Bold="true">
                                                        Response:
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:TextBox ID="txtResponse" runat="server"></asp:TextBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <a href="Claim3C.aspx">Claim3C.aspx</a>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Right">
                                            <asp:Table runat="server">
                                                <asp:TableRow>
                                                    <asp:TableCell Visible="false">
                                                        <asp:Button ID="btnHiddenDetail" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnCloseTicket" OnClick="btnCloseTicket_Click" CssClass="button1" runat="server" Text="Close Ticket" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnCancelClaimDetail" OnClick="btnCancelClaimDetail_Click" CssClass="button1" runat="server" Text="Cancel" />
                                                    </asp:TableCell>
                                                    <asp:TableCell>
                                                        <asp:Button ID="btnSaveClaimDetail" OnClick="btnSaveClaimDetail_Click" CssClass="button1" runat="server" Text="Save" />
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfAssignedTo" runat="server" />
                <asp:HiddenField ID="hfServiceCenterID" runat="server" />
                <asp:HiddenField ID="hfClaimDetailID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfAgentID" runat="server" />
                <asp:HiddenField ID="hfSubAgentID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    `   <asp:HiddenField ID="hfError" runat="server" />

    </form>
</body>
</html>
