﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimGAPCalc.aspx.cs" Inherits="VeritasHomeC.ClaimGAPCalc" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<a href="../Properties/">../Properties/</a>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Loss Loan Balance
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtLossLoanBalance" OnTextChanged="txtLossLoanBalance_TextChanged" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Schedule Loan Balance
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtScheduleLoanBalance" OnTextChanged="txtScheduleLoanBalance_TextChanged" AutoPostBack="true" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Interest Earned
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtInterestEarned" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Total Schedule
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtTotalSchedule" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            ACV
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtACV" OnTextChanged="txtACV_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            NADA Loss
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtNADALoss" OnTextChanged="txtNADALoss_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Owner Salvage
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtOwnerSalvage" OnTextChanged="txtOwnerSalvage_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Mechanical Contract Refund
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtMechanicalContractRefund" OnTextChanged="txtMechanicalContractRefund_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Credit Insurance Refund
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtCreditInsRefunds" OnTextChanged="txtCreditInsRefunds_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Unearned GAP Refund
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtUnearnedGAPRefund" OnTextChanged="txtUnearnedGAPRefund_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Amount Exceding 150
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtAmountExceding150" OnTextChanged="txtAmountExceding150_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Insurance Deduct
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtInsDeduct" OnTextChanged="txtInsDeduct_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Other Deduct
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtOtherDeduct" OnTextChanged="txtOtherDeduct_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Gap Payout
                        </asp:TableCell>
                        <asp:TableCell>
                            <telerik:RadNumericTextBox ID="txtGAPPayout" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" CssClass="button1" runat="server" Text="Update" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
