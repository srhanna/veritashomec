﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;


namespace VeritasHomeC
{
    public partial class Claim1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsActivity.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsServiceCenter.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsClaimStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsContract.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsLossCode.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsAssignedTo.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            if (!IsPostBack)
            {
                btnChangeAssignedTo.Visible = false;
                GetServerInfo();
                tsClaim.Tabs[5].Visible = false;
                txtAssignedTo.Enabled = false;
                pvSurcharges.Selected = true;
                hfClaimID.Value = Request.QueryString["claimid"];
                pvNotes.ContentUrl = "~/claim/ClaimNotes.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pv3C.ContentUrl = "~/claim/Claim3C.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvDocuments.ContentUrl = "~/claim/ClaimDocument.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvInspection.ContentUrl = "~/claim/ClaimInspection.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvParts.ContentUrl = "~/claim/ClaimPart.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvJob.ContentUrl = "~/claim/ClaimJobs.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvPayment.ContentUrl = "~/claim/ClaimPayment.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvAlert.ContentUrl = "~/claim/ClaimAlert.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvHistory.ContentUrl = "~/claim/ClaimHistory.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvHCC.ContentUrl = "~/claim/ClaimHCC.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvSurcharges.ContentUrl = "~/claim/ClaimContractSurcharge.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvContractNote.ContentUrl = "~/claim/ContractNote.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value;

                if (hfUserID.Value == "0")
                    Response.Redirect("~/default.aspx");

                if (hfClaimID.Value == "")
                    hfClaimID.Value = "0";

                if (hfClaimID.Value == "0")
                    ClearScreen();
                else
                    FillScreen();

                pnlSeekSC.Visible = false;
                pnlSeekContract.Visible = false;
                pnlContractConfirm.Visible = false;
                pnlSeekLossCode.Visible = false;
                pnlLossCode.Visible = false;
                pnlAddSC.Visible = false;
                pnlAssignedToChange.Visible = false;
                tbLock.Visible = false;

                if (!CheckLock())
                {
                    hfClaimLocked.Value = "False";
                    LockClaim();
                }
                else
                    ShowLockedClaim();

                CheckNewNote();
                CheckAllNote();
                ReadOnlyButtons();
                GetHasSlush();
                InsertAccess();
            }

            if (hfDenied.Value == "Visible")
                ShowDenied();
            else
                HideDenied();

            if (hfOpen.Value == "Visible")
                ShowOpen();
            else
                HideOpen();

            if (hfAlert.Value == "Visible")
            {
                ShowAlert();
                hfAlert.Value = "";
            }
            else
                HideAlert();

            if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }
        }

        private void InsertAccess()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "insert into claimaccess (claimid, userid, accessdate) ";
            SQL = SQL + "values (";
            SQL = SQL + hfClaimID.Value + ", ";
            SQL = SQL + hfUserID.Value + ", ";
            SQL = SQL + "'" + DateTime.Now + "') ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void GetHasSlush()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and ratetypeid > 1 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                chkHasSlush.Checked = true;
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    hfReadOnly.Visible = true;
                    btnAddSC.Enabled = false;
                    btnChangeAssignedTo.Enabled = false;
                    btnSeekContract.Enabled = false;
                    btnSeekLossCode.Enabled = false;
                    btnServiceCenters.Enabled = false;
                    btnCalcLabor.Enabled = false;
                    btnCalcPart.Enabled = false;
                }
            }
        }

        private void CheckNewNote()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clA = new clsDBO();
            string nl = System.Environment.NewLine;

            SQL = "Select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("addnew")))
                {
                    if (Convert.ToInt64(clR.GetFields("contractid")) > 0)
                    {
                        SQL = "Select * from dealernote dn ";
                        SQL = SQL + "inner join contract c On c.dealerid = dn.dealerid ";
                        SQL = SQL + "where contractid = " + clR.GetFields("contractid") + " ";
                        SQL = SQL + "And dn.contractnewentry <> 0 ";
                        txtAlertPopup.Text = "";
                        clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                        if (clA.RowCount() > 0)
                        {
                            tsClaim.Tabs[5].Visible = true;
                            tsClaim.Tabs[0].Selected = true;
                            pvCustomer.Selected = true;
                            txtAlertPopup.Text = "";
                            for (int i = 0; i <= clA.RowCount() - 1; i++)
                            {
                                clA.GetRowNo(i);
                           
                                txtAlertPopup.Text = txtAlertPopup.Text + clA.GetFields("note") + nl + nl;
                            }
                        }
                    }
                }
            }

            SQL = "Select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("addnew")))
                {
                    if (Convert.ToInt64(clR.GetFields("contractid")) > 0)
                    {
                        SQL = "Select * from agentsnote an ";
                        SQL = SQL + "inner join Dealer d On d.agentsid = an.agentid ";
                        SQL = SQL + "inner join contract c On c.dealerid = d.dealerid ";
                        SQL = SQL + "where contractid = " + clR.GetFields("contractid") + " ";
                        SQL = SQL + "And an.claimnewentry <> 0 ";
                        txtAlertPopup.Text = "";
                        clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                        if (clA.RowCount() > 0)
                        {
                            tsClaim.Tabs[5].Visible = true;
                            tsClaim.Tabs[0].Selected = true;
                            pvCustomer.Selected = true;
                            txtAlertPopup.Text = "";
                            for (int i = 0; i <= clA.RowCount() - 1; i++)
                            {
                                clA.GetRowNo(i);
                                
                                txtAlertPopup.Text = txtAlertPopup.Text + clA.GetFields("note") + nl + nl;
                            }
                        }
                    }
                }
            }

            SQL = "Select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("addnew")))
                {
                    if (Convert.ToInt64(clR.GetFields("contractid")) > 0)
                    {
                        SQL = "Select * from subagentnote an ";
                        SQL = SQL + "inner join Dealer d On d.subagentid = an.subagentid ";
                        SQL = SQL + "inner join contract c On c.dealerid = d.dealerid ";
                        SQL = SQL + "where contractid = " + clR.GetFields("contractid") + " ";
                        SQL = SQL + "And an.claimnewentry <> 0 ";
                        txtAlertPopup.Text = "";
                        clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                        if (clA.RowCount() > 0)
                        {
                            tsClaim.Tabs[5].Visible = true;
                            tsClaim.Tabs[0].Selected = true;
                            pvCustomer.Selected = true;
                            txtAlertPopup.Text = "";
                            for (int i = 0; i <= clA.RowCount() - 1; i++)
                            {
                                clA.GetRowNo(i);
                                txtAlertPopup.Text = txtAlertPopup.Text + clA.GetFields("note") + nl + nl;
                            }
                        }
                    }
                }
            }

            if (txtAlertPopup.Text.Length > 0)
                hfAlert.Value = "Visible";
        }

        private void CheckAllNote()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string nl = System.Environment.NewLine;

            SQL = "Select * from dealernote dn ";
            SQL = SQL + "inner join contract c On c.dealerid = dn.dealerid ";
            SQL = SQL + "inner join claim cl On c.contractid = cl.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And dn.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                tsClaim.Tabs[5].Visible = true;
                tsClaim.Tabs[0].Selected = true;
                txtAlertPopup.Text = "";
                pvCustomer.Selected = true;
                
                for (int i = 0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + nl + nl;
                }
               
            }

            SQL = "Select * from agentsnote an ";
            SQL = SQL + "inner join Dealer d On d.agentsid = an.agentid ";
            SQL = SQL + "inner join contract c On d.dealerid = c.dealerid ";
            SQL = SQL + "inner join claim cl On c.contractid = cl.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And an.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                tsClaim.Tabs[5].Visible = true;
                tsClaim.Tabs[0].Selected = true;
                txtAlertPopup.Text = "";
                pvCustomer.Selected = true;
                
                for (int i = 0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + nl + nl;
                }
               
            }

            if (txtAlertPopup.Text.Length > 0)
                hfAlert.Value = "Visible";

            SQL = "Select * from subagentnote an ";
            SQL = SQL + "inner join Dealer d On d.subagentid = an.subagentid ";
            SQL = SQL + "inner join contract c On d.dealerid = c.dealerid ";
            SQL = SQL + "inner join claim cl On c.contractid = cl.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And an.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                tsClaim.Tabs[5].Visible = true;
                tsClaim.Tabs[0].Selected = true;
                txtAlertPopup.Text = "";
                pvCustomer.Selected = true;
               
                for (int i = 0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + nl + nl;
                }
              
            }

            if (txtAlertPopup.Text.Length > 0)
                hfAlert.Value = "Visible";

            SQL = "select * from contractnote cn ";
            SQL = SQL + "inner join claim cl On cn.contractid = cl.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and cn.claimalways <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    txtAlertPopup.Text = txtAlertPopup.Text + clR.GetFields("note") + nl + nl;
                }
            }
        }

        private void ShowLockedClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "Select userid, fname, lname from userinfo ui ";
            SQL = SQL + "inner join claim cl On ui.userid = cl.lockuserid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("userid") != hfUserID.Value)
                {
                    hfClaimLocked.Value = "True";
                    tbLock.Visible = true;
                    lblLocked.Text = "Claim is locked by " + clR.GetFields("fname") + " " + clR.GetFields("lname");
                }
            }
        }

        private bool CheckLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "Select claimid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return false;
            else
                return true;
        }

        private void LockClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update claim ";
            SQL = SQL + "Set lockuserid = " + hfUserID.Value + ", ";
            SQL = SQL + "lockdate = '" + DateTime.Today + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void ClearScreen()
        {
            GetClaimNo();
            Response.Redirect("claim.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value);
        }

        private void GetClaimNo()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            string sClaimNo;
            long lClaimNo = 0;

        MoveHere:

            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claim ";
            SQL = SQL + "where claimno like 'C1%' ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sClaimNo = clC.GetFields("claimno");
                sClaimNo = sClaimNo.Replace("C1", "");

                if (sClaimNo.Length == 0)
                    sClaimNo = "100000000";

                lClaimNo = Convert.ToInt64(sClaimNo);
                lClaimNo = lClaimNo + 1;
                sClaimNo = lClaimNo.ToString("100000000");
                sClaimNo = "C" + sClaimNo;
            }

            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claim ";
                SQL = SQL + "where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clC.RowCount() > 0)
                    goto MoveHere;
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", "Open");
                    if (hfContractID.Value.Length > 0)
                        clC.SetFields("contractid", hfContractID.Value);
                    else
                        clC.SetFields("contractid", "0");

                    clC.SetFields("lossdate", DateTime.Today.ToString());
                    clC.SetFields("claimactivityid", "1");
                    clC.SetFields("creby", hfUserID.Value);
                    clC.SetFields("assignedto", hfUserID.Value);
                    clC.SetFields("Modby", hfUserID.Value);
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("addnew", "True");
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claim ";
            SQL = SQL + "where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtClaimNo.Text = clC.GetFields("claimno");
                hfClaimID.Value = clC.GetFields("claimid");
            }

            CheckClaimDuplicate();

            SQL = "insert into claimopenhistory ";
            SQL = SQL + "(claimid, opendate, openby) ";
            SQL = SQL + "values (" + hfClaimID.Value + ",'";
            SQL = SQL + DateTime.Today + "',";
            SQL = SQL + hfUserID.Value + ")";
            clC.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void GetServerInfo()
        {
            tcLR1.Visible = false;
            tcLR2.Visible = false;
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
                CheckAssignedToChange();
                if (hfUserID.Value == "12" || hfUserID.Value == "5" || hfUserID.Value == "17" || hfUserID.Value == "18" || hfUserID.Value == "1")
                {
                    tcLR2.Visible = true;
                    tcLR1.Visible = false;
                }
            }
        }

        private void CheckAssignedToChange()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("username").ToLower() == "srhanna")
                    btnChangeAssignedTo.Visible = true;
                if (clR.GetFields("username").ToLower() == "jfager")
                    btnChangeAssignedTo.Visible = true;
                if (clR.GetFields("username").ToLower() == "kfulton")
                    btnChangeAssignedTo.Visible = true;
                if (clR.GetFields("username").ToLower() == "zpeters")
                    btnChangeAssignedTo.Visible = true;
                if (clR.GetFields("username").ToLower() == "jsteenbergen")
                    btnChangeAssignedTo.Visible = true;
                if (clR.GetFields("username").ToLower() == "dhanely")
                    btnChangeAssignedTo.Visible = true;
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
                if (!Convert.ToBoolean(clSI.GetFields("claimpayment")))
                    tsClaim.Tabs[10].Enabled = false;
            }
        }

        private void ShowError()
        {
            hfError.Value = "Visible";
            string script = "function f(){$find(\"" + rwError.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowDenied()
        {
            hfDenied.Value = "Visible";
            string script = "function f(){$find(\"" + rwDenied.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        private void ShowOpen()
        {
            hfOpen.Value = "Visible";
            string script = "function f(){$find(\"" + rwOpen.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwOpen", script, true);
            rwOpen.Visible = true;
        }

        private void ShowAlert()
        {
            hfAlert.Value = "Visible";
            string script = "function f(){$find(\"" + rwAlert.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, true);
        }

        protected void btnErrorOK_Click(object sender, EventArgs e)
        {
            hfError.Value = "";
            string script = "function f(){$find(\"\"" + rwError.ClientID + "\"\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void HideDenied()
        {
            hfDenied.Value = "";
            string script = "function f(){$find(\"" + rwDenied.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        private void HideOpen()
        {
            hfOpen.Value = "";
            string script = "function f(){$find(\"" + rwOpen.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
            rwOpen.Visible = false;
        }

        private void HideAlert()
        {
            hfAlert.Value = "";
            string script = "function f(){$find(\"" + rwAlert.ClientID + "\").hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, true);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/agents/agentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/dealer/dealersearch.aspx");
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/contract/contractsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            ClearLock();
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        private void FillScreen()
        {
            clsDBO clCL = new clsDBO();
            string SQL;
            SQL = "select * from claim cl ";
            SQL = SQL + "left join contract c on cl.contractid = c.contractid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                txtClaimNo.Text = clCL.GetFields("claimno");
                cboClaimStatus.SelectedValue = clCL.GetFields("status");
                tsClaim.SelectedIndex = 0;

                if (clCL.GetFields("claimactivityid") == "0")
                    cboActivity.SelectedValue = "1";
                else
                    cboActivity.SelectedValue = clCL.GetFields("claimactivityid");

                txtLossDate.Text = clCL.GetFields("lossdate");

                if (clCL.GetFields("saledate").Length > 0)
                {
                    if (clCL.GetFields("lossdate").Length > 0)
                    {
                        TimeSpan datediff = DateTime.Parse(clCL.GetFields("lossdate")).Subtract(DateTime.Parse(clCL.GetFields("saledate")));
                        txtDaysSale.Text = datediff.Days.ToString();
                    }
                    else
                        txtDaysSale.Text = "0";
                }
                else
                    txtDaysSale.Text = "0";

                PlaceServiceCenter(clCL.GetFields("servicecenterid"));
                txtServiceCenterContact.Text = clCL.GetFields("sccontactinfo");
                hfServiceCenterID.Value = clCL.GetFields("servicecenterid");
                txtShopRate.Text = clCL.GetFields("shoprate");

                if (clCL.GetFields("rodate").Length > 0)
                    rdpRODate.SelectedDate = DateTime.Parse(clCL.GetFields("rodate"));

                txtRONumber.Text = clCL.GetFields("ronumber");
                if (clCL.GetFields("clipid") == "")
                {
                    if (clCL.GetFields("contractno").Length > 0)
                    {
                        if (clCL.GetFields("contractno").Substring(0, 2) == "VA")
                            txtCompanyInfo.Text = "0000976 Veritas GPS";
                    }
                    goto MoveHere;
                }

                if (clCL.GetFields("clipid") == "1" || clCL.GetFields("clipid") == "2")
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";

                if (clCL.GetFields("clipid") == "0")
                {
                    if (clCL.GetFields("contractno").Substring(0, 2) == "VA")
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                }

                if (clCL.GetFields("clipid") == "3" || clCL.GetFields("clipid") == "4" || clCL.GetFields("clipid") == "11")
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "CHJ")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "DRV")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAC")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAD")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RAN")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RDI")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "REP")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSA")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSD")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "RSW")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else if (clCL.GetFields("Contractno").Substring(0, 3) == "VEL")
                        txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";
                    else
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                }

                if (clCL.GetFields("inscarrierid") == "4")
                {
                    if (isFL())
                        txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL";
                    else
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                }

                if (clCL.GetFields("inscarrierid") == "5")
                {
                    if (isFL())
                        txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL";
                    else
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                }

                if (clCL.GetFields("clipid") == "5" || clCL.GetFields("clipid") == "6" || clCL.GetFields("clipid") == "14")
                    txtCompanyInfo.Text = "0000976 Veritas GPS";

                if (clCL.GetFields("clipid") == "7" || clCL.GetFields("clipid") == "8" || clCL.GetFields("clipid") == "13")
                    txtCompanyInfo.Text = "0000976 Veritas GPS";

                if (clCL.GetFields("clipid") == "9" || clCL.GetFields("clipid") == "10" || clCL.GetFields("clipid") == "12")
                    txtCompanyInfo.Text = "0000977 Veritas GPA Central Admin Srv FL";

                if (clCL.GetFields("clipid") == "11")
                    txtCompanyInfo.Text = "0000978 Veritas GPS Red Shield";

                if (clCL.GetFields("clipid") == "0")
                {
                    if (clCL.GetFields("contractno").Substring(0, 3) == "VA1")
                        txtCompanyInfo.Text = "0000976 Veritas GPS";
                }

            MoveHere:;

                if (clCL.GetFields("parttax").Length == 0)
                    clCL.SetFields("parttax", "0");

                if (clCL.GetFields("labortax").Length == 0)
                    clCL.SetFields("labortax", "0");

                chkHCC.Checked = Convert.ToBoolean(clCL.GetFields("hcc"));

                if (chkHCC.Checked)
                    tsClaim.Tabs[10].Visible = true;
                else
                    tsClaim.Tabs[10].Visible = false;

                txtPartTax.Text = (Convert.ToDouble(clCL.GetFields("parttax")) * 100).ToString();
                txtLaborTax.Text = (Convert.ToDouble(clCL.GetFields("labortax")) * 100).ToString();
                hfContractID.Value = clCL.GetFields("contractid");
                pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value;

                txtLossCodeClaim.Text = clCL.GetFields("losscode");
                GetLossCodeInfo();
                GetContractInfo();
                hfAssignedTo.Value = clCL.GetFields("assignedto");
                GetAssignedTo();
                FillDealerInfo();
                CheckSecurity();
            }
        }

        private void CheckSecurity()
        {
            string SQL;
            clsDBO clUSI = new clsDBO();
            cboClaimStatus.Enabled = true;
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clUSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clUSI.RowCount() > 0)
            {
                clUSI.GetRow();
                if (cboClaimStatus.SelectedValue == "Denied" || cboClaimStatus.SelectedValue == "Paid")
                {
                    if (Convert.ToBoolean(clUSI.GetFields("AllowUndenyClaim")))
                        cboClaimStatus.Enabled = true;
                    else
                        cboClaimStatus.Enabled = false;
                }
            }
        }

        private bool isFL()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select c.State from claim cl inner join contract c on c.contractid = cl.contractid ";
            SQL = SQL + "where cl.ClaimID = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("state").ToUpper() == "FL")
                    return true;
            }
            return false;
        }

        private void FillDealerInfo()
        {
            OpenContract();
            OpenDealer();
            OpenLossRatio();
            OpenAgent();
        }

        private void OpenLossRatio()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            txtLossRatio.Text = "";
            SQL = "select lossratio from VeritasReports.dbo.DealerRatio ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("lossratio").Length > 0)
                    txtLossRatio.Text = Convert.ToDouble(clR.GetFields("lossratio")).ToString("p");
            }
        }

        private void OpenAgent()
        {
            if (hfAgentID.Value.Length == 0)
            {
                txtAgentName.Text = "";
                txtAgentNo.Text = "";
                return;
            }

            clsDBO clA = new clsDBO();
            string SQL;
            SQL = "select * from agents ";
            SQL = SQL + "where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAgentName.Text = clA.GetFields("agentname");
                txtAgentNo.Text = clA.GetFields("agentno");
            }
        }

        private void OpenDealer()
        {
            if (hfDealerID.Value.Length == 0)
            {
                txtDealerAddr1.Text = "";
                txtDealerAddr2.Text = "";
                txtDealerAddr3.Text = "";
                txtDealerName.Text = "";
                txtDealerNo.Text = "";
                txtDealerPhone.Text = "";
            }
            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtDealerAddr1.Text = clD.GetFields("addr1");
                txtDealerAddr2.Text = clD.GetFields("addr2");
                txtDealerAddr3.Text = clD.GetFields("city") + ", " + clD.GetFields("state") + " " + clD.GetFields("zip");
                txtDealerPhone.Text = clD.GetFields("phone");
            }
        }

        private void OpenContract()
        {
            string SQL;
            clsDBO clC = new clsDBO();

            if (hfContractID.Value.Length == 0)
                return;

            SQL = "Select dealerid, agentsid from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfDealerID.Value = clC.GetFields("dealerid");
                hfAgentID.Value = clC.GetFields("agentsid");
            }
            else
            {
                hfDealerID.Value = "0";
                hfAgentID.Value = "0";
            }
        }

        private void GetAssignedTo()
        {
            txtAssignedTo.Text = "";
            string SQL;
            clsDBO clR = new clsDBO();

            if (hfAssignedTo.Value == "")
                hfAssignedTo.Value = hfUserID.Value;

            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfAssignedTo.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAssignedTo.Text = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
        }

        private void GetLossCodeInfo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimlosscode ";
            SQL = SQL + "where losscode = '" + txtLossCodeClaim.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeDescClaim.Text = clR.GetFields("losscodedesc").Trim();
            }
        }

        private void GetContractInfo()
        {
            string SQL;
            clsDBO clC = new clsDBO();

            if (hfContractID.Value.Length == 0)
                return;

            SQL = "select * from contract c ";
            SQL = SQL + "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtContractNo.Text = clC.GetFields("contractno");
                txtStatus.Text = clC.GetFields("status");

                if (txtStatus.Text == "Cancelled")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Cancelled Before Paid")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Expired")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Pending Expired")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Invalid")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Pending Cancel")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "PendingCancel")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Quote")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Rejected")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Sale pending")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "test")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Training")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Void")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Pending")
                    txtStatus.BackColor = System.Drawing.Color.Yellow;
                if (txtStatus.Text == "Paid")
                    txtStatus.BackColor = System.Drawing.Color.Green;

                txtPaidDate.Text = DateTime.Parse(clC.GetFields("datepaid")).ToString("d");
                txtSaleDate.Text = DateTime.Parse(clC.GetFields("saledate")).ToString("d"); //formats date to mm/dd/yyyy format
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                txtCoveredAddr1.Text = clC.GetFields("coveredaddr1");
                txtCoveredAddr2.Text = clC.GetFields("coveredaddr2");
                txtCoveredCity.Text = clC.GetFields("coveredcity");
                txtInspCarrier.Text = clC.GetFields("inscarriername");
                txtCoveredState.Text = clC.GetFields("state");
                txtCoveredZip.Text = clC.GetFields("coveredzip");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");

                hfProgram.Value = clC.GetFields("programid");
                txtTerm.Text = clC.GetFields("termmonth");

                if (clC.GetFields("effdate").Length > 0)
                    txtEffDate.Text = DateTime.Parse(clC.GetFields("effdate")).ToString("d"); //formats date to mm/dd/yyyy format

                if (clC.GetFields("expdate").Length > 0)
                    txtExpDate.Text = DateTime.Parse(clC.GetFields("expdate")).ToString("d"); //formats date to mm/dd/yyyy format

                GetProgram();
                GetPlanType();

                if (clC.GetFields("decpage").Length == 0)
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                }
                else
                {
                    tcDec.Visible = true;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clC.GetFields("decpage");
                    hlDec.Text = clC.GetFields("decpage");
                }

                if (clC.GetFields("tcpage").Length == 0)
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                }
                else
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clC.GetFields("tcpage");
                    hlTC.Text = clC.GetFields("tcpage");
                }

            }
        }

        public void GetPlanType()
        {
            string SQL;
            clsDBO clPT = new clsDBO();
            SQL = "select * from plantype ";
            SQL = SQL + "where plantypeid = " + hfPlanTypeID.Value;
            clPT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clPT.RowCount() > 0)
            {
                clPT.GetRow();
                txtPlan.Text = clPT.GetFields("plantype");
            }
        }

        private void GetProgram()
        {
            string SQL;
            clsDBO clP = new clsDBO();
            SQL = "select * from program ";
            SQL = SQL + "where programid = " + hfProgram.Value;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                txtProgram.Text = clP.GetFields("programname");
            }
        }

        private void PlaceServiceCenter(string xServiceCenterID)
        {
            clsDBO clSC = new clsDBO();
            string SQL;

            if (xServiceCenterID.Length == 0)
                return;

            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = " + xServiceCenterID;
            clSC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clSC.RowCount() > 0)
            {
                clSC.GetRow();
                txtServiceCenter.Text = clSC.GetFields("servicecenterno");
                txtServiceInfo.Text = clSC.GetFields("servicecentername") + " " + clSC.GetFields("addr1") + " " +
                                      clSC.GetFields("addr2") + " " + clSC.GetFields("city") + " " + clSC.GetFields("state") + " " +
                                      clSC.GetFields("zip") + " " + clSC.GetFields("phone");
                txtSCEmail.Text = clSC.GetFields("email");
                txtSCFax.Text = clSC.GetFields("fax");

                if (clSC.GetFields("zip").Length > 0)
                    GetLaborInfo(clSC.GetFields("zip"));
            }
        }

        private void GetLaborInfo(string xZip)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            VeritasGlobalToolsV2.clsWISZipCode clWZ = new VeritasGlobalToolsV2.clsWISZipCode();
            clWZ.ZipCode = xZip;
            clWZ.RunZip();
            clWZ.RunZip();
            SQL = "select * from laborrate ";
            SQL = SQL + "where zipcode = '" + xZip + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtAvgRate.Text = (Convert.ToDouble(clR.GetFields("avgrate")).ToString("#,##0.00"));
            }
            else
                txtAvgRate.Text = Convert.ToDouble("0").ToString("#,##0.00");
        }

        protected void btnSeekServiceCenter_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekSC.Visible = true;
            rgServiceCenter.DataSourceID = "dsServiceCenter";
            rgServiceCenter.Rebind();
        }

        protected void rgServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlSeekSC.Visible = false;
            pnlDetail.Visible = true;
            hfServiceCenterID.Value = rgServiceCenter.SelectedValue.ToString();
            PlaceServiceCenter(hfServiceCenterID.Value);
            UpdateClaimPayee();
            UpdateClaim();
            UpdateDeduct();
        }

        private void UpdateClaimPayee()
        {
            string SQL;
            clsDBO clS = new clsDBO();
            clsDBO clR = new clsDBO();

            if (hfReadOnly.Value == "True")
                return;

            SQL = "select * from claimpayee ";
            SQL = SQL + "where payeeno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                SQL = "select * from servicecenter ";
                SQL = SQL + "where servicecenterno = '" + txtServiceCenter.Text + "' ";
                clS.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clS.RowCount() > 0)
                {
                    clS.GetRow();
                    clR.NewRow();
                    clR.SetFields("payeeno", clS.GetFields("servicecenterno"));
                    clR.SetFields("payeename", clS.GetFields("servicecentername"));
                    clR.SetFields("addr1", clS.GetFields("addr1"));
                    clR.SetFields("addr2", clS.GetFields("addr2"));
                    clR.SetFields("city", clS.GetFields("city"));
                    clR.SetFields("state", clS.GetFields("state"));
                    clR.SetFields("zip", clS.GetFields("zip"));
                    clR.SetFields("phone", clS.GetFields("phone"));
                    clR.AddRow();
                    clR.SaveDB();
                }
            }
        }

        private void UpdateDeduct()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clCP = new clsDBO();
            clsDBO clCD = new clsDBO();

            if (hfReadOnly.Value == "True")
                return;

            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = " + hfServiceCenterID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from claimpayee ";
                SQL = SQL + "where payeeno = '" + clR.GetFields("servicecenterno") + "' ";
                clCP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clCP.RowCount() > 0)
                {
                    clCP.GetRow();
                    SQL = "update claimdetail ";
                    SQL = SQL + "set claimpayeeid = " + clCP.GetFields("claimpayeeid") + " ";
                    SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
                    SQL = SQL + "and jobno = 'A01' ";
                    clCD.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
                }
            }
        }

        protected void btnSeekContract_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlSeekContract.Visible = true;
            rgContract.DataSourceID = "dsContract";
            rgContract.Rebind();
        }

        private bool AllowCancelExpire()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("allowCancelExpire")))
                    return true;
            }
            return false;
        }

        protected void rgContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clC = new clsDBO();

            if (hfReadOnly.Value == "True")
                return;

            pnlContract.Visible = false;
            SQL = "select * from contract ";
            SQL = SQL + "where contractid = " + rgContract.SelectedValue;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("status") == "Paid")
                {
                    hfContractID.Value = rgContract.SelectedValue.ToString();
                    pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" + hfID.Value + "&contractid=" + hfClaimID.Value;
                    UpdateClaim();
                    AddClaimDetail();
                    FillDealerInfo();
                    pnlDetail.Visible = true;
                    Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
                    return;
                }
                else
                {
                    hfContractID.Value = rgContract.SelectedValue.ToString();
                    UpdateClaim();
                    txtContractStatus2.Text = clC.GetFields("status");
                    pnlContractConfirm.Visible = true;
                    FillDealerInfo();
                    AddClaimDetail();
                    return;
                }
            }
            UpdateClaim();
            UpdateDeduct();
        }

        public void AddClaimDetail()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            long lDeductID = 0;
            double dDeduct = 0;

            SQL = "select deductid from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lDeductID = Convert.ToInt64(clR.GetFields("deductid"));
            }

            SQL = "select * from deductible ";
            SQL = SQL + "where deductid = " + lDeductID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                dDeduct = Convert.ToDouble(clR.GetFields("deductamt"));
            }

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and jobno = 'A01' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clR.RowCount() == 0)
                clR.NewRow();
            else
                clR.GetRow();

            GetClaimPayee();
            clR.SetFields("ratetypeid", "1");
            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimdetailtype", "Deductible");
            clR.SetFields("jobno", "A01");
            clR.SetFields("reqqty", "1");
            clR.SetFields("reqcost", (dDeduct * -1).ToString());
            clR.SetFields("Claimpayeeid", hfClaimPayeeID.Value);
            clR.SetFields("claimdetailstatus", "Requested");
            clR.SetFields("reqamt", (dDeduct * -1).ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("credate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Today.ToString());

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        private void GetClaimPayee()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clSC = new clsDBO();

            if (txtServiceCenter.Text.Length == 0)
            {
                hfClaimPayeeID.Value = "0";
                return;
            }

            SQL = "select * from claimpayee ";
            SQL = SQL + "where payeeno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                SQL = "select * from servicecenter ";
                SQL = SQL + "where servicecenterno = '" + txtServiceCenter.Text + "' ";
                clSC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clSC.RowCount() > 0)
                {
                    clSC.GetRow();
                    clR.NewRow();
                    clR.SetFields("payeeno", clSC.GetFields("servicecenterno"));
                    clR.SetFields("payeename", clSC.GetFields("servicecentername"));
                    clR.SetFields("addr1", clSC.GetFields("addr1"));
                    clR.SetFields("addr2", clSC.GetFields("addr2"));
                    clR.SetFields("city", clSC.GetFields("city"));
                    clR.SetFields("state", clSC.GetFields("state"));
                    clR.SetFields("zip", clSC.GetFields("zip"));
                    clR.SetFields("phone", clSC.GetFields("phone"));
                    clR.AddRow();
                    clR.SaveDB();
                    SQL = "select * from claimpayee ";
                    SQL = SQL + "where payeeno = '" + txtServiceCenter.Text + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clR.RowCount() > 0)
                    {
                        clR.GetRow();
                        hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
                    }
                }
            }
            else
            {
                clR.GetRow();
                hfClaimPayeeID.Value = clR.GetFields("claimpayeeid");
            }
        }

        private void UpdateClaim()
        {
            string SQL;
            clsDBO clClaim = new clsDBO();
            VeritasGlobalToolsV2.clsClaimHistory clCH = new VeritasGlobalToolsV2.clsClaimHistory();
            if (hfReadOnly.Value == "True")
                return;

            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clClaim.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clClaim.RowCount() > 0)
                clClaim.GetRow();
            else
                clClaim.NewRow();

            if (hfContractID.Value == "")
                hfContractID.Value = "0";

            if (clClaim.GetFields("contractid") != hfContractID.Value) {
                clCH.ClaimID = Convert.ToInt64(hfClaimID.Value);
                clCH.FieldName = "ContractID";
                clCH.PrevFieldInfo = clClaim.GetFields("contractid");
                clCH.FieldInfo = hfContractID.Value;
                clCH.CreBy = Convert.ToInt64(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("contractid", hfContractID.Value);
            if (clClaim.GetFields("claimactivityid") != cboActivity.SelectedValue) {
                clCH.ClaimID = Convert.ToInt64(hfClaimID.Value);
                clCH.FieldName = "ClaimActivityID";
                clCH.PrevFieldInfo = clClaim.GetFields("claimactivityid");
                clCH.FieldInfo = cboActivity.SelectedValue;
                clCH.CreBy = Convert.ToInt64(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("claimactivityid", cboActivity.SelectedValue);
            if (clClaim.GetFields("status") != cboClaimStatus.SelectedValue) {
                clCH.ClaimID = Convert.ToInt64(hfClaimID.Value);
                clCH.FieldName = "Status";
                clCH.PrevFieldInfo = clClaim.GetFields("status");
                clCH.FieldInfo = cboClaimStatus.SelectedValue;
                clCH.CreBy = Convert.ToInt64(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("status", cboClaimStatus.SelectedValue);
            if (hfServiceCenterID.Value == "")
                hfServiceCenterID.Value = "0";
            if (clClaim.GetFields("servicecenterid") != hfServiceCenterID.Value) {
                clCH.ClaimID = Convert.ToInt64(hfClaimID.Value);
                clCH.FieldName = "ServiceCenterID";
                clCH.PrevFieldInfo = clClaim.GetFields("servicecenterid");
                clCH.FieldInfo = hfServiceCenterID.Value;
                clCH.CreBy = Convert.ToInt64(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("servicecenterid", hfServiceCenterID.Value);
            if (clClaim.GetFields("sccontactinfo") != txtServiceCenterContact.Text) {
                clCH.ClaimID = Convert.ToInt64(hfClaimID.Value);
                clCH.FieldName = "SCContactInfo";
                clCH.PrevFieldInfo = clClaim.GetFields("sccontactinfo");
                clCH.FieldInfo = txtServiceCenterContact.Text;
                clCH.CreBy = Convert.ToInt64(hfUserID.Value);
                clCH.AddClaimHistory();
            }
            clClaim.SetFields("sccontactinfo", txtServiceCenterContact.Text);
            if (clClaim.RowCount() > 0) {
                clClaim.SetFields("moddate", DateTime.Today.ToString());
                clClaim.SetFields("modby", hfUserID.Value);
            } 
            else {
                clClaim.SetFields("credate", DateTime.Today.ToString());
                clClaim.SetFields("creby", hfUserID.Value);
                clClaim.SetFields("assignedto", hfUserID.Value);
                clClaim.SetFields("moddate", DateTime.Today.ToString());
                clClaim.SetFields("modby", hfUserID.Value);
                clClaim.AddRow();
            }
            clClaim.SaveDB();
            CheckClaimDuplicate();
        }

        protected void btnProceedClaim_Click(object sender, EventArgs e)
        {
            UpdateClaimNote();
            UpdateContractClaim();
            pnlContractConfirm.Visible = false;
            pnlDetail.Visible = false;
            Response.Redirect("~/claim/claim.aspx?sid=" + hfID.Value + "&ClaimID=" + hfClaimID.Value);
        }

        protected void btnCancelClaim_Click(object sender, EventArgs e)
        {
            UpdateContractClaimCancel();
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        private  void UpdateContractClaimCancel()
        {
            clsDBO clCL = new clsDBO();
            string SQL;
            if (hfReadOnly.Value == "True") 
                return;
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                clCL.SetFields("status", "Void");
                clCL.SetFields("contractid", hfContractID.Value);
                clCL.SaveDB();
            }
        }

        private void UpdateContractClaim()
        {
            clsDBO clCL = new clsDBO();
            string SQL;
            if (hfReadOnly.Value == "True")
                return;
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clCL.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                clCL.SetFields("contractid", hfContractID.Value);
                clCL.SaveDB();
            }
        }

        private void UpdateClaimNote()
        {
            if (txtNote.Text.Length == 0)
                return;

            clsDBO clCN = new clsDBO();
            string SQL;

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnoteid = 0 ";
            clCN.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clCN.NewRow();
            clCN.SetFields("claimid", hfClaimID.Value);
            clCN.SetFields("claimnotetypeid", "5");
            clCN.SetFields("note", txtNote.Text);
            clCN.SetFields("credate", DateTime.Today.ToString());
            clCN.SetFields("creby", hfUserID.Value);
            clCN.AddRow();
            clCN.SaveDB();
        }


        private bool CheckAlert()
        {
            if (!tsClaim.Tabs[5].Visible)
                return false;

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and Alert <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        protected void txtServiceCenterContact_TextChanged(object sender, EventArgs e)
        {
            UpdateClaim();
        }

        protected void cboActivity_TextChanged(object sender, EventArgs e)
        {
            UpdateClaim();

            if (cboActivity.SelectedValue == "11")
                CloseClaim();
            if (cboActivity.SelectedValue == "14")
                CloseClaim();
            if (cboActivity.SelectedValue == "15")
                CloseClaim();
            if (cboActivity.SelectedValue == "17")
                CloseClaim();
            if (cboActivity.SelectedValue == "18")
                CloseClaim();
            if (cboActivity.SelectedValue == "21")
                CloseClaim();
            if (cboActivity.SelectedValue == "22")
                CloseClaim();
            if (cboActivity.SelectedValue == "23")
                CloseClaim();
            if (cboActivity.SelectedValue == "24")
                CloseClaim();
            if (cboActivity.SelectedValue == "25")
                CloseClaim();
            if (cboActivity.SelectedValue == "26")
                CloseClaim();
            if (cboActivity.SelectedValue == "28")
                CloseClaim();
            if (cboActivity.SelectedValue == "29")
                CloseClaim();
            if (cboActivity.SelectedValue == "30")
                CloseClaim();
            if (cboActivity.SelectedValue == "31")
                CloseClaim();
            if (cboActivity.SelectedValue == "32")
                CloseClaim();
            if (cboActivity.SelectedValue == "33")
                CloseClaim();
            if (cboActivity.SelectedValue == "35")
                CloseClaim();
            if (cboActivity.SelectedValue == "36")
                CloseClaim();
            if (cboActivity.SelectedValue == "37")
                CloseClaim();
            if (cboActivity.SelectedValue == "38")
                CloseClaim();
            if (cboActivity.SelectedValue == "39")
                CloseClaim();
            if (cboActivity.SelectedValue == "40")
                CloseClaim();
            if (cboActivity.SelectedValue == "42")
                CloseClaim();
            if (cboActivity.SelectedValue == "43")
                CloseClaim();
            if (cboActivity.SelectedValue == "45")
                CloseClaim();
            if (cboActivity.SelectedValue == "46")
                CloseClaim();
            if (cboActivity.SelectedValue == "47")
                CloseClaim();
            if (cboActivity.SelectedValue == "48")
                CloseClaim();
          
            if (cboClaimStatus.SelectedValue == "Open")
            {
                if (!CheckNoteOpen())
                    if (cboClaimStatus.SelectedValue == "Open")
                       if (cboActivity.SelectedValue != "11" && cboActivity.SelectedValue != "14")
                           if (cboActivity.SelectedValue != "15" && cboActivity.SelectedValue != "17")
                               if (cboActivity.SelectedValue != "18" && cboActivity.SelectedValue != "21")
                                   if (cboActivity.SelectedValue != "22" && cboActivity.SelectedValue != "23")
                                       if (cboActivity.SelectedValue != "24" && cboActivity.SelectedValue != "25") 
                                           if (cboActivity.SelectedValue != "26" && cboActivity.SelectedValue != "28")
                                               if (cboActivity.SelectedValue != "29" && cboActivity.SelectedValue != "30")
                                                   if (cboActivity.SelectedValue != "31" && cboActivity.SelectedValue != "32")
                                                       if (cboActivity.SelectedValue != "33" && cboActivity.SelectedValue != "35")
                                                           if (cboActivity.SelectedValue != "36" && cboActivity.SelectedValue != "37")
                                                               if (cboActivity.SelectedValue != "38" && cboActivity.SelectedValue != "39")
                                                                   if (cboActivity.SelectedValue != "40" && cboActivity.SelectedValue != "42")
                                                                       if (cboActivity.SelectedValue != "43" && cboActivity.SelectedValue != "45")
                                                                           if (cboActivity.SelectedValue != "46" && cboActivity.SelectedValue != "47")
                                                                               if (cboActivity.SelectedValue != "48") {
                                                                                    ShowOpen();
                                                                                    return;
                                                                               }
            }
        }

        private void CloseClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfReadOnly.Value == "True")
                return;

            SQL = "update claim ";
            SQL = SQL + "set closedate = '" + DateTime.Today + "', ";
            SQL = SQL + "closeby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and closedate is null ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "update claimopenhistory ";
            SQL = SQL + "set closedate = '" + DateTime.Today + "', ";
            SQL = SQL + "closeby = " + hfUserID.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and closedate is null ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void txtPartTax_TextChanged(object sender, EventArgs e)
        {
            if (txtPartTax.Text == "")
                txtPartTax.Text = "0";

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "update claim ";
            SQL = SQL + "set parttax = " + Convert.ToDouble(txtPartTax.Text) / 100 + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void txtLaborTax_TextChanged(object sender, EventArgs e)
        {
            if (txtLaborTax.Text == "")
                txtLaborTax.Text = "0";

            string SQL;
            clsDBO clR = new clsDBO();

            if (hfReadOnly.Value == "True")
                return;

            SQL = "update claim ";
            SQL = SQL + "set labortax = " + Convert.ToDouble(txtLaborTax.Text) / 100 + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private void ClearLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            if (hfReadOnly.Value == "True")
                return;

            SQL = "update claim ";
            SQL = SQL + "set lockuserid = null, lockdate = null ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and lockuserid = " + hfUserID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        private bool CheckNote()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        protected void btnCalcPart_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailtype = 'Part' ";
            SQL = SQL + "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount() - 1; i++)
                {
                    clR.GetRowNo(i);
                    ProcessClaimTax(Convert.ToInt64(clR.GetFields("claimdetailid")));
                }
            }
        }

        private void ProcessClaimTax(long xClaimDetailID)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfReadOnly.Value == "True")
                return;

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailid = " + xClaimDetailID;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("claimdetailtype") == "Part")
                {
                    if (clR.GetFields("authamt").Length > 0)
                    {
                        if (Convert.ToDouble(clR.GetFields("authamt")) > 0)
                        {
                            string taxAmtCalc = (Math.Round(Convert.ToDouble(clR.GetFields("authamt")) * (Convert.ToDouble(txtPartTax.Text))) / 100).ToString();
                            string totalAmtCalc = (Convert.ToDouble(clR.GetFields("authamt") + Convert.ToDouble(clR.GetFields("taxamt")))).ToString();
                            clR.SetFields("taxper", (Convert.ToDouble(txtPartTax.Text) / 100).ToString());
                            clR.SetFields("taxamt", taxAmtCalc );
                            clR.SetFields("totalamt", totalAmtCalc);
                        }
                        else
                        {
                            clR.SetFields("taxper", (Convert.ToDouble(txtPartTax.Text) / 100).ToString());
                            clR.SetFields("taxamt", "0");
                            clR.SetFields("totalamt", "0");      
                        }
                    }
                    else
                    {
                        clR.SetFields("taxper", (Convert.ToDouble(txtPartTax.Text) / 100).ToString());
                        clR.SetFields("taxamt", "0");
                        clR.SetFields("totalamt", "0");   
                    }
                }

                if (clR.GetFields(("claimdetailtype")) == "Labor")
                {
                    if (clR.GetFields("authamt").Length > 0)
                    {
                        if (Convert.ToDouble(clR.GetFields("authamt")) > 0) {

                            string taxAmtCalc2 = (Math.Round(Convert.ToDouble(clR.GetFields("authamt")) * (Convert.ToDouble(txtLaborTax.Text))) / 100).ToString();
                            string totalAmtCalc2 = (Convert.ToDouble(clR.GetFields("authamt")) + Convert.ToDouble(clR.GetFields("taxamt"))).ToString();
                            clR.SetFields("taxper", (Convert.ToDouble(txtLaborTax.Text) / 100).ToString());
                            clR.SetFields("taxamt", taxAmtCalc2);
                            clR.SetFields("totalamt", totalAmtCalc2);
                        }
                        else
                        {
                            clR.SetFields("taxper", (Convert.ToDouble(txtLaborTax.Text) / 100).ToString());
                            clR.SetFields("taxamt", "0");
                            clR.SetFields("totalamt", "0");
                        }
                    }
                    else
                    {
                        clR.SetFields("taxper", (Convert.ToDouble(txtLaborTax.Text) / 100).ToString());
                        clR.SetFields("taxamt", "0");
                        clR.SetFields("totalamt", "0");
                    }
                }

                clR.SaveDB();
            }
        }

        protected void btnCalcLabor_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimdetailtype = 'Labor' ";
            SQL = SQL + "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount()-1; i++)
                {
                    clR.GetRowNo(i);
                    ProcessClaimTax(Convert.ToInt64(clR.GetFields("claimdetailid")));
                }
            }
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            ClearLock();
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnAddSC_Click(object sender, EventArgs e)
        {
            pnlAddSC.Visible = true;
            pnlSeekSC.Visible = false;
        }

        protected void btnSCCancel_Click(object sender, EventArgs e)
        {
            pnlSeekSC.Visible = true;
            pnlAddSC.Visible = false;
        }

        protected void btnSCSave_Click(object sender, EventArgs e)
        {
            AddServiceCenter();
            rgServiceCenter.Rebind();
            pnlSeekSC.Visible = true;
            pnlAddSC.Visible = false;
        }

        private void AddServiceCenter()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.NewRow();
            GetNextServiceCenterNo();
            clR.SetFields("servicecenterno", hfServiceCenterNo.Value);
            clR.SetFields("servicecentername", txtServiceCenterName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("fax", txtFax.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.AddRow();
            clR.SaveDB();
        }

        private void GetNextServiceCenterNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select max(servicecenterno) as mSCN from servicecenter ";
            SQL = SQL + "where servicecenterno like 'rf0%' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            clR.GetRow();
            hfServiceCenterNo.Value = "RF" + String.Format((Convert.ToInt64(clR.GetFields("mscn").Substring(clR.GetFields("mscn").Length - 6)) + 1).ToString(), "0000000");
        }

        protected void cboClaimStatus_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            tbLock.Visible = false;
            if (hfReadOnly.Value == "True")
                return;

            if (cboClaimStatus.SelectedValue == "Denied")
            {
                if (CheckRequested())
                {
                    tbLock.Visible = true;
                    lblLocked.Text = "You must change all Jobs from requested to denied or authorized.";
                    cboClaimStatus.SelectedValue = "Open";
                    return;
                }
            }

            if (CheckPaidDenied())
            {
                if (!CheckUndeny())
                {
                    tbLock.Visible = true;
                    lblLocked.Text = "Only Jeff, Zach, Darius, or Joe can reactivate a claim";
                    return;
                }
            }

            SQL = "update claim ";
            SQL = SQL + "set status = '" + cboClaimStatus.Text + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (cboClaimStatus.SelectedValue == "Denied")
            { 
                if (!CheckNoteDenied())
                    ShowDenied();
            }

            if (cboClaimStatus.SelectedValue == "Open")
            {
                if (!CheckNoteOpen())
                    if (cboClaimStatus.SelectedValue == "Open")
                        if (cboActivity.SelectedValue != "11" && cboActivity.SelectedValue != "14")
                            if (cboActivity.SelectedValue != "15" && cboActivity.SelectedValue != "17")
                                if (cboActivity.SelectedValue != "18" && cboActivity.SelectedValue != "21")
                                    if (cboActivity.SelectedValue != "22" && cboActivity.SelectedValue != "23")
                                        if (cboActivity.SelectedValue != "24" && cboActivity.SelectedValue != "25")
                                            if (cboActivity.SelectedValue != "26" && cboActivity.SelectedValue != "28")
                                                if (cboActivity.SelectedValue != "29" && cboActivity.SelectedValue != "30")
                                                    if (cboActivity.SelectedValue != "31" && cboActivity.SelectedValue != "32")
                                                        if (cboActivity.SelectedValue != "33" && cboActivity.SelectedValue != "35")
                                                            if (cboActivity.SelectedValue != "36" && cboActivity.SelectedValue != "37")
                                                                if (cboActivity.SelectedValue != "38" && cboActivity.SelectedValue != "39")
                                                                    if (cboActivity.SelectedValue != "40" && cboActivity.SelectedValue != "42")
                                                                        if (cboActivity.SelectedValue != "43" && cboActivity.SelectedValue != "45")
                                                                            if (cboActivity.SelectedValue != "46" && cboActivity.SelectedValue != "47")
                                                                                if (cboActivity.SelectedValue != "48")
                                                                                {
                                                                                    ShowOpen();
                                                                                    return;
                                                                                }
            }

        }

        private bool CheckUndeny()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value + " ";
            SQL = SQL + "and allowundenyclaim <> 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private bool CheckRequested()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailstatus = 'Requested' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;

        }

        private bool CheckPaidDenied()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where (status =  'Paid' ";
            SQL = SQL + "or status = 'Denied') ";
            SQL = SQL + "and claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private bool CheckNoteOpen()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 6 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }

        private bool CheckNoteDenied()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 7 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                return true;

            return false;

        }

        protected void btnAlertOK_Click(object sender, EventArgs e)
        {
            HideAlert();
        }

        protected void txtRONumber_TextChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            if (hfReadOnly.Value == "True")
                return;

            SQL = "update claim ";
            SQL = SQL + "set ronumber = '" + txtRONumber.Text + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            CheckClaimDuplicate();
        }

        protected void rdpRODate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            if (hfReadOnly.Value == "True")
                return;

            SQL = "update claim ";
            SQL = SQL + "set rodate = '" + rdpRODate.SelectedDate + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void txtShopRate_TextChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clCD = new clsDBO();
            if (txtShopRate.Text.Length == 0)
                return;

            SQL = "update claim ";
            SQL = SQL + "set shoprate = " + txtShopRate.Text + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "select * from claimdetail ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimdetailtype = 'Labor' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                for (int i=0; i <= clR.RowCount()-1; i++)
                {
                    clR.GetRowNo(i);
                    SQL = "select * from claimdetail ";
                    SQL = SQL + "where claimdetailid = " + clR.GetFields("claimdetailid");
                    clCD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clCD.RowCount() > 0)
                    {
                        clCD.GetRow();
                        clCD.SetFields("reqcost", txtShopRate.Text);
                        clCD.SetFields("reqamt", (Convert.ToDouble(txtShopRate.Text) * Convert.ToDouble(clR.GetFields("reqqty"))).ToString());
                        clCD.SaveDB();
                    }
                }
            }
        }

        protected void btnSeekLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = true;
            pnlDetail.Visible = false;
            rgLossCode.Rebind();
            ColorLossCode();

        }

        protected void rgLossCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            txtLossCodeClaim.Text = rgLossCode.SelectedValue.ToString();
            SQL = "select * from claimlosscode ";
            SQL = SQL + "where losscode = '" + rgLossCode.SelectedValue + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtLossCodeDescClaim.Text = clR.GetFields("losscodedesc");
            }
            pnlDetail.Visible = true;
            pnlSeekLossCode.Visible = false;
            SQL = "update claim ";
            SQL = SQL + "set losscode = '" + rgLossCode.SelectedValue + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnAddLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = false;
            pnlLossCode.Visible = true;
        }

        protected void btnCancelLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = true;
            pnlLossCode.Visible = false;
        }

        protected void btnSaveLossCode_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimlosscode ";
            SQL = SQL + "where losscode = '" + txtLossCode.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("losscode", txtLossCode.Text);
                clR.SetFields("losscodedesc", txtLossCodeDesc.Text);
                clR.AddRow();
                clR.SaveDB();
            }
            rgLossCode.Rebind();
            pnlSeekLossCode.Visible = true;
            pnlLossCode.Visible = false;
        }

        protected void btnChangeAssignedTo_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlAssignedToChange.Visible = true;
        }

        protected void rgAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            hfAssignedTo.Value = rgAssignedTo.SelectedValue.ToString();
            if (hfReadOnly.Value == "True")
                return;

            GetAssignedTo();
            SQL = "update claim ";
            SQL = SQL + "set assignedto = " + hfAssignedTo.Value + " ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            pnlAssignedToChange.Visible = false;
            pnlDetail.Visible = true;
        }

        protected void txtSCEmail_TextChanged(object sender, EventArgs e)
        {
            if (txtServiceCenter.Text.Length == 0)
                return;

            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                clR.SetFields("email", txtSCEmail.Text);
                clR.SaveDB();
            }
        }

        protected void txtSCFax_TextChanged(object sender, EventArgs e)
        {
            if (txtServiceCenter.Text.Length == 0)
                return;

            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from servicecenter ";
            SQL = SQL + "where servicecenterno = '" + txtServiceCenter.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) 
            { 
                clR.GetRow();
                clR.SetFields("fax", txtSCFax.Text);
                clR.SaveDB();
            }
        } 
        
        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            if (!CheckNote())
            {
                lblError.Text = "You need to enter in Note.";
                ShowError();
                return;
            }
            ClearLock();
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void chkHCC_CheckedChanged(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            if (chkHCC.Checked)
            {
                SQL = "select * from claimhcc ";
                SQL = SQL + "where claimid = " + hfClaimID.Value;
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() == 0)
                {
                    clR.NewRow();
                    clR.SetFields("Adjusterid", hfUserID.Value);
                    clR.SetFields("DateSubmitted", DateTime.Today.ToString());
                    clR.SetFields("claimid", hfClaimID.Value);
                    clR.AddRow();
                    clR.SaveDB();
                }
                tsClaim.Tabs[10].Visible = true;
                SQL = "update claim ";
                SQL = SQL + "set hcc = 1 ";
                SQL = SQL + "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }
            else
            {
                tsClaim.Tabs[10].Visible = false;
                SQL = "update claim ";
                SQL = SQL + "set hcc = 0 ";
                SQL = SQL + "where claimid = " + hfClaimID.Value;
                clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            }
        }

        protected void btnDeniedClose_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select status from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboClaimStatus.SelectedValue = clR.GetFields("status");
            }

            SQL = "update claim ";
            SQL = SQL + "set status = '" + cboClaimStatus.Text + "', ";
            SQL = SQL + "closedate = null, opendate = '" + DateTime.Today + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "insert into claimopenhistory ";
            SQL = SQL + "(claimid, opendate, openby) ";
            SQL = SQL + "values (" + hfClaimID.Value + ",'";
            SQL = SQL + DateTime.Today + "',";
            SQL = SQL + hfUserID.Value + ") ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);

        }

        protected void btnDeniedSave_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnoteid = 7 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimnotetypeid", "7");
            clR.SetFields("note", txtDeniedNote.Text);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("notetext", txtDeniedNote.Text);

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
            SQL = "update claim ";
            SQL = SQL + "set status = '" + cboClaimStatus.Text + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            HideDenied();
        }


        protected void btnOpenClose_Click(object sender, EventArgs e)
        {
            HideOpen();
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select status, ClaimActivityID from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboClaimStatus.SelectedValue = clR.GetFields("status");
                cboActivity.SelectedValue = clR.GetFields("ClaimActivityID");
            }

            SQL = "update claim ";
            SQL = SQL + "set status = '" + cboClaimStatus.Text + "', ";
            SQL = SQL + "closedate = null, opendate = '" + DateTime.Today + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "insert into claimopenhistory ";
            SQL = SQL + "(claimid, opendate, openby) ";
            SQL = SQL + "values (" + hfClaimID.Value + ",'";
            SQL = SQL + DateTime.Today + "',";
            SQL = SQL + hfUserID.Value + ") ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
        }

        protected void btnOpenSave_Click(object sender, EventArgs e)
        {
            clsDBO clR = new clsDBO();
            string SQL;

            if (txtOpenNote.Text.Length < 25)
                return;

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnoteid = 6 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimnotetypeid", "6");
            clR.SetFields("note", txtOpenNote.Text);
            clR.SetFields("credate", DateTime.Now.ToString());
            clR.SetFields("creby", hfUserID.Value);
            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("notetext", txtOpenNote.Text);
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }

            clR.SaveDB();
            hfOpen.Value = "";
            SQL = "update claim ";
            SQL = SQL + "set status = '" + cboClaimStatus.Text + "', ";
            SQL = SQL + "closedate = null, opendate = '" + DateTime.Today + "' ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "insert into claimopenhistory ";
            SQL = SQL + "(claimid, opendate, openby) ";
            SQL = SQL + "values (" + hfClaimID.Value + ",'";
            SQL = SQL + DateTime.Today + "',";
            SQL = SQL + hfUserID.Value + ") ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            HideOpen();
        }

        private void CheckClaimDuplicate()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            tbLock.Visible = false;
            if (hfContractID.Value.Length > 0)
            {
                SQL = "select * from claim ";
                SQL = SQL + "where contractid = " + hfContractID.Value + " ";
                SQL = SQL + "and credate > '" + DateTime.Today.AddDays(-3) + "' ";
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 1)
                {
                    lblLocked.Text = "There is a claim already entered within the last 3 days.";
                    tbLock.Visible = true;
                    return;
                }
            }
            if (hfServiceCenterID.Value.Length > 0)
            {
                if (txtRONumber.Text.Length > 0)
                {
                    SQL = "select * from claim ";
                    SQL = SQL + "where servicecenterid = " + hfServiceCenterID.Value + " ";
                    SQL = SQL + "and ronumber = '" + txtRONumber.Text + "' ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clR.RowCount() > 1)
                    {
                        lblLocked.Text = "There is a claim already have RO Number for this Service Center.";
                        tbLock.Visible = true;
                        return;
                    }
                }
            }
        }

        private void ColorLossCode()
        {
            rgLossCode.Columns[3].Visible = true;
            for (int i = 0; i <= rgLossCode.Items.Count - 1; i++)
            {
                if (rgLossCode.Items[i].Cells[3].Text == "1")
                    rgLossCode.Items[i].Cells[1].BackColor = System.Drawing.Color.Red;
            }
            rgLossCode.Columns[3].Visible = false;
        }

        protected void tsClaim_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (tsClaim.SelectedTab.Value == "Payment")
                pvPayment.Selected = true;
            if (tsClaim.SelectedTab.Value == "Customer")
                pvCustomer.Selected = true;
            if (tsClaim.SelectedTab.Value == "3C")
                pv3C.Selected = true;
            if (tsClaim.SelectedTab.Value == "Documents")
                pvDocuments.Selected = true;
            if (tsClaim.SelectedTab.Value == "Insp")
                pvInspection.Selected = true;
            if (tsClaim.SelectedTab.Value == "Part")
                pvParts.Selected = true;
            if (tsClaim.SelectedTab.Value == "Notes")
                pvNotes.Selected = true;
            if (tsClaim.SelectedTab.Value == "Jobs")
                pvJob.Selected = true;
            if (tsClaim.SelectedTab.Value == "Alert")
                pvAlert.Selected = true;
            if (tsClaim.SelectedTab.Value == "History")
                pvHistory.Selected = true;
            if (tsClaim.SelectedTab.Value == "HCC")
                pvHCC.Selected = true;
            if (tsClaim.SelectedTab.Value == "ContractNote")
                pvContractNote.Selected = true;
            if (tsClaim.SelectedTab.Value == "Surcharges")
                pvSurcharges.Selected = true;
        }
    }
}