﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGAPLoanInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            dsStates.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                FillBank();
            }
        }

        private void FillBank()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgaploaninfo ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                txtFName.Text = clR.GetFields("bankname");
                txtAddr1.Text = clR.GetFields("addr1");
                txtAddr2.Text = clR.GetFields("addr2");
                txtCity.Text = clR.GetFields("city");
                cboState.SelectedValue = clR.GetFields("state");
                txtZip.Text = clR.GetFields("zip");
                txtPhone.Text = clR.GetFields("phone");
                txtEMail.Text = clR.GetFields("email");
                txtAccountNo.Text = clR.GetFields("accountno");
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgaploaninfo ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimgapid", hfClaimID.Value);
                clR.SetFields("creby", hfUserID.Value);
                clR.SetFields("credate", DateTime.Today.ToString());
            }
            else
            {
                clR.GetRow();
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", hfUserID.Value);
            }

            clR.SetFields("bankname", txtFName.Text);
            clR.SetFields("addr1", txtAddr1.Text);
            clR.SetFields("addr2", txtAddr2.Text);
            clR.SetFields("city", txtCity.Text);
            clR.SetFields("state", cboState.SelectedValue);
            clR.SetFields("zip", txtZip.Text);
            clR.SetFields("phone", txtPhone.Text);
            clR.SetFields("email", txtEMail.Text);
            clR.SetFields("accountno", txtAccountNo.Text);

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }
    }
}