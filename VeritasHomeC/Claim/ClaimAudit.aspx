﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimAudit.aspx.cs" Inherits="VeritasHomeC.ClaimAudit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" Font-Size="20" ForeColor="#4F81BD" Width="1500">
                            Claim Quality Review
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadTabStrip ID="RadTabStrip1" runat="server">
                                <Tabs>
                                    <telerik:RadTab Text="Form" Value="Form" Width="120"></telerik:RadTab>
                                    <telerik:RadTab Text="Scoring" Value="Scoring" Width="120"></telerik:RadTab>
                                    <telerik:RadTab Text="Options" Value="Options" Width="120"></telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadMultiPage ID="rmAudit" runat="server">
                                <telerik:RadPageView ID="pvForm" runat="server">RadPageView</telerik:RadPageView>
                                <telerik:RadPageView ID="pvScoring" runat="server">RadPageView</telerik:RadPageView>
                                <telerik:RadPageView ID="pvOptions" runat="server">RadPageView</telerik:RadPageView>
                            </telerik:RadMultiPage>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
