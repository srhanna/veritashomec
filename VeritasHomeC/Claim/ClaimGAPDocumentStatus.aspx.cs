﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGAPDocumentStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                GetInfo();
            }
        }

        private void GetInfo()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimgapdocstatus ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                chk60DayLetter.Checked = Convert.ToBoolean(clR.GetFields("gap60dayletter"));
                chkDenialLetter.Checked = Convert.ToBoolean(clR.GetFields("gapdenielletter"));
                chkGAPClaimNotice.Checked = Convert.ToBoolean(clR.GetFields("gapclaimnotice"));
                chkGAPNoGAPDueLetter.Checked = Convert.ToBoolean(clR.GetFields("gapnogapdueletter"));
                chkMissedOptions.Checked = Convert.ToBoolean(clR.GetFields("gapmissedoptions"));
                chkPaymentHistory.Checked = Convert.ToBoolean(clR.GetFields("paymenthistory"));
                chkPaymentLetter.Checked = Convert.ToBoolean(clR.GetFields("gappaymentletter"));
                chkPoliceFireReport.Checked = Convert.ToBoolean(clR.GetFields("policefirereport"));
                chkProofInsPayments.Checked = Convert.ToBoolean(clR.GetFields("ProofInsPayments"));
                chkRISC.Checked = Convert.ToBoolean(clR.GetFields("risc"));
                chkStatusLetter.Checked = Convert.ToBoolean(clR.GetFields("gapstatusletter"));
                chkTotalLossInsSettlementLetter.Checked = Convert.ToBoolean(clR.GetFields("TotalLossSettlement"));
                chkTotalLossInsValuation.Checked = Convert.ToBoolean(clR.GetFields("TotalLossValuation"));
            }
            else
            {
                chk60DayLetter.Checked = false;
                chkDenialLetter.Checked = false;
                chkGAPClaimNotice.Checked = false;
                chkGAPNoGAPDueLetter.Checked = false;
                chkMissedOptions.Checked = false;
                chkPaymentHistory.Checked = false;
                chkPaymentLetter.Checked = false;
                chkPoliceFireReport.Checked = false;
                chkProofInsPayments.Checked = false;
                chkRISC.Checked = false;
                chkStatusLetter.Checked = false;
                chkTotalLossInsSettlementLetter.Checked = false;
                chkTotalLossInsValuation.Checked = false;
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgapdocstatus ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
            }

            clR.SetFields("risc", chkRISC.Checked.ToString());
            clR.SetFields("paymenthistory", chkPaymentHistory.Checked.ToString());
            clR.SetFields("totallossvaluation", chkTotalLossInsValuation.Checked.ToString());
            clR.SetFields("totallosssettlement", chkTotalLossInsSettlementLetter.Checked.ToString());
            clR.SetFields("policefirereport", chkPoliceFireReport.Checked.ToString());
            clR.SetFields("proofinspayments", chkProofInsPayments.Checked.ToString());
            clR.SetFields("gapclaimnotice", chkGAPClaimNotice.Checked.ToString());
            clR.SetFields("gap60dayletter", chk60DayLetter.Checked.ToString());
            clR.SetFields("gapdenielletter", chkDenialLetter.Checked.ToString());
            clR.SetFields("gapmissedoptions", chkMissedOptions.Checked.ToString());
            clR.SetFields("gapnogapdueletter", chkGAPNoGAPDueLetter.Checked.ToString());
            clR.SetFields("gappaymentletter", chkPaymentLetter.Checked.ToString());
            clR.SetFields("gapstatusletter", chkStatusLetter.Checked.ToString());
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        protected void btnDenialLetter_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.CreateDenialLetter();
        }

        protected void btnGAPNoGAPDueLetter_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.CreateGAPNoGAPDueLetter();
        }

        protected void btnGenerate60DayLetter_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.CreateGAP60DayLetter();
        }

        protected void btnGenerateClaimNotice_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.CreateGAPClaimNotice();
        }

        protected void btnMissedOptions_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.CreateMissedOptions();
        }

        protected void btnPaymentLetter_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.CreatePaymentLetter();
        }

        protected void btnStatusLetter_Click(object sender, EventArgs e)
        {
            VeritasGlobalToolsV2.clsGAPDocuments clD = new VeritasGlobalToolsV2.clsGAPDocuments();
            clD.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clD.UserID = Convert.ToInt64(hfUserID.Value);
            clD.DirLoc = HttpContext.Current.Server.MapPath("~");
            clD.CreateStatusLetter();
        }
    }
}