﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimHistory.aspx.cs" Inherits="VeritasHomeC.ClaimHistory" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell Font-Bold="true">
                            Total Claims Paid:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtTotalClaimsPaid" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
               <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="rgHistory"></telerik:AjaxUpdatedControl>
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                   </telerik:RadAjaxManager>

                    <telerik:RadGrid RenderMode="Lightweight" ID="rgHistory" OnSelectedIndexChanged="rgHistory_SelectedIndexChanged"
                        DataSourceID="dsHistory" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" AllowMultiRowSelection="False" Width="1000">
                        <PagerStyle Mode="NumericPages"></PagerStyle>
                        <MasterTableView EnableHierarchyExpandAll="true" DataSourceID="dsHistory" DataKeyNames="ClaimID" AllowMultiColumnSorting="True">
                            <DetailTables>
                                <telerik:GridTableView EnableHierarchyExpandAll="true" DataKeyNames="ClaimDetailID" DataSourceID="dsHistory2"
                                    runat="server">
                                    <ParentTableRelation>
                                        <telerik:GridRelationFields DetailKeyField="ClaimID" MasterKeyField="ClaimID"></telerik:GridRelationFields>
                                    </ParentTableRelation>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ClaimDetailStatus" UniqueName="ClaimDetailStatus" HeaderText="Status"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="LossCode" UniqueName="LossCode" HeaderText="Loss Code"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ClaimDesc" UniqueName="PartDesc" HeaderText="Part Desc"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ReqAmt" UniqueName="ReqAmt" HeaderText="Req Amt" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AuthAmt" UniqueName="AuthAmt" HeaderText="Auth Amt" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="Paid Amt" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                    </Columns>
                                    <SortExpressions>
                                        <telerik:GridSortExpression FieldName="LossCode"></telerik:GridSortExpression>
                                    </SortExpressions>
                                </telerik:GridTableView>
                            </DetailTables>
                            <Columns>
                                <telerik:GridBoundColumn DataField="ContractID" ReadOnly="true" Visible="false" UniqueName="ContractID"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Status"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LossDate" UniqueName="LossDate" HeaderText="Loss Date"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LossMile" UniqueName="LossMile" HeaderText="Loss Mile"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="RONumber" UniqueName="RONumber" HeaderText="RO Number"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Amt" UniqueName="Amt" HeaderText="Paid Amt" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                            </Columns>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="claimno"></telerik:GridSortExpression>
                            </SortExpressions>
                        </MasterTableView>
                    </telerik:RadGrid>

                    <asp:SqlDataSource ID="dsHistory" ProviderName="System.Data.SqlClient" 
                            SelectCommand="select contractid, cl.claimid, claimno, status, LossDate, lossmile, ronumber, sum(paidAmt) as amt from claim cl
                            inner join claimdetail cd on cd.claimid = cl.claimid  
                            where contractid = @ContractID 
                            group by contractid, cl.claimid, claimno, status, LossDate, lossmile, ronumber 
                            order by LossDate desc "
                            runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfContractID" Name="ContractID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                    </asp:SqlDataSource>

                    <asp:SqlDataSource ID="dsHistory2" ProviderName="System.Data.SqlClient"
                        SelectCommand="select ClaimDetailID, claimdesc, JobNo, ClaimDetailStatus, LossCode, 
                        ReqAmt, authamt, PaidAmt  from claimdetail 
                        where claimid = @ClaimID order by jobno desc" 
                        runat="server">
                        <SelectParameters>
                            <asp:SessionParameter SessionField="ClaimID" Name="ClaimID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgClaim">
                    <telerik:RadGrid ID="rgClaimOpenHistory" DataSourceID="dsClaimOpenHistory" runat="server" AutoGenerateColumns="false" AllowSorting="true" Width="1000">
                        <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                            CommandItemDisplay="TopAndBottom">
                            <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="ClaimID" UniqueName="ClaimID" Visible="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OpenDate" UniqueName="OpenDate" HeaderText="Open Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OpenUserName" UniqueName="OpenUserName" HeaderText="Open User Name"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CloseDate" UniqueName="CloseDate" HeaderText="Close Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CloseUserName" UniqueName="CloseUserName" HeaderText="Close User Name"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>

                    <asp:SqlDataSource ID="dsClaimOpenHistory" ProviderName="System.Data.SqlClient"
                        SelectCommand="select OpenDate, uio.UserName as OpenUserName, CloseDate, uic.UserName as CloseUserName from ClaimOpenHistory coh
                        left join userinfo uio on uio.UserID = coh.OpenBy
                        left join userinfo uic on uic.UserID = coh.CloseBy
                        where claimid = @claimid
                        order by OpenDate desc" 
                        runat="server">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                    </asp:SqlDataSource>



                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
