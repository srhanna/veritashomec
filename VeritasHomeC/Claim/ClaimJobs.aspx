﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimJobs.aspx.cs" Inherits="VeritasHomeC.ClaimJobs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlList" DefaultButton="btnHiddenList">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell ID="tcManagerAuth">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddDetail" OnClick="btnAddDetail_Click" runat="server" Text="Add Detail" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Job No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboJobNo" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAuthIt" OnClick="btnAuthIt_Click" runat="server" Text="Authorize" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnApproveIt" OnClick="btnApproveIt_Click" runat="server" Text="Approve" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnDenyIt" OnClick="btnDenyIt_Click" runat="server" Text="Deny" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ID="tcManageSlush">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Job No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboSlushJobNo" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Slush:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboSlushRateType" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnUpdateSlush" OnClick="btnUpdateSlush_Click" runat="server" Text="Update Slush" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblAuthorizedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblApprovedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                    <telerik:RadGrid ID="rgJobs" OnSelectedIndexChanged="rgJobs_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" DataSourceID="dsJobs" 
                                        AllowSorting="true" Width="1000" ShowFooter="true">
                                        <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimDetailID" ShowFooter="true">
                                            <GroupByExpressions>
                                                <telerik:GridGroupByExpression>
                                                    <SelectFields>
                                                        <telerik:GridGroupByField FieldAlias="JobNo" FieldName="JobNo" HeaderText="Job No" HeaderValueSeparator=": " />
                                                        <telerik:GridGroupByField FieldAlias="LossCodDesc" FieldName="LossCodeDesc" HeaderValueSeparator=": " />
                                                    </SelectFields>
                                                    <GroupByFields>
                                                        <telerik:GridGroupByField FieldName="JobNo" SortOrder="Ascending" />
                                                        <telerik:GridGroupByField FieldName="LossCodeDesc" SortOrder="Ascending" />
                                                    </GroupByFields>
                                                </telerik:GridGroupByExpression>
                                            </GroupByExpressions>
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimPartID"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ClaimDetailType" UniqueName="ClaimDetailType" HeaderText="Detail Type"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ClaimDetailStatus" UniqueName="ClaimDetailStatus" HeaderText="Status"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ClaimDesc" UniqueName="ClaimDesc" HeaderText="Claim Desc"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="LossCode" UniqueName="LossCode" HeaderText="Loss Code"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ReqAmt" UniqueName="ReqAmt" HeaderText="Req Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="AuthAmt" UniqueName="AuthAmt" HeaderText="Auth Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TaxAmt" UniqueName="TaxAmt" HeaderText="Tax Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TotalAmt" UniqueName="TotalAmt" HeaderText="Total Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="PaidAmt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsJobs" ProviderName="System.Data.SqlClient" 
                                    SelectCommand="select claimdetailid, claimdetailtype,cd.partno, cd.claimdesc, claimdetailstatus, jobno, cd.losscode, lc.losscodedesc, payeename, reqamt, authamt, taxamt, paidamt, totalamt
                                    from claimdetail cd left join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid 
                                    left join claimlosscode lc on lc.losscode = cd.losscode where claimid =  @ClaimID order by jobno" 
                                    runat="server">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenList" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Totals:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPayeeTotal" OnSelectedIndexChanged="cboPayeeTotal_SelectedIndexChanged" Width="250" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Jobs:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboJobsTotal" OnSelectedIndexChanged="cboJobsTotal_SelectedIndexChanged" Width="250" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                    <telerik:RadGrid ID="rgClaimTotal" runat="server" AutoGenerateColumns="false" 
                                        AllowSorting="true" Width="1000" ShowFooter="true">
                                        <MasterTableView AutoGenerateColumns="false" ShowFooter="true">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="ClaimProcess" UniqueName="ClaimProcess" HeaderText="Process"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Part" UniqueName="Part" HeaderText="Part" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Labor" UniqueName="Labor" HeaderText="Labor" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Other" UniqueName="Other" HeaderText="Other" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="SubTotal" UniqueName="SubTotal" HeaderText="Sub Total" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TaxAmt" UniqueName="TaxAmt" HeaderText="Tax Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Deduct" UniqueName="Deduct" HeaderText="Deduct" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Total" UniqueName="Total" HeaderText="Total" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlDetail" DefaultButton="btnHiddenDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell runat="server">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Detail Type:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboClaimDetailType" OnSelectedIndexChanged="cboClaimDetailType_SelectedIndexChanged" DataSourceID="dsCDT" AutoPostBack="true" DataTextField="ClaimDetailType" DataValueField="ClaimDetailType" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsCDT"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select ClaimDetailType from claimdetail group by ClaimDetailType order by ClaimDetailType" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Job No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtJobNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Detail Status:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboClaimDetailStatus" OnSelectedIndexChanged="cboClaimDetailStatus_SelectedIndexChanged" DataValueField="ClaimDetailStatus" AutoPostBack="true" DataSourceID="dsClaimDetailStatus" DataTextField="ClaimDetailStatusDesc" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsClaimDetailStatus" ProviderName="System.Data.SqlClient" SelectCommand="select ClaimDetailStatus, ClaimDetailStatusDesc from ClaimDetailStatus" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Reason Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboReason" DataValueField="ClaimReasonID" DataSourceID="dsClaimReason" DataTextField="ReasonDesc" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsClaimReason" ProviderName="System.Data.SqlClient" SelectCommand="select ClaimReasonID, ReasonDesc from ClaimReason order by reasondesc" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Part No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPartNo" Enabled="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimDesc" Enabled="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCode" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekLossCode" OnClick="btnSeekLossCode_Click" runat="server" Text="Seek Loss Code" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCodeDesc" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeName" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekPayee" OnClick="btnSeekPayee_Click" runat="server" Text="Seek Payee" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Req Qty:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqQty" OnTextChanged="txtReqQty_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Req Cost:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqCost" OnTextChanged="txtReqCost_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Req Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Auth Qty:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthQty" OnTextChanged="txtAuthQty_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Auth Cost:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthCost" OnTextChanged="txtAuthCost_TextChanged" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Tax Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtTaxRate" OnTextChanged="txtTaxRate_TextChanged" AutoPostBack="true" Type="Percent" NumberFormat-DecimalDigits="4" runat="server" ></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Tax Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtTaxAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Total Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtTotalAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Authorized Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Paid Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtPaidAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Authorized Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpAuthorizedDate" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Approved Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpDateApprove" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Paid Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpDatePaid" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Account:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboRateType" runat="server"></telerik:RadComboBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenDetail" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnMakePayment" OnClick="btnMakePayment_Click" CssClass="button1" runat="server" Text="Make Payment" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCancelClaimDetail" OnClick="btnCancelClaimDetail_Click" CssClass="button1" runat="server" Text="Cancel" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSaveClaimDetail" OnClick="btnSaveClaimDetail_Click" CssClass="button1" runat="server" Text="Save" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlLossCode" DefaultButton="btnHiddenLC">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgLossCode" OnSelectedIndexChanged="rgLossCode_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                    AllowSorting="true"  Width="1000" ShowFooter="true" DataSourceID="dsLossCode">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="LossCode" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="LossCodeID"  ReadOnly="true" Visible="false" UniqueName="LossCodeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCode" FilterCheckListWebServiceMethod="LoadLossCode" UniqueName="LossCode" HeaderText="Loss Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCodeDesc" FilterCheckListWebServiceMethod="LoadLossCodeDesc" UniqueName="LossCodeDesc" HeaderText="Loss Code Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ACP" UniqueName="ACP" HeaderText="ACP"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsLossCode" ProviderName="System.Data.SqlClient" runat="server" 
                                    SelectCommand="(select clc.*, 0 as ACP from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid
                                    union
                                    select clc.*, 0 as ACP from ClaimLossCode ClC 
                                    where LossCode in (select csc.losscode from ContractSurchargecoverage csc
                                    inner join ContractSurcharge cs on csc.surchargeid = cs.surchargeid
                                    inner join contract c on c.contractid = cs.ContractID
                                    inner join claim cl on cl.contractid = c.contractid
                                    where cl.claimid = @claimid)
									union 
									select clc.*, 1 as ACP from ClaimLossCode ClC left join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    where not LossCodePrefix in(
									select LossCodePrefix from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid)
                                    and not LossCode in (select csc.losscode from ContractSurchargecoverage csc
                                    inner join ContractSurcharge cs on csc.surchargeid = cs.surchargeid
                                    inner join contract c on c.contractid = cs.ContractID
                                    inner join claim cl on cl.contractid = c.contractid
                                    where cl.claimid = @claimid)
									) order by losscode">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenLC" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSeekPayee" DefaultButton="btnHiddenCP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAddClaimPayee" OnClick="btnAddClaimPayee_Click" runat="server" Text="Add Claim Payee" BackColor="#1eabe2" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimPayee" OnSelectedIndexChanged="rgClaimPayee_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" DataSourceID="dsClaimPayee"
                                    AllowSorting="true" Width="1000" ShowFooter="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ClaimPayeeID" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimPayeeID"  ReadOnly="true" Visible="false" UniqueName="ClaimPayeeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeNo" FilterCheckListWebServiceMethod="LoadPayeeNo" UniqueName="PayeeNo" HeaderText="Payee No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName" FilterCheckListWebServiceMethod="LoadPayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="DealerCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsClaimPayee"
                                ProviderName="System.Data.SqlClient" SelectCommand="select claimpayeeid, payeeno, payeename, city, state from claimpayee" runat="server"></asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenCP" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel ID="pnlAddClaimPayee" runat="server" DefaultButton="btnHiddenACP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Service Center Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtServiceCenterName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 1:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 2:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            City:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            State:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsStates"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Zip Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Phone:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenACP" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSCCancel" OnClick="btnSCCancel_Click" runat="server" Text="Cancel" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSCSave" OnClick="btnSCSave_Click" runat="server" Text="Save" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlToBePaidDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayAmt" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPaymentMethod" OnTextChanged="cboPaymentMethod_TextChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow ID="trACH">
                                        <asp:TableCell Font-Bold="true">
                                            Confirmation No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtConfirmNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessACH" OnClick="btnProcessACH_Click" runat="server" Text="Process ACH" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trCheck">
                                        <asp:TableCell Font-Bold="true">
                                            Check No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCheckNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessCheck" OnClick="btnProcessCheck_Click" runat="server" Text="Process Check" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trCC">
                                        <asp:TableCell Font-Bold="true">
                                            Credit Card No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCCNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessCC" OnClick="btnProcessCC_Click" runat="server" Text="Process Credit Card" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trWex">
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDropDownList ID="cboWexMethod" DataSourceID="dsWexMethod" DataTextField="wexmethod" DataValueField="wexmethod" runat="server"></telerik:RadDropDownList>
                                            <asp:SqlDataSource ID="dsWexMethod" ProviderName="System.Data.SqlClient"
                                                 SelectCommand="select wexmethod from wexmethod" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Addess:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexAddress" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessWex" OnClick="btnProcessWex_Click" runat="server" Text="Process Wex" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCloseToBePaidDetail" runat="server" Text="Close"  CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:HiddenField ID="hfSlushNote" runat="server" />
                <telerik:RadWindow ID="rwSlushNote"  runat="server" Width="500" Title="Note" Height="300" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label runat="server" ID="Label1" Text="You need to enter in Note."></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtSlushNote" TextMode="MultiLine" Width="450" Height="175" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnCloseSlushNote" OnClick="btnCloseSlushNote_Click" runat="server" CssClass="button2" Width="75" Text="Cancel" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnSaveSlushNote" OnClick="btnSaveSlushNote_Click" runat="server" CssClass="button2" Width="75" Text="Save" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>

                <asp:HiddenField ID="hfFieldCheck" runat="server" />
                <telerik:RadWindow ID="rwFieldCheck"  runat="server" Width="500" Title="Note" Height="300" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtFieldChk" TextMode="MultiLine" Width="450" Height="175" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" CssClass="button2" Width="75" Text="Close" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>


                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfClaimDetailID" runat="server" />
                <asp:HiddenField ID="hfClaimPayeeID" runat="server" />
                <asp:HiddenField ID="hfJobNo" runat="server" />
                <asp:HiddenField ID="hfServiceCenterNo" runat="server" />
                <asp:HiddenField ID="hfClaimPaymentID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfWexCCno" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfWexCode" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCompanyNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfClaimNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRONo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfContractNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCustomerName" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfVIN" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfPayeeContact" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfContractID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfTicket" runat="server"></asp:HiddenField>

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
