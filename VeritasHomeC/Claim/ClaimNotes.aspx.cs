﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class ClaimNotes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNoteType.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            dsNote.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlDetail.Visible = false;
                pnlList.Visible = true;
                rgNote.Rebind();
                FillNoteHeader();
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                {
                    btnAddNote.Enabled = false;
                    btnSave.Enabled = false;
                }
            }
        }

        private void FillNoteHeader()
        {
            cboHeader.Items.Add("None");
            cboHeader.Items.Add("Statement");
            cboHeader.Items.Add("Parts Sourcing / Ordered");
            cboHeader.Items.Add("TSB");
            cboHeader.Items.Add("Contract Verification");
            cboHeader.Items.Add("Approval");
            cboHeader.Items.Add("Service Center Communication ");
            cboHeader.Items.Add("Management");
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillList()
        {
            clsDBO clR = new clsDBO();
            string SQL;
            SQL = "select claimnoteid, ClaimNoteTypeDesc, notetext, cn.CreDate, username from claimnote cn ";
            SQL = SQL + "left join userinfo ui on ui.userid = cn.creby ";
            SQL = SQL + "left join claimnotetype nt on nt.claimnotetypeid = cn.claimnotetypeid ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and (cn.claimnotetypeid = 4 or cn.claimnotetypeid = 5 or cn.claimnotetypeid = 6 or cn.claimnotetypeid = 7 ) ";
            SQL = SQL + "order by credate desc ";
            rgNote.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgNote.DataBind();
        }

        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            pnlList.Visible = false;
            pnlDetail.Visible = true;
            cboNoteType.ClearSelection();
            cboHeader.Text = "None";
            txtNote.Content = "";
            hfClaimNoteID.Value = "0";
            btnSave.Visible = true;
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            hfClaimNoteID.Value = rgNote.SelectedValue.ToString();
            SQL = "select * from claimnote cn ";
            SQL = SQL + "inner join userinfo ui on ui.userid = cn.creby ";
            SQL = SQL + "where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                cboNoteType.SelectedValue = clR.GetFields("claimnotetypeid");
                txtNote.Content = clR.GetFields("note");
                txtCreBy.Text = clR.GetFields("username");
                txtCreDate.Text = clR.GetFields("credate");
                btnSave.Visible = true;
            }

            if (hfUserID.Value != clR.GetFields("creby"))
                btnSave.Visible = false;

            if (clsFunc.DateDiffHours(Convert.ToDateTime(txtCreDate.Text), DateTime.Now) > 24)
                btnSave.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.SelectedIndexes.Clear();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            string nl = System.Environment.NewLine;

            SQL = "select * from claimnote cn ";
            SQL = SQL + "where claimnoteid = " + hfClaimNoteID.Value + " ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
                clR.NewRow();

            clR.SetFields("claimid", hfClaimID.Value);
            clR.SetFields("claimnotetypeid", cboNoteType.SelectedValue);
            string sTemp = txtNote.Text;

            if (cboHeader.Text == "None")
            {
                clR.SetFields("note", txtNote.Content);
                clR.SetFields("notetext", txtNote.Text);
            }
            else 
            {
                clR.SetFields("note", "***** " + cboHeader.Text + " *****" + nl + nl + txtNote.Content);
                clR.SetFields("notetext", "***** " + cboHeader.Text + " *****" + nl + nl + txtNote.Text);
            }

            clR.SetFields("moddate", DateTime.Now.ToString());
            clR.SetFields("modby", hfUserID.Value);
            if (clR.RowCount() == 0) 
            {
                clR.SetFields("credate", DateTime.Now.ToString());
                clR.SetFields("creby", hfUserID.Value);
                clR.AddRow();
            }

            clR.SaveDB();
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.Rebind();
        }

        protected void rgNote_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                rgNote.ExportSettings.ExportOnlyData = false;
                rgNote.ExportSettings.IgnorePaging = true;
                rgNote.ExportSettings.OpenInNewWindow = true;
                rgNote.ExportSettings.UseItemStyles = true;
                rgNote.ExportSettings.Excel.FileExtension = "xlsx";
                rgNote.ExportSettings.FileName = "Note";
                rgNote.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgNote.MasterTableView.ExportToExcel();
            }
        }

        private string GetClaimNo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("claimno");
            }

            return "";
        }
    }
}