﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceCenters.aspx.cs" Inherits="VeritasHomeC.ServiceCenters" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
     <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Service Centers</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsOpen" OnClick="btnClaimsOpen_Click" runat="server" Text="My Open Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimTeamOpen" OnClick="btnClaimTeamOpen_Click" runat="server" Text="Claims Team Open"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketMessage" OnClick="btnTicketMessage_Click" runat="server" Text="Ticket Message"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketResponse" OnClick="btnTicketResponse_Click" runat="server" Text="Ticket Response"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" OnClick="btnUnlockClaim_Click" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimAudit" OnClick="btnClaimAudit_Click" runat="server" Text="Claim Audit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnRFClaimSubmit" OnClick="btnRFClaimSubmit_Click" runat="server" Text="RF Claim Submit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" OnClick="btnLossCode_Click" runat="server" Text="Loss Code"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAutonationACH" OnClick="btnAutonationACH_Click" runat="server" Text="AutoNation ACH"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click"  runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:UpdatePanel runat="server" ID="pnlGrid">
                                            <ContentTemplate>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Table runat="server">
                                                                <asp:TableRow>
                                                                    <asp:TableCell>
                                                                        <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add Service Center" CssClass="button1" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:Button ID="btnANAdd" OnClick="btnANAdd_Click" runat="server" Text="Add AutoNation Service Center" CssClass="button1" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:Button ID="btnPrimeAdd" OnClick="btnPrimeAdd_Click" runat="server" Text="Add Prime Service Center" CssClass="button1" />
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <telerik:RadGrid ID="rgServiceCenter" OnSelectedIndexChanged="rgServiceCenter_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                                                AllowSorting="true" AllowPaging="true"  Width="1000" ShowFooter="true" DataSourceID="SQLDataSource1">
                                                                <GroupingSettings CaseSensitive="false" />
                                                                <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ServiceCenterID" PageSize="10" ShowFooter="true">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn DataField="ServiceCenterID"  ReadOnly="true" Visible="false" UniqueName="DealerID"></telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ServiceCenterNo" FilterCheckListWebServiceMethod="LoadServiceCenterNo" UniqueName="ServiceCenterNo" HeaderText="Service Center No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="ServiceCenterName" FilterCheckListWebServiceMethod="LoadServiceCenterName" UniqueName="ServiceCenterName" HeaderText="ServiceCenter Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadServiceCenterCity" UniqueName="ServiceCenterCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                        <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadServiceCenterState" UniqueName="ServiceCenterState" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                                    <Selecting AllowRowSelect="true" />
                                                                </ClientSettings>
                                                            </telerik:RadGrid>
                                            
                                                            <asp:SqlDataSource ID="SqlDataSource1"
                                                            ProviderName="System.Data.SqlClient" SelectCommand="select Servicecenterid, ServiceCenterName, ServiceCenterNo, City, State from ServiceCenter" runat="server"></asp:SqlDataSource>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:UpdatePanel runat="server" ID="pnlSC" Visible="false">
                                            <ContentTemplate>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Label ID="lblSCError" ForeColor="Red" Width="400" BorderColor="Red" BorderStyle="Solid" BorderWidth="1" runat="server" Text="Label"></asp:Label>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Service Center No: *
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtServiceCenterNo" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Service Center Name: *
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtServiceCenterName" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Seller No:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtDealerNo" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Address 1: *
                                                        </asp:TableCell>
                                                        <asp:TableCell ColumnSpan="2">
                                                            <asp:TextBox ID="txtAddr1" Width="200" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Address 2:
                                                        </asp:TableCell>
                                                        <asp:TableCell ColumnSpan="2">
                                                            <asp:TextBox ID="txtAddr2" Width="200" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            City: *
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            State: *
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                                            <asp:SqlDataSource ID="dsStates"
                                                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Zip: *
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            Phone: *
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Fax:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <telerik:RadMaskedTextBox ID="txtFax" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell Font-Bold="true">
                                                            E-Mail:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtEMail" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                        <asp:TableCell Font-Bold="true">
                                                            Contact:
                                                        </asp:TableCell>
                                                        <asp:TableCell>
                                                            <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                                <asp:Table runat="server" Width="900">
                                                    <asp:TableRow HorizontalAlign="Right">
                                                        <asp:TableCell>
                                                            <asp:Table runat="server">
                                                                <asp:TableRow>
                                                                    <asp:TableCell>
                                                                        <asp:Button ID="btnCancel" OnClick="btnCancel_Click" runat="server" Text="Cancel" CssClass="button1" />
                                                                    </asp:TableCell>
                                                                    <asp:TableCell>
                                                                        <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="Save" CssClass="button2" />
                                                                    </asp:TableCell>
                                                                </asp:TableRow>
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfServiceCenterID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
