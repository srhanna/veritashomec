﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimPart.aspx.cs" Inherits="VeritasHomeC.ClaimPart" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlPartList" runat="server" DefaultButton="btnHiddenPL">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddPart" OnClick="btnAddPart_Click" runat="server" Text="Add Part" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="lblAuthorizedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="lblApprovedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgParts" OnSelectedIndexChanged="rgParts_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" 
                                                AllowSorting="true"  Width="600" ShowFooter="true" DataSourceID="dsParts">
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimDetailID" ShowFooter="true">
                                                    <GroupByExpressions>
                                                        <telerik:GridGroupByExpression>
                                                            <SelectFields>
                                                                <telerik:GridGroupByField FieldAlias="JobNo" FieldName="JobNo" HeaderText="Job No" HeaderValueSeparator=": " />
                                                                <telerik:GridGroupByField FieldAlias="LossCodeDesc" FieldName="LossCodeDesc" HeaderText="Loss Code Desc" HeaderValueSeparator=": " />
                                                            </SelectFields>
                                                            <GroupByFields>
                                                                <telerik:GridGroupByField FieldName="JobNo" SortOrder="Ascending" />
                                                                <telerik:GridGroupByField FieldName="LossCodeDesc" SortOrder="Ascending" />
                                                            </GroupByFields>
                                                        </telerik:GridGroupByExpression>
                                                    </GroupByExpressions>
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimDetailID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimDesc" FilterCheckListWebServiceMethod="PartDesc" UniqueName="PartDesc" HeaderText="Part Desc"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AuthQty" UniqueName="AuthQty" HeaderText="Qty"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AuthCost" UniqueName="AuthCost" HeaderText="Cost" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsParts" ProviderName="System.Data.SqlClient" 
                                                SelectCommand="select claimdetailid, jobno, partno, losscodedesc, claimdesc, authqty, authcost, payeename from claimdetail cp left join claimpayee cp2 on cp2.claimpayeeid = cp.claimpayeeid 
                                                left join claimlosscode lc on lc.losscode = cp.losscode
                                                where claimid =  @ClaimID and claimdetailtype = 'Part' " 
                                                runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddLabor" OnClick="btnAddLabor_Click" runat="server" Text="Add Labor" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadGrid ID="rgLabor" OnSelectedIndexChanged="rgLabor_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" 
                                                AllowSorting="true"  Width="600" ShowFooter="true" DataSourceID="dsLabor">
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimDetailID" ShowFooter="true">
                                                    <GroupByExpressions>
                                                        <telerik:GridGroupByExpression>
                                                            <SelectFields>
                                                                <telerik:GridGroupByField FieldAlias="JobNo" FieldName="JobNo" HeaderText="Job No" HeaderValueSeparator=": " />
                                                                <telerik:GridGroupByField FieldAlias="LossCodeDesc" FieldName="LossCodeDesc" HeaderText="Loss Code Desc" HeaderValueSeparator=": " />
                                                            </SelectFields>
                                                            <GroupByFields>
                                                                <telerik:GridGroupByField FieldName="JobNo" SortOrder="Ascending" />
                                                                <telerik:GridGroupByField FieldName="LossCodeDesc" SortOrder="Ascending" />
                                                            </GroupByFields>
                                                        </telerik:GridGroupByExpression>
                                                    </GroupByExpressions>
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimLaborID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AuthQty" UniqueName="AuthQty" HeaderText="Labor Hours"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AuthCost" UniqueName="AuthCost" HeaderText="Labor Rate" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="AuthAmt" UniqueName="LaborTotal" HeaderText="Labor Total" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="dsLabor" ProviderName="System.Data.SqlClient" 
                                                SelectCommand="select claimDetailid, jobno, losscodedesc, authqty, authcost, authamt, cp2.payeename from claimdetail cp inner join claimpayee cp2 on cp2.claimpayeeid = cp.claimpayeeid 
                                                left join claimlosscode lc on lc.losscode = cp.losscode
                                                where claimid =  @ClaimID and claimdetailtype = 'Labor' " 
                                                runat="server">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenPL" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlPartDetail" runat="server" DefaultButton="btnHiddenPart">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Part No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPartNo" OnTextChanged="txtPartNo_TextChanged" AutoPostBack="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Part Description:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPartDesc" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCodePart" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekLossCodePart" OnClick="btnSeekLossCodePart_Click" runat="server" Text="Seek" CssClass="button1"  />
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCodePartDesc" Width="400" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Req Qty:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqQty" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Req Cost:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqCost" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Auth Qty:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthQty" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Auth Cost:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthCost" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Tax Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtPartTax" Type="Percent" NumberFormat-DecimalDigits="4" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Shipping:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtShipping" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayee" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekPayee" OnClick="btnSeekPayee_Click" runat="server" Text="Seek" CssClass="button1"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trPartLabor">
                                        <asp:TableCell Font-Bold="true">
                                            Labor Req Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLaborReqAmtPart" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Req Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLaborReqRatePart" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Auth Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLaborAuthAmtPart" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Auth Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLaborAuthRatePart" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenPart" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCancelPart" OnClick="btnCancelPart_Click" runat="server" Text="Cancel" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnUpdatePart" OnClick="btnUpdatePart_Click" runat="server" Text="Update Part" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSendQuote" OnClick="btnSendQuote_Click" Visible="false" runat="server" Text="Send Part Quote" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlSearchClaimPayee" runat="server" DefaultButton="btnHiddenSCP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAddClaimPayee" OnClick="btnAddClaimPayee_Click" runat="server" Text="Add Claim Payee" CssClass="button1" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimPayee" OnSelectedIndexChanged="rgClaimPayee_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                    AllowSorting="true" AllowPaging="true" Width="1000" ShowFooter="true" DataSourceID="dsClaimPayee">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ClaimPayeeID" PageSize="20" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimPayeeID"  ReadOnly="true" Visible="false" UniqueName="ClaimPayeeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeNo" FilterCheckListWebServiceMethod="LoadPayeeNo" UniqueName="PayeeNo" HeaderText="Payee No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName" FilterCheckListWebServiceMethod="LoadPayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="City" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Zip" FilterCheckListWebServiceMethod="LoadZip" UniqueName="Zip" HeaderText="Zip" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsClaimPayee"
                                ProviderName="System.Data.SqlClient" SelectCommand="select claimpayeeid, payeeno, payeename, city, state, zip from claimpayee" runat="server"></asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenSCP" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlSeekLossCode">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAddLossCode" runat="server" Text="Add Loss Code" CssClass="button1" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgLossCode" OnSelectedIndexChanged="rgLossCode_SelectedIndexChanged" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" DataSourceID="dsLossCode"
                                    AllowSorting="true" Width="1000" ShowFooter="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="LossCode" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="losscodeid" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCode" FilterCheckListWebServiceMethod="LoadLossCode" UniqueName="LossCode" HeaderText="Loss Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCodeDesc" FilterCheckListWebServiceMethod="LoadLossCodeDesc" UniqueName="LossCodeDesc" HeaderText="Loss Code Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ACP" UniqueName="ACP" HeaderText="ACP"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsLossCode" ProviderName="System.Data.SqlClient" runat="server"
                                    SelectCommand="(select clc.*, 0 as ACP from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid
                                    union
                                    select clc.*, 0 as ACP from ClaimLossCode ClC 
                                    where LossCode in (select csc.losscode from ContractSurchargecoverage csc
                                    inner join ContractSurcharge cs on csc.surchargeid = cs.surchargeid
                                    inner join contract c on c.contractid = cs.ContractID
                                    inner join claim cl on cl.contractid = c.contractid
                                    where cl.claimid = @claimid)
									union 
									select clc.*, 1 as ACP from ClaimLossCode ClC left join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    where not LossCodePrefix in(
									select LossCodePrefix from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid)
                                    and not LossCode in (select csc.losscode from ContractSurchargecoverage csc
                                    inner join ContractSurcharge cs on csc.surchargeid = cs.surchargeid
                                    inner join contract c on c.contractid = cs.ContractID
                                    inner join claim cl on cl.contractid = c.contractid
                                    where cl.claimid = @claimid)
									) order by losscode">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel ID="pnlAddClaimPayee" runat="server" DefaultButton="btnHiddenSC">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Service Center Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtServiceCenterName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 1:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 2:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            City:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            State:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsStates"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Zip Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Phone:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Fax:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadMaskedTextBox ID="txtFax" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            E-Mail:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtEMail" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>

                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenSC" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSCCancel" OnClick="btnSCCancel_Click" runat="server" Text="Cancel" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSCSave" OnClick="btnSCSave_Click" runat="server" Text="Save" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlLaborDetail" DefaultButton="btnHiddenLabor">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Req Hours
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtLaborReqHours" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Req Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtLaborReqRate" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Auth Hours
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtLaborAuthHours" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Auth Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtLaborAuthRate" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Tax Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtLaborTaxRate" Type="Percent" NumberFormat-DecimalDigits="4" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Tax:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtLaborTax" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Labor Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLaborDesc" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCodeLabor" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekLossCodeLabor" OnClick="btnSeekLossCodeLabor_Click" runat="server" Text="Seek" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            Loss Code Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCodeDescLabor" Width="200" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeNoLabor" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeNameLabor" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekClaimPayeeLabor" OnClick="btnSeekClaimPayeeLabor_Click" runat="server" Text="Seek" CssClass="button1"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenLabor" runat="server" Text="Cancel" CssClass="button1" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCancelLabor" OnClick="btnCancelLabor_Click" runat="server" Text="Cancel" CssClass="button1" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnUpdateLabor" OnClick="btnUpdateLabor_Click" runat="server" Text="Update Labor" CssClass="button1" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfClaimDetailID" runat="server" />
                <asp:HiddenField ID="hfClaimPayeeID" runat="server" />
                <asp:HiddenField ID="hfJobNo" runat="server" />
                <asp:HiddenField ID="hfServiceCenterNo" runat="server" />
                <asp:HiddenField ID="hfLossCodeSeek" runat="server" />
                <asp:HiddenField ID="hfPartTax" runat="server" />
                <asp:HiddenField ID="hfLaborTax" runat="server" />
                <asp:HiddenField ID="hfShopLabor" runat="server" />
                <asp:HiddenField ID="hfAddPart" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
