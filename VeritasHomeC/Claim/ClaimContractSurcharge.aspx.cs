﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimContractSurcharge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            if (!IsPostBack)
            {
                GetServerInfo();
                FillSurchargeGrid();
            }
        }

        private void FillSurchargeGrid()
        {
            GetContractID();
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select ContractSurchargeID, surcharge from contractSurcharge cs " +
                "inner join Surcharge s on s.SurchargeID = cs.surchargeid ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            rgSurcharge.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            rgSurcharge.DataBind();
        }

        private void GetContractID()
        {
            string SQL;
            clsDBO clCl = new clsDBO();

            SQL = "select * from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clCl.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clCl.RowCount() > 0)
            {
                clCl.GetRow();
                hfContractID.Value = clCl.GetFields("contractid");
            }
            else
                hfContractID.Value = "0";
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

    }
}