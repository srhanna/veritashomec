﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimDealer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            OpenClaim();
            OpenContract();
            OpenDealer();
            OpenAgent();
        }

        private void OpenAgent()
        {
            if (hfAgentID.Value.Length == 0)
                return;

            clsDBO clA = new clsDBO();
            string SQL;

            SQL = "select * from agents ";
            SQL = SQL + "where agent id= " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAgentName.Text = clA.GetFields("agentname");
                txtAgentNo.Text = clA.GetFields("agentno");
            }
        }

        private void OpenDealer()
        {
            if (hfDealerID.Value.Length == 0)
                return;

            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtAddr1.Text = clD.GetFields("addr1");
                txtAddr2.Text = clD.GetFields("addr2");
                txtAddr3.Text = clD.GetFields("city") + ", " + clD.GetFields("state") + " " + clD.GetFields("zip");
                txtPhone.Text = clD.GetFields("phone");
            }

        }

        private void OpenClaim()
        {
            if (hfDealerID.Value.Length == 0)
                return;

            string SQL;
            clsDBO clC = new clsDBO();
            SQL = "select contractid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfContractID.Value = clC.GetFields("contractid");
            }
            else
                hfContractID.Value = "0";
        }

        private void OpenContract()
        {
            string SQL;
            clsDBO clC = new clsDBO();

            if (hfContractID.Value.Length == 0)
                return;

            SQL = "select dealerid, agentsid from contract ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                hfDealerID.Value = clC.GetFields("dealerid");
                hfAgentID.Value = clC.GetFields("agentsid");
            }
            else
            {
                hfDealerID.Value = "0";
                hfAgentID.Value = "0";
            }
        }
    }
}