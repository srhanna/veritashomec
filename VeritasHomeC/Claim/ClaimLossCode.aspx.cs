﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimLossCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsLossCode.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlLossCode.Visible = false;
                ReadOnlyButtons();
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    Response.Redirect("~/users/claimssearch.aspx?sid=" + hfID.Value);
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void btnAddLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = false;
            pnlLossCode.Visible = true;
        }

        protected void btnCancelLossCode_Click(object sender, EventArgs e)
        {
            pnlSeekLossCode.Visible = true;
            pnlLossCode.Visible = false;
        }

        protected void btnSaveLossCode_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimlosscode ";
            SQL = SQL + "where losscode = '" + txtLossCode.Text + "' ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("losscode", txtLossCode.Text);
                clR.SetFields("losscodedesc", txtLossCodeDesc.Text);
                clR.AddRow();
                clR.SaveDB();
            }

            rgLossCode.Rebind();
            pnlSeekLossCode.Visible = true;
            pnlLossCode.Visible = false;
        }
    }
}