﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGAPCalc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];

            if (!IsPostBack)
            {
                GetServerInfo();
                FillCalc();
            }
        }

        private void FillCalc() {

            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from claimgapcalc ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtACV.Text = clR.GetFields("acv");
                txtAmountExceding150.Text = clR.GetFields("amountexceding150");
                txtCreditInsRefunds.Text = clR.GetFields("txtcreditinsrefunds");
                txtGAPPayout.Text = clR.GetFields("txtgappayout");
                txtInsDeduct.Text = clR.GetFields("insdeduct");
                txtInterestEarned.Text = clR.GetFields("interestearned");
                txtLossLoanBalance.Text = clR.GetFields("lossloanbalance");
                txtMechanicalContractRefund.Text = clR.GetFields("MechanicalContractRefund");
                txtNADALoss.Text = clR.GetFields("nadaloss");
                txtOtherDeduct.Text = clR.GetFields("otherdeduct");
                txtOwnerSalvage.Text = clR.GetFields("OwnerSalvage");
                txtScheduleLoanBalance.Text = clR.GetFields("ScheduleLoanBalance");
                txtTotalSchedule.Text = clR.GetFields("TotalSchedule");
                txtUnearnedGAPRefund.Text = clR.GetFields("UnearnedGAPRefund");
            }
            else
            {
                txtACV.Text = "0";
                txtAmountExceding150.Text = "0";
                txtCreditInsRefunds.Text = "0";
                txtGAPPayout.Text = "0";
                txtInsDeduct.Text = "0";
                txtInterestEarned.Text = "0";
                txtLossLoanBalance.Text = "0";
                txtMechanicalContractRefund.Text = "0";
                txtNADALoss.Text = "0";
                txtOtherDeduct.Text = "0";
                txtOwnerSalvage.Text = "0";
                txtScheduleLoanBalance.Text = "0";
                txtTotalSchedule.Text = "0";
                txtUnearnedGAPRefund.Text = "0";
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void txtLossLoanBalance_TextChanged(object sender, EventArgs e)
        {
            txtScheduleLoanBalance.Focus();
        }

        protected void txtScheduleLoanBalance_TextChanged(object sender, EventArgs e)
        {
            string SQL, sLossDate, sStartMonthDate;
            double dStartInterest, dEndInterest;
            long lDays;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgap ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sLossDate = clR.GetFields("lossdate");
                if (sLossDate.Length > 0)
                {
                    SQL = "select * from claimgapamortization ";
                    SQL = SQL + "where claimgapid = " + hfClaimID.Value + " ";
                    SQL = SQL + "and monthdate < '" + sLossDate + "' ";
                    SQL = SQL + "order by paymentno desc ";
                    clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                    if (clR.RowCount() > 0)
                    {
                        clR.GetRow();
                        sStartMonthDate = clR.GetFields("monthdate");
                        dStartInterest = Convert.ToDouble(clR.GetFields("interest"));
                        SQL = "select * from claimgapamortization ";
                        SQL = SQL + "where claimgapid = " + hfClaimID.Value + " ";
                        SQL = SQL + "and monthdate > '" + sLossDate + "' ";
                        SQL = SQL + "order by paymentno ";
                        clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                        if (clR.RowCount() > 0)
                        {
                            TimeSpan datediff = DateTime.Parse(sLossDate).Subtract(DateTime.Parse(sStartMonthDate));

                            clR.GetRow();
                            dEndInterest = Convert.ToDouble(clR.GetFields("interest"));
                            lDays = datediff.Days;
                            txtInterestEarned.Text = ((dStartInterest - dEndInterest) / Convert.ToDouble(lDays)).ToString("#.00");
                        }
                    }
                }
            }

            CalcTotalSchedule();
            CalcGAPPayout();
            txtACV.Focus();
        }

        private void CalcTotalSchedule()
        {
            if (txtScheduleLoanBalance.Text.Length > 0)
                txtTotalSchedule.Text = txtScheduleLoanBalance.Text;

            if (txtInterestEarned.Text.Length > 0)
                txtTotalSchedule.Text = (Convert.ToDouble(txtScheduleLoanBalance.Text) + Convert.ToDouble(txtInterestEarned.Text)).ToString();
        }

        private void CalcGAPPayout()
        {
            txtGAPPayout.Text = "0";
            if (txtTotalSchedule.Text.Length > 0)
                txtGAPPayout.Text = txtTotalSchedule.Text;

            if (txtACV.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtACV.Text)).ToString();

            if (txtNADALoss.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtNADALoss.Text)).ToString();

            if (txtOwnerSalvage.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtOwnerSalvage.Text)).ToString();

            if (txtMechanicalContractRefund.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtMechanicalContractRefund.Text)).ToString();

            if (txtCreditInsRefunds.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtCreditInsRefunds.Text)).ToString();

            if (txtUnearnedGAPRefund.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtUnearnedGAPRefund.Text)).ToString();

            if (txtInsDeduct.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtInsDeduct.Text)).ToString();

            if (txtAmountExceding150.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtAmountExceding150.Text)).ToString();

            if (txtOtherDeduct.Text.Length > 0)
                txtGAPPayout.Text = (Convert.ToDouble(txtGAPPayout.Text) - Convert.ToDouble(txtOtherDeduct.Text)).ToString();

        }

        protected void txtACV_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtNADALoss.Focus();
        }

        protected void txtNADALoss_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtOwnerSalvage.Focus();
        }

        protected void txtOwnerSalvage_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtMechanicalContractRefund.Focus();
        }

        protected void txtMechanicalContractRefund_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtCreditInsRefunds.Focus();
        }

        protected void txtCreditInsRefunds_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtUnearnedGAPRefund.Focus();
        }

        protected void txtUnearnedGAPRefund_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtAmountExceding150.Focus();
        }

        protected void txtInsDeduct_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtOtherDeduct.Focus();
        }

        protected void txtAmountExceding150_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
            txtInsDeduct.Focus();
        }

        protected void txtOtherDeduct_TextChanged(object sender, EventArgs e)
        {
            CalcGAPPayout();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgapcalc ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
            {
                clR.NewRow();
                clR.SetFields("claimgapid", hfClaimID.Value);
            }

            clR.SetFields("LossLoanBalance", txtLossLoanBalance.Text);
            clR.SetFields("ScheduleLoanBalance", txtScheduleLoanBalance.Text);
            clR.SetFields("InterestEarned", txtInterestEarned.Text);
            clR.SetFields("TotalSchedule", txtTotalSchedule.Text);
            clR.SetFields("ACV", txtACV.Text);
            clR.SetFields("NADALoss", txtNADALoss.Text);
            clR.SetFields("OwnerSalvage", txtOwnerSalvage.Text);
            clR.SetFields("MechanicalContractRefunds", txtMechanicalContractRefund.Text);
            clR.SetFields("CreditInsRefunds", txtCreditInsRefunds.Text);
            clR.SetFields("UnearnedGAPRefund", txtUnearnedGAPRefund.Text);
            clR.SetFields("AmountExceding150", txtAmountExceding150.Text);
            clR.SetFields("InsDeduct", txtInsDeduct.Text);
            clR.SetFields("OtherDeduct", txtOtherDeduct.Text);
            clR.SetFields("GapPayout", txtGAPPayout.Text);

            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }
    }
}