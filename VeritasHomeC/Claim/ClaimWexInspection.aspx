﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimWexInspection.aspx.cs" Inherits="VeritasHomeC.ClaimWexInspection" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" OnClick="btnUnlockClaim_Click" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAutonationACH" OnClick="btnAutonationACH_Click" runat="server" Text="AutoNation ACH"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnInspectionWex" OnClick="btnInspectionWex_Click" runat="server" Text="Wex Inspection"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCarfaxPayment" runat="server" Text="CarFax Payment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="rgClaim">
                                            <telerik:RadGrid ID="rgToBePaid" runat="server" AutoGenerateColumns="false"
                                                AllowSorting="true" AllowPaging="true"  Width="1500" AllowFilteringByColumn="true" ShowFooter="true" DataSourceID="SQLDataSource1" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimID" PageSize="10" ShowFooter="true" CommandItemDisplay="TopAndBottom">
                                                <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimDetaiilID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" AllowFiltering="false" ItemStyle-Width="100" HeaderStyle-Width ="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TotalAmt" UniqueName="TotalAmt" HeaderText="Total Amt" AllowFiltering="false" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ResultURL" UniqueName="ResultURL" HeaderText="Result Url" ItemStyle-Width="900" AllowFiltering="false" HeaderStyle-Width="900"></telerik:GridBoundColumn>
                                                        <telerik:GridClientSelectColumn UniqueName="ClaimSelect"></telerik:GridClientSelectColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings >
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="SqlDataSource1"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select cd.claimid, c.contractno, cl.claimno, cd.TotalAmt, ci.ResultURL from contract c 
                                                    inner join claim cl on c.contractid = cl.contractid 
                                                    inner join claimdetail cd on cl.claimid = cd.claimid 
                                                    inner join claiminspection ci on cl.claimid = ci.claimid 
                                                    where cd.JobNo = 'a02'
                                                    and (cd.ClaimDetailStatus = 'Authorized'
                                                    or cd.ClaimDetailStatus = 'Approved')
                                                    and not ci.ResultURL is null
                                                    and not claimdetailid in (select claimdetailid from claimwexinspectiontemp)
                                                    and cd.TotalAmt > 0 order by cd.CreDate" runat="server">
                                            </asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="1000" HorizontalAlign="Right">
                                        <asp:Button ID="btnMoveToPayment" runat="server" Text="Move To Payment"  CssClass="button1" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:Label ID="lblError2" ForeColor="Red" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPayment">
                                            <telerik:RadGrid ID="rgPayment" runat="server" AutoGenerateColumns="false" DataSourceID="dsPayment"
                                                AllowSorting="true" AllowPaging="true" ShowFooter="true"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small" AllowMultiRowSelection="true">
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500" DataKeyNames="ClaimID">
                                                    <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimDetaiilID"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No" AllowFiltering="false" ItemStyle-Width="100" HeaderStyle-Width ="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No" AutoPostBackOnFilter="true" AllowFiltering="true" CurrentFilterFunction="Contains" ItemStyle-Width="300" HeaderStyle-Width="300"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="TotalAmt" UniqueName="TotalAmt" HeaderText="Total Amt" AllowFiltering="false" ItemStyle-Width="150" HeaderStyle-Width="100"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ResultURL" UniqueName="ResultURL" HeaderText="Result Url" ItemStyle-Width="900" AllowFiltering="false" HeaderStyle-Width="900"></telerik:GridBoundColumn>
                                                        <telerik:GridButtonColumn CommandName="DeleteRow" CommandArgument="ClaimID" ButtonType="ImageButton" ImageUrl="~/images/delete.png"></telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                                <ClientSettings>
                                                    <Selecting AllowRowSelect="true" />
                                                </ClientSettings>
                                            </telerik:RadGrid>
                                        </telerik:RadAjaxPanel>
                                            <asp:SqlDataSource ID="dsPayment"
                                                ProviderName="System.Data.SqlClient" SelectCommand="select cd.claimid, c.contractno, cl.claimno, cd.TotalAmt, ci.ResultURL from contract c 
                                                    inner join claim cl on c.contractid = cl.contractid 
                                                    inner join claimdetail cd on cl.claimid = cd.claimid 
                                                    inner join claiminspection ci on cl.claimid = ci.claimid 
                                                    where cd.JobNo = 'a02'
                                                    and (cd.ClaimDetailStatus = 'Authorized'
                                                    or cd.ClaimDetailStatus = 'Approved')
                                                    and not ci.ResultURL is null
                                                    and claimdetailid in (select claimdetailid from claimwexinspectiontemp)
                                                    and cd.TotalAmt > 0 order by cd.CreDate" runat="server">
                                            </asp:SqlDataSource>                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    `   <asp:HiddenField ID="hfError" runat="server" />
    </form>
</body>
</html>
