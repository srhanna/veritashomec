﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class Claim3C : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfClaimID.Value = Request.QueryString["claimid"];
            GetServerInfo();
            if (!IsPostBack)
            {
                FillNotes();
                if (CheckLock())
                    btnUpdateNotes.Visible = false;
                else
                    btnUpdateNotes.Visible = true;
            }
        }

        private void ReadOnlyButtons()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("readonly") == "True")
                    btnUpdateNotes.Enabled = false;
            }
        }

        private bool CheckLock()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "Select claimid from claim ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "And lockuserid <> " + hfUserID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
                return false;
            else
                return true;
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        private void FillNotes()
        {
            GetComplaint();
            GetCause();
            GetCorrect();
        }

        private void GetCorrect()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select claimnoteid, note from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 2 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfCorrectNoteID.Value = clR.GetFields("claimnoteid");
                txtCorrect.Text = clR.GetFields("note");
            }
        }

        private void GetCause()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select claimnoteid, note from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 3 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfCauseNoteID.Value = clR.GetFields("claimnoteid");
                txtCause.Text = clR.GetFields("note");
            }
        }

        private void GetComplaint()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            SQL = "select claimnoteid, note from claimnote ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and claimnotetypeid = 1 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfComplaintNoteID.Value = clR.GetFields("claimnoteid");
                txtComplaint.Text = clR.GetFields("note");
            }
        }

        protected void btnUpdateNotes_Click(object sender, EventArgs e)
        {
            SaveComplaint();
            SaveCause();
            SaveCorrect();
            FillNotes();
        }

        private void SaveCorrect()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            if (hfCorrectNoteID.Value.Length == 0)
                hfCorrectNoteID.Value = "0";

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnoteid = " + hfCorrectNoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0) {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", "2");
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            else
                clR.GetRow();

            clR.SetFields("note", txtCorrect.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("scsclaimnoteid", "0");
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        private void SaveCause()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            if (hfCauseNoteID.Value.Length == 0)
                hfCauseNoteID.Value = "0";

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnoteid = " + hfCauseNoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", "3");
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            else
                clR.GetRow();

            clR.SetFields("note", txtCause.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("scsclaimnoteid", "0");
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        private void SaveComplaint()
        {
            string SQL;
            clsDBO clR = new clsDBO();

            if (hfComplaintNoteID.Value.Length == 0)
                hfComplaintNoteID.Value = "0";

            SQL = "select * from claimnote ";
            SQL = SQL + "where claimnoteid = " + hfComplaintNoteID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("claimnotetypeid", "2");
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", hfUserID.Value);
            }
            else
                clR.GetRow();

            clR.SetFields("note", txtCorrect.Text);
            clR.SetFields("moddate", DateTime.Today.ToString());
            clR.SetFields("modby", hfUserID.Value);
            clR.SetFields("scsclaimnoteid", "0");
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }
    }
}