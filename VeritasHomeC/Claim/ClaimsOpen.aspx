﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimsOpen.aspx.cs" Inherits="VeritasHomeC.ClaimsOpen" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claims Open</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" OnClick="btnHome_Click" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimSearch" OnClick="btnClaimSearch_Click" runat="server" Text="Claim Search"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnServiceCenters" OnClick="btnServiceCenters_Click" runat="server" Text="Service Centers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsOpen" runat="server" Text="My Open Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimAudit" OnClick="btnClaimAudit_Click" runat="server" Text="Claim Audit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketMessage" OnClick="btnTicketMessage_Click" runat="server" Text="Ticket Message"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnTicketResponse" OnClick="btnTicketResponse_Click" runat="server" Text="Ticket Response"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUnlockClaim" runat="server" Text="Unlock Claim"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnRFClaimSubmit" OnClick="btnRFClaimSubmit_Click" runat="server" Text="RF Claim Submit"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLossCode" runat="server" Text="CarFax Payment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAutonationACH" OnClick="btnAutonationACH_Click" runat="server" Text="AutoNation ACH"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnInspectionWex" runat="server" Text="Wex Inspection"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCarfaxPayment" runat="server" Text="CarFax Payment"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" OnClick="btnAgents_Click" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" OnClick="btnDealer_Click" runat="server" Text="Sellers"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" OnClick="btnContract_Click" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" OnClick="btnClaim_Click" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" OnClick="btnAccounting_Click" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" OnClick="btnReports_Click" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" OnClick="btnUsers_Click" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" OnClick="btnLogOut_Click" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                                <telerik:RadGrid ID="rgClaimOpen" OnSelectedIndexChanged="rgClaimOpen_SelectedIndexChanged" runat="server" OnDataBound="rgClaimOpen_DataBound" AutoGenerateColumns="false" Height="800"  Width="2000"
                                    AllowSorting="true" AllowPaging="false" ShowFooter="false"  AllowFilteringByColumn="false"
                                        Font-Names="Calibri" Font-Size="Small" >
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimID" ShowFooter="true" ShowHeader="true" Width="2000">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimID"  ReadOnly="true" UniqueName="ClaimID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DealerNo" UniqueName="DealerNo" HeaderText="Seller No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DealerName" UniqueName="DealerName" HeaderText="Seller Name"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ContractNo" UniqueName="ContractNo" HeaderText="Contract No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="fname" UniqueName="fname" HeaderText="First Name"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="lname" UniqueName="lname" HeaderText="Last Name"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="vin" UniqueName="vin" HeaderText="VIN"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ReportDate" UniqueName="ReportDate" HeaderText="Report Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LastMaint" UniqueName="LastMaint" HeaderText="Last Maint. Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Note" UniqueName="Note" HeaderText="Note"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ActivityDesc" UniqueName="ActivityDesc" HeaderText="Activity Desc"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AmtDue" UniqueName="AmtDue" HeaderText="Amt Due" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ClaimAge" UniqueName="ClaimAge" HeaderText="Claim Age"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NoActivity" UniqueName="NoActivity" HeaderText="No Activity"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ServiceCenterNo" UniqueName="ServiceCenterNo" HeaderText="Service Center No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ServiceCenterName" UniqueName="ServiceCenterName" HeaderText="Service Center Name"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OpenDate" UniqueName="OpenDate" HeaderText="Open Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="OpenDays" UniqueName="OpenDays" HeaderText="Open Days"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="InspectionRequest" UniqueName="InspectionRequest" HeaderText="Inspection Request" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="InspectionComplete" UniqueName="InspectionComplete" HeaderText="Inspection Complete" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" /> 
                                    </ClientSettings>
                                </telerik:RadGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" OnClick="btnErrorOK_Click" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </ContentTemplate>
        </telerik:RadWindow>

        <asp:HiddenField ID="hfAlert" runat="server" />
        <telerik:RadWindow ID="rwAlert"  runat="server" Width="750" Height="750"  Behaviors="Close" BackColor ="Red" BorderColor="Red" EnableViewState="false" ReloadOnShow="true" Title="Alert">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:TextBox ID="txtAlertPopup" Width="730" Height="600" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="730"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnAlertOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
    </form>
</body>
</html>
