﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Telerik.Web.UI;

namespace VeritasHomeC
{
    public partial class ClaimGAPFinance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsAmortization.ConnectionString = ConfigurationManager.AppSettings["connstring"];

            if (!IsPostBack)
            {
                hfClaimID.Value = Request.QueryString["ClaimID"];
                GetServerInfo();
                FillFinance();
                rgClaim.Rebind();
            }
        }

        private void FillFinance()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgapfinance ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                txtTradeIn.Text = clR.GetFields("tradein");
                txtTotalCashPrice.Text = clR.GetFields("totalcashprice");
                txtDownPayment.Text = clR.GetFields("downpayment");
                txtAmtFinance.Text = clR.GetFields("amountfinance");
                txtAnnualPer.Text = clR.GetFields("annualpercent");
                
                if (clR.GetFields("loanstartdate").Length > 0)
                    rdpLoanStartDate.SelectedDate = Convert.ToDateTime(clR.GetFields("loanstartdate"));
                else
                    rdpLoanStartDate.Clear();

                if (clR.GetFields("purchasedate").Length > 0)
                    rdpVehiclePurchaseDate.SelectedDate = Convert.ToDateTime(clR.GetFields("purchasedate"));
                else
                    rdpVehiclePurchaseDate.Clear();

                txtNoPayments.Text = clR.GetFields("nopayments");
                txtFinanceCharge.Text = clR.GetFields("financecharge");
                txtTotalLoan.Text = clR.GetFields("totalloan");
                txtPaymentAmt.Text = clR.GetFields("paymentamt");
            }
            else
            {
                txtTradeIn.Text = "0";
                txtTotalCashPrice.Text = "0";
                txtDownPayment.Text = "0";
                txtAmtFinance.Text = "0";
                txtAnnualPer.Text = "0";
                txtNoPayments.Text = "0";
                txtFinanceCharge.Text = "0";
                txtTotalLoan.Text = "0";
                txtPaymentAmt.Text = "0";
            }
        }

        private void CalcAmountFinance()
        {
            if (txtTotalCashPrice.Text.Length > 0)
                txtAmtFinance.Text = txtTotalCashPrice.Text;

            if (txtDownPayment.Text.Length > 0)
                txtAmtFinance.Text = (Convert.ToDouble(txtAmtFinance.Text) - Convert.ToDouble(txtDownPayment.Text)).ToString();
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
            }
        }

        protected void txtTradeIn_TextChanged(object sender, EventArgs e)
        {
            txtTotalCashPrice.Focus();
        }

        protected void btnCalLoan_Click(object sender, EventArgs e)
        {
            double dRatePerMonth, dDiscount1, dDiscount2, dDiscount3, dDiscount4, dDiscount5, dPaymentAmt, dAmtLoan, dAmtInterest;
            dRatePerMonth = (Convert.ToDouble(txtAnnualPer.Text) / 100) / 12;
            dDiscount1 = 1 + dRatePerMonth;
            dDiscount2 = (Math.Pow(dDiscount1, Convert.ToDouble(txtNoPayments.Text)));
            dDiscount3 = dDiscount2 - 1;
            dDiscount4 = dRatePerMonth * dDiscount2;
            dDiscount5 = dDiscount3 / dDiscount4;
            dPaymentAmt = Convert.ToDouble(txtAmtFinance.Text) / dDiscount5;
            dAmtLoan = dPaymentAmt * Convert.ToDouble(txtNoPayments.Text);
            dAmtInterest = dAmtLoan - Convert.ToDouble(txtAmtFinance.Text);

            txtFinanceCharge.Text = dAmtInterest.ToString();
            txtTotalLoan.Text = dAmtLoan.ToString();
            txtPaymentAmt.Text = dPaymentAmt.ToString();
            VeritasGlobalToolsV2.clsBuildAmortization clBA = new VeritasGlobalToolsV2.clsBuildAmortization();
            clBA.ClaimGapID = Convert.ToInt64(hfClaimID.Value);
            clBA.PaymentAmt = Convert.ToDouble(dPaymentAmt.ToString("#.00"));
            clBA.RatePerMonth = dRatePerMonth;
            clBA.LoanAmt = Convert.ToDouble(txtAmtFinance.Text);
            clBA.LoanAmtInt = Convert.ToDouble(dAmtLoan.ToString("#.00"));
            clBA.StartDate = rdpLoanStartDate.SelectedDate.ToString();
            clBA.NoPayment = Convert.ToInt64(txtNoPayments.Text);
            clBA.BuildIt();
            rgClaim.Rebind();
        }

        protected void txtDownPayment_TextChanged(object sender, EventArgs e)
        {
            CalcAmountFinance();
            txtAmtFinance.Focus();
        }

        protected void rdpLoanStartDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            txtTradeIn.Focus();
        }

        protected void rgClaim_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            { 
                rgClaim.ExportSettings.ExportOnlyData = false;
                rgClaim.ExportSettings.IgnorePaging = true;
                rgClaim.ExportSettings.OpenInNewWindow = true;
                rgClaim.ExportSettings.UseItemStyles = true;
                rgClaim.ExportSettings.Excel.FileExtension = "xlsx";
                rgClaim.ExportSettings.FileName = "Amortization";
                rgClaim.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                rgClaim.MasterTableView.ExportToExcel();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgapfinance ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
                clR.GetRow();
            else
            {
                clR.NewRow();
                clR.SetFields("claimgapid", hfClaimID.Value);
            }

            clR.SetFields("tradein", txtTradeIn.Text);
            clR.SetFields("totalcashprice", txtTotalCashPrice.Text);
            clR.SetFields("downpayment", txtDownPayment.Text);
            clR.SetFields("amountfinance", txtAmtFinance.Text);
            clR.SetFields("annualpercent", txtAnnualPer.Text);
            clR.SetFields("nopayments", txtNoPayments.Text);
            clR.SetFields("financecharge", txtFinanceCharge.Text);
            clR.SetFields("totalloan", txtTotalLoan.Text);
            clR.SetFields("paymentamt", txtPaymentAmt.Text);
            clR.SetFields("loanstartdate", rdpLoanStartDate.SelectedDate.ToString());
            clR.SetFields("purchasedate", rdpVehiclePurchaseDate.SelectedDate.ToString());
            if (clR.RowCount() == 0)
                clR.AddRow();

            clR.SaveDB();
        }

        protected void txtAnnualPer_TextChanged(object sender, EventArgs e)
        {
            txtNoPayments.Focus();
        }

        protected void rdpVehiclePurchaseDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            rdpLoanStartDate.Focus();
        }

        protected void txtTotalCashPrice_TextChanged(object sender, EventArgs e)
        {
            txtDownPayment.Focus();
        }

        protected void txtAmtFinance_TextChanged(object sender, EventArgs e)
        {
            txtAnnualPer.Focus();
        }
    }
}