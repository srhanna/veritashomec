﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimGap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsClaimStatus.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            hfClaimID.Value = Request.QueryString["ClaimID"];
            hfID.Value = Request.QueryString["sid"];
            if (System.Configuration.ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }

            if (!IsPostBack)
            {
                GetServerInfo();
                lblAtt.Visible = false;
                tsClaim.Tabs[7].BackColor = System.Drawing.Color.White;
                GetClaim();
                tsClaim.Tabs[0].Selected = true;
                pvCustomer.Selected = true;
                pvPOA.ContentUrl = "~/claim/ClaimGAPPOA.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvNotes.ContentUrl = "~/claim/ClaimGAPNote.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvCoBuyer.ContentUrl = "~/claim/ClaimGAPCoBuyer.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvAttorney.ContentUrl = "~/claim/ClaimGAPAttorney.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvDocuments.ContentUrl = "~/claim/ClaimGapDocument.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvCalc.ContentUrl = "~/claim/ClaimGapCalc.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvStatus.ContentUrl = "~/claim/ClaimGapDocumentStatus.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvFinance.ContentUrl = "~/claim/ClaimGapFinance.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvPayment.ContentUrl = "~/claim/ClaimGapPayment.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                pvLoanInfo.ContentUrl = "~/claim/ClaimGapLoanInfo.aspx?sid=" + hfID.Value + "&claimid=" + hfClaimID.Value;
                GetContractInfo();
                FillDealerInfo();
            }
        }

        private void GetContractInfo()
        {
            string SQL;
            clsDBO clC = new clsDBO();
            if (hfContractID.Value.Length == 0)
                return;

            SQL = "select * from contract c ";
            SQL = SQL + "left join InsCarrier ic on c.InsCarrierID = ic.InsCarrierID ";
            SQL = SQL + "where contractid = " + hfContractID.Value;
            clC.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                txtContractNo.Text = clC.GetFields("contractno");
                txtStatus.Text = clC.GetFields("status");
                if (txtStatus.Text == "Cancelled")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Cancelled Before Paid")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Expired")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Pending Expired")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Invalid")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Pending Cancel")
                    txtStatus.BackColor = System.Drawing.Color.Red; ;
                if (txtStatus.Text == "PendingCancel")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Quote")
                    txtStatus.BackColor = System.Drawing.Color.Red; ;
                if (txtStatus.Text == "Rejected")
                    txtStatus.BackColor = System.Drawing.Color.Red; ;
                if (txtStatus.Text == "Sale pending")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "test")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Training")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Void")
                    txtStatus.BackColor = System.Drawing.Color.Red;
                if (txtStatus.Text == "Pending")
                    txtStatus.BackColor = System.Drawing.Color.Yellow;
                if (txtStatus.Text == "Paid")
                    txtStatus.BackColor = System.Drawing.Color.Green;

                txtPaidDate.Text = clC.GetFields("datepaid");
                txtSaleDate.Text = DateTime.Parse(clC.GetFields("saledate")).ToString("d");
                txtFName.Text = clC.GetFields("fname");
                txtLName.Text = clC.GetFields("lname");
                hfPlanTypeID.Value = clC.GetFields("plantypeid");
                hfProgram.Value = clC.GetFields("programid");
                hfDealerID.Value = clC.GetFields("dealerid");
                hfAgentID.Value = clC.GetFields("agentsid");
                txtTerm.Text = clC.GetFields("termmonth") + "/" + (Convert.ToInt64(clC.GetFields("termmile")).ToString("#,##0"));

                if (clC.GetFields("effdate").Length > 0) 
                    txtEffDate.Text = DateTime.Parse(clC.GetFields("effdate")).ToString("d"); //converts format to M/d/yyyy

                if (clC.GetFields("expdate").Length > 0)
                    txtExpDate.Text = DateTime.Parse(clC.GetFields("expdate")).ToString("d");

                GetProgram();
                GetPlanType();

                if (clC.GetFields("decpage").Length == 0)
                {
                    tcDec.Visible = false;
                    tcDesc2.Visible = false;
                }
                else
                {
                    tcDec.Visible = true;
                    tcDesc2.Visible = true;
                    hlDec.NavigateUrl = clC.GetFields("decpage");
                    hlDec.Text = clC.GetFields("decpage");
                }

                if (clC.GetFields("tcpage").Length == 0)
                {
                    tcTC.Visible = false;
                    tcTC2.Visible = false;
                }
                else
                {
                    tcTC.Visible = true;
                    tcTC2.Visible = true;
                    hlTC.NavigateUrl = clC.GetFields("tcpage");
                    hlTC.Text = clC.GetFields("tcpage");
                }
            }
        }

        private void FillDealerInfo()
        {
            OpenDealer();
            OpenAgent();
        }

        private void OpenAgent()
        {
            if (hfAgentID.Value.Length == 0)
            {
                txtAgentName.Text = "";
                txtAgentNo.Text = "";
                return;
            }

            clsDBO clA = new clsDBO();
            string SQL;

            SQL = "select * fro agents ";
            SQL = SQL + "where agentid = " + hfAgentID.Value;
            clA.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clA.RowCount() > 0)
            {
                clA.GetRow();
                txtAgentName.Text = clA.GetFields("agentname");
                txtAgentNo.Text = clA.GetFields("agentno");
            }
        }

        private void OpenDealer()
        {
            if (hfDealerID.Value.Length == 0)
            {
                txtDealerAddr1.Text = "";
                txtDealerAddr2.Text = "";
                txtDealerAddr3.Text = "";
                txtDealerName.Text = "";
                txtDealerNo.Text = "";
                txtDealerPhone.Text = "";
                return;
            }

            string SQL;
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer ";
            SQL = SQL + "where dealerid = " + hfDealerID.Value;
            clD.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                txtDealerName.Text = clD.GetFields("dealername");
                txtDealerNo.Text = clD.GetFields("dealerno");
                txtDealerAddr1.Text = clD.GetFields("addr1");
                txtDealerAddr2.Text = clD.GetFields("addr2");
                txtDealerAddr3.Text = clD.GetFields("city") + ", " + clD.GetFields("state") + " " + clD.GetFields("zip");
                txtDealerPhone.Text = clD.GetFields("phone");
            }
        }

        private void GetPlanType()
        {
            string SQL;
            clsDBO clPT = new clsDBO();
            SQL = "select * from plantype ";
            SQL = SQL + "where plantypeid = " + hfPlanTypeID.Value;
            clPT.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);

            if (clPT.RowCount() > 0)
            {
                clPT.GetRow();
                txtPlan.Text = clPT.GetFields("plantype");
            }
        }

        private void GetProgram()
        {
            string SQL;
            clsDBO clP = new clsDBO();
            SQL = "select * from program ";
            SQL = SQL + "where programid = " + hfProgram.Value;
            clP.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clP.RowCount() > 0)
            {
                clP.GetRow();
                txtProgram.Text = clP.GetFields("programname");
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockScreen();
            }
        }

        private void LockScreen()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from userinfo ui ";
            SQL = SQL + "inner join usersecurityinfo usi on ui.userid = usi.userid ";
            SQL = SQL + "where ui.userid = " + hfUserID.Value;

            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (Convert.ToBoolean(clR.GetFields("readonly")))
                    btnUpdate.Visible = false;
            }
        }

        private void GetClaim()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            clsDBO clU = new clsDBO();
            SQL = "select * from claimgap ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                hfContractID.Value = clR.GetFields("contractid");
                pvVehicle.ContentUrl = "~/claim/ClaimVehicle.aspx?ContractID=" + hfContractID.Value;
                pvCustomer.ContentUrl = "~/claim/ClaimCustomer.aspx?sid=" + hfID.Value + "&contractid=" + hfContractID.Value;
                txtClaimNo.Text = clR.GetFields("claimno");
                cboClaimStatus.SelectedValue = clR.GetFields("status");
                txtClaimDate.Text = clR.GetFields("claimdate");
                txtClaimantPhone.Text = clR.GetFields("claimantphone");
                txtClaimantEmail.Text = clR.GetFields("claimantemail");
                txtClaimAuthorPerson.Text = clR.GetFields("ClaimAuthorizedPerson");
                txtNADAatLoss.Text = clR.GetFields("nadaatloss");
                txtNADAatPurchase.Text = clR.GetFields("nadaatpurchase");
                txtDeductForSalvage.Text = clR.GetFields("deductforsalvage");
                chkRideshareCommercial.Checked = Convert.ToBoolean(clR.GetFields("ridesharecommercial"));
                txtLossMile.Text = clR.GetFields("lossmile");

                if (clR.GetFields("lossdate").Length > 0)
                    rdpLossDate.SelectedDate = Convert.ToDateTime(clR.GetFields("lossdate"));
                else
                    rdpLossDate.Clear();

                SQL = "select * From claimgapattorney ";
                SQL = SQL + "where claimgapid = " + hfClaimID.Value;
                clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
                if (clR.RowCount() > 0)
                {
                    lblAtt.Visible = true;
                    tsClaim.Tabs[7].BackColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void tsClaim_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            if (tsClaim.SelectedTab.Value == "Customer")
                pvCustomer.Selected = true;
            if (tsClaim.SelectedTab.Value == "Vehicle")
                pvVehicle.Selected = true;
            if (tsClaim.SelectedTab.Value == "Status")
                pvStatus.Selected = true;
            if (tsClaim.SelectedTab.Value == "Documents")
                pvDocuments.Selected = true;
            if (tsClaim.SelectedTab.Value == "POA")
                pvPOA.Selected = true;
            if (tsClaim.SelectedTab.Value == "CoBuyer") 
                pvCoBuyer.Selected = true;
            if (tsClaim.SelectedTab.Value == "Attorney") 
                pvAttorney.Selected = true;
            if (tsClaim.SelectedTab.Value == "Notes") 
                pvNotes.Selected = true;            
            if (tsClaim.SelectedTab.Value == "Finance")
                pvFinance.Selected = true;           
            if (tsClaim.SelectedTab.Value == "Calc")
                pvCalc.Selected = true;
            if (tsClaim.SelectedTab.Value == "Payment")
                pvPayment.Selected = true;            
            if (tsClaim.SelectedTab.Value == "LoanInfo")
                pvLoanInfo.Selected = true;            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from claimgap ";
            SQL = SQL + "where claimgapid = " + hfClaimID.Value;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                clR.SetFields("status", cboClaimStatus.SelectedValue);
                clR.SetFields("claimantphone", txtClaimantPhone.Text);
                clR.SetFields("claimantemail", txtClaimantEmail.Text);
                clR.SetFields("claimauthorizedperson", txtClaimAuthorPerson.Text);
                clR.SetFields("nadaatpurchase", txtNADAatPurchase.Text);
                clR.SetFields("nadaatloss", txtNADAatLoss.Text);
                clR.SetFields("ridesharecommercial", chkRideshareCommercial.Checked.ToString());
                clR.SetFields("deductforsalvage", txtDeductForSalvage.Text);
                clR.SetFields("lossdate", rdpLossDate.SelectedDate.ToString());
                clR.SetFields("lossmile", txtLossMile.Text);
                clR.SaveDB();
            }
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnUnlockClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/unlockclaim.aspx?sid=" + hfID.Value);
        }

        protected void btnLossCode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }
    }
}