﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace VeritasHomeC
{
    public partial class ClaimTicketMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dsNote.ConnectionString = ConfigurationManager.AppSettings["connstring"];
            if (!IsPostBack)
            {
                GetServerInfo();
                pnlList.Visible = true;
                pnlDetail.Visible = false;
                FillClaimCombo();
            }

            if (ConfigurationManager.AppSettings["connstring"].Contains("test"))
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688");
            }
            else
            {
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2");
            }
        }

        private void GetServerInfo()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            DateTime sStartDate = DateTime.Today;
            DateTime sEndDate = DateTime.Today.AddDays(1);
            hfID.Value = Request.QueryString["sid"];
            SQL = "select * from serverinfo ";
            SQL = SQL + "where systemid = '" + hfID.Value + "' ";
            SQL = SQL + "and signindate >= '" + sStartDate + "' ";
            SQL = SQL + "and signindate <= '" + sEndDate + "' ";
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            hfUserID.Value = "0";
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                hfUserID.Value = clSI.GetFields("userid");
                LockButtons();
                UnlockButtons();
            }
        }

        private void LockButtons()
        {
            btnAccounting.Enabled = false;
            btnAgents.Enabled = false;
            btnClaim.Enabled = false;
            btnDealer.Enabled = false;
            btnContract.Enabled = false;
            btnSettings.Enabled = false;
            btnUsers.Enabled = false;
            btnUsers.Enabled = false;
            btnContract.Enabled = false;
            btnReports.Enabled = false;
            btnUnlockClaim.Enabled = false;
        }

        private void UnlockButtons()
        {
            string SQL;
            clsDBO clSI = new clsDBO();
            SQL = "select * from usersecurityinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clSI.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clSI.RowCount() > 0)
            {
                clSI.GetRow();
                btnUsers.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accounting")))
                    btnAccounting.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Settings")))
                    btnSettings.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Agents")))
                    btnAgents.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("Dealer")))
                    btnDealer.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claim")))
                    btnClaim.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("contract")))
                    btnContract.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("salesreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("accountreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("claimsreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("customreports")))
                    btnReports.Enabled = true;
                if (Convert.ToBoolean(clSI.GetFields("unlockclaim")))
                    btnUnlockClaim.Enabled = true;
                if (!Convert.ToBoolean(clSI.GetFields("claimpayment")))
                {
                    btnInspectionWex.Enabled = false;
                    btnCarfaxPayment.Enabled = false;
                }
            }
        }

        private void FillClaimCombo()
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select claimid, claimno from claim ";
            SQL = SQL + "where assignedto = " + hfUserID.Value + " ";
            SQL = SQL + "and claimid in (select claimid from veritasclaimticket.dbo.note) ";
            cboClaim.DataSource = clR.GetData(SQL, ConfigurationManager.AppSettings["connstring"]);
            cboClaim.DataBind();
            hfClaimID.Value = cboClaim.SelectedValue;
            rgNote.Rebind();
        }

        protected void btnClaimsOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsopen.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimTeamOpen_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimteamopen.aspx?sid=" + hfID.Value);
        }

        protected void btnRFClaimSubmit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" + hfID.Value);
        }

        protected void btnTicketResponse_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" + hfID.Value);
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/users/users.aspx?sid=" + hfID.Value);
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx?sid=" + hfID.Value);
        }

        protected void btnAgents_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/agents/AgentsSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/dealer/dealersearch.aspx?sid=" + hfID.Value);
        }

        protected void btnReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/reports/reports.aspx?sid=" + hfID.Value);
        }

        protected void btnContract_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contract/ContractSearch.aspx?sid=" + hfID.Value);
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/accounting/accounting.aspx?sid=" + hfID.Value);
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void btnServiceCenters_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/servicecenters.aspx?sid=" + hfID.Value);
        }

        protected void btnAutonationACH_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/autonationach.aspx?sid=" + hfID.Value);
        }

        protected void btnClaimSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/claim/claimsearch.aspx?sid=" + hfID.Value);
        }

        protected void rgNote_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();

            btnSave.Visible = false;
            txtNote.Enabled = false;
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            SQL = "select * from veritasclaimticket.dbo.note ";
            SQL = SQL + "where noteid = " + rgNote.SelectedValue;
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() > 0) {
                clR.GetRow();
                txtNote.Content = clR.GetFields("note");
                txtCreBy.Text = clR.GetFields("creby");
                txtCreDate.Text = clR.GetFields("credate");
                txtResponseBy.Text = clR.GetFields("responseby");
                txtResponseDate.Text = clR.GetFields("responsedate");
            }
        }

        protected void btnAddNote_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = true;
            pnlList.Visible = false;
            btnSave.Visible = true;
            txtNote.Enabled = true;
            txtNote.Content = "";
            txtCreBy.Text = "";
            txtCreDate.Text = "";
            txtResponseBy.Text = "";
            txtResponseDate.Text = "";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pnlDetail.Visible = false;
            pnlList.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string SQL;
            clsDBO clR = new clsDBO();
            SQL = "select * from veritasclaimticket.dbo.note ";
            SQL = SQL + "where noteid = 0 ";
            clR.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clR.RowCount() == 0) 
            {
                clR.NewRow();
                clR.SetFields("claimid", hfClaimID.Value);
                clR.SetFields("notetype", "Response");
                clR.SetFields("note", txtNote.Content);
                clR.SetFields("notetext", txtNote.Text);
                clR.SetFields("responseby", CalcUserName());
                clR.SetFields("responsedate", DateTime.Today.ToString());
                clR.AddRow();
                clR.SaveDB();
            }

            SQL = "update veritasclaimticket.dbo.ticket ";
            SQL = SQL + "set statusid = 1 ";
            SQL = SQL + "where claimid = " + hfClaimID.Value;
            SQL = SQL + "and statusid <> 4 ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            SQL = "update veritasclaimticket.dbo.ticket ";
            SQL = SQL + "set statusid = 2 ";
            SQL = SQL + "where claimid = " + hfClaimID.Value + " ";
            SQL = SQL + "and responseid <> 1 ";
            SQL = SQL + "and statusid <> 4 ";
            clR.RunSQL(SQL, ConfigurationManager.AppSettings["connstring"]);
            pnlDetail.Visible = false;
            pnlList.Visible = true;
            rgNote.Rebind();
            }

        protected void cboClaim_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfClaimID.Value = cboClaim.SelectedValue;
            rgNote.Rebind();
        }

        private string CalcUserName()
        {
            string SQL;
            clsDBO clU = new clsDBO();
            SQL = "select * from userinfo ";
            SQL = SQL + "where userid = " + hfUserID.Value;
            clU.OpenDB(SQL, ConfigurationManager.AppSettings["connstring"]);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                return clU.GetFields("username");
            }

            return "";
        }
    }
}