﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClaimContractSurcharge.aspx.cs" Inherits="VeritasHomeC.ClaimContractSurcharge" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/Styles.css" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel runat="server" ID="pnlSurcharge">
                                <telerik:RadGrid ID="rgSurcharge" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                    AllowSorting="true" AllowPaging="false"  Width="500" ShowFooter="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="false" DataKeyNames="ContractSurchargeID" PageSize="25" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ContractSurchargeID"  ReadOnly="true" Visible="false" UniqueName="ContractSurchargeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Surcharge" UniqueName="Surcharge" HeaderText="Surcharge"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfContractID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
