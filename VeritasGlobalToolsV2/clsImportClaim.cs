﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsImportClaim
    {
        private long lVeritasClaimID;
        private long lAssignedToID;
        private long lUserID;
        private clsDBO.clsDBO clVC = new clsDBO.clsDBO();
        private string SQL;
        private string sNewClaimNo;
        private long lClaimID;
        private long lClaimPayeeID;
        private double dDeductAmt;

        public long UserID
        {
            get
            {
                return lUserID;
            }
            set
            {
                lUserID = value;
            }
        }

        public long AssignedToID
        {
            get
            {
                return lAssignedToID;
            }
            set
            {
                lAssignedToID = value;
            }
        }

        public long VeritasClaimID
        {
            get
            {
                return lVeritasClaimID;
            }
            set
            {
                lVeritasClaimID = value;
            }
        }

        public void ProcessClaim()
        {
            SQL = "select * from veritasclaims.dbo.claim where claimid = " + lVeritasClaimID;
            clVC.OpenDB(SQL, Global.sCON);
            if (clVC.RowCount() > 0)
            {
                clVC.GetRow();
                GetClaimNo();
                PlaceEmailFax();
                CheckClaimPayee();
                InsertClaimDetailDeduct();
                InsertClaimDetailPart();
                InsertClaimDetailLabor();
                Place3Cs();
                CloseClaim();
                SendEMail();
                SendConfirm();
            }
        }

        private void Place3Cs()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote where claimid = " + lClaimID + " " + 
                  "and claimnotetypeid = 1 ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", lClaimID.ToString());
                clR.SetFields("claimnotetypeid", 1.ToString());
                clR.SetFields("note", clVC.GetFields("complaint"));
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", lUserID.ToString());
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", lUserID.ToString());
                clR.SetFields("scsclaimnoteid", 0.ToString());
                clR.SetFields("notetext", clVC.GetFields("complaint"));
                clR.AddRow();
                clR.SaveDB();
            }
            SQL = "select * from claimnote where claimid = " + lClaimID + " " +
                  "and claimnotetypeid = 2 ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", lClaimID.ToString());
                clR.SetFields("claimnotetypeid", 2.ToString());
                clR.SetFields("note", clVC.GetFields("correction"));
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", lUserID.ToString());
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", lUserID.ToString());
                clR.SetFields("scsclaimnoteid", 0.ToString());
                clR.SetFields("notetext", clVC.GetFields("correction"));
                clR.AddRow();
                clR.SaveDB();
            }
            SQL = "select * from claimnote where claimid = " + lClaimID + " " +
                  "and claimnotetypeid = 3 ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimid", lClaimID.ToString());
                clR.SetFields("claimnotetypeid", 3.ToString());
                clR.SetFields("note", clVC.GetFields("cause"));
                clR.SetFields("credate", DateTime.Today.ToString());
                clR.SetFields("creby", lUserID.ToString());
                clR.SetFields("moddate", DateTime.Today.ToString());
                clR.SetFields("modby", lUserID.ToString());
                clR.SetFields("scsclaimnoteid", 0.ToString());
                clR.SetFields("notetext", clVC.GetFields("cause"));
                clR.AddRow();
                clR.SaveDB();
            }
        }

        private void PlaceEmailFax()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter where servicecenterid = " + clVC.GetFields("servicecenterid");
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clVC.GetFields("email").Length > 0)
                {
                    clR.SetFields("email", clVC.GetFields("email"));
                }
                if (clVC.GetFields("fax").Length > 0)
                {
                    clVC.SetFields("fax", clVC.GetFields("fax").Replace("-", ""));
                    clVC.SetFields("fax", clVC.GetFields("fax").Replace(" ", ""));
                    clVC.SetFields("fax", clVC.GetFields("fax").Replace("(", ""));
                    clVC.SetFields("fax", clVC.GetFields("fax").Replace(")", ""));
                    clVC.SetFields("fax", clVC.GetFields("fax").Replace(".", ""));
                    clR.SetFields("fax", clVC.GetFields("fax"));
                }
                clR.SaveDB();
            }
        }

        private void SendEMail()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string sEMail;
            SQL = "select * from userinfo where userid = " + lAssignedToID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() == 0)
            {
                return;
            }
            else
            {
                clR.GetRow();
                sEMail = clR.GetFields("email");
            }
            SmtpClient client = new SmtpClient("smtp.office365.com", 587);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3");
            client.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.To.Add(sEMail);
            mail.From = new MailAddress("info@veritasglobal.com");
            mail.Bcc.Add("steve@hannaworld.com");
            mail.Subject = "New Claim assigned to you.";
            mail.IsBodyHtml = true;
            mail.Body = sNewClaimNo + " has been assigned to from the Veritas Global Claim.";
            client.Send(mail);
        }

        private void SendConfirm()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string sAdvisor;
            string sEMail;
            SQL = "select * from userinfo where userid = " + lAssignedToID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() == 0)
            {
                return;
            }
            else
            {
                clR.GetRow();
                sAdvisor = clR.GetFields("fname") + " " + clR.GetFields("lname");
            }
            SQL = "select * from VeritasClaims.dbo.Claim where claimno = '" + sNewClaimNo + "' ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sEMail = clR.GetFields("email");
            }
            else
            {
                return;
            }
            if (sEMail.Length == 0)
            {
                return;
            }
            SmtpClient client = new SmtpClient("smtp.office365.com", 587);
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3");
            client.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.To.Add(sEMail);
            mail.From = new MailAddress("info@veritasglobal.com");
            mail.Bcc.Add("steve@hannaworld.com");
            mail.Subject = "Your New Claim " + sNewClaimNo;
            mail.IsBodyHtml = true;
            string sBody;
            sBody = "Your claim has been assigned Claim No: " + sNewClaimNo + ".</br></br>" + 
                    "Claim Adjust has been assigned to " + sAdvisor + ".</br></br>" + 
                    "Thank you for contacting Veritas Global Protection.";
            mail.Body = sBody;
            client.Send(mail);
        }
        private void CloseClaim()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "update veritasclaims.dbo.claim " +
                  "set processclaimdate = '" + DateTime.Today.ToString() + "', " +
                  "processclaimby = " + lUserID + " " +
                  "where claimid = " + clVC.GetFields("claimid");
            clR.RunSQL(SQL, Global.sCON);
        }
        private void InsertClaimDetailLabor()
        {
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from veritasclaims.dbo.claimlabor where claimid = " + clVC.GetFields("claimid");
            clCL.OpenDB(SQL, Global.sCON);
            if (clCL.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clCL.RowCount() - 1; cnt++)
                {
                    clCL.GetRowNo(cnt);
                    if (clCL.GetFields("tax").Length == 0)
                    {
                        clCL.SetFields("tax", 0.ToString());
                    }
                    SQL = "insert into claimdetail ";
                    SQL = SQL + "(claimid, claimdetailtype, jobno, reqqty, reqcost, reqamt, taxper, taxamt, totalamt, claimdetailstatus, creby, credate, moddate, modby, claimpayeeid, claimdesc) ";
                    SQL = SQL + "values (" + lClaimID + ", ";
                    SQL = SQL + "'Labor', 'J01', ";
                    SQL = SQL + clCL.GetFields("qty") + ",";
                    SQL = SQL + clCL.GetFields("cost") + ",";
                    if (clCL.GetFields("cost").Length > 0 && clCL.GetFields("qty").Length > 0)
                    {
                        SQL = SQL + (Convert.ToDouble(clCL.GetFields("qty")) * Convert.ToDouble(clCL.GetFields("cost"))).ToString() + ",";
                    }
                    else
                    {
                        SQL = SQL + "0,";
                    }
                    SQL = SQL + clCL.GetFields("tax") + ",";
                    if (clCL.GetFields("cost").Length > 0 && clCL.GetFields("qty").Length > 0) 
                    {
                        SQL = SQL + (Convert.ToDouble(clCL.GetFields("qty")) * Convert.ToDouble(clCL.GetFields("cost")) * (Convert.ToDouble(clCL.GetFields("tax")) / 100)).ToString() + ",";
                    }
                    else
                    {
                        SQL = SQL + "0,";
                    }

                    SQL = SQL + clCL.GetFields("totalamt") + ",";
                    SQL = SQL + "'Requested', ";
                    SQL = SQL + lUserID + ",";
                    SQL = SQL + "'" + DateTime.Today + "', ";
                    SQL = SQL + "'" + DateTime.Today + "', ";
                    SQL = SQL + lUserID + ",";
                    SQL = SQL + lClaimPayeeID + ",";
                    SQL = SQL + "'" + clCL.GetFields("labordesc") + "')";
                    clR.RunSQL(SQL, Global.sCON);
                }
            }
        }
        private void InsertClaimDetailPart()
        {
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from veritasclaims.dbo.claimpart where claimid = " + clVC.GetFields("claimid");
            clCP.OpenDB(SQL, Global.sCON);
            if (clCP.RowCount() > 0)
            {
                for (int cnt = 0; cnt <= clCP.RowCount() - 1; cnt++)
                {
                    clCP.GetRowNo(cnt);
                    SQL = "insert into claimdetail " +
                          "(claimid, claimdetailtype, jobno, reqqty, reqcost, reqamt, taxper, taxamt, totalamt, claimdetailstatus, creby, credate, moddate, modby, claimpayeeid, claimdesc, PartNo) " +
                          "values (" + lClaimID + ", " +
                          "'Part', 'J01', " +
                          clCP.GetFields("qty") + "," +
                          clCP.GetFields("cost") + "," +
                          (Convert.ToDouble(clCP.GetFields("qty")) * Convert.ToDouble(clCP.GetFields("cost"))).ToString() +
                          clCP.GetFields("tax") + "," +
                          (Convert.ToDouble(clCP.GetFields("qty")) * Convert.ToDouble(clCP.GetFields("cost")) * (Convert.ToDouble(clCP.GetFields("tax")) / 100)).ToString() + "," +
                          clCP.GetFields("totalamt") + "," +
                          dDeductAmt.ToString() + ",'Requested', " + lUserID + "," +
                          "'" + DateTime.Today.ToString() + "', " +
                          "'" + DateTime.Today.ToString() + "', " + lUserID + "," + lClaimPayeeID + "," +
                          "'" + clCP.GetFields("partdesc") + "'," +
                          "'" + clCP.GetFields("partno") + "')";
                    clR.RunSQL(SQL, Global.sCON);
                }
            }
        }
        private void InsertClaimDetailDeduct()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            dDeductAmt = 0;
            GetDeduct();
            dDeductAmt = dDeductAmt * -1;
            SQL = "insert into claimdetail " +
                  "(claimid, claimdetailtype, jobno, reqqty, reqcost, reqamt, taxper, taxamt, totalamt, claimdetailstatus, creby, credate, moddate, modby, claimpayeeid, claimdesc) " +
                  "values (" + lClaimID + ", " +
                  "'Deductible', 'A01', 1, " +
                  dDeductAmt.ToString() + ", " +
                  dDeductAmt.ToString() + ", " + "0, 0, " +
                  dDeductAmt.ToString() + ",'Requested', " +
                  lUserID.ToString() + "," +
                  "'" + DateTime.Today.ToString() + "', " +
                  "'" + DateTime.Today.ToString() + "', " +
                  lUserID.ToString() + "," +
                  lClaimPayeeID.ToString() + ",'')";
            clR.RunSQL(SQL, Global.sCON);
        }
        private void GetDeduct()
        {
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            clsDBO.clsDBO clD = new clsDBO.clsDBO();
            SQL = "select deductid from contract " +
                  "where contractid = " + clVC.GetFields("contractid");
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                SQL = "select * from deductible " +
                      "where deductid = " + clC.GetFields("deductid");
                clD.OpenDB(SQL, Global.sCON);
                if (clD.RowCount() > 0)
                {
                    clD.GetRow();
                    dDeductAmt = Convert.ToDouble(clD.GetFields("deductamt"));
                }
            }
        }
        private void CheckClaimPayee()
        {
            clsDBO.clsDBO clSC = new clsDBO.clsDBO();
            clsDBO.clsDBO clCP = new clsDBO.clsDBO();
            SQL = "select * from servicecenter " +
                  "where servicecenterid = " + clVC.GetFields("servicecenterid");
            clSC.OpenDB(SQL, Global.sCON);
            if (clSC.RowCount() > 0)
            {
                clSC.GetRow();
            MoveHere:;
                SQL = "select * from claimpayee " +
                      "where payeeno = '" + clSC.GetFields("servicecenterno") + "' ";
                clCP.OpenDB(SQL, Global.sCON);
                if (clCP.RowCount() > 0)
                {
                    clCP.GetRow();
                    lClaimPayeeID = long.Parse(clCP.GetFields("claimpayeeid"));
                }
                else
                {
                    clCP.NewRow();
                    clCP.SetFields("payeeno", clSC.GetFields("servicecenterno"));
                    clCP.SetFields("payeename", clSC.GetFields("servicecentername"));
                    clCP.SetFields("addr1", clSC.GetFields("addr1"));
                    clCP.SetFields("addr2", clSC.GetFields("addr2"));
                    clCP.SetFields("city", clSC.GetFields("city"));
                    clCP.SetFields("state", clSC.GetFields("state"));
                    clCP.SetFields("zip", clSC.GetFields("zip"));
                    clCP.SetFields("phone", clSC.GetFields("phone"));
                    clCP.AddRow();
                    clCP.SaveDB();
                    goto MoveHere;
                }
            }
        }
        private void GetClaimNo()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            string sClaimNo;
            long lClaimNo = 0;
        MoveHere:;
            sClaimNo = "";
            SQL = "select max(claimno) as claimno from claim where claimno like 'C1%' ";
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sClaimNo = clC.GetFields("claimno");
                sClaimNo = sClaimNo.Replace("C1", "");
                lClaimNo = long.Parse(sClaimNo);
                lClaimNo = lClaimNo + 1;
                sClaimNo = lClaimNo.ToString().Insert(0, 1000.ToString());//string.Format(lClaimNo.ToString(), "100000000");
                sClaimNo = "C" + sClaimNo;
            }
            if (sClaimNo.Length > 0)
            {
                SQL = "select * from claim " +
                      "where claimno = '" + sClaimNo + "' ";
                clC.OpenDB(SQL, Global.sCON);
                if (clC.RowCount() > 0)
                {
                    goto MoveHere;
                }
                else
                {
                    clC.NewRow();
                    clC.SetFields("claimno", sClaimNo);
                    clC.SetFields("status", "Open");
                    if (clVC.GetFields("contractid").Length > 0)
                    {
                        clC.SetFields("contractid", clVC.GetFields("contractid"));
                    }
                    else
                    {
                        clC.SetFields("contractid", 0.ToString());
                    }
                    clC.SetFields("lossdate", DateTime.Today.ToString());
                    clC.SetFields("claimactivityid", 1.ToString());
                    clC.SetFields("servicecenterid", clVC.GetFields("servicecenterid"));
                    clC.SetFields("sccontactinfo", clVC.GetFields("contactname") + " " + clVC.GetFields("contactphone") + " " + clVC.GetFields("cell"));
                    if (clVC.GetFields("odometer").Length > 0)
                    {
                        clC.SetFields("lossmile", clVC.GetFields("odometer"));
                    }
                    clC.SetFields("ronumber", clVC.GetFields("ronumber"));
                    clC.SetFields("creby", lUserID.ToString());
                    clC.SetFields("assignedto", lAssignedToID.ToString());
                    clC.SetFields("Modby", lUserID.ToString());
                    clC.SetFields("credate", DateTime.Today.ToString());
                    clC.SetFields("moddate", DateTime.Today.ToString());
                    clC.SetFields("addnew", true.ToString());
                    clC.AddRow();
                    clC.SaveDB();
                }
            }
            SQL = "select * from claim " +
                  "where claimno = '" + sClaimNo + "' ";
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                sNewClaimNo = clC.GetFields("claimno");
                lClaimID = long.Parse(clC.GetFields("claimid"));
            }

            SQL = "update veritasclaims.dbo.claim " +
                  "set claimno = '" + sNewClaimNo + "' " +
                  "where claimid = " + clVC.GetFields("claimid");
            clC.RunSQL(SQL, Global.sCON);
            SQL = "insert into claimopenhistory " +
                  "(claimid, opendate, openby) " +
                  "values (" + clC.GetFields("claimid") + ",'" +
                  DateTime.Today.ToString() + "'," +
                  clC.GetFields("assignedto") + ")";
            clVC.RunSQL(SQL, Global.sCON);
        }
    }
}
