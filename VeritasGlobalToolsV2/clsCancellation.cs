﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsCancellation
    {
        private long lContractID;
        private string sCancelDate;
        private double dClaims;
        private double dDealerAmt;
        private double dAdminAmt;
        private double dCustomer;
        private clsDBO.clsDBO clC = new clsDBO.clsDBO();
        private clsDBO.clsDBO clD = new clsDBO.clsDBO();
        private clsDBO.clsDBO clCSR = new clsDBO.clsDBO();
        private clsDBO.clsDBO clCC = new clsDBO.clsDBO();
        private clsDBO.clsDBO clP = new clsDBO.clsDBO();
        private string SQL;
        private long lUsedDays;
        private double dCancelPer;
        private double dCancelFee;
        private double dCancelTermPer;
        private string sFormAddress;
        private string sFormAddressOut;
        private string sHTTPAddress;
        private bool bQuote;
        private string sCancelInfo;
        private double dGrossCustomer;
        private double dGrossDealer;
        private double dGrossAdmin;
        private bool bFLCustomer;
        private bool bFLAdmin;
        private double CalcClaim;

        public long ContractID
        {
            get
            {
                return lContractID;
            }
            set
            {
                lContractID = value;
            }
        }
        public string CancelDate
        {
            get
            {
                return sCancelDate;
            }
            set
            {
                sCancelDate = value;
            }
        }
        
        public double CancelFee
        {
            get
            {
                return dCancelFee;
            }
        }
        public string CancelInfo
        {
            get
            {
                return sCancelInfo;
            }
        }
       
        public double CancelTermPer
        {
            get
            {
                return dCancelTermPer;
            }
        }
        public double CancelPer
        {
            get
            {
                return dCancelPer;
            }
        }
        public double Claims
        {
            get
            {
                return dClaims;
            }
        }
        public double DealerAmt
        {
            get
            {
                return dDealerAmt;
            }
        }
        public double AdminAmt
        {
            get
            {
                return dAdminAmt;
            }
        }
        public string FormAddress
        {
            get
            {
                return sFormAddress;
            }
            set
            {
                sFormAddress = value;
            }
        }
        public string HTTPAddress
        {
            get
            {
                return sHTTPAddress;
            }
            set
            {
                sHTTPAddress = value;
            }
        }
        public bool FLCustomer
        {
            get
            {
                return bFLCustomer;
            }
            set
            {
                bFLCustomer = value;
            }
        }
        public bool FLAdmin
        {
            get
            {
                return bFLAdmin;
            }
            set
            {
                bFLAdmin = value;
            }
        }
        public string FormAddressOut
        {
            get
            {
                return sFormAddressOut;
            }
            set
            {
                sFormAddressOut = value;
            }
        }
        public bool Quote
        {
            get
            {
                return bQuote;
            }
            set
            {
                bQuote = value;
            }
        }
        public double CustomerCost
        {
            get
            {
                return dCustomer;
            }
        }
        public double GrossDealer
        {
            get
            {
                return dGrossDealer;
            }
        }
        public double GrossAdmin
        {
            get
            {
                return dGrossAdmin;
            }
        }
        public double GrossCustomer
        {
            get
            {
                return dGrossCustomer;
            }
        }

        private void GetCancelTable()
        {
            SQL = "select * from contractcancel " + 
                  "where contractid = " + lContractID;
            clCC.OpenDB(SQL, Global.sCON);
        }
        private void OpenContract()
        {
            SQL = "select * from contract " +
                  "where contractid = " + lContractID;
            clC.OpenDB(SQL, Global.sCON);
        }
        public void CalculateCancellation()
        {
            sCancelInfo = "";
            GetContractInfo();
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                GetDealerInfo();
                if (clD.RowCount() > 0)
                {
                    clD.GetRow();
                    GetCancelStateRules();
                    if (clCSR.RowCount() > 0)
                    {
                        clCSR.GetRow();
                        CalcCancel();
                    }
                }
            }
        }
        private void GetContractInfo()
        {
            SQL = "select * from contract " + 
                  "where contractid = " + lContractID;
            clC.OpenDB(SQL, Global.sCON);
        }
        private void GetDealerInfo()
        {
            SQL = "select * from dealer " +
                  "where dealerid = " + clC.GetFields("dealerid");
            clD.OpenDB(SQL, Global.sCON);
        }
        private void GetCancelStateRules()
        {
            SQL = "select * from cancelstaterules " +
                  "where state = '" + clC.GetFields("state") + "' ";
            clCSR.OpenDB(SQL, Global.sCON);
        }
        private void CalcCancel()
        {
            dAdminAmt = Convert.ToDouble(clC.GetFields("moxydealercost"));

            dDealerAmt = Convert.ToDouble(clC.GetFields("customercost")) - Convert.ToDouble(clC.GetFields("MoxyDealerCost"));

            if (Convert.ToBoolean(clCSR.GetFields("noclaimdeduct")) == true)
            {
                dClaims = 0;
            }
            else
            {
                dClaims = CalcClaims();
            }
            if (clC.GetFields("lienholder").ToLower() == "paylink" || clC.GetFields("lienholder").ToLower() == "mepco" || clC.GetFields("lienholder").ToLower() == "omnisure")
            {
                dClaims = 0;
            }
            if (dClaims == 86 || dClaims == 136 || dClaims == 172 || dClaims == 222 || dClaims == 258)
            {
                dClaims = 0;
            }
            lUsedDays = (DateTime.Parse(sCancelDate) - DateTime.Parse(clC.GetFields("saledate"))).Days;

            if (lUsedDays <= long.Parse(clCSR.GetFields("maxdays")))
            {
                dCancelFee = 0;
                if (Convert.ToBoolean(clCSR.GetFields("noclaimdeduct")) == false)
                {
                    dAdminAmt = dAdminAmt - dClaims;
                    if (dAdminAmt < 0)
                    {
                        dDealerAmt = dDealerAmt - dAdminAmt;
                        dAdminAmt = 0;
                    }
                    if (dDealerAmt < 0)
                    {
                        dDealerAmt = 0;
                    }
                }
                dGrossCustomer = dDealerAmt + dAdminAmt;
                dGrossAdmin = dAdminAmt;
                dGrossDealer = dDealerAmt;
                dCancelTermPer = 1;
                dCancelPer = 1;
                dCustomer = dGrossCustomer;
                return;
            }
            

            CalcCancelPer();
            dDealerAmt = dDealerAmt * dCancelPer;
            dAdminAmt = dAdminAmt * dCancelPer;
            dGrossCustomer = dDealerAmt + dAdminAmt;
            dGrossAdmin = dAdminAmt;
            dGrossDealer = dDealerAmt;

            CalcCancelFee();
            if (dCancelPer == 1)
            {
                dCancelFee = 0;
            }
            if (clC.GetFields("state") == "FL")
            {
                if (bFLAdmin)
                {
                    dCancelFee = 0;
                }
            }
            dAdminAmt = dAdminAmt - dCancelFee;
            if (Convert.ToBoolean(clC.GetFields("idtheft")))
            {
                dAdminAmt = dAdminAmt - 10;
            }
            if (Convert.ToBoolean(clCSR.GetFields("noclaimdeduct")) == false)
            {
                dAdminAmt = dAdminAmt - dClaims;
                if (dAdminAmt < 0)
                {
                    dDealerAmt = dDealerAmt + dAdminAmt;
                    dAdminAmt = 0;
                }
                if (dDealerAmt < 0)
                {
                    dDealerAmt = 0;
                }
            }
            dCustomer = dAdminAmt + dDealerAmt;
            if (Convert.ToBoolean(clCSR.GetFields("regulated")) == true)
            {
                if (Convert.ToInt32(clD.GetFields("dealerstatusid")) == 5)
                {
                    dAdminAmt = dAdminAmt + dDealerAmt;
                    dCustomer = dAdminAmt;
                }
            }
            if (dAdminAmt < 0)
            { 
                 dAdminAmt = 0;
            }
            if (dDealerAmt < 0)
            {
                dDealerAmt = 0;
            }
        }
        private double CalcClaims()
        {
            clsDBO.clsDBO clCL = new clsDBO.clsDBO();
            CalcClaim = 0;
            SQL = "select sum(cd.paidamt) as PaidAmt from claim cl " +
                  "inner join claimdetail cd on cl.ClaimID = cd.claimid " +
                  "inner join contract c on c.contractid = cl.ContractID " +
                  "where contractno = '" + clC.GetFields("contractno") + "' " +
                  "and cl.Status = 'Paid' " +
                  "and cd.JobNo <> 'A03' ";
            clCL.OpenDB(SQL, Global.sCON);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
                if (clCL.GetFields("paidamt").Length > 0)
                {
                    CalcClaim = Convert.ToDouble(clCL.GetFields("paidamt"));
                }
            }
            return CalcClaim;
        }
        private void CalcCancelFee()
        {
            double dCancelFeeTemp;
            if (clCSR.GetFields("cancelpercent") == "0")
            {
                dCancelFee = Convert.ToDouble(clCSR.GetFields("cancelfee"));
                return;
            }
            dCancelFeeTemp = (Convert.ToDouble(clC.GetFields("customercost")) - (Convert.ToDouble(clC.GetFields("customercost")) * dCancelPer)) * Convert.ToDouble(clCSR.GetFields("cancelpercent"));

            if (dCancelFeeTemp < Convert.ToDouble(clCSR.GetFields("cancelfee")) && dCancelFeeTemp > 0) 
            {
                dCancelFee = dCancelFeeTemp;
            }
            else
            {
                if (Convert.ToDouble(clCSR.GetFields("cancelfee")) > 0)
                {
                    dCancelFee = Convert.ToDouble(clCSR.GetFields("cancelfee"));
                }
                else
                {
                    dCancelFee = dCancelFeeTemp;
                }
            }
        }
        private void CalcCancelPer()
        {
            double dCanTime;
            double lTermDays;

            //lTermDays = DateDiff(DateInterval.Day, CDate(Format(CDate(clC.Fields("saledate")), "M/d/yyy")), CDate(clC.Fields("expdate")))
            lTermDays = (DateTime.Parse(clC.GetFields("expdate")) - DateTime.Parse(clC.GetFields("saledate"))).Days;
            dCanTime = 1 - (lUsedDays / (double)lTermDays);
            if (dCanTime < 0)
            {
                dCanTime = 0;
                sCancelInfo = "Cancel Date is larger than Exp. Date";
            }
            dCancelTermPer = dCanTime;
            dCancelPer = dCancelTermPer;
        }

    }
}
