﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop;

namespace VeritasGlobalToolsV2
{
    public class clsGAPDocuments
    {
        private long lClaimGapID;
        private string sDirLoc;
        private clsDBO.clsDBO clCL = new clsDBO.clsDBO();
        private clsDBO.clsDBO clC = new clsDBO.clsDBO();
        private string sFileName;
        private long lUserID;
        private clsDBO.clsDBO clL = new clsDBO.clsDBO();
        private clsDBO.clsDBO clGC = new clsDBO.clsDBO();
        public long UserID
        {
            get
            {
                return lUserID;
            }
            set
            {
                lUserID = value;
            }
        }
        public string DirLoc
        {
            get
            {
                return sDirLoc;
            }
            set
            {
                sDirLoc = value;
            }
        }
        public long ClaimGapID
        {
            get
            {
                return lClaimGapID;
            }
            set
            {
                lClaimGapID = value;
            }
        }

        public void CreateGAPClaimNotice()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAPClaimNotice.docx");
            GetClaim();
            if (clCL.RowCount() > 0)
            {
                GetContract();
                if (clC.RowCount() > 0)
                {
                    doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                    doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                    doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                    if (clC.GetFields("addr2").Length > 0)
                    {
                        doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                        doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    }
                    else
                    {
                        doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                        doc.Bookmarks["addr3"].Range.Text = "";
                    }
                    doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                }
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo2"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo3"].Range.Text = clCL.GetFields("claimno");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - ClaimNotice.docx";
            }
            doc.Bookmarks["VeritasAddr"].Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012";
            doc.Bookmarks["Fax"].Range.Text = "913-904-3450";
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-572-4310";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            doc.Bookmarks["EMail2"].Range.Text = GetUserEMail();

        MoveHere:;
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + " - ClaimNotice.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(7);
            AddDocStatus(7);
        }

        public void CreateGAP60DayLetter()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAP60DayLetter.docx");
            GetClaim();
            GetContract();
            if (clC.RowCount() > 0)
            {
                doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                if (clC.GetFields("addr2").Length > 0)
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                    doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    doc.Bookmarks["addr3"].Range.Text = "";
                }
                doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo2"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo3"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["LossMile"].Range.Text = clCL.GetFields("lossmile");
                doc.Bookmarks["LossDate"].Range.Text = clCL.GetFields("lossdate");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - GAP60DayLetter.docx";
            }
            doc.Bookmarks["VeritasAddr"].Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012";
            doc.Bookmarks["Fax"].Range.Text = "913-904-3450";
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-285-5950";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            doc.Bookmarks["EMail2"].Range.Text = GetUserEMail();
            OpenLoanInfo();
            if (clL.RowCount() > 0)
            {
                doc.Bookmarks["BankName"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankAddr1"].Range.Text = clL.GetFields("bankaddr1");
                if (clL.GetFields("bankaddr2").Length > 0)
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("bankaddr2");
                    doc.Bookmarks["BankAddr3"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                    doc.Bookmarks["BankAddr3"].Range.Text = "";
                }
            }
        MoveHere:
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + " - GAP60DayLetter.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(8);
            AddDocStatus(8);
        }
        public void CreateDenialLetter()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAPDenialLetter.docx");
            GetClaim();
            GetContract();
            if (clC.RowCount() > 0)
            {
                doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                if (clC.GetFields("addr2").Length > 0)
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                    doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    doc.Bookmarks["addr3"].Range.Text = "";
                }
                doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo2"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["LossMile"].Range.Text = clCL.GetFields("lossmile");
                doc.Bookmarks["LossDate"].Range.Text = clCL.GetFields("lossdate");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - GAPDenialLetter.docx";
            }
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-285-5950";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            OpenLoanInfo();
            if (clL.RowCount() > 0)
            {
                doc.Bookmarks["BankName"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankAddr1"].Range.Text = clL.GetFields("bankaddr1");
                if (clL.GetFields("bankaddr2").Length > 0)
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("bankaddr2");
                    doc.Bookmarks["BankAddr3"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                    doc.Bookmarks["BankAddr3"].Range.Text = "";
                }
            }
        MoveHere:;
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + " - GAPDenialLetter.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(9);
            AddDocStatus(9);

        }
        public void CreateMissedOptions()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAPMissedOptions.docx");
            GetClaim();
            GetContract();
            if (clC.RowCount() > 0)
            {
                doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                if (clC.GetFields("addr2").Length > 0)
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                    doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    doc.Bookmarks["addr3"].Range.Text = "";
                }
                doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo2"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo3"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["LossMile"].Range.Text = clCL.GetFields("lossmile");
                doc.Bookmarks["LossDate"].Range.Text = clCL.GetFields("lossdate");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - GAPMissedOptions.docx";
            }
            doc.Bookmarks["VeritasAddr"].Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012";
            doc.Bookmarks["Fax"].Range.Text = "913-904-3450";
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-285-5950";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            OpenLoanInfo();
            if (clL.RowCount() > 0)
            {
                doc.Bookmarks["BankName"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankAddr1"].Range.Text = clL.GetFields("bankaddr1");
                if (clL.GetFields("bankaddr2").Length > 0)
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("bankaddr2");
                    doc.Bookmarks["BankAddr3"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                    doc.Bookmarks["BankAddr3"].Range.Text = "";
                }
            }
        MoveHere:
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + "-GAPMissedOptions.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(10);
            AddDocStatus(10);
        }
        public void CreateGAPNoGAPDueLetter()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAPNoGAPDueLetter.docx");
            GetClaim();
            GetContract();
            if (clC.RowCount() > 0)
            {
                doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                if (clC.GetFields("addr2").Length > 0)
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                    doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    doc.Bookmarks["addr3"].Range.Text = "";
                }
                doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["LossMile"].Range.Text = clCL.GetFields("lossmile");
                doc.Bookmarks["LossDate"].Range.Text = clCL.GetFields("lossdate");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - GAPNoGAPDueLetter.docx";
            }
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-285-5950";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            OpenLoanInfo();
            if (clL.RowCount() > 0)
            {
                doc.Bookmarks["BankName"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankName2"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankAddr1"].Range.Text = clL.GetFields("bankaddr1");
                if (clL.GetFields("bankaddr2").Length > 0)
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("bankaddr2");
                    doc.Bookmarks["BankAddr3"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                    doc.Bookmarks["BankAddr3"].Range.Text = "";
                }
            }
            OpenGapCalc();
            if (clGC.RowCount() > 0)
            {
                doc.Bookmarks["ScheduleLoanBalance"].Range.Text = clGC.GetFields("ScheduleLoanBalance");
                doc.Bookmarks["InterestEarned"].Range.Text = clGC.GetFields("InterestEarned");
                doc.Bookmarks["MechanicalContractRefunds"].Range.Text = clGC.GetFields("MechanicalContractRefunds");
                doc.Bookmarks["CreditInsRefunds"].Range.Text = clGC.GetFields("CreditInsRefunds");
                doc.Bookmarks["ACV"].Range.Text = clGC.GetFields("ACV");
                doc.Bookmarks["OwnerSalvage"].Range.Text = clGC.GetFields("OwnerSalvage");
                doc.Bookmarks["NADALoss"].Range.Text = clGC.GetFields("NADALoss");
                doc.Bookmarks["AmountExceding150"].Range.Text = clGC.GetFields("AmountExceding150");
                doc.Bookmarks["UnearnedGapRefund"].Range.Text = clGC.GetFields("UnearnedGapRefund");
                doc.Bookmarks["InsDeduct"].Range.Text = clGC.GetFields("InsDeduct");
                doc.Bookmarks["OtherDeduct"].Range.Text = clGC.GetFields("OtherDeduct");
                doc.Bookmarks["GapPayout"].Range.Text = clGC.GetFields("GapPayout");
            }
        MoveHere:;
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + "-GAPNoGAPDueLetter.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(11);
            AddDocStatus(11);
        }
        public void CreatePaymentLetter()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAPPaymentLetter.docx");
            GetClaim();
            GetContract();
            if (clC.RowCount() > 0)
            {
                doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                if (clC.GetFields("addr2").Length > 0)
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                    doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    doc.Bookmarks["addr3"].Range.Text = "";
                }
                doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["LossMile"].Range.Text = clCL.GetFields("lossmile");
                doc.Bookmarks["LossDate"].Range.Text = clCL.GetFields("lossdate");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - GAPPaymentLetter.docx";
            }
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-285-5950";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            OpenLoanInfo();
            if (clL.RowCount() > 0)
            {
                doc.Bookmarks["BankName"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankName2"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankAddr1"].Range.Text = clL.GetFields("bankaddr1");
                if (clL.GetFields("bankaddr2").Length > 0)
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("bankaddr2");
                    doc.Bookmarks["BankAddr3"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                    doc.Bookmarks["BankAddr3"].Range.Text = "";
                }
            }
            OpenGapCalc();
            if (clGC.RowCount() > 0)
            {
                doc.Bookmarks["ScheduleLoanBalance"].Range.Text = clGC.GetFields("ScheduleLoanBalance");
                doc.Bookmarks["InterestEarned"].Range.Text = clGC.GetFields("InterestEarned");
                doc.Bookmarks["MechanicalContractRefunds"].Range.Text = clGC.GetFields("MechanicalContractRefunds");
                doc.Bookmarks["CreditInsRefunds"].Range.Text = clGC.GetFields("CreditInsRefunds");
                doc.Bookmarks["ACV"].Range.Text = clGC.GetFields("ACV");
                doc.Bookmarks["OwnerSalvage"].Range.Text = clGC.GetFields("OwnerSalvage");
                doc.Bookmarks["NADALoss"].Range.Text = clGC.GetFields("NADALoss");
                doc.Bookmarks["AmountExceding150"].Range.Text = clGC.GetFields("AmountExceding150");
                doc.Bookmarks["UnearnedGapRefund"].Range.Text = clGC.GetFields("UnearnedGapRefund");
                doc.Bookmarks["InsDeduct"].Range.Text = clGC.GetFields("InsDeduct");
                doc.Bookmarks["OtherDeduct"].Range.Text = clGC.GetFields("OtherDeduct");
                doc.Bookmarks["GapPayout"].Range.Text = clGC.GetFields("GapPayout");
            }
        MoveHere:;
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + "-GAPPaymentLetter.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(12);
            AddDocStatus(12);
        }
        public void CreateStatusLetter()
        {
            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
            long lDocNum = 1;
            doc = app.Documents.Open(sDirLoc + "GAPDocs\\GAP60DayLetter.docx");
            GetClaim();
            GetContract();
            if (clC.RowCount() > 0)
            {
                doc.Bookmarks["HolderName"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["HolderName2"].Range.Text = clC.GetFields("fname") + " " + clC.GetFields("lname");
                doc.Bookmarks["addr1"].Range.Text = clC.GetFields("addr1");
                if (clC.GetFields("addr2").Length > 0)
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("addr2");
                    doc.Bookmarks["addr3"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["addr2"].Range.Text = clC.GetFields("city") + " " + clC.GetFields("state") + " " + clC.GetFields("zip");
                    doc.Bookmarks["addr3"].Range.Text = "";
                }
                doc.Bookmarks["VIN"].Range.Text = clC.GetFields("vin");
                doc.Bookmarks["ClaimNo1"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo2"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["ClaimNo3"].Range.Text = clCL.GetFields("claimno");
                doc.Bookmarks["LossMile"].Range.Text = clCL.GetFields("lossmile");
                doc.Bookmarks["LossDate"].Range.Text = clCL.GetFields("lossdate");
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + " - GAP60DayLetter.docx";
            }
            doc.Bookmarks["VeritasAddr"].Range.Text = "3550 N Central Ave, STE 800, Phoenix, AZ, 85012";
            doc.Bookmarks["Fax"].Range.Text = "913-904-3450";
            doc.Bookmarks["TodayDate"].Range.Text = DateTime.Today.ToString();
            doc.Bookmarks["GapPhone"].Range.Text = "888-285-5950";
            doc.Bookmarks["GapPhone2"].Range.Text = "888-285-5950";
            doc.Bookmarks["EMail"].Range.Text = GetUserEMail();
            doc.Bookmarks["EMail2"].Range.Text = GetUserEMail();
            OpenLoanInfo();
            if (clL.RowCount() > 0)
            {
                doc.Bookmarks["BankName"].Range.Text = clL.GetFields("bankname");
                doc.Bookmarks["BankAddr1"].Range.Text = clL.GetFields("bankaddr1");
                if (clL.GetFields("bankaddr2").Length > 0)
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("bankaddr2");
                    doc.Bookmarks["BankAddr3"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                }
                else
                {
                    doc.Bookmarks["BankAddr2"].Range.Text = clL.GetFields("city") + " " + clL.GetFields("state") + " " + clL.GetFields("zip");
                    doc.Bookmarks["BankAddr3"].Range.Text = "";
                }
            }
        MoveHere:;
            if (System.IO.File.Exists(sDirLoc + sFileName))
            {
                lDocNum = lDocNum + 1;
                sFileName = "documents\\gap\\" + clCL.GetFields("claimno") + lDocNum + " - GAP60DayLetter.docx";
                goto MoveHere;
            }
            doc.SaveAs2(sDirLoc + sFileName);
            AddDocument(13);
            AddDocStatus(13);
        }

        private void OpenGapCalc()
        {
            string SQL;
            SQL = "select * from claimgapcalc where claimgapid = " + lClaimGapID;
            clGC.OpenDB(SQL, Global.sCON);
            if (clGC.RowCount() > 0)
            {
                clGC.GetRow();
            }
        }
        private void OpenLoanInfo()
        {
            string SQL;
            SQL = "select * from claimgaploaninfo where claimgapid = " + lClaimGapID;
            clL.OpenDB(SQL, Global.sCON);
            if (clL.RowCount() > 0)
            {
                clL.GetRow();
            }
        }
        private void GetClaim()
        {
            string SQL;
            SQL = "select * from claimgap where claimgapid = " + lClaimGapID;
            clCL.OpenDB(SQL, Global.sCON);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
            }
        }
        private void GetContract()
        {
            string SQL;
            SQL = "select * from contract where contractid = " + clCL.GetFields("contractid");
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
            }
        }
        private string GetUserEMail()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select email from userinfo where userid = " + lUserID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return clR.GetFields("email");
            }
            return "";
        }
        private void AddDocument(long xDocType)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgapdocument where claimgapid = " + lClaimGapID;
            clR.OpenDB(SQL, Global.sCON);
            clR.NewRow();
            clR.SetFields("claimgapid", lClaimGapID.ToString());
            clR.SetFields("claimgapdoctypeid", xDocType.ToString());
            if (xDocType == 7)
            {
                clR.SetFields("DocumentName", "Claim Notice");
                clR.SetFields("documentdesc", "Claim Notice");
            }                                                              
            clR.SetFields("documentlink", "~\\" + sFileName);
            clR.SetFields("creby", lUserID.ToString());
            clR.SetFields("credate", DateTime.Today.ToString());
            clR.AddRow();                                      
            clR.SaveDB();
        }
        private void AddDocStatus(long xDocType)
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimgapdocstatus where claimgapid = " + lClaimGapID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() == 0)
            {
                clR.NewRow();
                clR.SetFields("claimgapid", lClaimGapID.ToString());
            }
            else
            {
                clR.GetRow();
            }
            if (xDocType == 7)
            {
                clR.SetFields("gapclaimnotice", true.ToString());
            }
            if (clR.RowCount() == 0)
            {
                clR.AddRow();
            }
            clR.SaveDB();
        }

    }
}
