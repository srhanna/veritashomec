﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsSendTASAEMail
    {
        private long lClaimDetailID;
        private long lUserID;
        private long lClaimID;
        private string sTicketNo;
        private clsDBO.clsDBO clUI = new clsDBO.clsDBO();
        private string SQL;
        private clsDBO.clsDBO clC = new clsDBO.clsDBO();
        private clsDBO.clsDBO clCD = new clsDBO.clsDBO();
        private clsDBO.clsDBO clCL = new clsDBO.clsDBO();
        private clsDBO.clsDBO clPT = new clsDBO.clsDBO();
        private clsDBO.clsDBO clD = new clsDBO.clsDBO();
        private clsDBO.clsDBO clSC = new clsDBO.clsDBO();
        private string sComplaint;
        private string sCorrective;
        private string sCause;
        private bool bTruPic;
        private string sLink;
        private bool bInspection;
        private string sAdvisorName;

        public long UserID
        {
            get
            {
                return lUserID;
            }
            set
            {
                lUserID = value;
            }
        }
        public string TicketNo
        {
            get
            {
                return sTicketNo;
            }
            set
            {
                sTicketNo = value;
            }
        }
        public long ClaimDetailID
        {
            get
            {
                return lClaimDetailID;
            }
            set
            {
                lClaimDetailID = value;
            }
        }
        public long ClaimID
        {
            get
            {
                return lClaimID;
            }
            set
            {
                lClaimID = value;
            }
        }
        
        public void SendEMail()
        {
            GetUserName();
            SendTo(clUI.GetFields("email"));
            SendTo("steve@hannaworld.com");
            SendTo("tylerrockey@tasaky.com");
            SendTo("travisscott@tasaky.com");
            SendTo("jakemuck@tasaky.com");
            SendTo("zpeters@CLAIMSPROCESSINGDEPT.COM");
            SendTo("mhansen@CLAIMSPROCESSINGDEPT.COM");
        }

        private void SendTo(string xEMail)
        {
            string sBody;

            try
            {
                GetUserName();
                GetClaim();
                GetContract();
                GetDealer();
                GetPlanType();
                GetClaimDetail();
                GetServiceCenter();
                GetComplaint();
                GetCause();
                GetCorrective();
                GetTruPic();
                GetInspection();
                SmtpClient client = new SmtpClient("smtp.office365.com", 587);
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("info@veritasglobal.com", "SilverSurfer3");
                client.EnableSsl = true;

                MailMessage mail = new MailMessage();
                mail.To.Add(xEMail);
                mail.From = new MailAddress("info@veritasglobal.com");
                mail.Subject = clCL.GetFields("claimno") + "";
                mail.IsBodyHtml = true;
                sBody = "Ticket No: " + sTicketNo + "</br>";
                sBody = sBody + "Contract Coverage: " + clPT.GetFields("plantype") + "</br>";
                sBody = sBody + "Claim Number: " + clCL.GetFields("claimno") + "</br>";
                sBody = sBody + "Contract No: " + clC.GetFields("contractno") + "</br>";
                sBody = sBody + "Customer Name: " + clC.GetFields("fname") + " " + clC.GetFields("lname") + "</br>";
                sBody = sBody + "Vehicle Make and Model: " + clC.GetFields("make") + " " + clC.GetFields("model") + "</br>";
                sBody = sBody + "Date Sold: " + clC.GetFields("saledate") + "</br>";
                sBody = sBody + "Date of Claim: " + clCL.GetFields("credate") + "</br>";
                //sBody = sBody + "Days and Miles into Contract: " + DateDiff(DateInterval.Day, CDate(clC.GetFields("saledate")), CDate(clCL.GetFields("lossdate"))) + " / ";
                sBody = sBody + "Days and Miles into Contract: " + (DateTime.Parse(clCL.GetFields("lossdate")) - DateTime.Parse(clC.GetFields("saledate"))).ToString() + " / ";
                sBody = sBody + (Convert.ToDouble(clCL.GetFields("lossmile")) - Convert.ToDouble(clC.GetFields("effmile"))) + "</br>";
                sBody = sBody + "Aftersale (y/n): ";
                if (clC.GetFields("contractno").Contains("VEP"))
                {
                    sBody = sBody + "Y" + "</br>";
                }
                else
                {
                    if (clC.GetFields("contractno").Contains("REP"))
                    {
                        sBody = sBody + "Y" + "</br>";
                    }
                    else
                    {
                        sBody = sBody + "N" + "</br>";
                    }
                }
                sBody = sBody + "Selling Dealer: " + clD.GetFields("dealername") + "</br>";
                sBody = sBody + "Repair Facility: " + clSC.GetFields("servicecentername") + "</br>";
                if (clSC.GetFields("dealerno").Length > 0)
                {
                    if (clSC.GetFields("dealerno").Substring(0, 1) == "2")
                    {
                        sBody = sBody + "RF in AN Network (y/n): Y " + "</br>";
                    }
                    else
                    {
                        sBody = sBody + "RF in AN Network (y/n): N " + "</br>";
                    }
                }
                else
                {
                    sBody = sBody + "RF in AN Network (y/n): N " + "</br>";
                }
                sBody = sBody + "RF in Prime Network (y/n): N" + "</br>";
                sBody = sBody + "RF in Cooper Network (y/n): N" + "</br>";
                sBody = sBody + "Claims on Contract: " + (CountClaim() - 1) + "</br>";
                sBody = sBody + "Claims Total Paid: " + GetClaimPaidAmt().ToString("C") + "</br>";
                sBody = sBody + "Estimate: " + GetEstimate().ToString("C") + "</br>";
                sBody = sBody + "Failures: " + clCD.GetFields("ClaimDesc") + "</br>";
                sBody = sBody + "Tech finds: " + "</br>";
                sBody = sBody + sComplaint + "</br>" + "</br>";
                sBody = sBody + "Summary:" + "</br>";
                sBody = sBody + sCause + "</br>" + "</br>";
                sBody = sBody + "Detail:" + "</br>";
                sBody = sBody + sCorrective + "</br>" + "</br>";
                if (bTruPic)
                {
                    sBody = sBody + "Inspection: Tru Pic" + "</br>";
                    sBody = sBody + sLink + "</br>" + "</br>";
                }
                else if (bInspection)
                {
                    sBody = sBody + "Inspection: WIS Inspection" + "</br>";
                    sBody = sBody + sLink + "</br>" + "</br>";
                }
                else
                {
                    sBody = sBody + "Inspection: No Inspection or Pending Inspection" + "</br>" + "</br>";
                }
                
                sBody = sBody + "Please advise" + "</br>";

                sBody = sBody + "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
                sBody = sBody + "<tr>";
                sBody = sBody + "<td>";
                sBody = sBody + "<table cellspacing=\"6\" cellpadding=\"6\">";
                sBody = sBody + "<tr>";
                sBody = sBody + "<td style=\"border-radius: 2px;\" bgcolor=\"#1eabe2\">";
                sBody = sBody + "<a href=";
                sBody = sBody + "\"https://www.veritasgenerator.com/ticket/response.aspx?ticketno=";
                sBody = sBody + sTicketNo + "&Response=Approve&email=" + xEMail;
                sBody = sBody + "\" target=\"_blank\" style=\"padding: 8px 12px; border: 1px solid #1eabe2;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;\">";
                sBody = sBody + "Approve";
                sBody = sBody + "</a>";
                sBody = sBody + "</td>";
                sBody = sBody + "<td style=\"border-radius: 2px;\" bgcolor=\"#1eabe2\">";
                sBody = sBody + "<a href=\"https://www.veritasgenerator.com/ticket/response.aspx?ticketno=";
                sBody = sBody + sTicketNo + "&Response=Deny&email=" + xEMail;
                sBody = sBody + "\" target=\"_blank\" style=\"padding: 8px 12px; border: 1px solid #1eabe2;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;\">";
                sBody = sBody + "Deny";
                sBody = sBody + "</a>";
                sBody = sBody + "</td>";
                sBody = sBody + "</tr>";
                sBody = sBody + "</table>";
                sBody = sBody + "</td>";
                sBody = sBody + "</tr>";
                sBody = sBody + "</table>";
                sBody = sBody + "<b>" + sAdvisorName + "</b>" + "</br>";
                sBody = sBody + "Claims Adjuster" + "</br>";
                sBody = sBody + "VERITAS GLOBAL PROTECTION" + "</br>";

                mail.Body = sBody;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                string s;
                s = ex.Message;
            }
        }

        private void GetUserName()
        {
            SQL = "select * from userinfo where userid = " + lUserID + " ";
            clUI.OpenDB(SQL, Global.sCON);
            if (clUI.RowCount() > 0)
            {
                clUI.GetRow();
                sAdvisorName = clUI.GetFields("fname") + " " + clUI.GetFields("lname");
            }
        }
        private void GetClaim()
        {
            SQL = "select * from claim where claimid = " + lClaimID;
            clCL.OpenDB(SQL, Global.sCON);
            if (clCL.RowCount() > 0)
            {
                clCL.GetRow();
            }
        }
        private void GetContract()
        {
            SQL = "select * from contract where contractid = " + clCL.GetFields("contractid");
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
            }
        }
        private void GetDealer()
        {
            SQL = "select * from dealer where dealerid = " + clC.GetFields("dealerid");
            clD.OpenDB(SQL, Global.sCON);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
            }
        }
        private void GetPlanType()
        {
            SQL = "select * from plantype where plantypeid = " + clC.GetFields("plantypeid");
            clPT.OpenDB(SQL, Global.sCON);
            if (clPT.RowCount() > 0)
            {
                clPT.GetRow();
            }
        }
        private void GetClaimDetail()
        {
            SQL = "select * from claimdetail where claimdetailid = " + lClaimDetailID;
            clCD.OpenDB(SQL, Global.sCON);
            if (clCD.RowCount() > 0)
            {
                clCD.GetRow();
            }
        }
        private void GetServiceCenter()
        {
            SQL = "select * from servicecenter where servicecenterid = " + clCL.GetFields("servicecenterid");
            clSC.OpenDB(SQL, Global.sCON);
            if (clSC.RowCount() > 0)
            {
                clSC.GetRow();
            }
        }
        private void GetComplaint()
        {
            sComplaint = "";
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + lClaimID + " " +
                  "and claimnotetypeid = 1 ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sComplaint = clR.GetFields("note");
            }
        }
        private void GetCause()
        {
            sCause = "";
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + lClaimID + " " +
                  "and claimnotetypeid = 3 ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sCause = clR.GetFields("note");
            }
        }
        private void GetCorrective()
        {
            sCorrective = "";
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimnote " +
                  "where claimid = " + lClaimID + " " +
                  "and claimnotetypeid = 2 ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sCorrective = clR.GetFields("note");
            }
        }
        private void GetTruPic()
        {
            bTruPic = false;
            sLink = "";
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claimdocument " +
                  "where (DocumentName = 'truepic' " +
                  "or DocumentName = 'trupic') " +
                  "and claimid = " + lClaimID + " " +
                  "order by credate desc ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                bTruPic = true;
                sLink = clR.GetFields("documentlink");
                sLink = sLink.Replace("~", "https://www.veritasgenerator.com");
            }
        }
        private void GetInspection()
        {
            bInspection = false;
            sLink = "";
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claiminspection " +
                  "where claimid = " + lClaimID + " " +
                  "and not detailsurl is null " +
                  "order by requestdate desc ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                bInspection = true;
                sLink = clR.GetFields("detailsurl");
            }
        }
        private double GetClaimPaidAmt()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sum(totalamt) as Amt from claimdetail cd " + 
                  "inner join claim cl on cl.claimid = cd.claimid " + 
                  "where contractid = " + clC.GetFields("contractid") + " " + 
                  "and claimdetailstatus = 'Paid' ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    return Convert.ToDouble(clR.GetFields("amt"));
                }
            }
            return 0;
        }
        private double GetEstimate()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select sum(reqamt) as Amt from claimdetail cd " +
                  "inner join claim cl on cl.claimid = cd.claimid " +
                  "where cl.claimid = " + lClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                if (clR.GetFields("amt").Length > 0)
                {
                    return Convert.ToDouble(clR.GetFields("amt"));
                }
            }
            return 0;
        }
        private long CountClaim()
        {
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where contractid = " + clC.GetFields("contractid");
            clR.OpenDB(SQL, Global.sCON);
            return clR.RowCount();
        }
    }
}
