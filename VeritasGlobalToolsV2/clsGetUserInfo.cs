﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsGetUserInfo
    {
        private long lUserID;
        private string sFName;
        private string sLName;

        public long UserID
        {
            get
            {
                return lUserID;
            }
            set
            {
                lUserID = value;
            }
        }
        public string FName
        {
            get
            {
                return sFName;
            }
            set
            {
                sFName = value;
            }
        }
        public string LName
        {
            get
            {
                return sLName;
            }
            set
            {
                sLName = value;
            }
        }

        public void GetUserInfo()
        {
            string SQL;
            clsDBO.clsDBO clU = new clsDBO.clsDBO();
            SQL = "select * from userinfo " +
                  "where userid = " + lUserID;
            clU.OpenDB(SQL, Global.sCON);
            if (clU.RowCount() > 0)
            {
                clU.GetRow();
                sFName = clU.GetFields("fname");
                sLName = clU.GetFields("lname");
            }
        }
    }
}
