﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsParts
    {
        private string sPartNo;
        private string sPartName;
        private string sVIN;
        private long lClaimID;
        private string sClaimNo;
        private string sBody;
        private long lServiceCenterID;

        public long ClaimID
        {
            get
            {
                return lClaimID;
            }
            set
            {
                lClaimID = value;
            }
        }
        public string PartNo
        {
            get
            {
                return sPartNo;
            }
            set
            {
                sPartNo = value;
            }
        }
        public string PartName
        {
            get
            {
                return sPartName;
            }
            set
            {
                sPartName = value;
            }
        }
        public string VIN
        {
            get
            {
                return sVIN;
            }
            set
            {
                sVIN = value;
            }
        }

        public void SendPartRequest()
        {
            OpenClaim();
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(Global.sSendEMail);
                message.To.Add("steve@hannaworld.com");
                message.To.Add("srhanna@veritasglobal.com");
                message.Subject = "Claim No: " + sClaimNo;
                message.IsBodyHtml = true;
                sBody = "<p>Can you provide pricing on Part No: " + sPartNo + " Part Desc: " + sPartName +
                               "</p>" + "<p><b>Vehicle Info</b></br>" + "VIN: " + sVIN + "</br>";
                GetVINInfo();
                sBody = sBody + "</p>" +
                        "<p><b>Repair Shop</b></br> ";
                GetServiceCenter();
                sBody = sBody + "</p>";
                message.Body = sBody;
                smtp.Port = 587;
                smtp.Host = "smtp.office365.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(Global.sSendEMail, Global.sSendEMailPassword);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void OpenClaim()
        {
            sClaimNo = "";
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from claim where claimid = " + lClaimID;
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sClaimNo = clR.GetFields("claimno");
                lServiceCenterID = long.Parse(clR.GetFields("servicecenterid"));
            }
        }

        private void GetVINInfo()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            clsDBO.clsDBO clB = new clsDBO.clsDBO();
            clsDBO.clsDBO clE = new clsDBO.clsDBO();
            SQL = "select * from vin.dbo.vin " + 
                  "where vin = '" + sVIN.Substring(0, 11) + "' ";
            clR.OpenDB(SQL, Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                SQL = "select * from vin.dbo.basicdata where vinid = " + clR.GetFields("vinid");
                clB.OpenDB(SQL, Global.sCON);
                if (clB.RowCount() > 0)
                {
                    clB.GetRow();
                    sBody = sBody + "Vehicle Type: " + clB.GetFields("vehicletype") + "</br>" +
                            "Year: " + clB.GetFields("year") + " " +
                            "Make: " + clB.GetFields("make") + " " +
                            "Model: " + clB.GetFields("model") + " " +
                            "Trim: " + clB.GetFields("Trim") + "</br>" +
                            "Drive Type: " + clB.GetFields("drivetype") + " ";
                }
                SQL = "select * from vin.dbo.engine where vinid = " + clR.GetFields("vinid");
                clE.OpenDB(SQL, Global.sCON);
                if (clE.RowCount() > 0)
                {
                    clE.GetRow();
                    sBody = sBody + "Aspiration: " + clE.GetFields("aspiration") + "</br> " +
                            "Fuel: " + clE.GetFields("fueltype") + " " +
                            "Valve: " + clE.GetFields("valves") + " " +
                            "Engine: " + clE.GetFields("name");
                }
            }
        }

        private void GetServiceCenter()
        {
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            SQL = "select * from servicecenter " +
                  "where servicecenterid = " + lServiceCenterID;
            clR.OpenDB(SQL,Global.sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                sBody = sBody + clR.GetFields("servicecentername") + "</br>" + clR.GetFields("addr1") + "</br>";
                if (clR.GetFields("addr2").Length > 0)
                {
                    sBody = sBody + clR.GetFields("addr2") + "</br>";
                }
                sBody = sBody + clR.GetFields("city") + " " + clR.GetFields("state") +
                        " " + clR.GetFields("zip") + "</br>" + clR.GetFields("phone");
            }
        }
    }
}
