﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace VeritasGlobalToolsV2
{
    public class clsVIN
    {
        private string sVIN;
        private string sContract;
        public string responseText;
        private long lVINID;
        private DataSet ds = new DataSet();
        private string SQL;

        public string VIN
        {
            get
            {
                return sVIN;
            }
            set
            {
                sVIN = value;
            }
        }
        public string Contract
        {
            get
            {
                return sContract;
            }
            set
            {
                sContract = value;
            }
        }

        public void ProcessVIN()
        {
            try
            {
                XmlDocument xmlD = new XmlDocument();
                XmlNodeReader xmlNR;
                if (CheckVIN())
                {
                    return;
                }
                xmlD.LoadXml(Decode());
                xmlNR = new XmlNodeReader(xmlD);
                ds.ReadXml(xmlNR);
                if (ds.Tables[3].Rows[0][0].ToString().Length > 0) 
                {
                    return;
                }
                CheckBasicData();
                CheckEngine();
                CheckTransmission();
                CheckWarranty();
            }
            catch (Exception ex)
            {

            }
        }

        private bool CheckVIN()
        {
            clsDBO.clsDBO clV = new clsDBO.clsDBO();
            //CheckVIN = true;

            try
            {
                SQL = "select * from vin.dbo.vin " +
                      "where vin = '" + sVIN.Substring(0, 11) + "' ";

                clV.OpenDB(SQL, Global.sCON);

                if (clV.RowCount() > 0)
                {
                    return true;
                }

                clV.NewRow();
                clV.SetFields("vin", sVIN.Substring(0, 11));
                clV.SetFields("createdate", DateTime.Now.ToString());
                clV.AddRow();
                clV.SaveDB();
                clV.OpenDB(SQL, Global.sCON);
                if (clV.RowCount() > 0)
                {
                    clV.GetRow();
                    lVINID = long.Parse(clV.GetFields("vinid"));
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void CheckBasicData()
        {
            clsDBO.clsDBO clBD = new clsDBO.clsDBO();
            DataTable dt = new DataTable();
            DataRow dr;
            try {
                SQL = "select * from vin.dbo.basicdata where vinid = " + lVINID;
                clBD.OpenDB(SQL, Global.sCON);
                if (clBD.RowCount() > 0)
                {
                    clBD.GetRow();
                }
                else 
                {
                    clBD.NewRow();
                }
                clBD.SetFields("vinid", lVINID.ToString());
                dt = ds.Tables[7];
                dr = dt.Rows[0];

                clBD.SetFields("year", dr["year"].ToString());
                clBD.SetFields("make", dr["make"].ToString());
                clBD.SetFields("model", dr["model"].ToString());
                clBD.SetFields("trim", dr["trim"].ToString());
                clBD.SetFields("vehicletype", dr["vehicle_type"].ToString());
                clBD.SetFields("bodytype", dr["body_type"].ToString());
                clBD.SetFields("bodysubtype", dr["body_subtype"].ToString());
                clBD.SetFields("oembodystyle", dr["oem_body_style"].ToString());
                clBD.SetFields("door", dr["doors"].ToString());
                clBD.SetFields("oemdoor", dr["oem_doors"].ToString());
                clBD.SetFields("modelnumber", dr["model_number"].ToString());
                clBD.SetFields("packagecode", dr["package_code"].ToString());
                clBD.SetFields("packagesummary", dr["package_summary"].ToString());
                clBD.SetFields("rearaxle", dr["rear_axle"].ToString());
                clBD.SetFields("drivetype", dr["drive_type"].ToString());
                clBD.SetFields("brakesystem", dr["brake_system"].ToString());
                clBD.SetFields("restrainttype", dr["restraint_type"].ToString());
                clBD.SetFields("countrymfr", dr[18].ToString());
                clBD.SetFields("plant", dr["plant"].ToString());

                if (dr["glider"].ToString().ToLower() == "y")
                {
                    clBD.SetFields("glider", true.ToString());
                }
                clBD.SetFields("chassistype", dr["chassis_type"].ToString());
                if (clBD.RowCount() == 0)
                {
                    clBD.AddRow();
                }
                clBD.SaveDB();
            }
            catch (Exception ex)
            {

            }
        }
        public void CheckEngine()
        {
            clsDBO.clsDBO clE = new clsDBO.clsDBO();
            DataTable dt = new DataTable();
            DataRow dr;

            try
            {
                dt = ds.Tables[9];
                dr = dt.Rows[0];
                SQL = "select * from vin.dbo.engine where vinid = " + lVINID;

                clE.OpenDB(SQL, Global.sCON);
                if (clE.RowCount() > 0)
                {
                    clE.GetRow();
                }
                else
                {
                    clE.NewRow();
                }
                clE.SetFields("vinid", lVINID.ToString());
                clE.SetFields("availability", dr["availability"].ToString());
                clE.SetFields("aspiration", dr["ice_aspiration"].ToString());
                clE.SetFields("blocktype", dr["ice_block_type"].ToString());
                clE.SetFields("bore", dr["ice_bore"].ToString());
                clE.SetFields("CamType", dr["ice_cam_type"].ToString());
                clE.SetFields("Compression", dr["ice_compression"].ToString());
                clE.SetFields("Cyclinders", dr["ice_cylinders"].ToString());
                clE.SetFields("Displacement", dr["ice_displacement"].ToString());
                clE.SetFields("ElectricMotorConfig", dr["electric_motor_configuration"].ToString());
                clE.SetFields("ElectricMaxHP", dr["electric_max_hp"].ToString());
                clE.SetFields("ElectricMaxKW", dr["electric_max_kw"].ToString());
                clE.SetFields("ElectricMaxTorque", dr["electric_max_torque"].ToString());
                clE.SetFields("EngineType", dr["engine_type"].ToString());
                clE.SetFields("FuelInduction", dr["ice_fuel_induction"].ToString());
                clE.SetFields("FuelQuality", dr["fuel_quality"].ToString());
                clE.SetFields("FuelType", dr["fuel_type"].ToString());
                if (dr["fleet"].ToString().ToLower() == "y")
                {
                    clE.SetFields("fleet", true.ToString());
                }
                clE.SetFields("GeneratorDesc", dr["generator_description"].ToString());
                clE.SetFields("GeneratorMaxHP", dr["generator_max_hp"].ToString());
                clE.SetFields("MaxHP", dr["ice_max_hp"].ToString());
                clE.SetFields("MaxHPAtRPM", dr["ice_max_hp_at"].ToString());
                clE.SetFields("MaxPayload", dr["max_payload"].ToString());
                clE.SetFields("MaxTorque", dr["ice_max_torque"].ToString());
                clE.SetFields("MaxTorqueAtRPM", dr["ice_max_torque_at"].ToString());
                clE.SetFields("OilCapacity", dr["oil_capacity"].ToString());
                clE.SetFields("OrderCode", dr["order_code"].ToString());
                clE.SetFields("RedLine", dr["redline"].ToString());
                clE.SetFields("Stroke", dr["ice_stroke"].ToString());
                clE.SetFields("TotalMaxHP", dr["total_max_hp"].ToString());
                clE.SetFields("TotalMaxHPAtRPM", dr["total_max_hp_at"].ToString());
                clE.SetFields("TotalMaxTorque", dr["total_max_torque"].ToString());
                clE.SetFields("TotalMaxTorqueAtRPM", dr["total_max_torque_at"].ToString());
                clE.SetFields("ValveTiming", dr["ice_valve_timing"].ToString());
                clE.SetFields("Valves", dr["ice_valves"].ToString());
                clE.SetFields("Name", dr["name"].ToString());
                clE.SetFields("Brand", dr["brand"].ToString());
                clE.SetFields("MarketingName", dr["marketing_name"].ToString());

                if (clE.RowCount() == 0)
                {
                    clE.AddRow();
                }
                clE.SaveDB();
            }
            catch (Exception ex)
            {

            }
        }
        public void CheckTransmission()
        {
            clsDBO.clsDBO clT = new clsDBO.clsDBO();
            DataTable dt = new DataTable();
            DataRow dr;
            try
            {
                dt = ds.Tables[11];

                dr = dt.Rows[0];

                SQL = "select * from vin.dbo.transmission where vinid = " + lVINID;

                clT.OpenDB(SQL, Global.sCON);

                if (clT.RowCount() > 0)
                {

                    clT.GetRow();
                }
                else
                {
                    clT.NewRow();
                }

                clT.SetFields("vinid", lVINID.ToString());
                clT.SetFields("Availability", dr["availability"].ToString());
                clT.SetFields("TransmissionType", dr["type"].ToString());
                clT.SetFields("DetailType", dr["detail_type"].ToString());
                clT.SetFields("Gears", dr["gears"].ToString());
                clT.SetFields("OrderCode", dr["order_code"].ToString());

                if (dr["fleet"].ToString().ToLower() == "y")
                {
                    clT.SetFields("fleet", true.ToString());
                }

                clT.SetFields("Name", dr["name"].ToString());
                clT.SetFields("Brand", dr["brand"].ToString());
                clT.SetFields("MarketingName", dr["marketing_name"].ToString());

                if (clT.RowCount() == 0)
                {
                    clT.AddRow();
                }
                clT.SaveDB();
            }
            catch (Exception ex)
            {

            }
        }
        public void CheckWarranty()
        {
            clsDBO.clsDBO clW = new clsDBO.clsDBO();
            DataTable dt = new DataTable();
            DataRow dr;
            try
            {
                dt = ds.Tables[13];
                for (int cnt = 0; cnt <= dt.Rows.Count - 1; cnt++)
                {
                    dr = dt.Rows[cnt];
                    SQL = "select * from vin.dbo.warranty where vinid = " + lVINID + " " +
                          "and WarrantyType = '" + dr["type"] + "' ";
                    clW.OpenDB(SQL, Global.sCON);
                    if (clW.RowCount() > 0)
                    {
                        goto MoveNext;
                    }
                    else
                    {
                        clW.NewRow();
                    }
                    clW.SetFields("vinid", lVINID.ToString());
                    clW.SetFields("WarrantyType", dr["type"].ToString());
                    clW.SetFields("Months", dr["months"].ToString());
                    clW.SetFields("Miles", dr["miles"].ToString());
                    clW.SetFields("Name", dr["name"].ToString());
                    if (clW.RowCount() == 0)
                    {
                        clW.AddRow();
                    }
                    clW.SaveDB();
                MoveNext:;
                }
            }
            catch (Exception ex)
            {

            }
        }

        public bool ValidateServerCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string Decode()
        {
            try
            {
                string url = "https://api.dataonesoftware.com/webservices/vindecoder/decode";
                string postData = "client_id=19047&authorization_code=e806f6cca0026cfab447f424bfb69d859a1e4f62&decoder_query=";
			    postData += "" +
			    "<decoder_query>" +
			    	"<decoder_settings>" +
			    	"<display>full</display>" +
			    		"<styles>on</styles>" +
			    		"<style_data_packs>" +
			    			"<basic_data>on</basic_data>" +
			    			"<pricing>off</pricing>" +
			    			"<engines>on</engines>" +
			    			"<transmissions>on</transmissions>" +
			    			"<specifications>on</specifications>" +
			    			"<installed_equipment>of</installed_equipment>" +
			    			"<optional_equipment>off</optional_equipment>" +
			    			"<generic_optional_equipment>off</generic_optional_equipment>" +
			    			"<colors>off</colors>" +
			    			"<warranties>on</warranties>" +
			    			"<fuel_efficiency>off</fuel_efficiency>" +
			    			"<green_scores>off</green_scores>" +
			    			"<crash_test>off</crash_test>" +
			    			"<recalls>off</recalls>" +
			    			"<service_bulletins>off</service_bulletins>" +
			    		"</style_data_packs>" +
			    		"<common_data>off</common_data>" +
			    		"<common_data_packs>" +
			    			"<basic_data>on</basic_data>" +
			    			"<pricing>on</pricing>" +
			    			"<engines>on</engines>" +
			    			"<transmissions>on</transmissions>" +
			    			"<specifications>on</specifications>" +
			    			"<installed_equipment>off</installed_equipment>" +
			    			"<generic_optional_equipment>off</generic_optional_equipment>" +
			    		"</common_data_packs>" +
			    	"</decoder_settings>" +
			    	"<query_requests>" +
			    		"<query_request identifier=\"" + sContract + "\">" +
			    			"<vin>" + sVIN + "</vin>" +
			    			"<year/>" +
			    			"<make/>" +
			    			"<model/>" +
			    			"<trim/>" +
			    			"<model_number/>" +
			    			"<package_code/>" +
			    			"<drive_type/>" +
			    			"<vehicle_type/>" +
			    			"<body_type/>" +
			    			"<doors/>" +
			    			"<bedlength/>" +
			    			"<wheelbase/>" +
			    			"<msrp/>" +
			    			"<invoice_price/>" +
			    			"<engine description=\"\">" +
			    				"<block_type/>" +
			    				"<cylinders/>" +
			    				"<displacement/>" +
			    				"<fuel_type/>" +
			    			"</engine>" +
			    			"<transmission description=\"\">" +
			    				"<trans_type/>" +
			    				"<trans_speeds/>" +
			    			"</transmission>" +
			    			"<optional_equipment_codes/>" +
			    			"<installed_equipment_descriptions/>" +
			    			"<interior_color description=\"\">" +
			    				"<color_code/>" +
			    			"</interior_color>" +
			    			"<exterior_color description=\"\">" +
                                "<color_code/>" +
                            "</exterior_color>" +
                        "</query_request>" +
                    "</query_requests>" +
                "</decoder_query>";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = postData.Length;

                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                Stream writeStream = request.GetRequestStream();

                // Encode the string to be posted
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bytes = encoding.GetBytes(postData);
                writeStream.Write(bytes, 0, bytes.Length);
                writeStream.Close();

                // Get Response
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                return reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
