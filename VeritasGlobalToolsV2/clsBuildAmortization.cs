﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsBuildAmortization
    {
        private long lClaimGapID;
        private double dPaymentAmt;
        private double dRatePerMonth;
        private double dLoanAmt;
        private double dLoanAmtInt;
        private long lNoPayment;
        private string sStartDate;
        private bool bMonthly;
        private bool bBiWeekly;

        public long ClaimGapID
        {
            get
            {
                return lClaimGapID;
            }
            set
            {
                lClaimGapID = value;
            }
        }
        public double LoanAmt
        {
            get
            {
                return dLoanAmt;
            }
            set
            {
                dLoanAmt = value;
            }
        }
        public double LoanAmtInt
        {
            get
            {
                return dLoanAmtInt;
            }
            set
            {
                dLoanAmtInt = value;
            }
        }
        public string StartDate
        {
            get
            {
                return sStartDate;
            }
            set
            {
                sStartDate = value;
            }
        }
        public long NoPayment
        {
            get
            {
                return lNoPayment;
            }
            set
            {
                lNoPayment = value;
            }
        }
        public double PaymentAmt
        {
            get
            {
                return dPaymentAmt;
            }
            set
            {
                dPaymentAmt = value;
            }
        }
        public double RatePerMonth
        {
            get
            {
                return dRatePerMonth;
            }
            set
            {
                dRatePerMonth = value;
            }
        }
        public bool Monthly
        {
            get
            {
                return bMonthly;
            }
            set
            {
                bMonthly = value;
            }
        }
        public bool BiWeekly
        {
            get
            {
                return bBiWeekly;
            }
            set
            {
                bBiWeekly = value;
            }
        }

        public void BuildIt()
        {
            long cnt;
            string SQL;
            clsDBO.clsDBO clR = new clsDBO.clsDBO();
            string sMonth = "";
            double dOldBalance;
            double dOldBalanceInt;
            double dTotInt = 0;
            dOldBalanceInt = dLoanAmtInt;
            dOldBalance = dLoanAmt;
            for (cnt = 1;cnt < lNoPayment; cnt++)
            {
                SQL = "select * from claimgapamortization " +
                      "where claimgapid = " + lClaimGapID + " " +
                      "and paymentno = " + cnt;
                clR.OpenDB(SQL, Global.sCON);
                if (clR.RowCount() == 0)
                {
                    clR.NewRow();
                    clR.SetFields("claimgapid", lClaimGapID.ToString());
                    clR.SetFields("paymentno", cnt.ToString());
                }
                else
                {
                    clR.GetRow();
                }
                if (bMonthly)
                {
                    //sMonth = DateAdd(DateInterval.Month, cnt - 1, DateTime.Parse(sStartDate));
                    sMonth = DateTime.Parse(sStartDate).AddMonths((int)cnt-1).ToString();
                }
                if (bBiWeekly)
                {
                    //sMonth = DateAdd(DateInterval.Day, (cnt - 1) * 14, DateTime.Parse(sStartDate));
                    sMonth = DateTime.Parse(sStartDate).AddDays(((int)cnt-1)*14).ToString();
                }

                clR.SetFields("monthdate", sMonth);
                clR.SetFields("payment", dPaymentAmt.ToString());
                clR.SetFields("interest", (dOldBalance * dRatePerMonth).ToString());
                clR.SetFields("principal", (dPaymentAmt - Convert.ToDouble((clR.GetFields("interest")))).ToString());
                dTotInt = dTotInt + Convert.ToDouble((clR.GetFields("interest")));
                clR.SetFields("totalinterest", dTotInt.ToString());
                dOldBalance = dOldBalance - Convert.ToDouble((clR.GetFields("principal")));
                clR.SetFields("balremaining" , dOldBalance.ToString());
                dOldBalanceInt = dOldBalanceInt - dPaymentAmt;
                clR.SetFields("loanremaining", dOldBalanceInt.ToString());
                if (clR.RowCount() == 0)
                {
                    clR.AddRow();
                }
                clR.SaveDB();
            }
        }
    }
}
