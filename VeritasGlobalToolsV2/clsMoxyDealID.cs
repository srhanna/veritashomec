﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsMoxyDealID
    {
        private long lContractID;
        private long lDealID;
        private string sDBType;
        public long ContractID
        {
            get
            {
                return lContractID;
            }
            set
            {
                lContractID = value;
            }
        }
        public string DBType
        {
            get
            {
                return sDBType;
            }
            set
            {
                sDBType = value;
            }
        }
        public long DealID
        {
            get
            {
                return lDealID;
            }
            set
            {
                lDealID = value;
            }
        }

        public void GetMoxyDealID()
        {
            string SQL;
            clsDBO.clsDBO clC = new clsDBO.clsDBO();
            clsDBO.clsDBO clM = new clsDBO.clsDBO();
            SQL = "select * from contract " +
                  "where contractid = " + lContractID;
            clC.OpenDB(SQL, Global.sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                if (clC.GetFields("ContractNo").Substring(0, 3) == "RAC")
                {
                    lDealID = 0;
                    sDBType = "";
                    return;
                }
                if (clC.GetFields("contractno").Substring(0, 3) == "REP")
                {
                    SQL = "select * from VeritasMoxy.dbo.epcontract " + 
                          "where contractno = '" + clC.GetFields("contractno") + "' ";
                    sDBType = "EP";
                    goto MoveHere;
                }
                if (long.Parse(clC.GetFields("programid")) > 46 && long.Parse(clC.GetFields("programid")) < 61) 
                {
                    SQL = "select * from VeritasMoxy.dbo.moxyproduct " +
                          "where contractno = '" + clC.GetFields("contractno") + "' ";
                    DBType = "Product";
                    goto MoveHere;
                }
                if (long.Parse(clC.GetFields("programid")) == 67)
                {
                    SQL = "select * from VeritasMoxy.dbo.moxyproduct " + 
                          "where contractno = '" + clC.GetFields("contractno") + "' ";
                    DBType = "Product";
                    goto MoveHere;
                }
                sDBType = "Normal";
                SQL = "select * from VeritasMoxy.dbo.moxycontract " + 
                      "where contractno = '" + clC.GetFields("contractno") + "' ";
                MoveHere:;
            }
            clM.OpenDB(SQL, Global.sCON);
            if (clM.RowCount() == 0)
            {
                lDealID = 0;
                return;
            }
            clM.GetRow();
            lDealID = long.Parse(clM.GetFields("dealid"));
        }
    }
}
