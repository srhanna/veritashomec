﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsWISZipCode
    {
        private WIS.AuthenticateHeader AuthUser = new WIS.AuthenticateHeader();
        private string sUserName = "redshield";
        private string sPassword = "rs@@p120gh";
        private double dAvg;
        private double dHigh;
        private double dLow;
        private clsDBO.clsDBO clZ = new clsDBO.clsDBO();
        private clsDBO.clsDBO clR = new clsDBO.clsDBO();
        private string SQL;
        private string sZip;

        public string ZipCode
        {
            get
            {
                return sZip;
            }
            set
            {
                sZip = value;
            }
        }

        public void RunZip()
        {
            dAvg = 0;
            dHigh = 0;
            dLow = 0;
            try
            {
                AuthUser.Username = sUserName;
                AuthUser.Password = sPassword;
                WIS.wiswebserviceSoapClient Service = new WIS.wiswebserviceSoapClient();
                WIS.LaborRatesByZipRequest ZipRequest = new WIS.LaborRatesByZipRequest();
                ZipRequest.AuthenticateHeader = AuthUser;
                ZipRequest.Term = 12.ToString();
                ZipRequest.Zip = sZip;
                WIS.LaborRatesByZipResponse ZipRep = new WIS.LaborRatesByZipResponse();
                ZipRep.LaborRatesByZipResult = Service.LaborRatesByZip(ZipRequest.AuthenticateHeader, ZipRequest.Zip, ZipRequest.Term);
                DataSet ds = new DataSet();
                ds = ZipRep.LaborRatesByZipResult;
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                DataRow dr;
                dr = dt.Rows[0];
                if (!DBNull.Value.Equals(dr[1]))
                {
                    dAvg = Convert.ToDouble(dr[1]);
                }
                if (!DBNull.Value.Equals(dr[2]))
                {
                    dHigh = Convert.ToDouble(dr[2]);
                }
                if (!DBNull.Value.Equals(dr[3]))
                {
                    dLow = Convert.ToDouble(dr[3]);
                }
                SQL = "select * from laborrate where zipcode = '" + sZip + "' ";
                clR.OpenDB(SQL, Global.sCON);
                if (clR.RowCount() == 0)
                {
                    clR.NewRow();
                    clR.SetFields("zipcode", sZip);
                    clR.SetFields("avgrate", dAvg.ToString());
                    clR.SetFields("maxrate", dHigh.ToString());
                    clR.SetFields("minRate", dLow.ToString());
                    clR.SetFields("credate", DateTime.Now.ToString());
                    clR.AddRow();
                }
                else
                {
                    clR.GetRow();
                    clR.SetFields("avgrate", dAvg.ToString());
                    clR.SetFields("maxrate", dHigh.ToString());
                    clR.SetFields("minRate", dLow.ToString());
                    clR.SetFields("credate", DateTime.Now.ToString());
                }
                clR.SaveDB();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
