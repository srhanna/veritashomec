﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeritasGlobalToolsV2
{
    public class clsAutoNationACH
    {
        private string sCON;
        private long lClaimID;
        private string sDealerNo;
        private string sDateApproved;
        private string sPaidDate;
        //private double dPaidAmt;
        private string sConfirmNumber;
        private string SQL;
        private clsDBO.clsDBO clCD = new clsDBO.clsDBO();
        private long lUserID;

        public string ConnectiionString
        {
            get
            {
                return sCON;
            }
            set
            {
                sCON = value;
            }
        }
        public long ClaimID
        {
            get
            {
                return lClaimID;
            }
            set
            {
                lClaimID = value;
            }
        }
        public string ConfirmNumber
        {
            get
            {
                return sConfirmNumber;
            }
            set
            {
                sConfirmNumber = value;
            }
        }
        public string DealerNo
        {
            get
            {
                return sDealerNo;
            }
            set
            {
                sDealerNo = value;
            }
        }
        public string DateApproved
        {
            get
            {
                return sDateApproved;
            }
            set
            {
                sDateApproved = value;
            }
        }
        public string PaidDate
        {
            get
            {
                return sPaidDate;
            }
            set
            {
                sPaidDate = value;
            }
        }
        public long UserID
        {
            get
            {
                return lUserID;
            }
            set
            {
                lUserID = value;
            }
        }

        public void ProcessClaim()
        {
            UpdateANACH();
            SQL = "select * from claimdetail cd " +
                  "where claimid = " + lClaimID.ToString() + " " +
                  "and dateapprove = '" + sDateApproved + "' " +
                  "and claimdetailstatus = 'Approved' " +
                  "and ClaimPayeeID in ( " +
                  "select claimpayeeid from ClaimPayee cp " +
                  "inner join ServiceCenter sc " +
                  "on sc.ServiceCenterNo = cp.PayeeNo " +
                  "and sc.dealerno = '" + sDealerNo + "') ";
            clCD.OpenDB(SQL, sCON);
            if (clCD.RowCount() > 0)
            {
                for (int i = 0; i < clCD.RowCount(); i++)
                {
                    clCD.GetRowNo(i);
                    UpdatePaid();
                }
            }

            clsClaim newClaim = new clsClaim();
            newClaim.ClaimID = lClaimID;
            newClaim.DatePaid = sPaidDate;
            newClaim.CON = sCON;
            newClaim.UserID = lUserID;
            newClaim.ProcessClaimStatus();
        }
        
        private void UpdateANACH()
        {
            clsDBO.clsDBO dBOClm = new clsDBO.clsDBO();

            SQL = "select * from claimautonationach " +
                  "where claimid = " + lClaimID + " " +
                  "and dateapprove = '" + sDateApproved + "' " +
                  "and dealerno = '" + sDealerNo + "' ";
            dBOClm.OpenDB(SQL, sCON);
            if (dBOClm.RowCount() > 0)
            {
                dBOClm.GetRow();
                dBOClm.SetFields("datepaid", sPaidDate);
                dBOClm.SetFields("ConfirmNumber", sConfirmNumber);
                dBOClm.SaveDB();
            }
        }

        private void UpdatePaid()
        {
            clsDBO.clsDBO dBOClm = new clsDBO.clsDBO();
            SQL = "update claimdetail " +
                  "set datepaid = '" + sPaidDate + "', " +
                  "claimdetailstatus = 'Paid', " +
                  "paidamt = totalamt, " +
                  "paidby = " + lUserID + ", " +
                  "moddate = '" + DateTime.Today + "', " +
                  "modby = " + lUserID + " " +
                  "where claimdetailid = " + clCD.GetFields("claimdetailid");
            dBOClm.RunSQL(SQL, sCON);
        }
    }
}
